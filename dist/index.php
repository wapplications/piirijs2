<?php
ini_set('display_errors', 'on');
ini_set('display_startup_errors', 'on');
ini_set('html_errors', 'on');
//ini_set('log_errors', 'on');
error_reporting(-1);

$config = include 'config.php';

// von $_SERVER['SCRIPT_FILENAME'] hinten $_SERVER['PHP_SELF'] wegnehmen
// dann von __DIR__ vorne das ergebnis von oben wegnehmen
define('P', $path = (substr(__DIR__, strlen(substr($_SERVER['SCRIPT_FILENAME'], 0, -strlen($_SERVER['PHP_SELF']))))));
// /piiri2

$uri = trim(strstr($_SERVER['REQUEST_URI'].'?', '?', true), '/'); // ? dran hängen: kleiner hack ;)
if (P) {
  $uri = trim(substr($uri, strlen(trim(P, '/'))), '/');
}
if ($uri === '') {
  $customerName = null;
} else {
  $customerName = $uri;
}

function isApiCall(): bool {
  return $_SERVER['HTTP_ACCEPT'] === 'application/json';
}

require_once 'src/db.php';
require_once 'src/Backend.php';
require_once 'src/api.php';


if ($customerName) {
  
  $piiri = new Backend($config);
  $customer = $piiri->findCustomer($customerName);

  if (!$customer) die ('Kunde nicht gefunden.');
  
  if (isApiCall()) {
    $api = new PiiriApi($config, $customer);
    $api->handleRequest();
    exit;
  }
  
  $listGroups = $customer->settings->listGroups;
  if ($listGroups) {
    $groups = $customer->getGroups();
  }
  
} else {
  $listGroups = false;
  if (isApiCall()) {
    $api = new PiiriApi($config, null);
    $api->handleRequest();
    exit;
  }
}



?><!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  
  <link rel="icon" href="<?= P ?>/img/logo-simple-48x48.png"/>
  <link rel="apple-touch-icon" href="<?= P ?>/img/logo-192x192.png"/>
  <meta name="theme-color" content="#ff6600"/>

  <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1, user-scalable=no, interactive-widget=overlays-content">
  <title>[PiiRi]</title>


  <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
  <meta http-equiv="Pragma" content="no-cache" />
  <meta http-equiv="Expires" content="0" />

  <script  src="<?= P ?>/alpine.js" defer></script>

  <style>
    @import url("<?= P ?>/ui.css?<?= time() ?>");
  </style>
</head>
<body>
    <div class="uiSplitPane" data-direction="horizontal">

      <div data-size="270" class="uiSplitPane" data-direction="vertical">

        <div data-size="0.7" class="uiVerticalSplit">
          <div>

            <div id="userAtGroup">--</div>
            <div id="toolbar" style="border-bottom:solid 1px #555;">
              <button class="uiCommandButton toolbarItem groupCommand" data-command="rename" title="Komponente umbenennen"></button>
              <button class="uiCommandButton toolbarItem groupCommand" data-command="remove" title="Komponente entfernen"></button>
              <button class="uiCommandButton toolbarItem groupCommand" data-command="share_group" title="Komponente für Gruppe freigeben"></button>
              <button class="uiCommandButton toolbarItem groupCommand" data-command="reload" title="Komponenten neu einlesen"></button>
              <button class="uiCommandButton toolbarItem groupCommand" data-command="admin" title="Gruppen-Admin"></button>
              <button class="uiCommandButton toolbarItem" data-command="import" title="Import"></button>
            </div>

          </div>


          <div class="uiAccordeon" data-expand="true" data-interactive="1">
            <div title="Logik-Gatter" class="uiAccordeonTab">
              <div class="pad cirPaletteContainer" id="BuiltinPalette1">
                <?php foreach(["and", "or", "nand", "nor", "not", "xor"] as $cir) : ?>
                  <div class="uiCirButton" data-cir="<?= $cir ?>" data-canopen="false"></div>
                <?php endforeach; ?>
              </div>
            </div>
            <div title="Eingebaute Komponenten" class="uiAccordeonTab">
              <div class="pad cirPaletteContainer" id="BuiltinPalette2">
                <?php foreach([
                "led" =>  (object)['props' => (object)['color'=>'red'], 'inputs' => (object)['i'=>1] ],
                "node" => new \stdclass,
                "adapter" => (object)['props' => (object)['size'=>3] ],
                "value" => new \stdclass,
                "clock" => new \stdclass,
                "segments" =>  (object)['props' => (object)['digits'=>1, 'multipin'=>true], 'inputs' => (object)['in'=>7] ],
                "ram" => new \stdclass,
                "pla" => (object)['props' => (object)['inBits'=>2, 'outBits'=>2, 'andBits'=>2, 'andArray'=>'1 0 | 0 1 | 0 1' ] ],
                "switch" => new \stdclass,
                "indicator" => new \stdclass,
                "text" => new \stdclass,
                "embedded" => new \stdclass,                  
                ] as $cir=>$data) : ?>
                  <div class="uiCirButton" data-cir="<?= $cir ?>" data-canopen="false" data-props="<?= htmlentities(json_encode($data->props ?? null)) ?>" data-inputs="<?= htmlentities(json_encode($data->inputs ?? null)) ?>"></div>
                <?php endforeach; ?>
              </div>
            </div>
            <div title="Eigene Komponenten" class="uiAccordeonTab">
              <div class="pad cirPaletteContainer" id="ComponentsPalette">
              </div>
            </div>
          </div>

        </div>

        <div data-size="0.3"  class="" id="cirProperties" style="overflow-y:auto;">
        </div>
      </div>

      <div data-size="300" data-ref="tabs" class="uiTabs">

      </div>

    </div>
    <script>


function setCookie(name, value, days) {
    const d = new Date();
    const sameSite = 'Lax';
    d.setTime(d.getTime() + (days * 24 * 60 * 60 * 1000));
    const expires = "expires=" + d.toUTCString();
    document.cookie = `${name}=${value}; ${expires}; path=/; SameSite=${sameSite}`;
}
function getCookie(name) {
    const nameEQ = name + "=";
    const ca = document.cookie.split(';');
    for(let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

let lightmodeCookie = getCookie('lightmode');
let isLightmode = !!JSON.parse(lightmodeCookie);
setLightMode(isLightmode);

function setLightMode(lm) {
  if (lm) {
    document.body.classList.add('light-mode')
  } else {
    document.body.classList.remove('light-mode')
  }
  document.dispatchEvent(new CustomEvent('piiri:lightmode-changed'));
  setCookie('lightmode', lm?'true':'false', 1000);
}
    </script>

    <button id="darkModeButton" onclick="setLightMode(!document.body.classList.contains('light-mode'));">
<svg class="lightmodeicon" height="20" width="20" viewBox="0 0 100 100">  <g>
   <circle cx="50" cy="50" r="20" style="fill:currentColor;" />
   <line x1="50" y1="20" x2="50" y2="0" style="stroke:currentColor; stroke-width:3;" />
   <line x1="50" y1="80" x2="50" y2="100" style="stroke:currentColor; stroke-width:3;" />
   <line y1="50" x1="20" y2="50" x2="0" style="stroke:currentColor; stroke-width:3;" />
   <line y1="50" x1="80" y2="50" x2="100" style="stroke:currentColor; stroke-width:3;" />
   <g transform="rotate(45,50,50)">
   <line x1="50" y1="20" x2="50" y2="0" style="stroke:currentColor; stroke-width:3;" />
   <line x1="50" y1="80" x2="50" y2="100" style="stroke:currentColor; stroke-width:3;" />
   <line y1="50" x1="20" y2="50" x2="0" style="stroke:currentColor; stroke-width:3;" />
   <line y1="50" x1="80" y2="50" x2="100" style="stroke:currentColor; stroke-width:3;" />
   </g>
   </g></svg>
   <svg  class="darkmodeicon" height="20" width="20" viewBox="0 0 100 100">  <g>
   <path fill="currentColor" d="M 50.309074 13.567647
   A 36.634579 36.634579 0 0 0 13.674618 50.202104
   A 36.634579 36.634579 0 0 0 50.309074 86.83656
   A 36.634579 36.634579 0 0 0 81.913346 68.571008
   A 30.170155 30.170155 0 0 1 72.004907 70.318188
   A 30.170155 30.170155 0 0 1 41.835173 40.147937
   A 30.170155 30.170155 0 0 1 56.807385 14.158826
   A 36.634579 36.634579 0 0 0 50.309074 13.567647
   z"/>
   </g></svg>

  </button>

  <script defer>
    window['P'] = <?= json_encode(P) ?>;
  </script>


  <template id="piiriTab">
      <div data-size="300" class="uiSplitPane" data-direction="vertical">
        <div data-size="70%" class="uiVerticalSplit">
          <div id="toolbar">
            <button class="uiCommandButton toolbarItem" data-command="in" title="Eingang hinzufügen"></button>
            <button class="uiCommandButton toolbarItem" data-command="out" title="Ausgang hinzufügen"></button>
            <button class="uiCommandButton toolbarItem" data-command="component" title="Element hinzufügen"></button>
            <hr>
            <button class="uiCommandButton toolbarItem" data-command="node" title="Ecke einfügen"></button>
            <button class="uiCommandButton toolbarItem" data-command="disconnect" title="Trennen"></button>
            <button class="uiCommandButton toolbarItem" data-command="delete" title="löschen"></button>
            <button class="uiCommandButton toolbarItem" data-command="clone" title="Duplizieren"></button>
            <!--Achtung läad aktuell die palette neu! <button class="uiCommandButton toolbarItem" data-command="reload" title="Neu laden"></button>-->
            <hr>
            <button class="uiCommandButton toolbarItem" data-command="zoom-out" title="Zoom out"></button>
            <button class="uiCommandButton toolbarItem" data-command="zoom-in" title="Zoom in"></button>
            <hr>
            <button class="uiCommandButton toolbarItem" data-command="clear" title="leeren"></button>
            <button class="uiCommandButton toolbarItem groupCommand" data-command="save" title="Komponente speichern"></button>
            <hr>
            <div class="uiCommandButton uiDropDown toolbarItem" data-command="timer">
              <button class=" toolbarItem" style="box-shadow:none;" title="Taktgeber"></button>
              <div>
                <div style="display:flex;">
                  <input type="checkbox" id="timerCheck" />
                  <select id="timerSelect"></select>
                  <input id="timerInterval" type="number" size="5"/>
                </div>
              </div>
            </div>
            <hr>
            <button class="uiCommandButton toolbarItem" data-command="embed" title="Komponente einbetten"></button>
            <button class="uiCommandButton toolbarItem" data-command="enter-embedded" title="Eingebettete Komponente betreten"></button>
            <button class="uiCommandButton toolbarItem" data-command="leave-embedded" title="Eingebettete Komponente verlassen"></button>
            <hr>
            <button class="uiCommandButton toolbarItem" data-command="table" title="Wertetabelle"></button>
            <button class="uiCommandButton toolbarItem" data-command="impulse" title="Impulsdiagram aufzeichnen"></button>
            <hr>
            <button class="uiCommandButton toolbarItem" data-command="toggle-rubber" title="Rubber band selection"></button>
            <button class="uiCommandButton toolbarItem" data-command="export" title="Export"></button>
            <button class="uiCommandButton toolbarItem" data-command="reload" title="Komponente neu laden"></button>
            </div>
          <div data-expand="true" data-interactive="1" data-ref="mainCanvas" class="uiPiiriPane"  style="overflow:auto;">
            <canvas  tabindex="1" style="box-sizing:border-box;" />
          </div>
        </div>
        <div class="uiAnalysisView" data-ref="analysisView"></div>
        </div>
  </template>






  <template id="sharedDialog">
    <div style="display:flex; flex-direction:column;" x-data="{
      shared: [],
      selection: [],
      init() {
        this.$dispatch('backdrop:shared:startup', this)
      },
      cancel() {
        this.$dispatch('backdrop:shared:cancel')
      },
      importw() {
        let selectedComponents = [...this.selection];
        this.$dispatch('backdrop:shared:import', selectedComponents)
      }
    }">
      <ul>
        <template x-for="w in shared">
          <li>
            <ul>
              <span x-text="w.name"></span>
              <template x-for="c in w.components">
                <li>
                  <span x-text="c"></span><input type="checkbox" :value="w.name + '|' + c" x-model="selection" />
                </li>
              </template>
            </ul>
          </li>
        </template>
      </ul>
      <button @click="importw">importieren</button>
      <button @click="cancel">abbrechen</button>
    </div>
  </template>

  <template id="shareDialog">
    <div style="display:flex; flex-direction:column;" x-data="{
        deps: [],
        component: '',
        init() {
          this.$dispatch('backdrop:share:startup', this)
        },
        cancel() {
          this.$dispatch('backdrop:share:cancel')
        },
        execute() {
          this.$dispatch('backdrop:share:execute')
        },
      }">
      <h1>Komponente freigeben</h1>
      <p>
        Diese Komponente wird freigegeben und kann von anderen in ihren eigenen Arbeitsbereich importiert werden:<br/>
        <strong x-text="component"></strong><br/>
        <br/>
        Zusätzlich werden benötigt und auch freigegeben:
      </p>
      <ul>
        <template x-for="a in deps">
          <li><span x-text="a"></span></li>
        </template>
      </ul>
      <button @click="cancel">abbrechen</button>
      <button @click="execute">freigeben</button>
    </div>
  </template>

  <template id="login">
    <div class="loginForm popupContainer" x-data="{
    groups: <?= isset($groups) ? htmlspecialchars(json_encode(array_map(function($g) {return $g->toJSON(); }, $groups))) : 'null' ?>,
    group: null,
    password: '',
    username: '',
    pin: '',
    message: '',

    customerSearchQuery: '',
    searchResults: null,


    init() {
      let hash = window.location.hash;
      if (hash.startsWith('#')) {
        hash = hash.substring(1)
      }
      this.$nextTick(()=>{
        console.log('Voreingestellte Gruppe: '+hash);
        this.group = hash;

        if (this.$refs.ig) {
          this.$refs.ig.focus();
        } else if (this.group !== '' && this.$refs.ip) {
          this.$refs.ip.focus();
        } else if (this.$refs.icu) {
          this.$refs.icu.focus();
        }
      });
    },
    get isOpen() {
      for (let i in this.groups) {
        if (this.groups[i].name === this.group) {
          return this.groups[i].is_open;
        }
      }
      return false;
    },
    async req(command, body, error) {
      let url = '?c='+encodeURIComponent(command);
      let resp = await fetch(url, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json'
        },
        body: body
      })
      let data;
      try {
        data = await resp.json();
      } catch (e) {
        data = null;
      }
      if (resp.status === 200) {
        return data;
      }
      error(resp.status, data);
      return false;
    },
    enterGuest() {
      $dispatch('piiri:enter-guest');
      $dispatch('prompt:cancel');
    },
    enterGroup(g, u, t) {
      this.$dispatch('piiri:enter-group', {group: g, user: u, token: t});
      $dispatch('prompt:cancel');
    },

    async go2() {
      if (!this.customerSearchQuery) return;
      let resp = await this.req('searchCustomer', JSON.stringify({q: this.customerSearchQuery}), (status, data) => {
        if (status===404) {
          this.searchResults = [];
          return;
        }
        console.error(status, data);
      })
      if (!resp) return;
      this.searchResults = resp;
    },
    goToCustomer(name) {
      window.location.href = this.customerUrl(name);
    },
    customerUrl(name) {
      return window.P + '/' + name;
    },

    async go() {
      this.message = '';
      if (!this.group) return;

      let resp = await this.req('login', JSON.stringify({
        group: this.group,
        password: this.password,
        username: this.username,
        pin: this.pin
      }), (status, data) => {
        this.$root.classList.add('shaking')
        setTimeout(()=>this.$root.classList.remove('shaking'), 500)
        console.error(data);
        if (typeof data === 'string') this.message = data;
        else if (typeof data.toString !== 'undefined') {
          this.message = data.toString();
        } else {
          this.message = 'Ein Fehler ist aufgetreten.'
        }
      })
      if (resp) {
        this.enterGroup(resp.group, resp.user, resp.token.token);
        this.state = 'fin'
        return;
      }
    }
    }" x-on:keyup.esc="enterGuest()">
      <?php if ($customer ?? null) : ?>
        <h1 style="margin-bottom:0;"><?= e($customer->title) ?></h1>
        <h2 style="font-weight:normal; margin-top:0;">
          <template x-if="isOpen">
            <span>Anmeldung offen</span>
          </template>
          <template x-if="!isOpen">
            <span>Anmelden</span>
          </template>
        </h2>
        <template x-if="message">
          <span class="errorMessage" x-text="message"></span>
        </template>
        
        <div style="display:flex; flex-direction: row;">
          <p class="formP" style="margin-right:0.5rem; flex:1 1 50%;">
            <label>Gruppe</label>
            <?php if ($listGroups) : ?>
              <select x-model="group" x-ref="ig">
                <option value="">- Gruppe wählen -</option>
                <template x-for="g in groups">
                  <option x-bind:value="g.name" x-text="g.title"></option>
                </template>
              </select>
            <?php else: ?>
              <input type="text" x-model="group" x-ref="ig" @keyup.enter="go" />
            <?php endif; ?>
          </p>
          <p class="formP" style="margin-left:0.5rem; flex:1 1 50%;">
            <label>Gruppen-Passwort</label>
            <input type="password" x-model="password" x-ref="ip" @keyup.enter="go" />
          </p>
        </div>
        <div style="display:flex; flex-direction: row; margin-top:1rem;">
          <p class="formP" style="margin-right:0.5rem; flex:1 1 50%;">
            <label>Benutzer</label>
            <input type="text" x-model="username" @keyup.enter="go" />
          </p>
          <p class="formP" style="margin-left:0.5rem; flex:1 1 50%;">
            <label>Benutzer-PIN</label>
            <input type="password" x-model="pin" x-ref="ipi" @keyup.enter="go" />
          </p>
        </div>
        <hr/>
        <button @click="go">Anmelden</button>
      <?php else: ?>
        <p class="formP" style="margin-left:0.5rem; flex:1 1 50%;">
          <label>Einrichtung suchen</label>
          <input type="text" x-model="customerSearchQuery" x-ref="icu" @keyup.enter="go2" />
          <button @click="go2">Suchen</button>
          <template x-if="searchResults !== null">
            <div>
              <p x-show="searchResults.length==0"><em>Nichts gefunden.</em></p>
              <template x-for="c in searchResults">
                <a :href="customerUrl(c.name)" class="customerSearchResult"><span x-text="c.title"></span> (<span x-text="c.name"></span>)</div>
              </template>
            </div>
          </template>
        </p>
      <?php endif; ?>
      <p>
        <a href="#" @click.prevent="enterGuest()">PiiRi ausprobieren</a> | 
        <a href="<?= P ?>/piiri-for-education">PiiRi für Bildungseinrichtungen</a>
      </p>
      
    </div>
  </template>

  <div id="backdrop" class="open opaque backdrop">
          <div style="font-size:1.6rem; color:#aaa;">
            <img src="<?= P ?>/img/logo.svg" width="200" />
            <p>
            by Stefan Beyer
            </p>
          </div>
    </div>

    <script src="<?= P ?>/app.js?<?= time() ?>"></script>

</body>
</html>
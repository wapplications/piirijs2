<?php
if (!defined('ENTRY_POINT')) die();

function startLayout($title, $heading) {
  define('START', 1);
  include 'start.php';
}

function endLayout() {
  define('END', 1);
  include 'end.php';  
}



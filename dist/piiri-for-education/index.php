<?php
define('ENTRY_POINT', __FILE__);
require_once 'layout.php';

startLayout("PiiRi für Bildungseinrichtungen", "Features, Nutzung & Preise");

?>

<p>

</p>

  <table class="features container">
    <thead>
      <tr>
        <th colspan="" style="border:none;"></th>
        <th class="plan-demo">Demo</th>
        <th class="plan-basic">Vollversion</th>
        <!--<th class="plan-medium">Medium</th><th class="plan-individuel">Individuell</th>-->
      </tr>
      <?= prices() ?>
      <?= actions() ?>
    </thead>
    <tbody>

      <?= fcat('Zugang') ?>
      <tr>
        <td>Schaltungen von überall verfügbar<small>Der eigene Arbeitsbereich ist überall verfügbar.</small></td>
        <td class="negative">nein</td><td class="positive plan-basic">ja</td>
        <!--<td class="positive plan-medium">ja</td><td class="positive plan-individuel">ja</td>-->
      </tr>
      <tr>
        <td>eigene URL<small>PiiRi ist über eine URL mit dem Namen der Einrichtung erreichbar.</small></td>
        <td class="negative">nein</td><td class="positive plan-basic">ja</td>
        <!--<td class="positive plan-medium">ja</td><td class="positive plan-individuel">ja</td>-->
      </tr>
      <tr>
        <td>Benutzer &amp; Gruppen<small></small></td>
        <td class="negative">nein</td>
        <td class="positive plan-basic">ja</td>
        <!--
        <td class="restricted plan-basic">2</td><td class="restricted plan-medium">6</td><td class="unlimited plan-individuel">individuell</td>-->
      </tr>
      <!--
      <tr>
        <td>Benutzer pro Gruppe<small>Maximale Benutzer-Zahl pro Gruppe</small></td>
        <td class="negative">nein</td>
        <td class="restricted plan-basic">30</td><td class="restricted plan-medium">30</td><td class="unlimited plan-individuel">individuell</td>
      </tr>-->


      
      <?= fcat('Speicherung von Schaltungen & Komponenten') ?>
      <tr>
        <td>Schaltungen speichern<small>Schaltungen und Komponenten dauerhaft speichern.</small></td>
        <td>local storage</td><td class="plan-basic">Server</td>
        <!--<td class="plan-medium">Server</td><td class="plan-individuel">Server</td>-->
      </tr>
      <tr>
        <td>Eigene Komponenten<small></small></td>
        <td class="positive">ja</td><td class="positive plan-basic">ja</td>
        <!--<td class="unlimited plan-medium">unbegrenzt</td><td class="positive plan-individuel">unbegrenzt</td>-->
      </tr>
      
      

      <?= fcat('Aufbau von Schaltungen & Komponenten') ?>
      <tr>
        <td>Inputs und Outputs beliebig benennen<small></small></td>
        <td class="positive">ja</td><td class="positive plan-basic">ja</td>
        <!--<td class="positive plan-medium">ja</td><td class="positive plan-individuel">ja</td>-->
      </tr>
      <tr>
        <td>Die klassischen Logikbausteine<small>AND, OR, XOR, NOT, NOR und NAND Gatter.</small></td>
        <td class="positive">ja</td><td class="positive plan-basic">ja</td>
        <!--<td class="positive plan-medium">ja</td><td class="positive plan-individuel">ja</td>-->
      </tr>
      <tr>
        <td>Schaltungs-Rückkopplungen möglich<small>Flipflops können aus Gattern gebaut werden.</small></td>
        <td class="positive">ja</td><td class="positive plan-basic">ja</td>
        <!--<td class="positive plan-medium">ja</td><td class="positive plan-individuel">ja</td>-->
      </tr>
      <tr>
        <td>Schalter und Anzeigen<small>LED, Konstante, 7-Segment, Bit-Schalter, Bit-Anzeige, Beschriftungstext.</small></td>
        <td class="positive">ja</td><td class="positive plan-basic">ja</td>
        <!--<td class="positive plan-medium">ja</td><td class="positive plan-individuel">ja</td>-->
      </tr>
      <tr>
        <td>Spezial-Komponenten
          <small>Tektgeber-Komponente, RAM, eingebettete Schaltung, pla</small></td>
        <td class="positive">ja</td><td class="positive plan-basic">ja</td>
        <!--<td class="positive plan-medium">ja</td><td class="positive plan-individuel">ja</td>-->
      </tr>
      <tr>
        <td>Eigene Komponenten erstellen und wiederverwenden<small>Eigene Komponenten bauen und in Schaltungen wieder verwenden.</small></td>
        <td class="positive">ja</td><td class="positive plan-basic">ja</td>
        <!--<td class="positive plan-medium">ja</td><td class="positive plan-individuel">ja</td>-->
      </tr>
      <tr>
        <td>Zoom<small></small></td>
        <td class="positive">ja</td><td class="positive plan-basic">ja</td>
        <!--<td class="positive plan-medium">ja</td><td class="positive plan-individuel">ja</td>-->
      </tr>
      <tr>
        <td>In Komponenten "hineinschauen"<small></small></td>
        <td class="positive">ja</td><td class="positive plan-basic">ja</td>
        <!--<td class="positive plan-medium">ja</td><td class="positive plan-individuel">ja</td>-->
      </tr>



      <?= fcat('Weitere Funktionen') ?>
      <tr>
        <td>Komponenten in der Gruppe teilen<small>Die Gruppen-Leitung kann Komponenten zentral für alle Gruppenmitglieder zur Verfügung stellen.</small></td>
        <td class="negative">nein</td><td class="positive plan-basic">ja</td>
        <!--<td class="positive plan-medium">ja</td><td class="positive plan-individuel">ja</td>-->
      </tr>
      <tr>
        <td>Schaltungsteile klonen<small>Schnelles Verdoppeln von Teilen einer Schaltung.</small></td>
        <td class="negative">nein</td><td class="positive plan-basic">ja</td>
        <!--<td class="positive plan-medium">ja</td><td class="positive plan-individuel">ja</td>-->
      </tr>
      <tr>
        <td>Umwandeln zu eingebetteter Komponenten<small></small></td>
        <td class="negative">nein</td><td class="positive plan-basic">ja</td>
        <!--<td class="positive plan-medium">ja</td><td class="positive plan-individuel">ja</td>-->
      </tr>
      <tr>
        <td>Takt-Signal<small>Ein Takt-Signal auf einen Input legen.</small></td>
        <td class="positive">ja</td><td class="positive plan-basic">ja</td>
        <!--<td class="positive plan-medium">ja</td><td class="positive plan-individuel">ja</td>-->
      </tr>
      <tr>
        <td>Werte-Tabelle<small>Eine Wertetabelle der Schaltung erstellen lassen.</small></td>
        <td class="positive">ja</td><td class="positive plan-basic">ja</td>
      </tr>
      <tr>
        <td>Impulsdiagramm<small>Impulsdiagramme aufzeichnen</small></td>
        <td class="positive">ja</td><td class="positive plan-basic">ja</td>
      </tr>



      <?= fcat('Datenschutz') ?>
      <tr>
        <td>Keine Verarbeitung persönlicher Daten<small>von normalen Benutzern</small></td>
        <td class="positive">ja</td><td class="positive plan-basic">ja</td>
        <!--<td class="positive plan-medium">ja</td><td class="positive plan-individuel">ja</td>-->
      </tr>

    </tbody>
    <tfoot>
      <tr>
        <td colspan="5" class="fcat"></td>
      </tr>
      <?= prices() ?>
      <?= actions() ?>
    </tfoot>
  </table>
  <br/>
  <br/>
  <br/>
  <br/>
  <br/>


<?php endLayout(); ?>

<!--
<script>

  function price(g, u, i) {
    let w = i == 2 ? 0.97 : (i == 0.5 ? 1.1 : 1);
    let unit = 1;
    return Math.floor( g * u * unit * i * w );
  }

  function yearlyPrice(p, i) {
    return Math.floor(p / i)
  }


  function priceVorschlag(g,u,i) {
    return {
      groups: g,
      users: u,
      interval: i,

      get price() {
        return price(g, u, i);
      },

      get yearlyPrice() {
        return yearlyPrice(this.price, i);
      }
    }
  }


  function rechner() {
    return {
      groups: 1,
      users: 20,
      interval: 1, // 0.5 1

      get intervalNumber() {
        return parseFloat(this.interval);
      },

      get price() {
        return price(this.groups, this.users, this.intervalNumber);
      },

      get yearlyPrice() {
        return yearlyPrice(this.price, this.intervalNumber);
      }

    }

  }

</script>


<div x-data="rechner()">
  <p>
    <label>Klassen | Gruppen</label>
    <input x-model="groups"  type="number" />
  </p>
  <p>
    <label>Schüler/innen | Benutzer <small>pro Gruppe</small></label>
    <input x-model="users" type="number" />
  </p>
  <p>
    <label>Abrechnungsinterval</label>
    <select x-model="interval">
      <option value="0.5">halbjährlich</option>
      <option value="1">jährlich</option>
      <option value="2">2-jährig</option>
    </select>
  </p>


  <p>
    <span x-text="groups"></span>
    <span x-text="users"></span>
    <span x-text="interval"></span>
    Preis: <span x-text="price"></span>
    yearlyPrice: <span x-text="yearlyPrice"></span>
  </p>


</div>



<div x-data="priceVorschlag(1,20,1)">
    Eine Gruppe mit 20 Benutzern:
    <span x-text="yearlyPrice"></span> pro Jahr
    (Zahlung: <span x-text="price"></span> jährlich)
</div>

<div x-data="priceVorschlag(2,22,2)">
    2 Gruppen mit 22 Benutzern:
    <span x-text="yearlyPrice"></span> pro Jahr
    (Zahlung: <span x-text="price"></span> 2-jährig)
</div>

<div x-data="priceVorschlag(4,25,2)">
    4 Gruppen mit 25 Benutzern:
    <span x-text="yearlyPrice"></span> pro Jahr
    (Zahlung: <span x-text="price"></span> 2-jährig)
</div>

Name
Einrichtung
-->







<?php

function actionUrl($a) {
  //return "./anfrage.php?plan=$a";
  return "mailto:info@kursbegleiter.de?subject=Piiri%20Anfrage%20$a";
}

function actions() {
  return '<tr class="actions">
        <td style="border:none;"></td>
        <td style="border:none;"  class="plan-demo"><a href="../?demo">Demo</a></td>
        <td style="border:none;" class="plan-basic"><a href="'.actionUrl('basic').'">anfragen</a></td>
        <!--<td style="border:none;" class="plan-medium"><a href="'.actionUrl('medium').'">anfragen</a></td>
        <td style="border:none;" class="plan-individuel"><a href="'.actionUrl('individuel').'">anfragen</a></td>-->
      </tr>';
}

function prices() {
  return '<tr>
  <td>Kosten <!--<span>pro Jahr</span>--><!--<small>Laufzeit 1 Jahr</small>--></td>
  <td class="">kostenlos</td><td class="plan-basic">
  auf Anfrage
  </td>
  <!--<td class="plan-medium">
  auf Anfrage
  </td>
  <td class="plan-individuel">
  auf Anfrage
  </td>-->
</tr>
';
}

function fcat($t) {
  return '      <tr>
  <td colspan="3" class="fcat">' . htmlentities($t) . '</td>
</tr>
';
}



?>
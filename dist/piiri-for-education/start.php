<?php
if (!defined('START')) die();
?><!DOCTYPE html>
<html>
<head>
  <title><?= htmlentities($title) ?></title>
    <script defer src="https://cdn.jsdelivr.net/npm/alpinejs@3.x.x/dist/cdn.min.js"></script>
    <style>

      body {
        font-family: sans-serif;
        background-color: #303030;
        color:beige;
      }
      a {
        color: rgb(233, 77, 77);
      }
      .container {
        max-width: 800px;
        margin: 0 auto;
      }
      .features {
        border-collapse: collapse;
      }
      .features td, .features th {
        border: solid 1px gray;
        padding: 0.5rem;
        text-align: center;
      }
      .features tr td:first-child {
        text-align: left;
      }
      .features tr td:first-child small {
        display: block;
        color: gray;
      }
      .features tbody tr td.positive,
      .features tbody tr td.negative,
      .features tbody tr td.unlimited,
      .features tbody tr td.restricted {
        background-repeat: no-repeat;
        background-size: 1rem;
        background-position: center center;
      }
      .features tbody tr td.positive {
        /*color: rgb(48, 143, 48);*/
        color: transparent;
        background-image: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 20 20'%3E%3Cpath d='m 18,2 -10,16 -4,-8' stroke='rgb(48, 143, 48)' stroke-width='4' stroke-linecap='round' stroke-linejoin='round' fill='none'/%3E%3C/svg%3E");
      }
      .features tbody tr td.unlimited {
        /*color: rgb(48, 143, 48);*/
        color: transparent;
        background-image: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 20 20'%3E%3Cpath d='m 2.1964285,10.154751 c 0.1405948,11.54983 15.6249985,-11.7921196 15.6964285,-0.0893 0.07143,11.70281 -15.8370232,-11.4605296 -15.6964285,0.0893 z' stroke='rgb(48, 143, 48)' stroke-width='4' stroke-linecap='round' stroke-linejoin='round' fill='none'/%3E%3C/svg%3E");
      }
      .features tbody tr td.negative {
        /*color: rgb(180, 54, 54);*/
        color: transparent;
        background-image: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 20 20'%3E%3Cpath d='m 2,2 16,16' stroke='rgb(180, 54, 54)' stroke-width='4' stroke-linecap='round' /%3E%3Cpath d='m 18,2 -16,16' stroke='rgb(180, 54, 54)' stroke-width='4' stroke-linecap='round' /%3E%3C/svg%3E");
      }
      .features tbody tr td.restricted {
        color: rgb(155, 155, 250);
      }
      .features th {
        width: 15%;
      }
      .features th:first-child {
        width: 40%;
      }
      td.fcat {
        font-size: 100%;
        border: none;
        font-variant: small-caps;
        padding-top:1rem;
        color:white;
      }

      .plan-demo {
      }
      .plan-basic {
        background-color: rgba(0, 255, 242, 0.06);
      }
      .plan-medium {
        background-color: rgba(255, 153, 0, 0.06);
      }
      .plan-individuel {
        background-color: rgba(187, 0, 255, 0.06);
      }

      .actions td {
        background-color: transparent;
      }
      .actions td a {
        margin-top: 1rem;
        display: inline-block;
        padding: 0.6rem 0.3rem ;
        color: black;
        text-decoration: none;
        background-color: rgba(255,255,255,0.6);
        border:2px solid white;
        width:100%;
        box-sizing: border-box;
      }
      .actions td a:hover {
        background-color: rgba(255,255,255,0.8);
      }
      .actions td.plan-demo a {
        border-color: rgba(255, 255, 255, 0.5);
      }
      .actions td.plan-basic a {
        border-color: rgba(0, 255, 242, 0.5);
      }
      .actions td.plan-medium a {
        border-color: rgba(255, 153, 0, 0.5);
      }
      .actions td.plan-individuel a {
        border-color: rgba(187, 0, 255, 0.5);
      }

    </style>
</head>
<body>

<div class="container" style="display: flex; flex-direction: row; align-items: center;">
  <img src="../img/logo.svg" width="200" />
  <h1 style="font-weight: 100; font-size: 3rem;"><?= htmlentities($heading) ?></h1>
</div>

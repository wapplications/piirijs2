<?php
define('ENTRY_POINT', __FILE__);
require_once 'layout.php';


$planCode = $_GET['plan'] ?? null;

$plans = [
  'basic',
  'medium',
  'individuel'
];

if (!in_array($planCode, $plans)) die();


startLayout("PiiRi für Bildungseinrichtungen", "Anfrage");

?>

<style>
  form p label {
    display: block;
  }
  form p input, form p textarea {
    display: block;
    width:100%;
    box-sizing: border-box;
    border:none;
    padding: 0.5rem;
    background-color: #555;
    color:white;
    font-size: 120%;
  }
</style>
<div class="container">
  <form>
  <p>
      <label>Name der Einrichtung</label>
      <input/>
    </p>
    <p>
      <label>Ort der Einrichtung</label>
      <input/>
    </p>

    Ansprechpartner:
    <p>
      <label>Name</label>
      <input/>
    </p>
    <p>
      <label>E-Mail-Adresse</label>
      <input type="email"/>
    </p>

    Umfang:
    <?php if ($planCode === 'individuel') : ?>
      <p>
        <label>Anzahl Gruppen</label>
        <input type="number" />
      </p>
      <p>
        <label>Anzahl Personen je Gruppen</label>
        <input type="number" />
      </p>
      <p>
        <label>Mitteilung</label>
        <textarea rows="6"></textarea>
      </p>
    <?php endif ?>




  </form>
</div>


<?php endLayout(); ?>

<!--
<script>

  function price(g, u, i) {
    let w = i == 2 ? 0.97 : (i == 0.5 ? 1.1 : 1);
    let unit = 1;
    return Math.floor( g * u * unit * i * w );
  }

  function yearlyPrice(p, i) {
    return Math.floor(p / i)
  }


  function priceVorschlag(g,u,i) {
    return {
      groups: g,
      users: u,
      interval: i,

      get price() {
        return price(g, u, i);
      },

      get yearlyPrice() {
        return yearlyPrice(this.price, i);
      }
    }
  }


  function rechner() {
    return {
      groups: 1,
      users: 20,
      interval: 1, // 0.5 1

      get intervalNumber() {
        return parseFloat(this.interval);
      },

      get price() {
        return price(this.groups, this.users, this.intervalNumber);
      },

      get yearlyPrice() {
        return yearlyPrice(this.price, this.intervalNumber);
      }

    }

  }

</script>


<div x-data="rechner()">
  <p>
    <label>Klassen | Gruppen</label>
    <input x-model="groups"  type="number" />
  </p>
  <p>
    <label>Schüler/innen | Benutzer <small>pro Gruppe</small></label>
    <input x-model="users" type="number" />
  </p>
  <p>
    <label>Abrechnungsinterval</label>
    <select x-model="interval">
      <option value="0.5">halbjährlich</option>
      <option value="1">jährlich</option>
      <option value="2">2-jährig</option>
    </select>
  </p>


  <p>
    <span x-text="groups"></span>
    <span x-text="users"></span>
    <span x-text="interval"></span>
    Preis: <span x-text="price"></span>
    yearlyPrice: <span x-text="yearlyPrice"></span>
  </p>


</div>



<div x-data="priceVorschlag(1,20,1)">
    Eine Gruppe mit 20 Benutzern:
    <span x-text="yearlyPrice"></span> pro Jahr
    (Zahlung: <span x-text="price"></span> jährlich)
</div>

<div x-data="priceVorschlag(2,22,2)">
    2 Gruppen mit 22 Benutzern:
    <span x-text="yearlyPrice"></span> pro Jahr
    (Zahlung: <span x-text="price"></span> 2-jährig)
</div>

<div x-data="priceVorschlag(4,25,2)">
    4 Gruppen mit 25 Benutzern:
    <span x-text="yearlyPrice"></span> pro Jahr
    (Zahlung: <span x-text="price"></span> 2-jährig)
</div>

Name
Einrichtung
-->







<?php




?>
<?php

class PiiriApi extends DB {
  private $token;
  private $user;
  private $group;
  private $customer;

  private $error = null;


  function __construct($dbC, $customer) {
    parent::__construct($dbC);
    $this->customer = $customer;
  }

  private $headers = null;
  function getHeader($key) {
    if ($this->headers === null) {
      $this->headers = array_change_key_case(getallheaders(), CASE_LOWER);
    }
    if (!is_array($this->headers)) {
      return null;
    }
    $key = strtolower($key);
    return $this->headers[$key] ?? null;
  }

  function handleRequest() {
    header("Content-Type: application/json", true);
    $method = strtolower($_SERVER['REQUEST_METHOD']);
    $command = $_GET['c'] ?? '';
    $tokenString = strtolower($this->getHeader('Authorization') ?? '');
    $this->token = Token::findBy('token', $tokenString);
    if ($this->token) {
      $this->group = Group::find($this->token->group_id);
      $this->user = User::find($this->token->user_id);
    }
    // TODO token gültig? timeout?
    if (!$this->commandAllowed($command)) {
      return $this->notAllowed($this->error);
    }
    if ($this->token) {
      $this->token->updateTimeout();
    }
    $handlerMethod = 'handle' . ucfirst($method).ucfirst($command);
    if (method_exists($this, $handlerMethod)) {
      $this->{$handlerMethod}();
    } else {
      $this->notFound();
    }
  }

  private function findUserForToken($token) {
    $stmt = $this->db->prepare("SELECT * FROM `".$this->pfx."users` WHERE `token` = ?");
    $stmt->bind_param("s", $token);
    $stmt->execute();
    $result = $stmt->get_result();
    $user = User::wrap($result->fetch_object());
    return $user;
  }

  function commandAllowed($command) {
    $openForAll = ['searchCustomer'];
    $openWithCustomer = ['login'];
    $openForGroupManager = ['managerData','group', 'user', 'share', 'unshare'];
    
    if (in_array($command, $openForAll)) {
      return true;
    }
    
    if (!$this->customer) return $this->errorAndReturnFalse('Ohne Kunde nicht erlaubt.');
    
    if (in_array($command, $openWithCustomer)) {
      return true;
    }
    
    if (!$this->token) return $this->errorAndReturnFalse('Ohne Token nicht erlaubt.');
    if (!$this->user) return $this->errorAndReturnFalse('Ohne User nicht erlaubt.');
    if (!$this->group) return $this->errorAndReturnFalse('Ohne Gruppe nicht erlaubt.');

    if ($this->group->customer_id !== $this->customer->id) return $this->errorAndReturnFalse('Kunde aus Gruppe stimmt nicht überein:' . $this->group->customer_id .'!=='. $this->customer->id);
    if ($this->user->customer_id !== $this->customer->id) return $this->errorAndReturnFalse('Kunde aus User stimmt nicht überein:' . $this->user->customer_id .'!=='. $this->customer->id);

    if (in_array($command, $openForGroupManager)) {
      if (!$this->user->isManagerInGroup($this->group->id)) return $this->errorAndReturnFalse('User is not Manager.');
    }
    
    return true;
  }


  function errorAndReturnFalse($err) {
    $this->error = $err;
    return false;
  }


  private function getComponent($name) {
    return Component::findByName($name, $this->group->id, $this->user->id);
  }



  function handlePostShare() {
    //$body = $this->getJsonBody();
    $name = $_GET['name'] ?? '';
    if (empty($name)) {
      return $this->badRequest('no name');
    }
    $comp = $this->getComponent($name);
    if (!$comp) return $this->notFound();
    // user_id = null
    $cid = $comp->id;
    $table = $this->pfx.'components';
    $sql = "UPDATE `{$table}` SET `user_id` = NULL WHERE `id` = ?";
    $stmt = $this->db->prepare($sql);
    $stmt->bind_param("i", $cid);
    if (!$stmt->execute()) {
      return $this->badRequest('Update failed');
    }
  }
  function handlePostUnshare() {
    $name = $_GET['name'] ?? '';
    if (empty($name)) {
      return $this->badRequest('no name');
    }
    $comp = $this->getComponent($name);
    if (!$comp) return $this->notFound();
    if (!$comp->isGroupComponent) {
      return $this->badRequest('is not GroupComponent');
    }
    $uid = $this->user->id;
    $cid = $comp->id;
    $table = $this->pfx.'components';
    $sql = "UPDATE `{$table}` SET `user_id` = ? WHERE `id` = ?";
    $stmt = $this->db->prepare($sql);
    $stmt->bind_param("ii", $uid, $cid);
    if (!$stmt->execute()) {
      return $this->badRequest('Update failed');
    }
  }


  function handlePostSearchCustomer() {
    $body = $this->getJsonBody();
    $q = $body['q'] ?? '';
    $results = [];
    if ($q) {
      $stmt = $this->db->prepare("SELECT `name`, `title` FROM `".$this->pfx."customers` WHERE INSTR(`title`, ?) > 0");
      $stmt->bind_param("s", $q);
      $stmt->execute();
      $result = $stmt->get_result();
      $results = $result->fetch_all(MYSQLI_ASSOC);
      if (empty($results)) return $this->notFound();
    }
    return $this->json($results);
  }


  function handlePostLogin() {
    $body = $this->getJsonBody();
    $group = strtolower($body['group'] ?? '');
    if (!$group) return $this->notAllowed("Keine Gruppe angegeben.");
    $password = $body['password'] ?? '';
    $username = $body['username'] ?? '';
    $pin = $body['pin'] ?? '';
    $loginResult = $this->customer->login($group, $password, $username, $pin);
    if (!$loginResult) {
      return $this->notAllowed("Login nicht erfolgreich.");
    }
    $userIsAdmininGroup = $loginResult['user']->isManagerInGroup($loginResult['group']->id);
    $loginResult = array_map(function($e) {
      if (!$e) return $e;
      return $e->toJSON();
    }, $loginResult);
    $loginResult['user']->is_manager = $userIsAdmininGroup;
    return $this->json($loginResult);
  }

  function handleGetList() {
    $gid = $this->group->id;
    $uid = $this->user->id;
    $table = $this->pfx."components";

    // user Components overwrite group components
    // we only need the names here, so make it distinct
    $sql = "SELECT DISTINCT `name`
    FROM `{$table}` 
    WHERE `group_id` = ? AND (`user_id` = ? OR `user_id` IS NULL)";
    $stmt = $this->db->prepare($sql);
    $stmt->bind_param("ii", $gid, $uid);
    $stmt->execute();
    $result = $stmt->get_result();
    $components = $result->fetch_all(MYSQLI_ASSOC);

    $data = [];
    foreach ($components as $c) {
      $data[] = $c['name'];
    }
    // Wollen wir irgendwie markieren, wenn eine user comp. eine shared comp. überschreibt?
    $this->json($data);
  }
  
  function handleGetComponent() {
    $name = $_GET['name'] ?? '';
    $comp = $this->getComponent($name);
    if (!$comp) return $this->notFound();
    $this->json($comp->toJSON());
  }

  function handlePostRename() {
    $name = $_GET['name'] ?? '';
    $newname = $this->getJsonBody();
    
    if (!$name || !$newname || !is_string($newname)) return $this->json(false);
    if ($name === $newname) return $this->json(false);

    $component = $this->getComponent($name);
    if (!$component) return $this->json(false);
    // shared gar nicht umbenennen!
    if ($component->isGroupComponent) return $this->json(false);
    
    $gid = $this->group->id;
    $uid = $this->user->id;
    $stmt = $this->db->prepare("UPDATE {$this->pfx}components SET `name` = ? WHERE `group_id` = ? AND `user_id` <=> ? AND `name` = ? LIMIT 1");
    $stmt->bind_param("siis", $newname, $gid, $uid, $name);
    $stmt->execute();
    
    return $this->json(true);
  }
  
  function handleDeleteComponent() {
    $name = $_GET['name'] ?? '';
    if (!$name) return $this->badRequest();

    $component = $this->getComponent($name);
    if (!$component) return $this->badRequest();
    // shared gar nicht löschen!
    if ($component->isGroupComponent) return $this->json(false);

    $stmt = $this->db->prepare("DELETE FROM `{$this->pfx}components` WHERE `group_id` = ? AND `user_id` = ? AND `name` = ? LIMIT 1");
    $gid = $this->group->id;
    $uid = $this->user->id;
    $stmt->bind_param("iis", $gid, $uid, $name);
    $stmt->execute();

    $this->json(true);
  }

  function handlePostComponent() {

    $postData = $this->getJsonBody();
    $component = Component::create();
    $component->setAttributes($postData);
    $data = $component->getUncastAttributes();


    /*
    if (self::has_group_prefix($data['name'])) {
      if (!$data['isGroupComponent']) {
        return $this->badRequest();
      }
      $data['name'] = self::remove_group_prefix($data['name']);
    }*/

    $sql = 'INSERT INTO `'.$this->pfx.'components` (`name`,	`group_id`,	`user_id`, `props`, `inputs`, `outputs`, `children`, `connections`) 
    VALUES(?,?,?,?,?,?,?,?)
    ON DUPLICATE KEY UPDATE `props`=?,	`inputs`=?,	`outputs`=?,	`children`=?,	`connections`=?';
    $stmt = $this->db->prepare($sql);
    $gid = $this->group->id;
    
    $uid = $component->isGroupComponent ? null : $this->user->id;

    $stmt->bind_param(
      "siisssss"."sssss",
      $data->name,
      $gid,
      $uid,
      $data->props,
      $data->inputs,
      $data->outputs,
      $data->children,
      $data->connections,

      $data->props,
      $data->inputs,
      $data->outputs,
      $data->children,
      $data->connections,
    );
    $stmt->execute();
    $this->json(true);
  }




  /** ADMIN */

  function handleGetManagerData() {
    $users = $this->group->getUsers();
    $users = array_map(function($u) {
      // wir bekommen is_manager von der methode mitgeliefert
      return [
        'name' => $u->name,
        'hasPin'=>!!$u->pin,
        'is_group_manager' => !!$u->is_manager,
        'components' => $u->getComponentCount()
      ];
    }, $users);
    return $this->json([
      'groupTitle' => $this->group->title,
      'isOpen' => $this->group->is_open,
      'users' => $users,
      'maxUsers' => $this->group->max_users
    ]);
  }


  function handlePutUser() {
    $u = $this->getJsonBody();
    if (!is_array($u)) return $this->badRequest();
    if (!is_string($u['name'])) return $this->badRequest();
    if ($this->group->getUsersCount() >= $this->group->max_users) {
      return $this->badRequest(['msg'=>'Benutzer kann nicht erstellt werden: Die maximale Anzahl an Benutzern in der Gruppe ist erreicht.', 'code'=>'group_max_users']);
    }
    try {
      $user = User::create();
      $cid = $this->customer->id;
      $gid = $this->group->id;
      $user->insert(['name'=>$u['name'], 'pin' => '', 'customer_id'=>$cid]);
      $user->addToGroup($gid, false);
      return $this->json(true);
    } catch (mysqli_sql_exception $e) {
      if ($e->getCode() === 1062) {
        return $this->conflict(['msg'=>"Benutzer schon vorhanden.", 'category'=>'db', 'code'=>'duplicate']);
      }
      return $this->badRequest(['msg'=>$e->getMessage(), 'category'=>'db', 'code'=>$e->getCode()]);
    } catch (\Throwable $t) {
      return $this->badRequest(['msg'=>$t->getMessage(), 'category'=>get_class($t), 'code'=>$t->getCode()]);
    }
  }

  function handleDeleteUser() {
    // group admin kann nicht gelöscht werden

    $n = $_GET['name'] ?? null;
    if (empty($n)) return $this->badRequest();
    $user = $this->group->findUser($n, null);

    if ($user->is_manager) return $this->badRequest('Cannot delete manager');

    $user->delete();

    //$stmt = $this->db->prepare("DELETE FROM `{$this->pfx}users` WHERE `group_id` = ? AND `name` = ? AND `is_group_admin` = 0 LIMIT 1");
    //$gid = $this->group->id;
    //$stmt->bind_param("is", $gid, $n);
    //$stmt->execute();
    $this->json(true);
  }

  function handlePostUser() {
    $n = $_GET['name'] ?? null;
    if (empty($n)) return $this->badRequest();
    $userData = $this->getJsonBody();
    if (!$this->containesOnlyAllowedFields($userData, ['name', 'pin']))  return $this->badRequest(['msg'=>'Versuch, nicht freigegebene Felder zu ändern.']);
    $user = $this->group->findUser($n);
    if (!$user) return $this->badRequest();

    // neuer name schon vorhanden?
    if (isset($userData['name'])) {
      $otherUser = $this->group->findUser($userData['name']);
      if ($otherUser) {
        return $this->conflict(['msg'=>"Benutzername schon vergeben.", 'category'=>'db', 'code'=>'duplicate']);
      }
    }

    $user->update($userData);
    return $this->json(true);
  }

  function handlePostGroup() {
    $data = $this->getJsonBody();
    if (!is_array($data)) return $this->badRequest();
    if (!$this->containesOnlyAllowedFields($data, ['is_open', 'password', 'title']))  return $this->badRequest(['msg'=>'Versuch, nicht freigegebene Felder zu ändern.']);
    $this->group->update($data);
    return $this->json(true);
  }


  ##########

  function containesOnlyAllowedFields($data, $allowedFields): bool {
    if (is_object($data)) {
      $data = get_object_vars($data);
    }
    $keys = array_keys($data);
    $diff = array_diff($keys, $allowedFields);
    return empty($diff);
  }

  function getJsonBody() {
    return json_decode(file_get_contents('php://input'), true);
  }


  /*function castComponent($c) {
    foreach (['props', 'inputs', 'outputs', 'children', 'connections'] as $p) {
      $c[$p] = json_decode($c[$p]);
    }
    unset($c['group']);
    return $c;
  }

  function backcastComponent($c) {
    foreach (['props', 'inputs', 'outputs', 'children', 'connections'] as $p) {
      $c[$p] = json_encode($c[$p]);
    }
    if ($c['props'] === '[]') $c['props'] = '{}';
    return $c;
  }*/




  function json($data) {
    echo json_encode($data);
  }

  function notAllowed($data = null) {
    header("HTTP/1.1 403 Forbidden", true, 403);
    if ($data) $this->json($data);
    exit();
  }

  function badRequest($data = null) {
    header("HTTP/1.1 400 Bad Request", true, 400);
    if ($data) $this->json($data);
    exit();
  }

  function notFound($data = null) {
    header("HTTP/1.1 404 Not Found", true, 404);
    if ($data) $this->json($data);
    exit();
  }

  function conflict($data = null) {
    header("HTTP/1.1 409 Conflict", true, 409);
    if ($data) $this->json($data);
    exit();
  }


  
  
}
<?php

class DataNotFoundException extends RuntimeException {
}

class DB {
  protected $db;
  protected $pfx;
  static $instance;

  function __construct($dbConfig) {
    mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
    $this->db = new mysqli($dbConfig['host'], $dbConfig['user'], $dbConfig['password'], $dbConfig['db']);
    $this->db->set_charset($dbConfig['charset'] ?? 'utf8mb4');
    $this->pfx = $dbConfig['prefix'] ?? '';
    static::$instance = $this;
  }

  function getDB(): mysqli {
    return $this->db;
  }

  function prefix(): string {
    return $this->pfx;
  }
}




class Model extends \stdClass {
  protected $attributes;

  function __construct($data) {
    if (is_array($data)) {
      $data = (object)$data;
    }
    $this->attributes = $this->cast($data);
  }

  function load(int $id) {
    $this->loadBy('id', $id);
  }

  function loadBy($col, $value=null) {
    if (!is_array($col)) {
      $cond = [$col => $value];
    } else {
      $cond = $col;
    }
    $table = $this->prefix().$this->_table;
    $sql = "SELECT * FROM `{$table}`";
    $stmt = $this->prepareConditions($sql, $cond);
    $stmt->execute();
    $result = $stmt->get_result();
    $data = $result->fetch_object();
    if (!$data) throw new DataNotFoundException("Row not found ".json_encode($cond));
    $this->attributes = $data;
  }


  static function findBy($col, $value=null) : ?static {
    $m = static::create();
    try {
      $m->loadBy($col, $value);
    } catch (DataNotFoundException $e) {
      return null;
    }
    return $m;
  }

  static function find(int $id) : ?static {
    return static::findBy('id', $id);
  }

  static function create() : static {
    return static::wrap(new \stdClass);
  }

  static function wrap($thing) {
    if (is_array($thing)) {
      return array_map(function($e) {
        return new static($e);
      }, $thing);
    }
    if (is_object($thing)) {
      return new static($thing);
    }
    return null;
  }

  private function cast($data) {
    if (!property_exists($this, '_cast')) return $data;
    $result = new \stdClass;
    foreach ($data as $k => $v) {
      if (key_exists($k, $this->_cast)) {
        $method = $this->_cast[$k] . 'Cast';
        if (method_exists($this, $method)) {
          $result->{$k} = $this->{$method}($v);
          continue;
        }
      }
      $result->{$k} = $v;
    }
    return $result;
  }

  private function uncast($attributes) {
    if (!property_exists($this, '_cast')) return $attributes;
    $result = new \stdClass;
    foreach ($attributes as $k => &$v) {
      if (key_exists($k, $this->_cast)) {
        $method = $this->_cast[$k] . 'Uncast';
        if (method_exists($this, $method)) {
          $result->{$k} = $this->{$method}($v);
          continue;
        } else {
          throw new ErrorException(static::class . ' needs caster '.$this->_cast[$k].' for field '.$k.' but is method '.$method.' not defined.');
        }
      }
      $result->{$k} = $v;
    }
    return $result;
  }

  private function jsonCast($value) {
    return json_decode($value);
  }

  private function jsonUncast($value) {
    return json_encode($value);
  }



  private function jsonObjectCast($value) {
    return json_decode($value);
  }

  private function jsonObjectUncast($value) {
    $ret = json_encode($value);
    if ($ret === '[]') $ret = '{}';
    return $ret;
  }

  private function jsonArrayCast($value) {
    return json_decode($value);
  }

  private function jsonArrayUncast($value) {
    return json_encode($value);
  }



  private function boolCast($value) {
    return boolval($value);
  }

  private function boolUncast($value) {
    return $value ? 1 : 0;
  }


  private function datetimeCast($value) {
    return new DateTime($value, new DateTimeZone('CET'));
  }
  
  private function datetimeUncast($value) {
    return $value->format('Y-m-d H:i:s');
  }

  function getUncastAttributes() {
    return $this->uncast($this->attributes);
  }
  function getAttributes() {
    return $this->attributes;
  }
  function setAttributes($attr) {
    $this->attributes = $attr;
  }


  function insert($attributes) {
    return $this->performInsertOrUpdate($attributes, 'INSERT');
  }
  
  function update($attributes) {
    return $this->performInsertOrUpdate($attributes, 'UPDATE');
  }
  
  private function performInsertOrUpdate($attributes, $operation) {
    if (is_array($attributes)) {
      $attributes = (object)$attributes;
    }
    foreach ($attributes as $k=>$v) {
      $this->attributes->{$k} = $v;
    }
    $data = $this->uncast($attributes);
    if (!property_exists($this, '_table')) throw new \Exception("No _table");
    if ($operation === 'UPDATE' && !$this->id) throw new \Exception("No id");
    $dataStatement = [];
    $bindParams = [];
    $bindString = '';
    foreach($data as $k => $v) {
      if (is_string($v)) {
        $dataStatement[] = '`'.$k.'` = ?';
        $bindString .= 's';
        $bindParams[] = $v;
      } else if (is_numeric($v)) {
        $dataStatement[] = '`'.$k.'` = ?';
        $bindString .= 'i';
        $bindParams[] = $v;
      }
    }
    $sql = $operation." `" . $this->prefix() . $this->_table . "` SET " . implode(', ', $dataStatement);
    if ($operation === 'UPDATE') {
      $sql .= " WHERE `id` = ?";
      $bindString .= 'i';
      $bindParams[] = $this->id;
    }
    $stmt = $this->db()->prepare($sql);
    $stmt->bind_param($bindString, ...$bindParams);
    $stmt->execute();
    if ($operation === 'INSERT') {
      $this->id = $this->db()->insert_id;
    }
  }

  function delete() {
    if (!isset($this->attributes->id)) throw new ErrorException('There is no id attribute for delete()');
    $this->deleteBy('id', $this->id);
  }

  function deleteBy($col, $value = null) {
    if (!is_array($col)) {
      $cond = [$col => $value];
    } else {
      $cond = $col;
    }
    $table = $this->prefix().$this->_table;
    $sql = "DELETE FROM {$table}";
    $stmt = $this->prepareConditions($sql, $cond);
    $stmt->execute();
  }

  function prepareConditions($sql, $cond): \mysqli_stmt {
    $values = [];
    $binding = '';
    $conditions = [];
    // cast wird hier nicht verwendet - nur int und string
    // nur string oder int möglich hier
    foreach ($cond as $k=>$v) {
      $conditions[] = "{$k}=?";
      $binding .= is_string($v) ? 's' : 'i';
      $values[] = $v;
    }
    $sql .= ' WHERE ' . implode(' AND ', $conditions);
    $stmt = $this->db()->prepare($sql);
    $stmt->bind_param($binding, ...$values);
    return $stmt;
  }

  function __get($name) {
    return $this->attributes->{$name} ?? null;
  }

  function __set($name, $value) {
    $this->attributes->{$name} = $value;
  }
  
  function db(): mysqli {
    return Backend::$instance->getDB();
  }

  function prefix(): string {
    return Backend::$instance->prefix();
  }

  function toJSON() {
    return clone $this->attributes;
  }

}

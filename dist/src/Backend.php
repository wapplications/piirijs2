<?php

use Group as GlobalGroup;


function toObjects($array) {
  return array_map(function($a) { return (object)$a; }, $array);
}

function e($d) {
  return htmlentities($d);
}

class Backend extends DB {
  function findCustomer($identifier) {
    if (is_numeric($identifier)) {
      $stmt = $this->db->prepare("SELECT * FROM `".$this->pfx."customers` WHERE `id` = ?");
      $stmt->bind_param("i", intval($identifier));
    } else if (is_string($identifier)) {
      $stmt = $this->db->prepare("SELECT * FROM `".$this->pfx."customers` WHERE `name` = ?");
      $stmt->bind_param("s", $identifier);
    }
    $stmt->execute();
    $result = $stmt->get_result();
    $data = $result->fetch_object();
    return Customer::wrap($data);
  }
}








class Customer extends Model {
  public $settings;

  protected $_cast = [
    'settings' => 'json',
  ];

  function __construct($data) {
    parent::__construct($data);
    if (!$this->attributes->settings) $this->attributes->settings = new \stdClass;
    $this->settings = new SettingsProxy($this->attributes->settings);
  }

  function getGroups() {
    $stmt = $this->db()->prepare("SELECT * FROM `".$this->prefix()."groups` WHERE `customer_id` = ?");
    $id = $this->id;
    $stmt->bind_param("i", $id);
    $stmt->execute();
    $result = $stmt->get_result();
    $data = toObjects($result->fetch_all(MYSQLI_ASSOC));
    return Group::wrap($data);
  }

  function login($groupName, $password, $username, $pin) {
    // TODO auslagern in methode findGroup(...)
    $customerId = $this->id;
    $stmt = $this->db()->prepare("SELECT *
    FROM `".$this->prefix()."groups` 
    WHERE `customer_id` = ? AND `name` = ? AND (`password` = ? OR (`password` IS NULL AND \"\" = ? ))");
    $stmt->bind_param("isss", $customerId, $groupName, $password, $password);
    $stmt->execute();
    $result = $stmt->get_result();
    $group = Group::wrap($result->fetch_object());

    if (!$group) return null;
    $user = $group->findUser($username, $pin);
    if (!$user) {
      $usersCount = $group->getUsersCount();
      if (!$group->is_open) {
        return null;
      }
      if ($usersCount >= $group->max_users) {
        return null; // TODO meldung dass gruppe voll!
      }
      $user = User::create();
      $isGroupManager = false;
      if ($usersCount == 0) {
        $isGroupManager = true;
      }
      $user->insert([
        'name'=>$username,
        'customer_id' => $customerId,
        'pin' => $pin, 
      ]);
      $user->addToGroup($group->id, $isGroupManager);
    }
    if (!$user) return null;
    $tokenString = sha1($this->name . '-' . $group->name . '-' . $user->name . time());
    $token = Token::findBy([
      'user_id'=>$user->id,
      'group_id'=>$group->id,
    ]);
    if ($token) {
      $token->deleteBy([
        'user_id'=>$user->id,
        'group_id'=>$group->id,
      ]);
    }
    $token = Token::create();
    $token->insert([
      'user_id'=>$user->id,
      'group_id'=>$group->id,
      'token'=>$tokenString,
      'timeout'=>$token->createTimeout()
    ]);
    return [
      'group' => $group,
      'token' => $token,
      'user' => $user,
    ];
  }
}


class Component extends Model {
  protected $_table = 'components';
  protected $_cast = [
    'isGroupComponent' => 'bool',
    'overwritesGroupComponent' => 'bool',
    'props'=>'jsonObject',
    'inputs'=>'jsonArray',
    'outputs'=>'jsonArray',
    'children'=>'jsonArray',
    'connections'=>'jsonArray'
  ];

  static function findByName($name, $gid, $uid): ?static {
    $c = new static([]);
    $table = $c->prefix()."components";
    // hier wird isGroupComponent und overwritesGroupComponent gleich per sql erzeugt
    $sql = "SELECT *, (`user_id` IS NOT NULL AND EXISTS (
      SELECT * FROM {$table}
      WHERE `group_id` = ? AND `name` = ? AND  `user_id` IS NULL
    )) as `overwritesGroupComponent`,
    (`user_id` IS NULL) as `isGroupComponent`
    FROM `{$table}` 
    WHERE `group_id` = ? AND `name` = ? AND 
    (`user_id` = ? OR `user_id` IS NULL)
    LIMIT 1";
    $stmt = $c->db()->prepare($sql);
    $stmt->bind_param("is"."isi", $gid, $name,     $gid, $name, $uid);
    $stmt->execute();
    $result = $stmt->get_result();
    $data = $result->fetch_assoc();
    if (!$data) return null;
    return static::wrap((object)$data);
  }

}





class Group extends Model {
  protected $_table = 'groups';
  protected $_cast = [
    'is_open' => 'bool',
  ];

  function getUsers() {
    $stmt = $this->db()->prepare("SELECT `u`.*, `ug`.`is_manager` as `is_manager`
    FROM `".$this->prefix()."users` AS `u` JOIN `".$this->prefix()."users_in_groups` AS `ug` ON (`u`.`id` = `ug`.`user_id`)
    WHERE `ug`.`group_id` = ?");
    $gid = $this->id;
    $stmt->bind_param("i", $gid);
    $stmt->execute();
    $result = $stmt->get_result();
    $data = toObjects($result->fetch_all(MYSQLI_ASSOC));
    return User::wrap($data);
  }
  

  function getUsersCount() {
    return 0; // TODO
    $stmt = $this->db()->prepare("SELECT count(*) as C FROM `".$this->prefix()."users` WHERE `group_id` = ?");
    $gid = $this->id;
    $stmt->bind_param("i", $gid);
    $stmt->execute();
    $result = $stmt->get_result();
    $c = $result->fetch_object();
    return $c->C ?? 0;
  }

  function toJSON() {
    $json = parent::toJSON();
    unset($json->password);
    return $json;
  }

  function findUser($username, $pin = null) {
    $gid = $this->id;
    $sql = "SELECT *, `ug`.`is_manager` AS `is_manager`
    FROM `".$this->prefix()."users` AS `u` JOIN `".$this->prefix()."users_in_groups` AS `ug` ON (`u`.id = `ug`.`user_id`)
    WHERE `ug`.`group_id` = ? AND `name` = ?";
    if ($pin !== null) {
      $sql .= ' AND `pin` = ? ';
    }
    $stmt = $this->db()->prepare($sql);
    if ($pin !== null) {
      $stmt->bind_param("iss", $gid, $username, $pin);
    } else {
      $stmt->bind_param("is", $gid, $username);
    }
    $stmt->execute();
    $result = $stmt->get_result();
    $user = User::wrap($result->fetch_object());
    return $user;
  }


}

class User extends Model {
  protected $_table = 'users';
  protected $_cast = [
    'is_manager' => 'bool',
  ];

  function addToGroup($gid, $isManager) {
    $uid = $this->id;
    if (!$uid) throw new ErrorException('addToGroup: No User-Id');
    $stmt = $this->db()->prepare("INSERT INTO `".$this->prefix()."users_in_groups` (`user_id`, `group_id`, `is_manager`) VALUES (?,?,?)");
    $stmt->bind_param('iii', $uid, $gid, $isManager);
    $stmt->execute();
  }

  function isManagerInGroup(int $group_id) : bool {
    $table = $this->prefix() . 'users_in_groups';
    $stmt = $this->db()->prepare("SELECT `is_manager` 
    FROM `{$table}` WHERE `user_id` = ? AND `group_id` = ?");
    $uid = $this->id;
    $stmt->bind_param("ii", $uid, $group_id);
    $stmt->execute();
    $result = $stmt->get_result();
    $c = $result->fetch_object();
    if (!$c) return false; // Warum sollte das passieren?
    return $c->is_manager ? true : false;
  }

  /*function getGroups() {
    $stmt = $this->db()->prepare("SELECT * 
    FROM `".$this->prefix()."groups` AS `g` join `".$this->prefix()."users_in_groups` AS `ug` ON (`ug`.`group_id` = `g`.`id`) WHERE `ug`.`user_id` = ?");
    $gid = $this->group_id;
    $stmt->bind_param("i", $gid);
    $stmt->execute();
    $result = $stmt->get_result();
    $data = toObjects($result->fetch_all(MYSQLI_ASSOC));
    return Group::wrap($data);
  }*/

  function getComponentCount() {
    $stmt = $this->db()->prepare("SELECT count(*) as C FROM `".$this->prefix()."components` WHERE `group_id` = ? AND `user_id` = ?"); // group unnötig
    $gid = $this->group_id;
    $uid = $this->id;
    $stmt->bind_param("ii", $gid, $uid);
    $stmt->execute();
    $result = $stmt->get_result();
    $c = $result->fetch_object();
    return $c->C ?? 0;
  }


}


class Token extends Model {
  protected $_table = 'tokens';
  protected $_cast = [
    'timeout' => 'datetime',
  ];

  function updateTimeout() {
    $this->update([
      'timeout'=>$this->createTimeout(),
    ]);
  }

  function createTimeout() {
    $timeout = new \DateTime('now', new DateTimeZone('CET'));
    return $timeout;
    //return $timeout->add(new \DateInterval('PT1H'));
  }
}



class SettingsProxy {
  private $ref;
  function __construct(&$ref) {
    $this->ref = $ref;
  }

  function __get($name) {
    return $this->get($name);
  }

  function get($name, $default = null) {
    return $this->ref->{$name} ?? $default;
  }
}
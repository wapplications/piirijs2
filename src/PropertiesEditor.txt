import Alpine from './alpinejs/module.esm.js'
import { IO_TYPE } from './components/IO.js'
//import { createApp } from './vue.global.js'

let subject = null;
let timer = null;

export default () => ({
  type: null,
  title: '',
  saving: false,
  properties: null,
  error: null,
  init() {
    this.$watch('properties', (value, oldValue) => {
      if (oldValue === null || value === null) return;
      if (timer) clearTimeout(timer);
      this.saving = false;
      setTimeout(()=>{this.saving = true;}, 10)
      timer = setTimeout(()=>{
        this.saving = false;
        timer = null;
        this.error = null;
        try {
          let changed = subject.fromEditorObject(this.properties)
          if (changed) {
            window.dispatchEvent(new CustomEvent('need-update'))
          }
        } catch (e) {
          if (typeof e === 'object' && typeof e.name !== 'undefined' && typeof e.msg !== 'undefined') {
            this.error = e;
          } else console.error(e)
        }
      }, 1000)
    });
  },
  binds: {
    ['@selection-changed'](event: CustomEvent) {
      if (timer) return;
      let detail = event.detail;
      if (!detail) {
        this.type = null;
        this.title = '';
        this.properties = null;
        subject = null;
        return;
      }
      this.type = detail.type;
      this.title = detail.title;
      this.properties = detail.properties;
      subject = detail.subject;
    }
  }
});



/*
export default class PropertiesEditor {
  propertiesContainer: HTMLElement;

  constructor(propertiesContainer: HTMLElement) {
    this.propertiesContainer = propertiesContainer;
  }


  renderIOProperies(io) {
    //propertiesData.title = (io.getType()===IO_TYPE.IN ? 'Eingang' : 'Ausgang') + ' ' + io.getName();
  
  }
  
  showProperties(visuals, io) {
    if (visuals.length === 1) {
      let visual = visuals[0]
      let comp = visual.getComponent()
      //propertiesContainer.innerHTML = comp.getName();
      this.propertiesContainer.style.marginRight = '0';
      return;
    } else if (io) {
      this.renderIOProperies(io);
      this.propertiesContainer.style.marginRight = '0';
      return;
    }
    this.hideProperties();
  }
  
  hideProperties() {
    //propertiesContainer.innerHTML = ""
    this.propertiesContainer.style.marginRight = '-200px';
  }
  


}


*/






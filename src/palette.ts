import CirButton from "./ui/CirButton";

const builtInComponents: string[] = [];

export function getPaletteContainer(): HTMLDivElement {
  let $palette = document.querySelector('#ComponentsPalette')
  if (!$palette) throw "no palette found";
  return $palette as HTMLDivElement;
}

export function getSelectedComponentOnPalette(): string | null {
  let $palette = getPaletteContainer();
  let button = $palette.querySelector('.uiCirButton.selected');
  if (!button) return null;
  return button.getAttribute('data-cir');
}

export function getPaletteButtonUi(name, all: boolean = false) {
  let $cont = getPaletteContainer();
  if (all) $cont = $cont.parentElement.parentElement.parentElement as HTMLDivElement;
  let button = $cont.querySelector('.uiCirButton[data-cir="'+name+'"]'); // [data-canopen=true]
  if (button) {
    let buttonUI = button['_ui'];
    if (typeof buttonUI === 'undefined') return null;
    return buttonUI;
  }
  return null;
}

export function initBuiltInComponents() {
  if (builtInComponents.length > 0) return;
  // Liste der built-in components erstellen aus palette
  let $c = getPaletteContainer();
  let $palette = $c.parentElement.parentElement.parentElement;
  let buttons = $palette.querySelectorAll('.uiCirButton');
  for (let i = 0; i < buttons.length; i++) {
    let cir = buttons.item(i).getAttribute('data-cir');
    builtInComponents.push(cir);
  }
  builtInComponents.push('dummy')
  //console.log(builtInComponents)
}
export function resetBuiltInComponents() {
  builtInComponents.length = 0;
}

export function isBuiltInComponent(cirname: string): boolean {
  return builtInComponents.includes(cirname);
}

export function removeFromPalette(name: string) {
  let $cont = getPaletteContainer();
  if (!$cont) return;
  let button = $cont.querySelector('[data-cir="'+name+'"]')
  if (!button) return;
  $cont.removeChild(button);
}

export function updatePaletteButton(name, all: boolean = false) {
  let buttonUI = getPaletteButtonUi(name, all);
  if (!buttonUI) return;
  buttonUI.update();
}

export function renamePaletteButton(old, name) {
  let buttonUI = getPaletteButtonUi(old);
  if (!buttonUI) return;
  buttonUI.updateName(name);
}


export async function addToPalette(name: string): Promise<CirButton|null> {
  let $cont = getPaletteContainer();
  if (!$cont) return null;
  let button = document.createElement('div')
  button.classList.add("uiCirButton");
  button.setAttribute('data-cir', name)
  button.setAttribute('data-canopen', "true")
  $cont.appendChild(button);
  let uiroot = $cont.parentElement['_ui'];
  try {
    let buttonUI = new CirButton(uiroot, button);
    button['_ui'] = buttonUI;
    await buttonUI.promise;
    return buttonUI;
  } catch (e) {
    throw e;
  }
}

export function emptyPalette() {
  let $cont = getPaletteContainer();
  if (!$cont) return;
  $cont.innerHTML = "";
}


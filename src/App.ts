import {ToplevelCircuit} from "./components/ToplevelCircuit";
import {ToplevelCircuitVisual} from "./visuals/ToplevelCircuitVisual";
import PiiRiTab from './PiiRiTab';
import PiiriPane from './ui/PiiriPane';


import CircuitLib, { RequestException } from './components/api';
import Tabs, { UiTab } from "./ui/Tabs";
import { ALLOWED_NAME_CHARACTERS_LOWER, commandEvent, makeName, testName, toolbarButtonEnabledIf, wait } from "./helpers";
import { addToPalette, emptyPalette, getSelectedComponentOnPalette, initBuiltInComponents, removeFromPalette, renamePaletteButton, resetBuiltInComponents, updatePaletteButton } from "./palette";
import createComponent from "./components/factory";
import { Container } from "./components/Container";
import { ButtonPrompt, DeadEndPrompt, StringPrompt } from "./ui/Prompt";
import { ComponentFactoryError, Point, SerializedCircuit } from "./types";
import { Progress } from "./ui/Progress";
import { GroupAdminPrompt } from "./ui/GroupAdminPrompt";
import LoginPrompt from "./ui/LoginPrompt";
import PIIRI_META from "../meta";
import { PopNotification } from "./ui/PopNotification";
import { ImportPrompt } from "./ui/ImportPrompt";
import AnalysisView from "./ui/AnalysisView";
import { refreshSettings } from "./visuals/VisualSettings";

interface UserInfo {
  group: {name: string, title: string},
  user: {name: string, is_manager: boolean},
  token: string
}

function isLocalDevelopement() {
  let host = window.location.hostname;
  return host === '127.0.0.1';
}

export default class App {

  tabs: Tabs;
  userInfo : UserInfo = null;
  progress: Progress;

  constructor(tabs) {
    this.tabs = tabs;
  
    let doc = this.getDomDocument();

    this.progress = new Progress(this.getDomDocument());


    doc.addEventListener('piiri:lightmode-changed', ()=>{
      refreshSettings()
      // aktuelle schaltung neu zeichnen
      this.forwardToCurrentTab((uitab)=>{
        uitab.data.tab.refresh(false, false);
      });
      // Palette neu zeichnen
      let cirB = document.querySelectorAll<HTMLButtonElement>('.uiCirButton');
      for (let i = 0; i < cirB.length; i++) {
        const element = cirB[i];
        let cir = element.dataset['cir'];
        // alle buttons mit einbeziehen
        updatePaletteButton(cir, true);
      }
    });

    doc.addEventListener('piiri:enter-guest', this.enterGuest.bind(this));
    doc.addEventListener('piiri:enter-group', this.enterGroup.bind(this));

    doc.addEventListener('piiri:command:rename', this.renameCommand.bind(this));
    doc.addEventListener('piiri:command:remove', this.removeCommand.bind(this));
    doc.addEventListener('piiri:command:share_group', this.shareGroupCommand.bind(this));
    doc.addEventListener('piiri:command:reload', this.reloadPaletteCommand.bind(this));
    doc.addEventListener('piiri:command:admin', this.groupAdminCommand.bind(this));
    doc.addEventListener('piiri:command:import', this.importCommand.bind(this));

    
    // debug 
    /*doc.addEventListener('piiri:command:share', ()=>{
      let data = this.component.toJsonObject()
      console.log(data);
    });*/ //this.shareCommand.bind(this)
    //doc.addEventListener('piiri:command:shared', this.sharedCommand.bind(this));

    doc.addEventListener('piiri:command:open', this.openCommand.bind(this));
    doc.addEventListener('piiri:command:add-component', this.addComponentCommand.bind(this));
    doc.addEventListener('piiri:update-palette', this.onUpdatePalette.bind(this));
    doc.addEventListener('piiri:rename-palette', this.onRenamePalette.bind(this));
    doc.addEventListener('piiri:refresh-current-tab', (e: CustomEvent) => {
      let {resize, update} = e.detail;
      this.forwardToCurrentTab((uitab)=>{
        uitab.data.tab.refresh(resize, update);
      })
    });

    this.tabs.$element.addEventListener('tab:new', (e) => {
      e.stopPropagation();
      this.newTab();
    })
    
    document.addEventListener('piiri:palette:selection-changed', ()=>{
      this.updateUi()
    })
    this.updateUi()

    this.tabs.$element.addEventListener('keyup', this.onKeyUp.bind(this));
    this.tabs.$element.addEventListener('keydown', this.onKeyDown.bind(this));

  }

  newTab() {
    this.openComponent(null);
  }

  addComponentCommand(e: CustomEvent<string>) {
    let cir = e.detail;
    this.forwardToCurrentTab((tab: UiTab)=>{
      tab.container.dispatchEvent(new CustomEvent('piiri:component', { detail: {cir, pos:null} }))
    })
  }

  openComponent(cir: null | string | SerializedCircuit) {

    if (typeof cir === 'string') {
      for (let uitab of this.tabs.tabs) {
        if (uitab.data.tab.rootComponent.getCirName() === cir) {
          this.tabs.activateTab(uitab);
          return uitab;
        }
      }
    }

    let uitab = this.tabs.getCurrentTab();

    if (uitab && cir !== null) {
      if (uitab.data.tab.isUsed()) {
        uitab = null;
      }
    }
    if (cir === null) uitab = null;

    if (uitab === null) {
      let template = this.tabs.getDomDocument().getElementById('piiriTab') as HTMLTemplateElement 
      let tabcontent = template.content.firstElementChild.cloneNode(true) as HTMLElement;
      // das ist alles ein bisschen verquer - kann man sicher vereinfachen
      let data = {
        tab: null,
        app: this,
      };
      uitab = this.tabs.createTab('Schaltung 1', tabcontent, data);
      let piiriPane = (this.tabs.root.refMap['mainCanvas'] as PiiriPane);
      let alalysis = this.tabs.root.refMap['analysisView'] as AnalysisView;
      let a = new ToplevelCircuit();
      new ToplevelCircuitVisual(a) // das muss ich hier machen um circuläre problem zu verhindern , das ist scheiße!
      let tab = new PiiRiTab(uitab, piiriPane, a, alalysis);
      data.tab = tab;
      tab.start();
      this.tabs.activateTab(uitab);
    } else {
      uitab.data.tab.emptyCircuit();
    }

    if (cir !== null && uitab) {
      uitab.data.tab.openComponent(cir)
    }
    return uitab;
  }


  openCommand(event: CustomEvent) {
    let cir = event.detail;
    this.openComponent(cir);
  }

  forwardToCurrentTab(f) {
    let uitab = this.tabs.getCurrentTab();
    if (!uitab) return;
    f(uitab);
  }



  getDomDocument() {
    return document;
  }

  async start() {

    
    if (isLocalDevelopement()) {
      //                 0        1          2        3
      let testLogin = ['user', 'manager', 'login', 'guest'][2];

      if (testLogin === 'user') {
        this.enterGroup({group: {name: 'klasse10a', title: "STATIC", },user: {name: "hermine", is_manager: false}, token: 'abc'});
        //this.enterGroup({group: {name: '10b', title: "STATIC", },user: {name: "anna", is_manager: false}, token: 'dfhdztsrtgadrga'});
      } else if (testLogin === 'manager') {
        this.enterGroup({group: {name: 'klasse10a', title: "STATIC", },user: {name: "beyer", is_manager: true}, token: 'd6c3d74b08866bfdf6bc7c7a6af67a95e23d829d'});
      } else if (testLogin === 'login') {
        await (new LoginPrompt(this.getDomDocument())).show()
      } else if (testLogin === 'guest') {
        this.enterGuest();
      }

      //this.test();
      /*setTimeout(()=> {
        this.openComponent('pla-test');
      }, 1000);*/



    } else {
      // login mode
      await (new LoginPrompt(this.getDomDocument())).show()
    }
    
    
    
    
    
  }


  test() {
    setTimeout(()=>{
      this.forwardToCurrentTab(async (uitab)=>{
        let tab: PiiRiTab = uitab.data.tab;

        let errors = [];
        await tab.rootComponent.loadCircuit({
          "isGroupComponent": false,
          overwritesGroupComponent: false,
          "name": "",
          inputs: [{name: 'in', pinCount: 1},],
          outputs: [{name: 'out', pinCount: 1}],
          children: [
            {
              component: 'and',
              name: 'a1',
              pos: {x: 20, y: 100},
              componentData: null,props: {}
            },
            {
              component: 'and',
              name: 'a2',
              pos: {x: 200, y: 100},
              componentData: null,props: {}
            },
            {
              component: 'node',
              name: 'n1',
              pos: {x: 100, y: 100},
              componentData: null,props: {}
            },
            {
              component: 'node',
              name: 'n2',
              pos: {x: 350, y: 200},
              componentData: null,props: {}
            },
          ],
          connections: [
            {from: 'a1.x', to: 'n1.i'},
            {from: 'n1.o', to: 'n2.i'},
            {from: 'n2.o', to: 'a2.a'},
            {from: 'in.in', to: 'a1.a'},
            {from: 'a2.x', to: 'out.out'},
          ],

          /*"isGroupComponent": false,
          overwritesGroupComponent: false,
          "name": "",
          "inputs": [
            {name: 'a', pinCount: 1},
            //{name: 'b', pinCount: 3},
            {name: 'c', pinCount: 1}],
          "outputs": [
            {name: 'x', pinCount: 1},
            {name: 'y', pinCount: 3},
            {name: 'z', pinCount: 1}],
          "children": [

            {component: '4add', name: '4add', componentData: null, pos: {x:400, y:200}, props: {}},

            {component: 'and', name: 'and1', componentData: null, pos: {x:100, y:100}, props: {}},
            {
              component: 'embedded', name: 'e', componentData: {
                inputs: [{name: 'A', pinCount: 1}],
                outputs: [{name: 'V', pinCount: 1}],
                children: [],
                connections: [
                  {from: 'in.A', to: 'out.V'}
                ],
                isGroupComponent: false,
                overwritesGroupComponent: false,
                props: {},
                name: 'embedded'
              },
              pos: {x: 0, y: 0},
              props: {}
            },
            {
              component: 'va',
              componentData: null,
              name: 'va',
              pos: {x: 300, y:40},
              props: {}
            }
          ],
          "connections": [
            { "from": "in.a", to: "and1.a" },
            { "from": "in.c", to: "and1.b" },
            { "from": "and1.x", to: "out.x" },
            { "from": "and1.x", to: "e.A" },
          ],
          */
          "props": {}
        }, errors)
        tab.updateUi();
        tab.updateTimerControl();        
        tab.refresh(true, true)
          
      })
    }, 100);
  }

  unload() {

  }

  async loadPalette() {
    emptyPalette();
    resetBuiltInComponents();
    initBuiltInComponents();

    let componentErrors = [];

    let waiter = (new PopNotification(this.getDomDocument())).text("Komponenten werden geladen.").waiting();

    this.progress.start();

    let compNames: string[];

    try {
      compNames = await CircuitLib.list();
    } catch (e) {
      if (e instanceof RequestException && e.isNotAllowed()) {
        (new DeadEndPrompt(this.getDomDocument())).show("Nicht erlaubt", "Der Vorgang ist nicht erlaubt. Vermutlich ist die Authentifizierung fehlgeschlagen. Bitte App neu laden.");
        return;
      }
      throw e;
    }

    let count = compNames.length;
    let counter = 0;
    
    for (let comp of compNames) {
      try {
        let ui = await addToPalette(comp);
        if (ui.componentErrors.length > 0) {
          componentErrors = componentErrors.concat(ui.componentErrors)
        }
      } catch (e) {
        console.warn("Die Komponent "+comp+" nicht zur Palette hinzugefügt werden: ", e);
      }
      counter++;
      this.progress.set(counter/count)
    }

    this.progress.finished();
    waiter.done();
    this.paletteError(componentErrors);
  }

  paletteError(componentErrors: ComponentFactoryError[]) {
    /*if (componentErrors.length === 0) return;
    let compiled = {};
    for (let ce of componentErrors) {
      if (typeof compiled[ce.childComponent] === 'undefined') {
        compiled[ce.childComponent] = [ce.parentComponent];
      }
    }
    let c = this.getDomDocument().createElement('div');
    c.classList.add('popupContainer')
    c.style.minWidth = '20rem';
    c.style.maxWidth = '30rem';
    c.style.display = 'flex';
    c.style.flexDirection = 'column';
    c.style.overflow = 'hidden';
    let cc = this.getDomDocument().createElement('div');
    cc.innerText="Beim Laden der Komponenten-Palette konnten folgende Komponenten nicht geladen werden:";
    //cc.style.backgroundColor="red"
    cc.style.overflow = 'auto';
    cc.style.flexGrow = "1";
    cc.style.flexShrink = "1";
    cc.style.flexBasis = "auto";
    let b = this.getDomDocument().createElement('button');
    b.innerText = 'Schließen';
    b.style.flexGrow = "0";
    b.style.flexShrink = "0";
    b.style.flexBasis = "auto";
    b.addEventListener('click', (e)=>{
      closeOnBackdrop(this.getDomDocument())
      //e.target.dispatchEvent(new CustomEvent('prompt:close', {bubbles:true}))
    })
    let ul = this.getDomDocument().createElement('ul');    
    for (let child in compiled) {
      let li = this.getDomDocument().createElement('li');
      let d = this.getDomDocument().createElement('d');
      d.style.fontSize = '70%';
      d.style.fontStyle = 'italic'
      d.innerText = 'Für: '+compiled[child].join(', ');
      let span = this.getDomDocument().createElement('div');
      span.innerText = child;
      li.appendChild(span);
      li.appendChild(d);
      ul.appendChild(li);
    }
    cc.appendChild(ul);
    c.appendChild(cc);
    c.appendChild(b);
    openOnBackdrop(this.getDomDocument(), c);
    */
  }

  onUpdatePalette(event: CustomEvent) {
    let {cirname, isNew} = event.detail;
    if (isNew) {
      addToPalette(cirname);
    } else {
      updatePaletteButton(cirname);
    }
  }


  onRenamePalette(event: CustomEvent) {
    let {old, cirname} = event.detail;
    renamePaletteButton(old, cirname);
  }




  
  protected async groupAdminCommand() {
    if (!this.isGroupManager()) {
      alert('Das darf nur der Gruppen-Admin.');
      return;
    }
    let p = new GroupAdminPrompt(this.getDomDocument());
    await p.show();
  }





  updateUserAtGroup() {
    let doc = this.getDomDocument();
    let $el = doc.getElementById('userAtGroup');
    if (this.userInfo === null) {
      doc.title = 'PiiRi ' + PIIRI_META.version + ' - Demo' ;
      $el.textContent = 'you@Demo';
      $el.classList.remove('isManager');
    } else {
      doc.title = 'PiiRi ' + PIIRI_META.version + ' - ' + this.userInfo.group.title;
      $el.textContent = this.userInfo.user.name + '@' + this.userInfo.group.name;
      if (this.isGroupManager()) {
        $el.classList.add('isManager');
      } else {
        $el.classList.remove('isManager');
      }
    }
  }

  enterGuest() {
    console.log('Bitte eintreten als Gast.')
    this.userInfo = null;
    this.updateUserAtGroup();
    //this.closeOnBackdrop();
    CircuitLib.token = null;
    this.enter();
  }

  async enterGroup(event: CustomEvent<UserInfo> | UserInfo) {
    console.log('Bitte eintreten als Mitglied einer Gruppe.')
    let userInfo : UserInfo;
    if (event instanceof CustomEvent) {
      userInfo = event.detail;
    } else {
      userInfo = event;
    }
    this.userInfo = userInfo;
    //await this.closeOnBackdrop();
    this.updateUserAtGroup();
    CircuitLib.token = userInfo.token;
    this.enter();
  }

  enter() {
    this.loadPalette();
    this.newTab();
    this.updateUi();
  }

  isGroupManager(): boolean {
    if (!this.userInfo) return false;
    return this.userInfo.user.is_manager;
  }

  /*closeOnBackdrop(): Promise<void> {
    return closeOnBackdrop(this.getDomDocument());
  }*/

  /*openOnBackdrop(elm: string): HTMLElement {
    return openOnBackdrop(this.getDomDocument(), elm);
  }*/

  async reloadPaletteCommand() {
    CircuitLib.resetCache();
    await this.loadPalette();    
  }

  async importCommand() {
    let p = new ImportPrompt();
    let json = await p.show();
    let open = false;
    if (!open && !!json.name) {
      let component = await CircuitLib.get(json.name);
      if (!component) {
        if (await CircuitLib.store(json)) {
          addToPalette(json.name);
          return;
        } 
        alert('Fehler beim Speichern.')
        return;
      }
      if (!confirm(`Komponente »${json.name}« ist schon vorhanden kann nicht direkt importiert werden. Soll sie als neue Komponente geöffnet werden?`)) {
        return;
      }
    }
    json.name = '';
    let uitab = this.openComponent(json);
  }


  async removeCommand() {
    let name = getSelectedComponentOnPalette();
    if (!name) return;

    let component = await CircuitLib.get(name);
    if (!component) return;

    // TODO informieren, wenn auf Gruppen-Kompnente zurückgesetzt wird

    if (component.isGroupComponent /*&& !this.isGroupManager()*/) {
      alert('Gruppen-Komponenten können nicht gelöscht werden.');
      return;
    }
    
    // TODO ermitteln, welche Komponenten davon betroffen sind und anzeigen (zumindest einen auszug davon wenn es zu viele sind)
    if (!confirm("Soll die Komponente '"+name+"' unwiderruflich gelöscht werden?\nBeachte: Komponenten die diese Komponente benötigen, werden nicht mehr funktionieren.")) return;

    // if is open in tab: close it!
    let uitabsToClose = this.findOpenTabsForComponent(name);
    uitabsToClose.forEach(uitab=>uitab.close())

    let answer = await CircuitLib.del(name);
    if (answer === true) {
      CircuitLib.resetCache(name);
      if (component.overwritesGroupComponent) {
        updatePaletteButton(name);
      } else {
        removeFromPalette(name);
      }
    } else {
      console.error('failed');
    }
  }


  async renameCommand() {
    // TODO betroffene komponenten angeben, erweitertes umbenennen ermöglichen? Dazu bräuchten wir vermutlich eine andere Datenstruktur
    let name = getSelectedComponentOnPalette();
    if (!name) return;
    let component = await CircuitLib.get(name);
    if (!component) return;
    if (component.isGroupComponent /*&& !this.isGroupManager()*/) {
      // auch manager sollte die geteilte comp nicht umbenennen, andere könnten darauf aufbauen...
      // TODO evtl anbieten, eine Kopie zu erzeugen
      // Soll eine Kopie erstellt werden?
      alert('Gruppen-Komponente kann nicht umbenannt werden. ');
      return;
    }
    if (this.findOpenTabsForComponent(name).length > 0) {
      alert("Komponente ist geöffnet. Vor dem Umbenennen, schließen.");
      return;
    }
    
    let oldname = name;

    let p = new StringPrompt(this.getDomDocument());
    p.title = "Komponente umbenennen";
    p.description = "Neuer Name für die Komponente ("+ALLOWED_NAME_CHARACTERS_LOWER+"):"
    p.setValidator((v)=>{
      if (!testName(v.toLowerCase())) return 'Name genügt nicht den Vorgaben.';
      return true;
    })
    p.start(name);
    // let cirname = prompt("Neuer Name für die Komponente:", name);
    let cirname = await p.result();

    cirname = makeName(cirname);
    if (!testName(cirname)) return;

    await CircuitLib.rename(oldname, cirname);
    renamePaletteButton(oldname, cirname);
  }


  findOpenTabsForComponent(name: string): UiTab[] {
    let result = [];
    for (let uitab of this.tabs.tabs) {
      if (uitab.data.tab.rootComponent.getCirName() === name) {
        result.push(uitab);
      }
    }
    return result;
  }


  async shareGroupCommand() {
    if (!this.isGroupManager()) {
      alert('Komponente für die Gruppe freigeben geht nur als Gruppen-Admin.');
      return;
    }
    let name = getSelectedComponentOnPalette();
    if (!name) return;

    let componentErrors = [];
    let component = await createComponent(null, name, name, false, componentErrors);
    
    // Fehler hier melden?
    if (!component) return;
    if (!(component instanceof Container)) return;

    let answer: string;

    if (component.isGroupComponent) {
      // TODO  nochmals alles sharen, weil sich was geändert hat (neue komponenten dazu gekommen sind?
      let p = new ButtonPrompt(this.getDomDocument());
      answer = await p.show(
        "Komponente ist bereits freigegeben",
        "",
        {
          again: "[Erneut einbetten und freigeben]",
          unshare: "Freigabe zurücknehmen",
          cancel: "Abbrechen"
        },
        '');
      if (answer === 'cancel') return;
      if (answer === 'again') {
        alert("Nicht implementiert");
        return;
      }
      if (answer === 'unshare') {
        await CircuitLib.unshareInGroup(name);
        CircuitLib.resetCache(name);
        updatePaletteButton(name);
        return;
      }
      return;
    }

    let p = new ButtonPrompt(this.getDomDocument());
    answer = await p.show(
      "Komponente für Gruppe freigeben",
      "Wie soll die Komponente freigegeben werden?",
      {
        outer: "Komponente ohne Abhängigkeiten",
        embeddedCopy: "[Kopie mit eingebettetem Inhalt]",
        cancel: "Abbrechen"
      },
      '');
    if (answer === 'cancel') return;
    if (answer === 'outer') {
      await CircuitLib.shareInGroup(name);
      CircuitLib.resetCache(name);
      updatePaletteButton(name);
      return;
    }
    if (answer === 'embeddedCopy') {
      alert('Diese Funktion ist noch nicht implementiert.');
    }

      /*
    let deps = [...component.getDependencies()];
    let newName = addGroupPrefix(name);
    let ccc = await CircuitLib.get(newName, true);
    if (ccc) {
      // TODO
      alert("Komponente kann nicht freigegeben werden, da eine freigegebene Komponente mit dem selben Namen bereits existiert.");
      return;
    }
    for (let child of component.getChildren()) {
      if (child instanceof Container) {
        child.embed();
      }
    }
    component.setCirName(newName);
    component.isGroupComponent = true;
    let serialized = component.toJsonObject();
    let answer = await CircuitLib.store(serialized);

    addToPalette(newName);
    */
  }

  onKeyUp(event: KeyboardEvent) {
    if (event.repeat) return;
    let handler;
    let key = event.key;
    if (event.ctrlKey) {
      key = 'STRG+'+key;
    }
    handler = {
      "+": 'component',
      "Delete": 'delete',
      "#": 'node',
    }[key];

    if (typeof handler === 'undefined') {
      this.handleKeyUp(event.key, event);
      return;
    }
    if (typeof handler === 'string') {
      event.preventDefault();
      this.forwardToCurrentTab((uitab)=>{
        let detail = null;
        uitab.container.dispatchEvent(new CustomEvent('piiri:command:'+handler, { detail }))
      })
      return;
    }
    handler();
    event.preventDefault();
  }

  onKeyDown(event: KeyboardEvent) {
    if (event.repeat) return;
    let handler;
    let key = event.key;
    if (event.ctrlKey) {
      key = 'STRG+'+key;
    }
    handler = {
      "STRG+i": 'in',
      "STRG+o": 'out',
    }[key]
    if (typeof handler === 'undefined') {
      this.handleKeyDown(event.key, event);
      return;
    }
    if (typeof handler === 'string') {
      event.preventDefault();
      this.forwardToCurrentTab((uitab)=>{
        let detail = null;
        uitab.container.dispatchEvent(new CustomEvent('piiri:command:'+handler, { detail }))
      })
      return;
    }
    handler();
    event.preventDefault();
  }

  handleKeyDown(key, event) {
    this.forwardToCurrentTab((uitab: UiTab)=>{
      let tab = uitab.data.tab;
      let input = tab.getSwitchableInput(key);
      if (!input) return;
      input.setBooleanValue(true)
      tab.refresh(false, true)
      event.preventDefault();
    })
  }
  
  handleKeyUp(key, event) {
    this.forwardToCurrentTab((uitab: UiTab)=>{
      let tab = uitab.data.tab;
      let input = tab.getSwitchableInput(key);
      if (!input) return;
      input.setBooleanValue(false)
      tab.refresh(false, true)
      event.preventDefault();
    })
  }



  updateUi() {
    let isManager = this.isGroupManager();
    let somethingSelected = ()=>(getSelectedComponentOnPalette()!==null);
    this.toolbarButtonEnabledIf('rename', somethingSelected());
    this.toolbarButtonEnabledIf('remove', somethingSelected());
    this.toolbarButtonEnabledIf('share_group', isManager && somethingSelected());
    this.toolbarButtonEnabledIf('admin', isManager);
    //this.toolbarButtonEnabledIf('reload', somethingSelected());
  }

  toolbarButtonEnabledIf(command: string, enabled: boolean) {
    // TODO main toolbar container??
    toolbarButtonEnabledIf(document, command, enabled);
  }


}


//import type from 'lockr/dist/index';


export enum CircuitStateChanged {
  SWITCHED,
  MOVING,
  MOVED,
  SELECTING,
  SELECTED,
  CONNECTING,
  CONNECTED,
};


export enum COMPONENT_AREA {
  NONE = 0,
  INNER = 1,
  TITLE = 2,
  IN = 4,
  OUT = 8,
};


export interface Selectable {
  select: ()=>void,
  deselect: ()=>void,
  isSelected: ()=>boolean,
}

export const SIMULATION_DEBUG: boolean = false;
export const COMMAND_LOG: boolean = false;

export type Point = {
  x: number,
  y: number
}

export type Size = {
  w: number,
  h: number
}

interface SerializedIO {
  name: string,
  pinCount: number
}

export type ComponentFactoryError = {
  parentComponent: string | null,
  childComponent: string
};

export interface SerializedChildComponent {
  component: string,
  componentData: SerializedCircuit | null,
  name: string,
  pos: Point,
  props: SerializedProps,
}

interface SerializedConnection {
  from: string,
  to: string
}

interface SerializedProps {
  [key: string]: unknown
}

export type SerializedCircuit = {
  name: string,
  isGroupComponent: boolean,
  overwritesGroupComponent: boolean,
  props: SerializedProps,
  inputs: SerializedIO[],
  outputs: SerializedIO[],
  children: SerializedChildComponent[],
  connections: SerializedConnection[],
}
import { IOValue } from "./components/IO";
import { ComponentFactoryError, Point, Size } from "./types";
import settings from "./visuals/VisualSettings";


export class TemporaryEvents {
  element: EventTarget;
  callbacks = [];

  constructor(element: EventTarget) {
    this.element = element;
  }

  listen(type: string, callback, options?: boolean | AddEventListenerOptions) {
    this.callbacks.push({type, callback});
    this.element.addEventListener(type, callback, options)
  }

  clear() {
    for (let l of this.callbacks) {
      this.element.removeEventListener(l.type, l.callback)
    }
  }
}




export function compileColor(a: number[]|string): string {
  if (typeof a === 'string') return a;
  let s = "rgb";
  let alpha;
  if (a.length === 4) {
    alpha = a.pop();
    s += "a"
  }
  s += '('+a.map(e=>Math.round(e*255)).join(',');
  if (typeof alpha !== 'undefined') {
    s += ',' + alpha;
  }
  s += ')';
  return s;
}

export function relativeLine(ctx: CanvasRenderingContext2D, x1: number, y1: number, dx: number, dy: number) {
  ctx.moveTo(x1, y1);
  ctx.lineTo(x1+dx, y1+dy);
}

export function line(ctx: CanvasRenderingContext2D, x1: number, y1: number, x2: number, y2: number) {
  ctx.moveTo(x1, y1);
  ctx.lineTo(x2, y2);
}

export function curvedWire(ctx: CanvasRenderingContext2D, x1: number, y1: number, x2: number, y2: number, startCurved: boolean, endCurved: boolean) {
  let xdelta = x2 - x1;
  if (xdelta < 0) {
    xdelta = Math.min(200, -0.7*xdelta);
  } else {
    xdelta = Math.min(100, 0.2*xdelta);
  }
  ctx.moveTo(x1, y1);
  ctx.bezierCurveTo(
    x1 + (startCurved?xdelta:0), y1,
    x2 - (endCurved?xdelta:0), y2,
    x2, y2
  );
}


export function wire(ctx: CanvasRenderingContext2D, x1: number, y1: number, x2: number, y2: number, sn1: boolean, sn2: boolean) {
  switch (settings.WIRE_MODE) {
    case 'line':
      line(ctx, x1, y1, x2, y2);
      break;
    case 'bezier':
      let startCurved = !sn1;
      let endCurved = !sn2;
      /*if (sn1 && x1>x2) {
        startCurved = false;
      }
      if (sn2 && x1>x2) {
        endCurved = false;
      }*/
      curvedWire(ctx, x1, y1, x2, y2, startCurved, endCurved);
      break;
  }
}


export function t(a: string): string {
    return a;
}

export function ui_text(a: string, b: string) {
  return {a: a, b: b};
}

export function clock(): number {
  return Date.now();
  //return (new Date).getTime();
}


export function toBoolean(v: IOValue) {
  return (v & 1) === 1;
}

export function toIOValue(v: boolean) {
  return v ? 1 : 0;
}


export function getViewPortSize(): Size {
  return {
    w: Math.max(document.documentElement.clientWidth || 0, window.innerWidth || 0),
    h: Math.max(document.documentElement.clientHeight || 0, window.innerHeight || 0)
  }
}


/// global commands
export function commandEvent(command, detail = null) {
  document.dispatchEvent(new CustomEvent('piiri:command:'+command, { detail }))
}

// global
export function dispatchRefreshCurrentTab(resize:boolean = true, update: boolean = true) {
  document.dispatchEvent(new CustomEvent('piiri:refresh-current-tab',  { detail: {resize, update} }))
}


export function getDomElementByIdOrThrow(id, doc: Document = null): HTMLElement {
  if (doc === null) doc = document;
  let a = doc.getElementById(id);
  if (a && a instanceof doc.defaultView.HTMLElement) {
    return a;
  }
  throw "element " + id + " not found"
}

export function star(ctx, x, y, r, n, inset) {
  ctx.beginPath();
  ctx.translate(x, y);
  ctx.moveTo(0,0-r);
  for (var i = 0; i < n; i++) {
      ctx.rotate(Math.PI / n);
      ctx.lineTo(0, 0 - (r*inset));
      ctx.rotate(Math.PI / n);
      ctx.lineTo(0, 0 - r);
  }
  ctx.closePath();
}



export function drawLED(ctx: CanvasRenderingContext2D, pos: Point, size: number, state: boolean, onColor: string = "") {
    // lämpchen-Farbe
    ctx.beginPath();
    ctx.arc(pos.x, pos.y, size, 0, Math.PI * 2)
    if (state) {
      ctx.fillStyle = onColor;
    } else {
      ctx.fillStyle = "#444444";
    }
    ctx.fill()
    ctx.lineWidth = 2;
    ctx.strokeStyle = '#666666';
    ctx.stroke();

    // glanzlicht
    ctx.beginPath();
    ctx.ellipse(pos.x, pos.y - size / 2.3, size / 2, size / 3, 0, 0, Math.PI * 2)
    if (state) ctx.fillStyle = "rgba(255,255,255,0.6)";
    else ctx.fillStyle = "rgba(255,255,255,0.2)";
    ctx.fill()
    // schatten
    ctx.beginPath();
    ctx.arc(pos.x, pos.y, size, Math.PI * 2 + 0.6, Math.PI - 0.2)
    ctx.fillStyle = "rgba(0,0,0,0.2)";
    ctx.fill()

    if (state) {
      ctx.save();
      star(ctx, pos.x+size/2.4, pos.y-size/1.5, 2, 5, 5)
      ctx.fillStyle = "white";
      ctx.fill();
      ctx.restore();
    }

}


export function drawSwitch(ctx: CanvasRenderingContext2D, pos: Point, size: number, state: boolean, onColor: string = "") {
  ctx.save();
  ctx.beginPath();
  ctx.moveTo(pos.x+size/2, pos.y);
  ctx.arcTo(
    pos.x-size*20, pos.y+size/2, // kontroll punkt 
    pos.x+size/2, pos.y+size, // endpunkt
    size/2
  );
  ctx.lineTo(pos.x+size+size/2, pos.y+size)
  ctx.arcTo(
    pos.x+size*22, pos.y+size/2, // kontroll punkt 
    pos.x+size/2+size, pos.y, // endpunkt
    size/2
  );
  /*ctx.arcTo(
    pos.x, pos.y, // kontroll punkt (ecke)
    pos.x, pos.y+size/2, // endpunkt
    size/2
  );
  ctx.arcTo(
    pos.x, pos.y+size,
    pos.x+size/2, pos.y+size, 
    size/2
  );
  ctx.lineTo(pos.x+size+size/2, pos.y+size)
  ctx.arcTo(
    pos.x+size*2, pos.y+size,
    pos.x+size*2, pos.y+size/2, 
    size/2
  );
  ctx.arcTo(
    pos.x+size*2, pos.y,
    pos.x+size+size/2, pos.y, 
    size/2
  );*/
  ctx.closePath();

  ctx.fillStyle = state ? (onColor==""?'green':onColor) : 'gray';
  ctx.fill();
  ctx.stroke();

  ctx.beginPath();
  ctx.arc(pos.x+size/2 + (state ? size : 0), pos.y+size/2, size/2-2, 0, Math.PI*2)
  ctx.fillStyle = "white"
  ctx.fill();
  ctx.stroke();
  ctx.restore();
}


export function distance(p1: Point, p2: Point): number {
  var x = p2.x-p1.x;
  var y = p2.y-p1.y;
  return Math.sqrt(x*x+y*y);
}


export function wait(ms: number): Promise<void> {
  return new Promise<void>((res, rej) => {
    setTimeout(()=>res(), ms)    
  });
}


export const ALLOWED_NAME_CHARACTERS_LOWER = "0-9, a-z, äöüß, -, _";
export const ALLOWED_NAME_CHARACTERS = "0-9, a-z, A-Z, äöüÄÖÜß, -, _";
export function testName(name: string | null, allowUpperCase: boolean = false): boolean {
  if (name === null) return false;
  if (allowUpperCase) {
    return /^[0-9a-zA-ZöäüÄÖÜß\-_]+$/.test(name);
  }
  return /^[0-9a-zäöüß\-_]+$/.test(name);
}
export function makeName(name: string | null, allowUpperCase: boolean = false): string {
  if (name === null) return "";
  if (allowUpperCase) {
    return name.replace(/[^0-9a-zA-ZöäüÄÖÜß\-_]/g, '');
  }
  return name.toLowerCase().replace(/[^0-9a-zäöüß\-_]/g, '');
}


export class PiiriCircularException {
  parentComponantName: string;
  componentName: string;
  
  constructor(parentComponantName: string, componentName: string) {
    this.componentName = componentName;
    this.parentComponantName = parentComponantName;
  }

  toString() {
    return "Zirkuläre Referenz in '"+ this.parentComponantName +"' gefunden: '"+this.componentName+"'."
  }
}


/*
export function hasGroupPrefix(name: string) {
  return name.substring(0,1) === '~';
}

export function addGroupPrefix(name: string) {
  return '~' + name;
}

export function removeGroupPrefix(name: string) {
  if (hasGroupPrefix(name)) {
    return name.substring(1);
  }
  return name;
}*/


export function toolbarButtonEnabledIf(container, command: string, enabled: boolean) {
  let button = container.querySelector('[data-command="'+command+'"]');
  if (button.tagName !== 'BUTTON') button = button.querySelector('button');
  if (button.disabled !== !enabled) button.disabled = !enabled;
}


export function handleComponentErrors(componentErrors: ComponentFactoryError[]) {
  if (componentErrors.length === 0) return;
  let s = new Set(componentErrors.map((e)=>e.childComponent));
  let msg = 'Diese Komponenten konnten nicht erzeugt werden: ' + Array.from(s).join(', ') + '.';
  alert(msg);
}





export function downloadText(text: string, filename: string) {
  let blob = new Blob([text], {type: 'text/plain'});
  downloadBlob(blob, filename)
}

export function downloadBlob(blob: Blob, filename: string) {
  let url = URL.createObjectURL(blob);
  let link = document.createElement('a');
  link.href = url;
  link.download = filename;
  link.click();
  URL.revokeObjectURL(url);
  //document.body.removeChild(link);
}


function startOpenFile(callback: (reader: FileReader, file: File)=>void, mime?: string, maxSize?: number, crop?: boolean): Promise<FileReader> {
  if (typeof crop === 'undefined') crop = false;
  let $input = document.createElement('input');
  $input.setAttribute('type', 'file');
  $input.setAttribute('accept', mime ?? '*/*');
  // TODO wenn abbruch getätigt wird, kommen wir aus dem Promise niemals raus oder?
  return new Promise<FileReader>((res, rej) => {
    $input.addEventListener('change', ()=>{
      let files = $input.files;
      if (files === null || files.length === 0) {
        rej('canceled');
        return;
      }
      let file: File = files[0]; // File extends Blob
      if (crop===false && typeof maxSize !== 'undefined' && file.size > maxSize) {
        rej('Datei zu groß. Grenze: ' + maxSize);
        return;
      }
      const reader = new FileReader();
      reader.onload = (e: ProgressEvent<FileReader>) => {
        res(reader);
      };
      callback(reader, file);

    });
    $input.click();
  })
}

/**
 * 
 * @param mime for ex. "image/*,application/pdf"
 * @param maxSize 
 * @returns 
 */
export async function openFileAsText(mime?: string, maxLength?: number, crop?: boolean): Promise<string> {
  if (typeof crop === 'undefined') crop = false;
  let fileReader = await startOpenFile((reader, file)=>(reader.readAsText(file)), mime);
  let str = fileReader.result as string;
  if (typeof maxLength !== 'undefined' && str.length > maxLength) {
    if (crop) {
      alert('Text wurde auf ' + maxLength + ' Zeichen gekürzt.');
      str = str.substring(0, maxLength);
    } else {
      throw "Text ist zu lang. Grenze: " + maxLength;
    }
  }
  return str; 
}

/**
 * 
 * @param mime for ex. "image/*,application/pdf"
 * @param maxSize 
 * @returns 
 */
export async function openFileAsArrayBuffer(mime?: string, maxSize?: number, crop?: boolean): Promise<ArrayBuffer> {
  let fileReader = await startOpenFile((reader, file)=>(reader.readAsArrayBuffer(file)), mime, maxSize, crop);
  let data = fileReader.result as ArrayBuffer;
  if (typeof crop === 'undefined') crop = false;
  if (crop && typeof maxSize !=='undefined' && data.byteLength > maxSize) {
    alert('Datei wurde auf ' + maxSize + ' Bytes gekürzt.');
    data = data.slice(0, maxSize);
  }
  return data;
}



export class Signal<Type extends Function> {
  connections: Type[] = [];

  connect(callback: Type) {
    this.connections.push(callback);
  }

  disconnect(callback: Type) {
    this.connections = this.connections.filter((e)=>(e!==callback));
  }

  emit(...args) {
    this.connections.forEach((e: Type)=>{
      //e.apply(a, args);
      e(...args)
    })
  }

  clear() {
    this.connections = [];
  }





}
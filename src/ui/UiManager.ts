import BodyScreen from "./BodyScreen";
import SplitPane from "./SplitPane";


const registered_ui = []

export function register_ui(name, constr) {
  registered_ui.push({name: name, 'class': constr})
}

export function init_ui(root: BodyScreen, element: HTMLElement) {
  element.querySelectorAll(":scope > *").forEach(e=>{
    if (typeof e['_ui'] !== 'undefined') {
      return;
    }
    let converted = false;
    registered_ui.forEach((r) => {
      if (e.classList.contains(r.name)) {
        let C = r.class;
        e['_ui'] = new C(root, e);
        e['_ui'].init();
        converted=true;
        if ((e as HTMLElement).dataset['ref']) {
          let ref = (e as HTMLElement).dataset['ref'];
          root.refMap[ref] = e['_ui'];
        }
      }
    });
    if (!converted) {
      init_ui(root, e as HTMLElement)
    }
  });
}


/*    element.querySelectorAll(":scope > .uiSplitPane").forEach(e=>{
      if (typeof e['ui'] === 'undefined') {
        e['ui'] = new SplitPane(e);
      }
    });*/

    /* TODO
    $('.uiVerticalSplit').each(function(){
      new VerticalSplit(this);
    });
    */



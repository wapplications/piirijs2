import Ui from './Ui'
import icons from './icons';

export default class Icon extends Ui {
  constructor(root, element) {
    super(root, element);
    let icon = element.dataset['icon'] ?? '';
    if (typeof icons[icon] === 'undefined') {
      element.innerText = icon;
    } else {
      element.innerHTML = icons[icon]
    }
  }
}



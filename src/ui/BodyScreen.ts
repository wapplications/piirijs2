import Ui from './Ui'
import {init_ui} from './UiManager'

export default class BodyScreen extends Ui {

  refMap: {
    [key: string]: Ui
  } = {};

  constructor(elm: HTMLElement) {
    super(null, elm);
    this.root = this;
    this.$element["_ui"] = this; // TODO nötig?
    this.$element.classList.add('screen');
    var win = this.getDomDocument().defaultView;
    win.addEventListener('resize', this.windowSizeChanged.bind(this));
  }

  windowSizeChanged(e) {
    //console.log("resize "+this.getDomDocument().defaultView.name)
    this.onSizeChanged();
  }

  onSizeChanged() {
    let div = this.$element.querySelector(':scope > div');
    if (!div) throw "BodyScreen.onSizeChanged: no div";
    if (div['_ui'] instanceof Ui) {
      div['_ui'].stretch(true,true);
    }
    this.sizeElements(div);
  }

}

import Ui from './Ui'
import createComponent from '../components/factory'
import createVisual from '../visuals/factory'
//import {ContainerVisual} from "../visuals/ContainerVisual";
import {Component} from '../components/Component'
import { commandEvent, hasGroupPrefix, PiiriCircularException, removeGroupPrefix } from '../helpers';
import { Point } from '../types';

export default class CirButton extends Ui {
  cir: string;
  canvas: HTMLCanvasElement;
  promise: Promise<Component>;
  span: HTMLSpanElement;
  icon: HTMLImageElement;
  enabled: boolean = true;
  componentErrors = [];

  constructor(root, element) {
    super(root, element);
    let c = this.$element.dataset['cir'];
    if (typeof c === 'undefined') throw "CirButton no cir"

    this.span = this.getDomDocument().createElement('span');
    this.span.style.fontSize = '0.9rem';
    this.$element.append(this.span);

    let div = this.getDomDocument().createElement('div');
    this.canvas = this.getDomDocument().createElement('canvas');
    this.canvas.setAttribute('width', "1");
    this.canvas.setAttribute('height', "1");
    div.appendChild(this.canvas);
    this.$element.append(div);

    this.$element.style.width = '80px';
    this.$element.style.height = '80px';
    this.$element.style.overflow = 'hidden';
    this.$element.style.verticalAlign = 'middle';
    this.$element.style.position = 'relative';


    this.icon = this.getDomDocument().createElement('img');
    this.icon.style.position = "absolute";
    this.icon.style.right = '0';
    this.icon.style.bottom = '0';
    this.$element.appendChild(this.icon);



    this.updateName(c);
    this.update();



    // https://developer.mozilla.org/en-US/docs/Web/API/HTML_Drag_and_Drop_API
    this.$element.setAttribute('draggable', 'true');
    this.$element.addEventListener('dragstart', (e) => {
      if (!this.enabled) {
        return false;
      }
      if (mouseTimer) {
        // sonst wird long click gefeuert
        clearTimeout(mouseTimer);
        mouseTimer = 0;
      }
      e.dataTransfer.setData("application/piiri-cir", JSON.stringify({
        cir: (this.cir),
        dx:e.offsetX, dy:e.offsetY
      }));
      e.dataTransfer.dropEffect = "copy";
      //e.originalEvent.dataTransfer.setDragImage(this.canvas, w, h);
    });


    // Long click to ann component if dnd not working
    let mouseTimer = null;
    let mousePos: Point = {x:0,y:0};
    this.$element.addEventListener('mousedown', (e) => {
      if (mouseTimer) clearTimeout(mouseTimer);
      mousePos = {x: e.clientX, y: e.clientY};
      mouseTimer = setTimeout(()=>{
        mousePos = {x: mousePos.x-e.clientX, y: mousePos.y-e.clientY};
        if (Math.sqrt(mousePos.x*mousePos.x+mousePos.y*mousePos.y)>10) {
          return;
        }
        this.dispatchAddComponent(this.cir);
      }, 500);
    });
    this.$element.addEventListener('mouseup', (e) => {
      if (mouseTimer) {
        clearTimeout(mouseTimer);
        mouseTimer = 0;
      }
    });


    let canopen = !!JSON.parse(this.$element.dataset['canopen'])
    if (canopen) {
      let select = () => {
        deselect();
        this.$element.classList.add('selected');
        this.dispatchSelectionChanged();
      }
      let deselect = () => {
        this.$element.parentElement.querySelector('.selected')?.classList.remove('selected');
      }
      this.$element.addEventListener('dblclick', (e) => {
        if (!this.enabled) return;
        commandEvent('open', this.cir)
        select();
      });
      this.$element.addEventListener('click', (e) => {
        //if (!this.enabled) return;
        if (this.$element.classList.contains('selected')) {
          this.$element.classList.remove('selected')
          this.dispatchSelectionChanged();
          return;
        }
        select();
      });
    }


  }

  dispatchAddComponent(cir: string) {
    commandEvent('add-component', this.cir)
  }

  dispatchSelectionChanged() {
    let event = new CustomEvent('piiri:palette:selection-changed');
    document.dispatchEvent(event);
  }

  /*prefixComponentNameIfGroupComponent(name) {
    if (this.isGroupComponent) {
      return '~'+name;
    }
    return name;
  }*/

  update() {
    let zoom = 1;

    let resolvePromise;
    let rejectPromise;
    this.promise = new Promise<Component>((res, rej)=>{
      resolvePromise = res;
      rejectPromise = rej;
    });

    this.componentErrors = [];

    createComponent(null, this.cir, this.cir, false, this.componentErrors).then(cirObject => {
      
      if (cirObject === null) {
        rejectPromise("no comp: ");
        return;
      }

      if (cirObject.isGroupComponent) {
        this.icon.src = window['P']+"/img/groupcomp.svg";
      } else if (cirObject.overwritesGroupComponent) {
        this.icon.src = window['P']+"/img/usercomp.svg";
      } else {
        this.icon.src = '';
      }
  
      let hasErrors = this.componentErrors.length > 0;

      // props setzen
      let props = this.getJsonData('props')
      if (props && typeof props === 'object') {
        cirObject.setProps(props);
      }
      // Eingänge setzen
      let inputs = this.getJsonData('inputs')
      if (typeof inputs === 'object' && !!inputs) {
        for (let i in inputs) {
          cirObject.setInput(i, inputs[i])
        }
      }
      let cirVisual = createVisual(cirObject);

      cirVisual.titleHeight = 2;
      
      let ctx = this.canvas.getContext("2d");
      if (ctx === null) throw "no ctx"
  

      //cirVisual.setPreviewMode(true);
      cirVisual.updateSize(ctx, 0, 0, false);
      let size = cirVisual.getSize();
      // wenn größer als 100, verkleinern
      let max = Math.max(size.w, size.h);
      let maxSize = 80;
      if (max > maxSize) {
        if (size.h > maxSize) {
          zoom = maxSize/size.h;
        } else {
          zoom = maxSize/size.w;
        }
      }
      this.canvas.setAttribute('width', ""+(size.w*zoom));
      this.canvas.setAttribute('height', ""+(size.h*zoom));
      /*this.$element.style.width = (size.w*zoom)+'px',
      this.$element.style.height = (size.h*zoom)+'px',*/
      //this.$element.style.background = 'none',
      ctx.save();
      ctx.scale(zoom, zoom);
      ctx.clearRect(0, 0, size.w, size.h);
      cirVisual.showName = false;
      cirVisual.paintAll(ctx, 0, 0, false);
      ctx.restore();



      if (hasErrors) {
        let img = this.getDomDocument().createElement('img');
        img.style.position = "absolute";
        img.style.left = '0';
        img.style.bottom = '0';
        this.$element.appendChild(img);
        img.src = window['P']+"/img/comperror.svg";
      }
  

      resolvePromise();
    }).catch((e) => {
      this.enabled = false;
      if (e instanceof PiiriCircularException) {
        this.$element.title = e.toString();
      } else if (typeof e === 'string') {
        this.$element.title = e;
      }
      let ctx = this.canvas.getContext("2d");
      if (ctx !== null) { // rotes error-symbol
        this.canvas.setAttribute('width', "20");
        this.canvas.setAttribute('height', "20");
        ctx.moveTo(0, 0);
        ctx.lineTo(20, 20);
        ctx.moveTo(20, 0);
        ctx.lineTo(0, 20);
        ctx.strokeStyle = "red";
        ctx.lineWidth = 3;
        ctx.stroke();
      }
      rejectPromise(e);
    })
  }


  updateName(cirname) {
    this.cir = cirname;
    this.span.innerText = this.cir;
    this.$element.setAttribute('title', cirname);
    this.$element.setAttribute('data-cir', cirname);
  }

}


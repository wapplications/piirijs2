import Ui from './Ui';


export class AccordeonTab extends Ui {
  $title;
  $container;


  createDom() {
    let doc = this.getDomDocument();
    this.$title = doc.createElement('div');
    this.$title.textContent = this.$element.getAttribute('title');
    this.$title.classList.add('uiAccTabTitle')
    this.$container = doc.createElement('div');
    this.$container.classList.add('uiAccTabContainer')
    let children = this.$element.children;
    while (this.$element.firstElementChild) {
      this.$container.appendChild(this.$element.firstElementChild);
    }
    this.$element.appendChild(this.$title);
    this.$element.appendChild(this.$container);

    this.$title.addEventListener('click', ()=>{
      if (this.isOpen()) {
        this.close();
      } else {
        this.open();
      }
    });
  }

  isOpen(): boolean {
    return this.$element.classList.contains('open');
  }

  open() {
    this.$element.classList.add('open');
    this.dispatchOpened();
  }

  close() {
    this.$element.classList.remove('open');
  }

  dispatchOpened() {
    let event = new CustomEvent('ui:accordeon:tab:opened', {detail: this, bubbles: true});
    this.$element.dispatchEvent(event);
  }

}

export default class Accordeon extends Ui {

  singleOpen: boolean = true;

  constructor(root, element) {
    super(root, element);

    this.$element.addEventListener('ui:accordeon:tab:opened', (event: CustomEvent)=>{
      event.stopPropagation();
      if (this.singleOpen) {
        let tabs = this.getTabs();
        for (let i = 0; i < tabs.length; i++) {
          let $tab = tabs.item(i);
          let ui = $tab['_ui'];
          if (typeof ui === 'undefined') continue;
          if (ui === event.detail) continue;
          ui.close();
        }
      }
    });
  }

  getTabs() {
    return this.$element.children;
  }

  createDom() {
  }



}



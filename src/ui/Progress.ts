import { wait } from "../helpers";



export class Progress {
  document: Document;
  $el: HTMLDivElement;

  constructor(doc: Document) {
    this.document = doc;
    this.create();
  }

  create() {
    this.$el = this.document.createElement('div');
    this.$el.classList.add('globalProgress');
    this.document.querySelector('body').appendChild(this.$el);
  }

  start() {
    this.$el.classList.add('show')
    this.$el.style.width = "0";
  }

  set(value: number) {
    this.$el.style.width = "" + (value*100) + '%'
    if (value >= 1) {
      this.$el.addEventListener('transitionend', async ()=>{
        await wait(1000);
        this.startHiding();
      }, {once:true})
    }
  }

  finished() {
    this.set(1);
  }
  
  startHiding() {
    this.$el.classList.remove('show')
    this.$el.addEventListener('transitionend', async ()=>{
      this.$el.style.width = "0";
    }, {once:true})
  }
}
import SplitPane from "./SplitPane";
import { init_ui } from "./UiManager";

export default class Ui {
  $element: HTMLElement;
  root = null;

  constructor(root, element: HTMLElement) {
    this.root = root;
    this.$element = element;
    this.createDom();
  }

  createDom() {
  }

  getDomDocument(): Document {
    return this.$element.ownerDocument;
  }

  init() {
    init_ui(this.root, this.$element);
  }

  getJsonData(key: string): number | object | string | boolean | null {
    let p = this.$element.dataset[key]
    let props = null
    if (typeof p !== 'undefined') {
      try {
        props = JSON.parse(p);
        return props;
      } catch(e) {
        console.error('UI error in getJsonData', e)
      }
    }
    return null;
  }



  onSizeChanged() {
    // nix
  }



  stretch(x: boolean, y: boolean) {
    let parent = this.$element.parentElement;
    if (!parent) return;
    if (y) {
      this.$element.style.height = parent.clientHeight + "px";
    }
    if (x) {
      this.$element.style.width = parent.clientWidth + "px";
    }
  }

  doSizeChanged() {
    this.sizeElements(this.$element)
  }

  sizeElements($ch) {
    if ($ch instanceof this.getDomDocument().defaultView.HTMLCollection) {
      for (let i = 0; i < $ch.length; i++) {
        this.sizeElements($ch.item(i))
      }
      return;
    }

    if ($ch instanceof this.getDomDocument().defaultView.NodeList || Array.isArray($ch)) {
      $ch.forEach(e => this.sizeElements(e))
      return;
    }

    if ($ch instanceof this.getDomDocument().defaultView.HTMLElement) {
      if (typeof $ch['_ui'] !== 'undefined') {
        $ch['_ui'].onSizeChanged();
      } else {
        let $_ch = ($ch as HTMLElement).children;
        this.sizeElements($_ch);
      }
    }



  }
}

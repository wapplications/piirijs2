import Ui from './Ui'
import { getViewPortSize, TemporaryEvents } from '../helpers';
import icons from './icons';

export default class CommandButton extends Ui {
  constructor(root, element) {
    super(root, element);

    let command = element.dataset['command'] ?? '';
    let button;

    if (element.tagName == 'BUTTON') {
      // normaler knopf
      button = element;
      element.addEventListener('click', () => {
        let detail = null;
        let event = new CustomEvent('piiri:command:'+command, { detail, bubbles: true, cancelable: true });
        this.$element.dispatchEvent(event)
      })
    } else { // TODO andere fälle
      //console.warn('NYI');
      // drop down settings
      let target = this.getDomDocument().defaultView;
      if (target === null) return;
      let te = new TemporaryEvents(target);
      button = element.querySelector('button');
      let dd = element.querySelector('button + div');
      button.addEventListener('click', (e) => {
        e.preventDefault();
        e.stopPropagation();
        if (element.classList.contains('open')) {
          element.classList.remove('open');
          te.clear();
        } else {
          te.listen('click', function(ee) {
            // click auf das dropdown?
            let p = ee.target; do { if (p===dd) { return; } p = p.parentElement; } while(p);
            // click elsewhere
            ee.preventDefault();
            ee.stopPropagation();
            element.classList.remove('open');
            te.clear();
          })
          element.classList.add('open');
          dd.style.right = '';
          setTimeout(()=>{
            let rect = dd.getBoundingClientRect();
            let w = getViewPortSize().w;
            if (rect.right > w) {
              dd.style.right = '0';
            }
          }, 50)
        }
        //commandEvent(command)
      })
    }

    if (typeof icons[command] === 'undefined') {
      button.innerText = command;
    } else {
      button.innerHTML = icons[command]
    }

  }



}



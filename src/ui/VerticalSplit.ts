import Ui from './Ui'

export default class VerticalSplit extends Ui {
  $p1: HTMLElement;
  $p2: HTMLElement;
  expand: string;

  constructor(root, element) {
    super(root, element);

    if (!this.$element.firstElementChild) throw 'VerticalSplit.constructor: no p1'
    if (!this.$element.lastElementChild) throw 'VerticalSplit.constructor: no p2'
    this.$p1 = this.$element.firstElementChild as HTMLElement;
    this.$p2 = this.$element.lastElementChild as HTMLElement;

    this.$p1.classList.add('pane');
    this.$p2.classList.add('pane');

    this.expand = 'bottom';
    if (this.$p1.dataset['expand']) {
      this.expand = 'top';
      this.$p1.style.height = 'auto';
    } else if (this.$p2.dataset['expand']) {
      this.expand = 'bottom';
      this.$p2.style.height = 'auto';
    } else {
      console.error("VerticalSplit: expand");
    }
  }


  onSizeChanged() {
    let w = this.$element.clientWidth;
    let h = this.$element.clientHeight;
    

    if (this.expand === 'bottom') {
      this.$p1.style.height = 'auto';
      let fixedHeight = this.$p1.clientHeight;
      this.$p1.style.height = fixedHeight + 'px'
      this.$p2.style.height = (h-fixedHeight) + 'px'
    } else if (this.expand === 'top') {
      this.$p2.style.height = 'auto';
      let fixedHeight = this.$p2.clientHeight;
      this.$p2.style.height = fixedHeight + 'px'
      this.$p1.style.height = (h-fixedHeight) + 'px'
    }

    this.$p1.style.width = w + 'px'
    this.$p2.style.width = w + 'px'


    this.sizeElements(this.$p2);
    this.sizeElements(this.$p1);
  }


}

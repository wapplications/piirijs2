import Ui from './Ui'

export default class Log extends Ui {
  constructor(root, element: HTMLElement) {
    super(root, element);
    element.style.backgroundColor = '#444444';
    element.style.color = '#bbbbbb';
    element.style.fontFamily = 'monospace,mono,typewriter'
    //element.style.padding = '0.4rem';
    element.style.overflow = 'auto';
    //element.style.boxSizing = 'border-box';
    element.classList.add('pad')
  }

  log(...data: any[]) {
    this.line('#bbbbbb', ...data)
  }

  warn(...data: any[]) {
    this.line('#bbaa77', ...data)
  }

  error(...data: any[]) {
    this.line('#bb7777', ...data)
  }

  line(c, ...data: any[]) {
    let el = this.getDomDocument().createElement('div');
    el.style.color = c;
    for (let l of data) {
      let span = this.getDomDocument().createElement('span');
      span.innerText = this.toString(l) + " ";
      el.appendChild(span);
    }
    this.$element.appendChild(el);
      setTimeout(()=>{this.$element.scrollTop = this.$element.scrollHeight;
    }, 100)
  }

  private toString(d) {
    if (d === null) return '<null>';
    if (typeof d === 'undefined') return '<undefined>'
    if (d === '') return '<empty>'
    
    if (Array.isArray(d)) return '['+d.toString()+']';
    if (typeof d === 'object') return JSON.stringify(d, null, 2);
    return d.toString();
    return ""+d;
  }

  onSizeChanged(): void {
    super.onSizeChanged();
  }
}

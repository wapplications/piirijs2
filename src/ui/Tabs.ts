import VerticalSplit from './VerticalSplit'
//import { getViewPortSize, TemporaryEvents } from '../helpers';
import icons from './icons';
import { init_ui } from './UiManager';


export type UiTab = {
  tab: HTMLDivElement,
  container: HTMLDivElement,
  data: any,
  setTitle: (a: string)=>void,
  close: ()=>void,
  listen: (et: string, callback: any, options?: any)=>void,
};

export default class Tabs extends VerticalSplit {

  tabs: UiTab[] = [];
  currentTab: UiTab | null = null;
  $newButton: HTMLButtonElement;

  $restBar: HTMLDivElement;


  constructor(root, element) {
    super(root, element);
  }

  createDom() {
    let p1 = this.getDomDocument().createElement('div');
    p1.classList.add('uiTabs-buttons');
    let p2 = this.getDomDocument().createElement('div');
    p2.classList.add('uiTabs-containers');
    p2.setAttribute('data-expand', '1');
    this.$element.appendChild(p1);
    this.$element.appendChild(p2);

    this.$restBar = this.getDomDocument().createElement('div');
    this.$restBar.classList.add('restBar')

    this.$newButton = this.getDomDocument().createElement('button');
    this.$newButton.textContent = '+';
    this.$newButton.classList.add('wantsNewTab')

    this.$restBar.appendChild(this.$newButton);
    p1.appendChild(this.$restBar);

    
    if (JSON.parse(this.$element.dataset['plusbutton'] ?? 'true')) {
      this.$newButton.addEventListener('click', ()=>{
        this.dispatchWantsNewTab();
      })
    } else {
      this.$newButton.style.display = 'none';
    }
  }

  getCurrentTab() : UiTab | null {
    return this.currentTab;
  }

  createTab(title, content, data, closable: boolean = true) {
    let tab = this.getDomDocument().createElement('div');
    let close = this.getDomDocument().createElement('button');
    let span = this.getDomDocument().createElement('span');

    if (typeof content === 'string') {
      let code = content;
      content = this.getDomDocument().createElement('div');
      content.innerHTML = code;
    }

    span.textContent = title;
    tab.appendChild(span);
    tab.classList.add('tab')
    if (closable) {
      close.innerHTML = '&times;';
      tab.appendChild(close);
      close.addEventListener('click', (e) => {
        e.stopPropagation();
        this.dispatchTabWantsClose(uitab);
      });
    }

    let uitab = {
      tab,
      container: content,
      data,
      setTitle: (s: string) => span.textContent = s,
      listen: (et: string, cb: any, o: any = null) => {
        content.addEventListener(et, (e)=>{
          e.stopPropagation();
          cb(e);
        }, o);
      },
      close: () => {
        // remove DOM
        this.$p1.removeChild(tab);
        this.$p2.removeChild(content);
        let n = this.tabs.findIndex((e)=>e.tab===tab);
        if (n<0) return;
        this.tabs = this.tabs.filter((e)=>e.tab!==tab);
        if (this.tabs.length == 0) {
          this.currentTab = null;
        } else if (this.currentTab && this.currentTab.tab === tab) {
          // only when closing active tab
          n++;
          if (n >= this.tabs.length) n = this.tabs.length-1;
          if (n>=0) {
            this.activateTab(this.tabs[n]);
          }
        }
      }
    };


    tab.addEventListener('click', () => {
      this.activateTab(uitab);
    });


    //this.$p1.appendChild(tab);
    this.$p1.insertBefore(tab, this.$restBar);
    this.$p2.appendChild(content);
    this.tabs.push(uitab);
    init_ui(this.root, content.parentElement as HTMLElement);
    //this.activateTab(uitab);
    return uitab;
  }

  activateTab(uitab: UiTab) {
    if (this.currentTab !== null) {
      this.currentTab.tab.classList.remove('active');
      this.currentTab.container.classList.remove('active');
      this.dispatchDeactivate();
    }
    this.currentTab = uitab;
    this.currentTab.tab.classList.add('active');
    this.currentTab.container.classList.add('active');
    this.dispatchActivate();
    this.onSizeChanged();
  }


  dispatchWantsNewTab() {
    let event = new CustomEvent('tab:new', { detail: null, bubbles: true });
    this.dispatchToApp(event);
  }

  dispatchUpdate() {
    let event = new CustomEvent('tab:update', { detail: this.currentTab });
    this.dispatchToCurrentTab(event);
  }

  dispatchActivate() {
    let event = new CustomEvent('tab:activate', { detail: this.currentTab });
    this.dispatchToCurrentTab(event);
  }

  dispatchDeactivate() {
    let event = new CustomEvent('tab:deactivate', { detail: this.currentTab });
    this.dispatchToCurrentTab(event);
  }
  
  dispatchTabWantsClose(tab: UiTab) {
    let event = new CustomEvent('tab:wants-close', { detail: tab });
    this.dispatchToTab(event, tab);
  }

  dispatchToCurrentTab(event: CustomEvent) {
    if (this.currentTab === null) return;
    this.dispatchToTab(event, this.currentTab)
  }

  dispatchToTab(event: CustomEvent, tab: UiTab) {
    tab.container.dispatchEvent(event);
  }

  dispatchToApp(event: CustomEvent) {
    // event need to bubble!!
    this.$element.dispatchEvent(event);
  }

  onSizeChanged() {
    super.onSizeChanged();
    if (this.currentTab !== null) {
      this.currentTab.container.style.height = '100%';
      this.sizeElements(this.currentTab.container);
    }
    this.dispatchUpdate();
  }


}



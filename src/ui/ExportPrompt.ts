import { Prompt } from "./Prompt"
import { ToplevelCircuit } from "../components/ToplevelCircuit";
import { SerializedCircuit } from "../types";
import { Container } from "../components/Container";
import { init_ui } from "./UiManager";
import BodyScreen from "./BodyScreen";
import { downloadText } from "../helpers";
import { TitlePrompt } from "./TitlePrompt";


export class ExportPrompt extends TitlePrompt<void> {
  $el: HTMLDivElement;
  component: Container;

  constructor(component: Container) {
    super();
    this.component = component;
    this.setTitle('Als JSON exportieren')
  }

  show(): Promise<void> {
    this.start();
    this.setOpaque();
    return this.result();
  }

  createMainContent(): void {
    let json = this.component.toJsonObject();
    let code = JSON.stringify(json, null, '  ');
    let $buttons = this.getDocument().createElement('div');
    $buttons.style.display = 'flex';
    $buttons.style.flexWrap = 'nowrap';
    $buttons.style.alignItems = 'center';
    $buttons.style.overflowX = 'auto';    
    $buttons.style.flex = '0 0 auto';
    this.$el.appendChild($buttons)
    let $download = this.getDocument().createElement('button');
    $download.textContent = 'herunterladen';
    $download.addEventListener('click', ()=>{
      let filename = this.component.getName();
      if (filename === '') filename = this.component.getCirName();
      if (filename === '') filename = 'schaltung';
      filename += '.json';
      downloadText(code, filename)
    });
    $buttons.appendChild($download)
    let $copy = this.getDocument().createElement('button');
    $copy.textContent = `kopieren`;
    $copy.addEventListener('click', ()=>{
      if (!navigator.clipboard) {
        alert('Clipboard API not available')
        return;
      }
      navigator.clipboard.writeText(code)
        .then(() => {console.log('Text copied to clipboard...')})
        .catch(err => {
          console.log('Something went wrong', err);
        })
    });
    $buttons.appendChild($copy)
    let $code = this.getDocument().createElement('textarea');
    $code.setAttribute('readonly', 'readonly');
    $code.setAttribute('rows', '10');
    $code.style.flex = '1 1 auto';
    this.$el.appendChild($code)
    $code.value = code;
  }


  

}

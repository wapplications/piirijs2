import { Prompt } from "./Prompt";


export default class LoginPrompt extends Prompt<void> {
  
  show() : Promise<void> {
    console.log('Zeige Login-Dialog.')
    this.start();
    return this.result();
  }
  
  getContent(value: void): HTMLElement {
    return this.createContentFromTemplateId('login')
  }
  
}
import { PropProxy, PropValidator, ValidatorResult } from "../Props";



function closeOnBackdrop(backdrop: HTMLElement): Promise<void> {
  return new Promise<void>((res, rej) => {
    function a() {
      backdrop.classList.remove('closing')
      backdrop.classList.remove('open');
      backdrop.classList.remove('opaque');
      backdrop.innerHTML = "";
      backdrop.parentElement.removeChild(backdrop);
      res();
    }
    if (!backdrop) return rej('no backdrop');
    if (!backdrop.classList.contains('open')) {
      a();
      return;
    }
    if (backdrop.classList.contains('closing')) {
      a();
      return;
    }
    backdrop.addEventListener('transitionend', ()=>{
      if (!backdrop) return;
      a();
    }, {once:true})  
    backdrop.classList.add('closing');
  })
}



function getStringContent(value: string) {

}

function getNumberContent(value: number) {

}


export abstract class Prompt<T> {
  $document: Document;
  $el: HTMLDivElement;
  $backdrop: HTMLElement;
  validator: PropValidator<T> | null = null;
  promise: {
    promise: Promise<T|null>,
    resolve: (v: T)=>void,
    reject: (e)=>void
  };
  title: string = "Eingabe";
  description: string = "Bitte Wert eingeben";

  constructor(doc: Document) {
    this.$document = doc;
    this.init();
  }

  init() {

  }


  setValidator(validator: PropValidator<T>) {
    this.validator = validator;
    return this;
  }

  setTitle(t: string) {
    this.title = t;
    return this;
  }

  setDescription(d: string) {
    this.description = d;
    return this;
  }

  getDocument(): Document {
    return this.$document
  }

  abstract getContent(value: T): HTMLElement;


  validate(v: T): ValidatorResult {
    if (!this.validator) return true;
    return this.validator(v);
  }


  result(): Promise<T|null> {
    return this.promise.promise;
  }

  resolve(v: T) {
    this.promise.resolve(v);
    this.close();    // müssen wir da irgendwie drauf warten? oder weitergeben?
  }

  cancel() {
    this.promise.resolve(null);
    this.close();
  }

  start(value: T) {
    let p = new Promise<T|null>((res, rej) => {
      this.promise = {
        promise: null,
        resolve: res,
        reject: rej
      }
    });
    this.promise.promise = p;
    let content = this.getContent(value);
    this.openOnBackdrop(content);

    let eventTarget = this.$backdrop.firstElementChild as HTMLElement;
    eventTarget.dispatchEvent(new CustomEvent('prompt:init'))
    eventTarget.addEventListener('prompt:cancel', ()=>{
      this.cancel();
    });
    return this;
  }

  setOpaque() {
    this.$backdrop.classList.add('opaque')
  }

  close(): Promise<void> {
    return closeOnBackdrop(this.$backdrop)
  }

  openOnBackdrop(content: HTMLElement) {
    this.$backdrop = document.createElement('div');
    this.$backdrop.classList.add('backdrop')
    this.$document.querySelector('body').appendChild(this.$backdrop);

    this.$backdrop.appendChild(content);
    this.$backdrop.classList.add('open');
  }

  createContentFromTemplate(template: HTMLTemplateElement): HTMLElement {
    return template.content.cloneNode(true) as HTMLElement;
  }

  createContentFromTemplateId(id: string): HTMLElement {
    let template = document.getElementById(id);
    if (!(template instanceof this.getDocument().defaultView.HTMLTemplateElement)) throw "not a template";
    return this.createContentFromTemplate(template);
  }

}




abstract class BaseInputPrompt<T> extends Prompt<T> {

  abstract fromString(value: string): T;
  abstract toString(value: T): string;

  abstract createInputElement(): HTMLElement;
  abstract valueToElement(v: T): void;
  abstract valueFromElement(): T;
  abstract initElement() : void;

  $inpElement: HTMLElement;
  $msgElement: HTMLDivElement;

  getContent(value: T): HTMLElement {
    this.$el = this.getDocument().createElement('div');
    this.$el.style.maxWidth = '30rem';
    this.$el.style.minWidth = '40%';
    this.$el.style.display = 'flex';
    this.$el.style.flexDirection = 'column';
    this.$el.classList.add('popupContainer');
    this.$inpElement = this.createInputElement();
    this.valueToElement(value);
    this.$inpElement.style.marginTop = '0.3rem';

    let $b1 = this.getDocument().createElement('button');
    $b1.innerText = 'Abbrechen';
    let $b2 = this.getDocument().createElement('button');
    $b2.innerText = 'Übernehmen';
    $b2.classList.add('default');
    $b1.style.marginTop = '0.3rem';
    $b2.style.marginTop = '0.3rem';

    let $title = this.getDocument().createElement('div');;
    $title.style.fontWeight = 'bold';
    $title.innerText = this.title;

    let $description = this.getDocument().createElement('div');;
    $description.style.fontSize = 'small';
    $description.innerText = this.description;

    this.$msgElement = this.getDocument().createElement('div');;
    this.$msgElement.style.marginTop = '0.3rem';
    this.$msgElement.classList.add('errorMessage');
    this.$msgElement.innerText = '';

    this.$el.appendChild($title)
    this.$el.appendChild($description)
    this.$el.appendChild(this.$inpElement)
    this.$el.appendChild(this.$msgElement)
    this.$el.appendChild($b1)
    this.$el.appendChild($b2)
    this.$el.addEventListener('prompt:init', () => {
      this.initElement();
    })
    $b1.addEventListener('click', (e) => {
      this.cancel();
    })
    $b2.addEventListener('click', (e) => {
      this.confirm();
    })

    return this.$el;
  }

  confirm() {
    this.error("")
    let value: T = this.valueFromElement();
    let result = this.validate(value);
    if (result === true) {
      this.resolve(value);
    } else {
      this.error(result)
    }
  }

  error(e: string) {
    this.$msgElement.innerText = e;
  }

}

abstract class SimpleInputFieldPrompt<T> extends BaseInputPrompt<T> {
  inputType: string = 'text';

  createInputElement(): HTMLElement {
    let $el = this.getDocument().createElement('input');
    $el.type = this.inputType;
    $el.addEventListener('keyup', (e: KeyboardEvent) => {
      if (e.key === 'Enter') {
        this.confirm();
      } else if (e.key === 'Escape') {
        this.cancel();
      }
    });
    return $el;
  }

  getInputElement(): HTMLInputElement {
    return this.$inpElement as HTMLInputElement;
  }
  
  valueToElement(v: T): void {
    this.getInputElement().value = this.toString(v);
  }
  
  valueFromElement(): T {
    return this.fromString(this.getInputElement().value)
  }
  
  initElement() {
    this.getInputElement().focus();
  }  
}

export class StringPrompt extends SimpleInputFieldPrompt<string> {
  toString(value: string): string {
    return value;
  }
  fromString(value: string): string {
    return value;
  }

  show(title:string, startValue: string, description: string = ""): Promise<string|null> {
    this.title = title;
    this.description = description
    this.start(startValue);
    return this.result();
  }
}


export class TextfieldPrompt extends BaseInputPrompt<string> {
  getTextarea(): HTMLTextAreaElement {
    return this.$inpElement as HTMLTextAreaElement;
  }

  fromString(value: string): string {
    return value;
  }

  toString(value: string): string {
    return value;
  }

  valueToElement(v: string): void {
    this.getTextarea().value = v;
  }

  valueFromElement(): string {
    return this.getTextarea().value;
  }

  initElement(): void {
    this.getTextarea().focus();
  }

  createInputElement(): HTMLElement {
    let $ta = this.getDocument().createElement('textarea');
    $ta.setAttribute('rows', '10');
    return $ta;
  }
  
}

export class NumberPrompt extends SimpleInputFieldPrompt<number> {
  init() {
    super.init();
    this.inputType = 'number';
  }
  toString(value: number): string {
    return ""+value;
  }
  fromString(str: string): number {
    let base = 10;
    if (/^b/i.test(str)) {
      str = str.substring(1);
      base = 2;
    } else if (/^x/i.test(str)) {
      str = str.substring(1);
      base = 16;
    }
    return parseInt(str, base);
  }
}




export class ButtonPrompt extends Prompt<string> {
  buttons: {[key: string]: string};

  show(title: string, description: string, buttons: {[key: string]: string}, value: string): Promise<string> {
    this.title = title;
    this.description = description;
    this.buttons = buttons;
    this.start(value);
    return this.result();
  }

  getContent(value: string): HTMLElement {
    this.$el = this.getDocument().createElement('div');
    this.$el.style.maxWidth = '30rem';
    this.$el.style.minWidth = '40%';
    this.$el.style.display = 'flex';
    this.$el.style.flexDirection = 'column';
    this.$el.classList.add('popupContainer');

    let $bc = this.getDocument().createElement('div');
    $bc.style.display = 'flex';
    $bc.style.flexDirection = 'column';
    for (let b in this.buttons) {
      let $b = this.getDocument().createElement('button');
      $b.innerText = this.buttons[b];
      $b.style.marginTop = '0.3rem';
      $bc.appendChild($b);
      $b.addEventListener('click', (e) => {
        this.resolve(b);
      })
    }
    //$b2.classList.add('default');

    let $title = this.getDocument().createElement('div');;
    $title.style.fontWeight = 'bold';
    $title.innerText = this.title;

    let $description = this.getDocument().createElement('div');;
    $description.style.fontSize = 'small';
    $description.innerText = this.description;

    this.$el.appendChild($title)
    this.$el.appendChild($description)
    this.$el.appendChild($bc)
    return this.$el;
  }

}





export class DeadEndPrompt extends Prompt<void> {

  show(title: string, description: string): Promise<void> {
    this.setTitle(title).setDescription(description)
    this.start();
    return this.result();
  }

  getContent(value: void): HTMLElement {
    this.$el = this.getDocument().createElement('div');
    this.$el.style.maxWidth = '30rem';
    this.$el.style.minWidth = '40%';
    this.$el.style.display = 'flex';
    this.$el.style.flexDirection = 'column';
    this.$el.classList.add('popupContainer');

    let $title = this.getDocument().createElement('div');;
    $title.style.fontWeight = 'bold';
    $title.innerText = this.title;

    let $description = this.getDocument().createElement('p');;
    //$description.style.fontSize = 'small';
    $description.innerText = this.description;

    this.$el.appendChild($title)
    this.$el.appendChild($description)
   
    return this.$el;
 }
  
}
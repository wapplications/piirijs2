import Tabs, { UiTab } from './Tabs';
import { Component } from '../components/Component';
import { Input, IOBase, IOValue, Output } from '../components/IO';
import { Container } from '../components/Container';
import createComponent from '../components/factory';
import { init_ui } from './UiManager';
import PiiriWindow from '../PiiriWindow';
import PiiriPane from './PiiriPane';

type AnalysisTabs = {
  valueTable?: UiTab,
  impulseDiagram?: UiTab,
  componentView?: UiTab
};
type AnalysisTabTools = {
  [key in keyof AnalysisTabs]: HTMLDivElement;
};

export default class AnalysisView extends Tabs {

  mytabs: AnalysisTabs = {};
  tabtools: AnalysisTabTools = {};
  
  rootComponent?: Container;
  mainPiiriWindow?: PiiriWindow;

  $startSvg: SVGSVGElement;
  $stopSvg: SVGSVGElement;


  constructor(root, element) {
    super(root, element);
    
    this.mytabs.valueTable = this.createTab('Tabelle', '', '', false);
    this.mytabs.valueTable.container.style.overflow = 'auto';

    this.mytabs.impulseDiagram = this.createTab('Diagramm', '', '', false);
    this.mytabs.impulseDiagram.container.style.overflow = 'auto';

    this.mytabs.componentView = this.createTab('Komponente', '', '', false);
    this.mytabs.componentView.container.style.overflow = 'auto';

    //let $tabsContainer = this.diagramTab.tab.parentElement!;

    let $spacer = this.getDomDocument().createElement('span');
    $spacer.style.flex = '1 1 auto';
    this.$restBar.appendChild($spacer);


    this.createImpulseDiagramTools();
    this.componentViewTools();
    
    for (let tabkey in this.mytabs) {
      if (typeof this.tabtools[tabkey] === 'undefined') continue;
      this.mytabs[tabkey]!.listen('tab:activate', ()=>{
        this.tabtools[tabkey]!.style.display = '';
      })
      this.mytabs[tabkey]!.listen('tab:deactivate', ()=>{
        this.tabtools[tabkey]!.style.display = 'none';
      })
    }

  }

  componentViewTools() {
    this.tabtools.componentView = this.getDomDocument().createElement('div');
    this.tabtools.componentView.style.display = 'none';
    this.$restBar.appendChild(this.tabtools.componentView);
    let $back = this.getDomDocument().createElement('button');
    $back.textContent = '<';
    this.tabtools.componentView.appendChild($back)
    $back.addEventListener('click', ()=>{
      if (!this.viewPiiriWindow) return;
      let parent = this.viewPiiriWindow.component.getParent();
      if (parent === null) return;
      if (parent.getParent() === null) return;
      this.$element.dispatchEvent(new CustomEvent<Container>('piiri:open-component', {
        detail: parent,
        bubbles: true,
      }));

    });
    let $refresh = this.getDomDocument().createElement('button');
    $refresh.textContent = 'R';
    this.tabtools.componentView.appendChild($refresh)
    $refresh.addEventListener('click', ()=>{
      if (!this.viewPiiriWindow) return;
      this.viewPiiriWindow.start();
      this.viewPiiriWindow.refresh(true, true);
    });
    let $zoomOut = this.getDomDocument().createElement('button');
    $zoomOut.textContent = '–';
    this.tabtools.componentView.appendChild($zoomOut)
    $zoomOut.addEventListener('click', ()=>{
      if (!this.viewPiiriWindow) return;
      this.mytabs.componentView.container.dispatchEvent(new CustomEvent<void>('piiri:command:zoom-out'));
    });
    let $zoomIn = this.getDomDocument().createElement('button');
    $zoomIn.textContent = '+';
    this.tabtools.componentView.appendChild($zoomIn)
    $zoomIn.addEventListener('click', ()=>{
      if (!this.viewPiiriWindow) return;
      this.mytabs.componentView.container.dispatchEvent(new CustomEvent<void>('piiri:command:zoom-in'));
    });
  }

  createImpulseDiagramTools() {
    this.tabtools.impulseDiagram = this.getDomDocument().createElement('div');
    this.tabtools.impulseDiagram.style.display = 'none';
    this.$restBar.appendChild(this.tabtools.impulseDiagram);
    let $impulseButton = this.getDomDocument().createElement('button');
    this.$startSvg = this.getDomDocument().createElementNS('http://www.w3.org/2000/svg', 'svg');
    this.$stopSvg = this.getDomDocument().createElementNS('http://www.w3.org/2000/svg', 'svg');
    this.$startSvg.setAttribute('viewBox', '0 0 20 20');
    this.$startSvg.setAttribute('width', '20');
    this.$startSvg.setAttribute('height', '20');
    let $path = this.getDomDocument().createElementNS('http://www.w3.org/2000/svg', 'path');
    $path.setAttribute('d', 'M3,3 L17,10 L3,17 z');
    $path.setAttribute('fill', 'green');
    $path.setAttribute('stroke', 'none');
    this.$startSvg.appendChild($path);
    this.$stopSvg.setAttribute('viewBox', '0 0 20 20');
    this.$stopSvg.setAttribute('width', '20');
    this.$stopSvg.setAttribute('height', '20');
    $path = this.getDomDocument().createElementNS('http://www.w3.org/2000/svg', 'path');
    $path.setAttribute('d', 'M3,3 h5 v14 h-5 z M12,3 h5 v14 h-5 z');
    $path.setAttribute('fill', 'red');
    $path.setAttribute('stroke', 'none');
    this.$stopSvg.appendChild($path);
    $impulseButton.appendChild(this.$startSvg);
    $impulseButton.appendChild(this.$stopSvg);
    this.$stopSvg.style.display = 'none';
    this.tabtools.impulseDiagram.appendChild($impulseButton);
    $impulseButton.addEventListener('click', ()=>{
      if (this.isDiagramRunning()) {
        this.stopImpulseDiagram();
      } else {
        this.startImpulseDiagram();
      }
    });
  }

  createDom(): void {
    this.$element.setAttribute('data-plusbutton', 'false');
    super.createDom();
    //(this.$element.querySelector('.uiTabs-buttons') as HTMLElement).style.display = 'none';
    //this.$newButton.style.display = 'none';
  }

  clear() {
    if (this.stopImpulse) {
      this.stopImpulse();
    }
  }

  setMainPiiriWindow(win: PiiriWindow) {
    this.mainPiiriWindow = win;
    this.setRootComponent(this.mainPiiriWindow.rootComponent);
  }

  getMainPiiriWindow(): PiiriWindow {
    if (typeof this.mainPiiriWindow === 'undefined') throw 'no main window set';
    return this.mainPiiriWindow;
  }

  setRootComponent(c: Container) {
    this.rootComponent = c;
  }

  getRootComponent(): Container {
    if (typeof this.rootComponent === 'undefined') throw 'no root component set';
    return this.rootComponent;
  }
  

  isDiagramRunning(): boolean {
    return this.stopImpulse!==null;
  }

  stopImpulse: ()=>void | null = null;
  stopImpulseDiagram() {
    this.$stopSvg.style.display = 'none';
    this.$startSvg.style.display = '';
    if (this.stopImpulse) {
      this.stopImpulse();
    }
  }

  startImpulseDiagram() {
    let startTime = time();
    let pos = 0;
    let doc: Document = this.getDomDocument();

    this.activateTab(this.mytabs.impulseDiagram!);

    this.stopImpulseDiagram();

    this.$stopSvg.style.display = '';
    this.$startSvg.style.display = 'none';

    let $container = this.mytabs.impulseDiagram!.container;
    let $svg = this.getDomDocument().createElementNS('http://www.w3.org/2000/svg', 'svg');
    $container.innerHTML = '';
    $container.appendChild($svg);
    $svg.setAttribute('width', ''+$container.clientWidth);
    $svg.setAttribute('height', ''+$container.clientWidth);
    $svg.setAttribute('viewBox', '0 0 '+$svg.getAttribute('width') + ' '+$svg.getAttribute('height'));
    let $g = this.getDomDocument().createElementNS('http://www.w3.org/2000/svg', 'g');
    $svg.appendChild($g);



    let lines: {
      [key:string]: {
        d: string,
        $path: SVGPathElement,
        value: number,
        timestamp: number,
        pins: number
      }
    } = {
    };

    const colors = [
      '#ffff00',
      '#00ffff',
      '#ff00ff',
      '#55ff55',
      '#ff5555',
      '#5555ff',
      '#ff7700',
    ];

    function addLine(io: IOBase) {
      let type = (io instanceof Input) ? 'in' : 'out';      
      let name = type + '.' + io.getName();
      let value = io.getValue();
      let $path = doc.createElementNS('http://www.w3.org/2000/svg', 'path');
      let d = 'M-5,' + value * -10;
      let yoffset = (15+pos*15);
      let color = colors.pop();
      colors.unshift(color);
      $path.setAttribute('transform', 'translate(0,'+yoffset+')');
      $path.setAttribute('d', d+' h5');
      $path.setAttribute('stroke', color);
      $path.setAttribute('fill', 'none');
      $g.appendChild($path);

      let $t = doc.createElementNS('http://www.w3.org/2000/svg', 'text');
      $t.textContent = io.getName();
      $t.setAttribute('fill', color);
      $t.setAttribute('x', '0');
      $t.setAttribute('y', '0');
      $t.setAttribute('transform', 'translate(0,'+yoffset+')');
      $svg.appendChild($t);

      lines[name] = { d, $path, value, timestamp: startTime, pins: io.getPinCount()};
      pos++;
    }

    for (let i = 0; i < this.rootComponent.inputs.length; i++) {
      addLine(this.rootComponent.inputs[i]);
    }
    for (let i = 0; i < this.rootComponent.outputs.length; i++) {
      addLine(this.rootComponent.outputs[i]);
    }


    function time() {
      return Date.now();
    }

    let xfactor = 0.04;

    function updateLine(line, v: IOValue) {
      let t = time();
      let timeLength = (t-line.timestamp)*xfactor;
      line.timestamp = t;
      let max = line.pins ** 2;
      line.d += 'h'+timeLength + ' V'+((v/max)*-10);
      line.$path.setAttribute('d', line.d);
      line.value = v;
    }

    function updateOthers(except: string) {
      let t = time();
      for (let l in lines) {
        if (l === except) continue;
        let line = lines[l];
        let timeLength = (t-line.timestamp)*xfactor;
        line.$path.setAttribute('d', line.d + ' h'+timeLength);
      }
      let timeLength = (t-startTime)*xfactor;
      let w = parseInt($svg.getAttribute('width'))
      if (timeLength > w) {
        // --> scale into view
        // $g.setAttribute('transform', 'scale('+(w/timeLength)+', 1)');

        // --> "scroll" it away
        $g.setAttribute('transform', 'translate('+(w-timeLength)+', 0)');
      }
    }

    const inHandler = (s: string, v: IOValue) => {
      let name = 'in.'+s;
      let line = lines[name];
      if (typeof line === 'undefined') {
        // TODO hinzufügen
        return;
      }
      updateLine(line, v);
      //updateOthers('');
    };

    const outHandler = (s: string, v: IOValue) => {
      let name = 'out.'+s;
      let line = lines[name];
      if (typeof line === 'undefined') {
        // TODO hinzufügen
        return;
      }
      updateLine(line, v);
      //updateOthers('');
    };

    const timerHandler = ()=>{
      updateOthers('');
    };

    this.rootComponent.signal_input_changed.connect(inHandler);
    this.rootComponent.signal_output_changed.connect(outHandler);
    const timer = setInterval(timerHandler, 200);

    this.stopImpulse = () => {
      clearInterval(timer);
      this.rootComponent.signal_input_changed.disconnect(inHandler);
      this.rootComponent.signal_output_changed.disconnect(outHandler);
      this.stopImpulse = null;
    }
  }


  async valueTable(caption: string, component: Component) {
    this.activateTab(this.mytabs.valueTable!);

    async function createTable(c: Component) {
      let table: {in: number[], out: number[]}[] = [];
      let inputs = c.getInputs();
      let outputs = c.getOutputs();


      function readOutputVector() {
        let pos = 0;
        for (let o = 0; o < outputs.length; o++) {
          let out = outputs[o];
          let v = out.getValue();
          let vec = numToVec(v, out.getPinCount());
          for (let oo=0; oo<out.getPinCount(); oo++) {
            outputVector[pos+oo] = vec[oo];
          }
          pos += out.getPinCount();
        }
      }
      function numToVec(n: number, l: number): number[] {
        let res = [];
        for (let i=0; i < l; i++) {
          res.unshift(n&1);
          n = n>>1;
        }
        return res;
      }
      function vecToNum(vec: number[]): number {
        let val = vec.reduce((p,c)=>{
          // [0 1 1] = 3
          let n = p << 1;
          n += c;
          return n;
        }, 0)
        return val;
      }
      function writeInputVecor() {
        let pos = 0;
        for (let i = 0; i < inputs.length; i++) {
          let inp = inputs[i];
          let vecPart = inputVector.slice(pos, pos+inp.getPinCount());
          inp.setValue(vecToNum(vecPart));
          pos+=inp.getPinCount();
        }
      }
      function next() {
        let sum = inputVector.reduce((p,c)=>(p+c), 0);
        if (sum === inputVector.length) return false;
        let pos = 0;
        inputVector[pos]+=1;
        while (inputVector[pos]===2 && pos < inputLength-1) {
          inputVector[pos] = 0;
          pos++;
          inputVector[pos]+=1;
        }
        return true;
      }

      let inputLength = 0;
      for (let i = 0; i < inputs.length; i++) {
        inputLength += inputs[i].getPinCount();
      }
      let outputLength = 0;
      for (let i = 0; i < outputs.length; i++) {
        outputLength+=outputs[i].getPinCount();
      }

      let inputVector = new Array(inputLength);
      inputVector.fill(0);
      let outputVector = new Array(outputLength);
      outputVector.fill(0);

      do {
        writeInputVecor();
        c.update();
        readOutputVector();
        table.push({in: [...inputVector], out: [...outputVector]});
      } while(next());

      return {
        inputs,
        outputs,
        table
      };
    }
    let tableData: {inputs: Input[], outputs: Output[], table: {in: number[], out: number[]}[]} = {
      inputs: [], outputs: [], table: []
    };

    // CLONE
    let err = [];
    /*if (component instanceof Container) {
      let serialized = component.toJsonObject();
      let cloneContainer = new ToplevelCircuit();
      await cloneContainer.loadCircuit(serialized, err)
      tableData = await createTable(cloneContainer);
    } else {
    }*/
    if (component instanceof Container /*&& component.cir_name === 'embedded'*/) {
      let cloneContainer = new Container('', null, true);
      cloneContainer.init({});
      await cloneContainer.loadCircuit(component.toJsonObject(), err)
      tableData = await createTable(cloneContainer);
    } else {
      let serialized = component.toJson();
      let cloned = await createComponent(null, serialized.component, '', true, err);
      cloned.setProps(serialized.props);
      tableData = await createTable(cloned);
    }
    if (err.length>0) console.error(err);
    // RENDER
    let $container = this.mytabs.valueTable!.container;
    let $table = this.getDomDocument().createElement('table');
    let $thead = this.getDomDocument().createElement('thead');
    let $tbody = this.getDomDocument().createElement('tbody');
    let $caption = this.getDomDocument().createElement('strong');
    $table.classList.add("valueTable");
    $container.innerHTML = '';
    $caption.textContent = caption;
    $caption.style.display = 'block';
    $table.appendChild($thead);
    $table.appendChild($tbody);
    $container.appendChild($caption);
    $container.appendChild($table);
    let $tr = this.getDomDocument().createElement('tr');
    for (let i = 0; i < tableData.inputs.length; i++) {
      let inp = tableData.inputs[i];
      let pc = inp.getPinCount();
      let n = inp.getName();
      let $th = this.getDomDocument().createElement('th');
      $th.setAttribute('colspan', ''+pc);
      $th.textContent = n;
      $tr.appendChild($th);
    }
    for (let i = 0; i < tableData.outputs.length; i++) {
      let out = tableData.outputs[i];
      let pc = out.getPinCount();
      let n = out.getName();
      let $th = this.getDomDocument().createElement('th');
      if (i===0) $th.classList.add('beforeOutput')
      $th.setAttribute('colspan', ''+pc);
      $th.textContent = n;
      $tr.appendChild($th);
    }
    $thead.appendChild($tr);

    for (let r = 0; r < tableData.table.length; r++) {

      let $tr = this.getDomDocument().createElement('tr');
      let pos = 0;
      for (let i = 0; i < tableData.inputs.length; i++) {
        let inp = tableData.inputs[i];
        let pc = inp.getPinCount();
        let $td = this.getDomDocument().createElement('td');
        $td.setAttribute('colspan', ''+pc);
        for (let p = 0; p < pc; p++) {
          $td.textContent += tableData.table[r].in[pos];
          pos++;
        }
        $tr.appendChild($td);
      }
      pos = 0;
      for (let i = 0; i < tableData.outputs.length; i++) {
        let out = tableData.outputs[i];
        let pc = out.getPinCount();
        let $td = this.getDomDocument().createElement('td');
        if (i===0) $td.classList.add('beforeOutput')
        $td.setAttribute('colspan', ''+pc);
        for (let p = 0; p < pc; p++) {
          $td.textContent += tableData.table[r].out[pos];
          pos++;
        }
        $tr.appendChild($td);
      }
      $thead.appendChild($tr);
    }
  }


  closeView: ()=>void | null = null;
  viewPiiriWindow: PiiriWindow | null = null;
  async view(container: Container) {

    if (this.closeView) {
      this.closeView();
      this.closeView = null;
    }

    this.activateTab(this.mytabs.componentView);
    
    let fullName = container.getNameRecursive();
    if (fullName.length > 20) {
      let firstSep = fullName.indexOf('>');
      let lastSep = fullName.lastIndexOf('>');
      if (lastSep !== firstSep) {
        fullName = fullName.substring(0,firstSep) + '…' + fullName.substring(lastSep);
      }
    }
    this.mytabs.componentView!.setTitle(fullName);

    if (!this.viewPiiriWindow) {
      let $container = this.mytabs.componentView!.container;
      $container.innerHTML = `
      <div class="uiPiiriPane" 
        data-interactive="1" 
        data-ref="subPane" 
        style="overflow:auto;">
        <canvas  tabindex="1" style="box-sizing:border-box;" />
      </div>
      `;
      init_ui(this.root, $container as HTMLElement);
      let pane = this.root.refMap.subPane;
      pane.setViewOnly(true);
      this.viewPiiriWindow = new PiiriWindow($container, pane, container);
      this.getMainPiiriWindow().openWindows.push(this.viewPiiriWindow);
    }

    this.viewPiiriWindow.setComponent(container)
    this.viewPiiriWindow.start();
    this.viewPiiriWindow.refresh(true, false);

    this.closeView = ()=>{
      // TODO
      // löschen wenn tab weg?
      //piiriWindow.visual.clear();
      //this.getMainPiiriWindow().openWindows = this.getMainPiiriWindow().openWindows.filter((w)=>w===piiriWindow);
    }

  }


}



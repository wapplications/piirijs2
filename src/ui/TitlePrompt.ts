import { Prompt } from "./Prompt"
import { init_ui } from "./UiManager";
import BodyScreen from "./BodyScreen";


export abstract class TitlePrompt<T> extends Prompt<T> {
  $el: HTMLDivElement;

  constructor() {
    super(document);
  }

  getContent(value: T): HTMLElement {
    this.$el = this.getDocument().createElement('div');
    
    this.$el.style.height = '100%';
    this.$el.style.width = '100%';
    this.$el.style.borderRadius = "0";
    this.$el.style.display = 'flex';
    this.$el.style.flexDirection = 'column';
    this.$el.style.overflowY = 'auto';
    this.$el.classList.add('popupContainer');
    

    let $closeButton = this.getDocument().createElement('button');
    $closeButton.innerHTML = "&times;";
    $closeButton.style.position = "absolute";
    $closeButton.style.top = "0";
    $closeButton.style.right = "0";
    $closeButton.style.border = "none";
    $closeButton.style.background = "none";
    $closeButton.style.outline = "none";
    $closeButton.style.padding = "0.5rem";
    $closeButton.style.boxSizing = "content-box";
    $closeButton.style.margin = "0";
    $closeButton.style.fontSize = "2rem";
    $closeButton.style.boxShadow = "none";
    $closeButton.style.display = "block";
    $closeButton.style.height = "2rem";
    $closeButton.style.width = "2rem";
    $closeButton.style.lineHeight = "2rem";
    
    this.$el.appendChild($closeButton);
    $closeButton.addEventListener('click', (e) => {
      this.resolve(null);
    })


    let $title = this.getDocument().createElement('div');
    $title.style.marginBottom = '1rem'
    $title.style.fontWeight = 'bold';
    $title.style.flex = '0 0 auto';
    $title.innerText = this.title;
    this.$el.appendChild($title)

    this.createMainContent();

    let root = document.body['_ui'] as BodyScreen;
    init_ui(root, this.$el);
    return this.$el;
  }

  abstract createMainContent(): void;
  

}

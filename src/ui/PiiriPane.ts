import { COMPONENT_AREA, Point, CircuitStateChanged } from '../types';
import Ui from './Ui'
import {ComponentVisual} from "../visuals/ComponentVisual";
import { Input, IO, IOBase, IOType, Output } from '../components/IO';
import {ContainerVisual} from "../visuals/ContainerVisual";
import { compileColor, curvedWire, line, wire } from '../helpers';
import settings from '../visuals/VisualSettings';

enum Operation {
  NONE,
  CONNECT,
  RECTSELECT,
  MOVING,
  PUTTING
};

enum MouseButton {
  MAIN = 0, // Main button pressed, usually the left button or the un-initialized state
  MIDDLE = 1, // Auxiliary button pressed, usually the wheel button or the middle button (if present)
  RIGHT = 2, // Secondary button pressed, usually the right button
  /*3: Fourth button, typically the Browser Back button
  4: Fifth button, typically the Browser Forward button*/
}


export default class PiiriPane extends Ui {
  canvas: HTMLCanvasElement;
  private ctx: CanvasRenderingContext2D;

  private _mx: number;
  private _my: number;
  private _dx: number;
  private _dy: number;
  private _old_mx: number;
  private _old_my: number;
  private last_moved_x: number;
  private last_moved_y: number;

  public zoom;

  private rubber: boolean = false;
  private view_only: boolean = false;
  private is_moving: boolean = false;

  public op: Operation = Operation.NONE;

  private imageBuffer: ImageData | null = null;

  //app: App;
  visual: ContainerVisual;

  rubberBandSelectionMode: boolean = true;
  interactive: boolean = false;

  selection: Array<IOBase | ComponentVisual> = [];

  constructor(root, element) {
    super(root, element);

    this.selection = [];

    this.interactive = !!JSON.parse(this.$element.dataset['interactive']);

    let c = this.$element.firstElementChild
    if (!(c instanceof this.getDomDocument().defaultView.HTMLCanvasElement)) throw 'PiiriPane: canvas not found'
    this.canvas =c;
    this.ctx = this.canvas.getContext("2d")
    this.zoom = 1;

    if (!this.interactive) {
      return;
    }
    
    this.canvas.addEventListener('dragover', this.onDragover.bind(this));
    this.canvas.addEventListener('drop', this.onDrop.bind(this));

    this.canvas.addEventListener('mousedown', (e)=>{
      if (this.onMousePress({
        mx: e.offsetX,
        my: e.offsetY,
        button: e.button,
        ctrlKey: e.ctrlKey,
        prevent: ()=>e.preventDefault()
      })) e.preventDefault();
    });
    this.canvas.addEventListener('mouseup', (e)=>{
      if(this.onMouseRelease({
        mx: e.offsetX,
        my: e.offsetY,
        button: e.button,
        ctrlKey: e.ctrlKey,
        prevent: ()=>e.preventDefault()
      })) e.preventDefault();
    });
    if (!this.isViewOnly()) {
      this.canvas.addEventListener('mousemove', (e)=>{
        if (this.onMotionEvent({
          mx: e.offsetX,
          my: e.offsetY,
          button: e.button,
          ctrlKey: e.ctrlKey,
          prevent: ()=>e.preventDefault()
        })) e.preventDefault();
      });

      this.canvas.addEventListener('touchmove', (ev: TouchEvent)=>{
        if (ev.targetTouches.length !== 1) return;
        let touch = ev.targetTouches[0];
        var rect = (ev.target as HTMLCanvasElement).getBoundingClientRect();
        var mx = touch.pageX - rect.left;
        var my = touch.pageY - rect.top
        if (this.onMotionEvent({
          mx,
          my,
          button: MouseButton.MAIN,
          ctrlKey: false,
          prevent: ()=>ev.preventDefault()
        })) ev.preventDefault();
      });
    }

    this.canvas.addEventListener('touchstart', (ev: TouchEvent)=>{
      if (ev.targetTouches.length !== 1) return;
      let touch = ev.targetTouches[0];
      var rect = (ev.target as HTMLCanvasElement).getBoundingClientRect();
      var mx = touch.pageX - rect.left;
      var my = touch.pageY - rect.top
      if (this.onMousePress({
        mx,
        my,
        button: MouseButton.MAIN,
        ctrlKey: false,
        prevent: ()=>ev.preventDefault()
      })) ev.preventDefault();
    });

    this.canvas.addEventListener('touchend', (ev: TouchEvent)=>{
      if (ev.changedTouches.length !== 1) return;
      let touch = ev.changedTouches[0];
      var rect = (ev.target as HTMLCanvasElement).getBoundingClientRect();
      var mx = touch.pageX - rect.left;
      var my = touch.pageY - rect.top
      if (this.onMouseRelease({
        mx,
        my,
        button: MouseButton.MAIN,
        ctrlKey: false,
        prevent: ()=>ev.preventDefault()
      })) ev.preventDefault();
    });

    this.canvas.addEventListener('dblclick', (e)=>{
      if (this.selection.length===1 && this.selection[0] instanceof ComponentVisual) {
        e.preventDefault();
        this.$element.dispatchEvent(new CustomEvent('piiri:open-component', {
          detail: this.selection[0].getComponent(),
          bubbles: true,
        }));
      }
    });

    // https://developer.mozilla.org/en-US/docs/Web/API/Touch_events
  }

  saveImgBuf() {
    this.imageBuffer = this.ctx.getImageData(0, 0, this.canvas.width, this.canvas.height);
  }
  
  restoreImgBuf() {
    if (this.imageBuffer === null) return;
    this.ctx.putImageData(this.imageBuffer, 0, 0);
  }

  setVisual(v: ContainerVisual) {
    this.visual = v;
  }

  public isViewOnly(): boolean {
    return this.view_only;
  }

  public setViewOnly(vo: boolean) {
    this.view_only = vo;
  }

  private getContextFromCanvas(canvas: HTMLCanvasElement): CanvasRenderingContext2D {
    let ctx = canvas.getContext("2d");
    if (!ctx) throw "No 2d Context for canvas";
    return ctx;
  }

  
  private dispatchSelectionChangedEvent() {
    this.$element.dispatchEvent(new CustomEvent('piiri:selection-changed', {bubbles:true}));
  }

  private dispatchRefreshEvent(resize: boolean, update: boolean) {
    this.$element.dispatchEvent(new CustomEvent('piiri:refresh', {
      detail: {
        resize,
        update
      },
      bubbles: true,
    }));
  }

  private dispatchCircuitstateChanged(state: CircuitStateChanged) {
    this.$element.dispatchEvent(new CustomEvent('piiri:state', {
      detail: {state},
      bubbles: true,
    }));
  }

  private dispatchComponent(cir: string, x, y) {
    this.$element.dispatchEvent(new CustomEvent('piiri:component', {
      detail: {cir, pos:{x, y}},
      bubbles: true,
    }));
  }

  
  onMousePress({mx, my, button, ctrlKey, prevent}): boolean { // on_my_press_event
    if (this.op == Operation.MOVING) {
      this.op = Operation.NONE;
      return false;
    }

    this._mx = mx;
    this._my = my;

    /* from piiri.js var x = e.offsetX - app.mainPos.x;
    var y = e.offsetY - app.mainPos.y;*/

    let r: Point = {
      x: mx / this.zoom,
      y: my / this.zoom
    };

    if (this.rubberBandSelectionMode) {
      this.op = Operation.RECTSELECT;
    }
  
    // wo sind wir im Visual der Toplevelschaltung dieses fensters
    // nicht diese umrechnung verwenden: sie verwendet die CirObj position!! aber das toplevel element liegt immer bei 0 0
    // evtl. visual->getAbsPos verwenden?
    //m_pVisual->parentToComponent(mx/zoom, my/zoom,  rx, ry);

    let area: COMPONENT_AREA = this.visual.areaOnComponent(r);
    r = this.visual.componentToArea(area, r);

    if ((area == COMPONENT_AREA.IN) && (!this.isViewOnly())) {
      // rx und ry sind koordinaten rel zum input-bereich!
      // Main IN markieren oder manin IN toggeln
      let pin: Input | null = this.visual.getInputFromPoint(r);
      if (pin) {
        if ((r.x > 6+this.visual.inExtraWidth) && ((button == MouseButton.MAIN) || (button == MouseButton.RIGHT))) {
          if (button == MouseButton.MAIN) this.op = Operation.CONNECT;
          else this.op = Operation.NONE;
          this.select(pin, ctrlKey)
          this.dispatchSelectionChangedEvent();
        } else if ((button == MouseButton.MAIN) || (button == MouseButton.RIGHT)) {
          this.deselect();
          this.dispatchSelectionChangedEvent();
          if (button == MouseButton.MAIN) pin.setBooleanValue(!pin.getBooleanValue());
          else pin.setBooleanValue(true); // TODO beim loslassen ausschalten!
          this.dispatchRefreshEvent(false, true); // TODO könnte man das nicht beim handling von SWITCHED machen? allgemeine frage
          this.dispatchCircuitstateChanged(CircuitStateChanged.SWITCHED);
        }
        return true;
      }
      return true;
    } else if ((area == COMPONENT_AREA.OUT) && (!this.isViewOnly())) {
      let pout: Output | null = this.visual.getOutputFromPoint(r);
      if (pout && ((button == MouseButton.MAIN) || (button == MouseButton.RIGHT))) {
        this.select(pout, ctrlKey)
        this.dispatchSelectionChangedEvent();
        return true;
      }
    } else if (area == COMPONENT_AREA.INNER) {
      // rx und ry sind koordinaten rel zum inner-bereich!
      // wir sind im innern und schauen, ob wir eine komponente unterm zeiger haben
      // Element suchen
      let visual: ComponentVisual | null = this.visual.getVisualFromPoint(r);
      if (visual) {
        // wir haben eine komponente!
        // zum bereich relative koordinaten holen
        let childPos = visual.parentToComponent(r);
        let childArea = visual.areaOnComponent(childPos);
        childPos = visual.componentToArea(area, childPos);
        this._dx = mx;
        this._dy = my;
        this.last_moved_x = 0;
        this.last_moved_y = 0;
  
        if ((childArea == COMPONENT_AREA.INNER) || (childArea == COMPONENT_AREA.TITLE)) {
          if (!visual.selected) {
            this.select(visual, ctrlKey)
            this.dispatchRefreshEvent(false, false)
            this.dispatchSelectionChangedEvent();
          }
          if ((button == MouseButton.MAIN) && !this.isViewOnly()) {
            this.op = Operation.MOVING;
            return true;
          } else {
            this.op = Operation.NONE;
          }
          if ((childArea == COMPONENT_AREA.INNER) && (!this.isViewOnly())) {
            //
          }
        } else if ((childArea == COMPONENT_AREA.IN) && (!this.isViewOnly())) {
          let pin = visual.getInputFromPoint(childPos);
          if (pin && ((button == MouseButton.MAIN) || (button == MouseButton.RIGHT))) {
            this.select(pin, ctrlKey)
            this.dispatchSelectionChangedEvent();
          }
        } else if ((childArea & COMPONENT_AREA.OUT) && (!this.isViewOnly())) {
          let pout = visual.getOutputFromPoint(childPos);
          if (pout && ((button == MouseButton.MAIN) || (button == MouseButton.RIGHT))) {
            this.select(pout, ctrlKey)
            this.dispatchSelectionChangedEvent();
            if (button == MouseButton.MAIN) this.op = Operation.CONNECT;
            else this.op = Operation.NONE;
          }
        }
      } else {
        this.select(null)
        this.dispatchSelectionChangedEvent();
      }
    } else {
      this.select(null)
      this.dispatchSelectionChangedEvent();
    }
    return false;
  }

  onMotionEvent({mx,my,button,ctrlKey, prevent}): boolean {
    let xend: boolean = false;
    let yend: boolean = false;

    if (/*(this.selectedComponents.length > 0) &&*/ (this.op == Operation.MOVING) && !this.isViewOnly()) {
      let move_x, move_y;

      move_x = (mx - this._dx)  /this.zoom;
      move_y = (my - this._dy)  /this.zoom;
  
      // threshold
      if ((move_x*move_x > 9) || (move_y*move_y > 9)) {
        if (!this.is_moving) {
          this.dispatchCircuitstateChanged(CircuitStateChanged.MOVING);
          this.is_moving = true;
        }


        let movePos = {x: move_x - this.last_moved_x, y: move_y - this.last_moved_y}
        // TODO sicherstellen, dass man nicht über oben links hinaus schiebt - prolematisch
        // weil wir last_moved_x verändern müssen
        /*for (let vis of this.selectedComponents) {
          let pos = {
            x: move_x + vis.getPosX() - this.last_moved_x,
            y: move_y + vis.getPosY() - this.last_moved_y
          }
          if (pos.x < 0) {
              xend = true;
          }
          if (pos.y < 0) {
              yend = true;
          }
        }
        if (xend) move_x = this.last_moved_x = 0;
        if (yend) move_y = this.last_moved_y = 0;
        */
        for (let vis of this.selection) {
          if (!(vis instanceof ComponentVisual)) break;
          let pos = {
            x: vis.getPosX() + movePos.x,
            y: vis.getPosY() + movePos.y
          }
          vis.setPos(pos);
        }
        this.dispatchRefreshEvent(true, false);


        
        this.last_moved_x = move_x;
        this.last_moved_y = move_y;
      } // threshold
      //this.update();
    } else if (!this.isViewOnly() && (this.op == Operation.CONNECT)) {
      if (!this.rubber) {
        this.dispatchCircuitstateChanged(CircuitStateChanged.CONNECTING);
        this.saveImgBuf();
        this.rubber = true;
      } else {
        this.restoreImgBuf();
      }
      this.ctx.save();
      this.ctx.lineWidth = 2;
      this.ctx.setLineDash([2, 2]);
      this.ctx.strokeStyle = compileColor([0.6, 0.5, 0.0, 0.6]); // TODO settings
      this.ctx.beginPath();
      wire(this.ctx, this._mx, this._my, mx, my, false, false);
      this.ctx.stroke();
      this.ctx.restore();
      return true;
    } else if (this.op == Operation.RECTSELECT && this.rubberBandSelectionMode) {
      if (!this.rubber) {
        this.dispatchCircuitstateChanged(CircuitStateChanged.SELECTING);
        this.saveImgBuf();
        this.rubber = true;
      } else {
        this.restoreImgBuf();
      }
      this.ctx.save();
      this.ctx.lineWidth = 2;
      this.ctx.setLineDash([2, 2]);
      this.ctx.strokeStyle = compileColor([0.6, 0.5, 0.0, 0.6]); // TODO settings
      this.ctx.beginPath();
      this.ctx.rect(this._mx, this._my, mx-this._mx, my-this._my);
      this.ctx.stroke();
      this.ctx.restore();
      return true;
    }
    return false;
  }

  onMouseRelease({mx, my, button, ctrlKey, prevent}): boolean { // on_my_release_event
    
    if (this.op == Operation.MOVING) {
      this.op = Operation.NONE;
      if (this.is_moving && !this.isViewOnly()) {
        this.dispatchCircuitstateChanged(CircuitStateChanged.MOVED);
        this.is_moving = false;
        return false;
      }
    }

    let r: Point = {
      x: mx / this.zoom,
      y: my / this.zoom
    };

    let pos1: Point = {x:0, y:0};
    let pos2: Point = {x:0, y:0};
    if (this.rubber) {
      this.restoreImgBuf();
      this.rubber = false;

      if (!this.isViewOnly() && (this.op == Operation.CONNECT)) {
        // Über welchen in/out haben wir angehalten?
        // IO-Verbindung (ziel ist entweder Main out oder input
        let area: COMPONENT_AREA;
        let start: IO | null = null;
        let end: IO | null = null;


        // Start-Punkt: Toplevel input oder Child-Output
        pos1.x = this._mx / this.zoom;
        pos1.y = this._my / this.zoom;
        // wo sind wir in d. main-komp.?
        area = this.visual.areaOnComponent(pos1);
        // koord. zu dieser komp umrechnen
        pos1 = this.visual.componentToArea(area, pos1);
        if (area == COMPONENT_AREA.IN) {
          // main-out: input holen
          start = this.visual.getInputFromPoint(pos1);
        } else if (area == COMPONENT_AREA.INNER) {
          // main-inner: zunächst komponente holen
          let vis = this.visual.getVisualFromPoint(pos1);
          if (vis) {
            pos1 = vis.parentToComponent(pos1);
            area = vis.areaOnComponent(pos1)
            // komponente gefunden
            if (area & COMPONENT_AREA.OUT) {
              pos1 = vis.componentToArea(COMPONENT_AREA.OUT, pos1);
              // wir sind auf dem out-bereich der komponente
              // wir holen uns den output
              start = vis.getOutputFromPoint(pos1);
            }
          }
        }

        
        // End-Punkt ist child Input oder Toplevel output
        pos2.x = mx / this.zoom;
        pos2.y = my / this.zoom;
        // wo sind wir in d. main-komp.?
        area = this.visual.areaOnComponent(pos2);
        // koord. zu dieser komp umrechnen
        pos2 = this.visual.componentToArea(area, pos2);
        if (area == COMPONENT_AREA.OUT) {
          // main-out: output holen
          end = this.visual.getOutputFromPoint(pos2);
        } else if (area == COMPONENT_AREA.INNER) {
          // main-inner: zunächst komponente holen
          let vis = this.visual.getVisualFromPoint(pos2);
          if (vis) {
            pos2 = vis.parentToComponent(pos2);
            area = vis.areaOnComponent(pos2)
            // komponente gefunden
            if (area & COMPONENT_AREA.IN) {
              pos2 = vis.componentToArea(COMPONENT_AREA.IN, pos2);
              // wir sind auf dem in-bereich der komponente
              // wir holen uns den input
              end = vis.getInputFromPoint(pos2);
            }
          }
        }


        // falls da einer ist:
        if (start && end)
        {
          // ende darf nicht main input sein
          // anfang darf nicht main output sein
          // input an output nur wenn beide von main sind
          // out an out nur wenn ziel von main ist
          // in an in nur wenn start in main ist

          // optimieren

          let startType: IOType = isIn(start) ? 'in' : 'out'
          let endType: IOType = isIn(end) ? 'in' : 'out'

          function isIn(o: IO): boolean {
            return o instanceof Input;
          }
          function isOut(o: IO): boolean {
            return o instanceof Output;
          }
          const ismain = (o: IO) =>  {
            return ((o.getOwner() == this.visual.getComponent()))
          }

          const ismainin = (o: IO) =>  {
            return (ismain(o) && isIn(o))
          }
          const ismainout = (o: IO) =>  {
            return (ismain(o) && isOut(o))
          }
          const isnmainin = (o: IO) =>  {
            return (!ismain(o) && isIn(o))
          }
          const isnmainout = (o: IO) =>  {
            return (!ismain(o) && isOut(o))
          }

          if (  // Nur gültige verbindungen zulassen
            // anfang darf nicht gleich ende sein
            (start != end)
            &&
            // anfang und ende dürfen nicht beides inputs vom selben typ im selben element sein
            ((startType != endType) || (start.getOwner() != end.getOwner()))
            &&
            ((ismainin(start) && ismainout(end))
            || (isnmainout(start) && isnmainin(end))
            || (ismainin(start) && isnmainin(end))
            || (isnmainout(start) && ismainout(end))
            ))
          {
            if (start.getPinCount() == end.getPinCount()) {
              start.addForwarding(end);
              start.getOwner().pushToSimulationList();
              this.dispatchRefreshEvent(false, true);
              this.dispatchCircuitstateChanged(CircuitStateChanged.CONNECTED);
            }
          }
        }
      } else {
        // Mehrere Componenten markieren
        let area: COMPONENT_AREA;
        // Punkte der bounding rubber box umrechnen in main-comp. inner koord.
        pos1 = this.visual.componentToArea(COMPONENT_AREA.INNER,  {x: this._mx/this.zoom, y: this._my/this.zoom});
        pos2 = this.visual.componentToArea(COMPONENT_AREA.INNER,  {x: mx/this.zoom, y: my/this.zoom});
        let selectedVisuals = this.visual.getVisualFromRect(pos1, pos2);
        if (selectedVisuals.length > 0) {
          let first = true;
          // TODO besser alle auf einmal, sonst wird jedes mal Properties geladen und angezeigt!!
          // stimmt nicht, nur beim ersten und dann sind ja mehrere selektiert und demach sollten keine props mehr angezeigt werden
          for (let vis of selectedVisuals) {
            this.select(vis, !first || ctrlKey);
            first = false;
          }
          this.dispatchRefreshEvent(false, false);
          this.dispatchCircuitstateChanged(CircuitStateChanged.SELECTED);
        }
      }
    } else if (!this.isViewOnly()) {
      // schauen wir wo wir losgelassen haben
      let p1 = {
        x: mx/this.zoom,
        y: my/this.zoom
      };
      p1 = this.visual.componentToArea(COMPONENT_AREA.INNER,  p1);
      let visual = this.visual.getVisualFromPoint(p1);
      if (visual != null) {
        p1 = visual.parentToComponent(p1);
        let area: COMPONENT_AREA = visual.areaOnComponent(p1);
        p1 = visual.componentToArea(area, p1);
        if (area == COMPONENT_AREA.INNER && (button == MouseButton.MAIN)) {
            visual.onInnerClick(p1)
        }
      } // visual
    } // rubber else

    this.op = Operation.NONE;

    /*if(event->button == 3)
    {
      // Popup wird vom fenser behandelt!
      return false; //It has been handled.
    }
    else return false;
    */

    // TODO this.update();
    return false;
  }
  

  onDragover(e: DragEvent) {
    e.preventDefault();
    if (!e.dataTransfer) return;
    e.dataTransfer.dropEffect = "move"
  }

  onDrop(e: DragEvent) {
    e.preventDefault();
    if (!e.dataTransfer) return;
    var rawData = e.dataTransfer.getData("application/piiri-cir");
    if (!rawData) return;
    var data = JSON.parse(rawData);
    var pos = this.visual.componentToArea(COMPONENT_AREA.INNER, {x: (e.offsetX-data.dx)/this.zoom, y: (e.offsetY-data.dy)/this.zoom});
    this.dispatchComponent(data.cir, pos.x, pos.y)
  }

  onSizeChanged() {
    let w = this.$element.clientWidth;
    let h = this.$element.clientHeight;
    // TODO minimum size
    this.canvas.setAttribute('width', ""+w);
    this.canvas.setAttribute('height', ""+h);
    this.dispatchRefreshEvent(true, false)
  }





  /**
   * Ohne events
   */
  deselect() {
    this.selection.forEach(s=>s.deselect());
    this.selection = [];
  }

  /**
   * Ohne events
   */
   select(something, add=false) {
    if (!something) add = false;
    var clearSelection = !add;

    if (add && this.selection.length) { // wenn das neue einen anderen typ hat hals das bisherige
      var typ1 = this.selection[0] instanceof IOBase ? 'io' : 'comp';
      var typ2 = something instanceof IOBase ? 'io' : 'comp';
      if (typ1 != typ2) clearSelection = true;
    }

    if (clearSelection) {
      this.deselect()
    }

    if (something) {
      if (this.selection.indexOf(something) >= 0) {
        if (add) {
          this.selection = this.selection.filter(s=>(s!=something)); // entfernen
          something.deselect();
        }
      } else {
        //if (typ1 == 'io' && selection.length==2) {
          //letztes ersetzen
          //selection[1].deselect();
          //selection[1] = something;
          //selection[1].select();
          //return;
        //}
        this.selection.push(something);
        something.select();
      }
    }
  }

  get width() { return this.ctx.canvas.width }
  get height() { return this.ctx.canvas.height }
  //get size(): Size { return { w: this.width, h: this.height } }

  refresh(resize=true, update=true) {
    //console.log('refresh', this.getDomDocument().defaultView.name);
    if (typeof this.visual === 'undefined' || !this.visual) return;
    let zoom = this.zoom;

    if (update) {
      this.visual.getComponent().update();
      /*if (CircuitComponent.simtyp == "STEP") {
        this.startSteps();
      }*/
    }
    if (resize) {
      // mindestens viewport füllend
      let minSize = {w: (this.canvas.parentElement.clientWidth-5)/zoom, h: (this.canvas.parentElement.clientHeight-5)/zoom};
      this.visual.setMinSize(minSize)
      this.visual.updateSize(this.ctx, 0, 0);
      let size = this.visual.getSize();
      this.canvas.setAttribute('width', ""+(size.w*zoom))
      this.canvas.setAttribute('height', ""+(size.h*zoom))
    }
    this.ctx.save();
    this.ctx.scale(zoom, zoom);
    this.ctx.clearRect(0, 0, this.width/zoom, this.height/zoom);
    this.visual.paintAll(this.ctx, 0, 0, true);
    this.ctx.restore();
  }


}

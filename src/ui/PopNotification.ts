

type PN = {

}

const stack: PN[] = [];
let container: HTMLDivElement | null = null;

export interface Waiter {
  done: ()=>void,
  failed: ()=>void,  
}

export class PopNotification {
  $document: Document;
  $el: HTMLDivElement;
  timer;
  content;
  isWaiting: boolean = false;

  constructor(doc: Document) {
    this.$document = doc;
  }

  text(t: string) {
    this.content = this.$document.createTextNode(t);
    return this;
  }

  html(c: string) {
    this.content = this.$document.createElement('div');
    this.content.innerHTML = c
    return this;
  }

  auto() {
    this.show();
    this.timer = setTimeout(()=>{
      this.remove();
    }, this.getDuration());
  }


  waiting(): Waiter {
    this.isWaiting = true;
    this.show();
    let self = this;
    return {
      failed() {
        self.$el.querySelector('.waiting').classList.add('failed')
        self.timer = setTimeout(()=>{
          self.remove();
        }, self.getDuration());
      },
      done() {
        self.$el.querySelector('.waiting').classList.add('done')
        self.timer = setTimeout(()=>{
          self.remove();
        }, self.getDuration());
      }
    }
  }

  private show() {
    this.createContainer();
    this.createNode();
    setTimeout(()=>this.$el.classList.add('show'), 10)
  }

  private remove() {
    if (!container) return;
    if (!this.$el) return;
    let finish = () => {
      container.removeChild(this.$el);
      this.$el = null;
    };
    this.$el.classList.add('removing');
    this.$el.addEventListener('transitionend', finish, {once:true})
  }

  destroy() {
    if (this.timer) clearTimeout(this.timer);
  }

  getDuration() {
    return 2000;
  }

  createNode() {
    if (!container) return;
    this.$el = this.$document.createElement('div');
    this.$el.classList.add('popNotification')
    if (this.isWaiting) {
      let w = this.$document.createElement('span');
      w.classList.add('waiting')
      this.$el.appendChild(w);
    }
    this.$el.appendChild(this.content);
    container.appendChild(this.$el)
  }

  createContainer() {
    if (container === null) {
      container = this.$document.createElement('div');
      container.classList.add('popNotificationContainer')
    }
    // wird immer ausgeführt um den container immer toplevel zu haben
    this.$document.querySelector('body').appendChild(container);
  }
}



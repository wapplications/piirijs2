import { Prompt, StringPrompt } from "./Prompt"
import CircuitLib, { RequestException } from "../components/api"
import { PopNotification } from "./PopNotification";
import { wait } from "../helpers";



export class GroupAdminPrompt extends Prompt<void> {
  $el: HTMLDivElement;
  $ul: HTMLDivElement;
  $open: HTMLInputElement;
  $actions: HTMLSelectElement;
  $groupName: HTMLSpanElement;
  $userInfo: HTMLDivElement;

  show(): Promise<void> {
    this.start();
    this.setOpaque();
    return this.result();
  }

  getContent(value: void): HTMLElement {
    this.$el = this.getDocument().createElement('div');
    this.$el.style.height = '100%';
    this.$el.style.width = '100%';
    this.$el.style.borderRadius = "0";
    this.$el.style.display = 'flex';
    this.$el.style.flexDirection = 'column';
    this.$el.style.overflowY = 'auto';
    this.$el.classList.add('popupContainer');

    let $closeButton = this.getDocument().createElement('button');
    $closeButton.innerHTML = "&times;";
    $closeButton.style.position = "absolute";
    $closeButton.style.top = "0";
    $closeButton.style.right = "0";
    $closeButton.style.border = "none";
    $closeButton.style.background = "none";
    $closeButton.style.outline = "none";
    $closeButton.style.padding = "0.5rem";
    $closeButton.style.boxSizing = "content-box";
    $closeButton.style.margin = "0";
    $closeButton.style.fontSize = "2rem";
    $closeButton.style.boxShadow = "none";
    $closeButton.style.display = "block";
    $closeButton.style.height = "2rem";
    $closeButton.style.width = "2rem";
    $closeButton.style.lineHeight = "2rem";
    
    this.$el.appendChild($closeButton);
    $closeButton.addEventListener('click', (e) => {
      this.resolve();
    })

    this.$groupName = this.getDocument().createElement('span');
    let $title = this.getDocument().createElement('div');
    $title.style.marginBottom = '1rem'
    $title.style.fontWeight = 'bold';
    $title.innerText = "Gruppen Admin: ";
    $title.appendChild(this.$groupName);
    this.$el.appendChild($title)

    let $oc = this.getDocument().createElement('div');
    $oc.style.marginBottom = '1rem'
    let $ol = this.getDocument().createElement('label');
    this.$open = this.getDocument().createElement('input');
    $ol.innerText = "Offene Registrierung";
    $ol.addEventListener('click', ()=>this.$open.click());
    this.$open.type = 'checkbox'
    this.$open.addEventListener('click', this.onOpenSwitch.bind(this))
    $oc.appendChild(this.$open);
    $oc.appendChild($ol);
    this.$el.appendChild($oc);

    let $actionsContainer = this.getDocument().createElement('div');
    $actionsContainer.style.display = 'flex';
    $actionsContainer.style.flexDirection = 'row';
    $actionsContainer.style.justifyContent = 'space-between'
    this.$actions = this.getDocument().createElement('select');
    this.$actions.innerHTML = `
    <option value="">Aktion wählen…</option>
    <option value="add">Benutzer erstellen…</option>
    <option value="del">Benutzer löschen</option>
    <option value="pin">PIN setzen…</option>
    <option value="rename">Benutzer umbenennen…</option>
    <option value="gpw">Gruppenpasswort setzen…</option>
    `;
    //let $actionButton = this.getDocument().createElement('button');
    let $reloadButton = this.getDocument().createElement('button');
    //$actionButton.innerText = "Ausführen"
    $reloadButton.innerText = "Aktualisieren"
    $actionsContainer.appendChild(this.$actions)
    //$actionsContainer.appendChild($actionButton)
    $actionsContainer.appendChild($reloadButton)
    //$actionButton.addEventListener('click', this.onAction.bind(this));
    this.$actions.addEventListener('change', this.onAction.bind(this));
    $reloadButton.addEventListener('click', ()=>this.updateData());
    this.$el.appendChild($actionsContainer)

    this.$userInfo = this.getDocument().createElement('div');
    this.$el.appendChild(this.$userInfo)


    let $table = this.getDocument().createElement('table');
    let $thead = this.getDocument().createElement('thead');
    let $u = this.getDocument().createElement('tr');      
    let $b = this.getDocument().createElement('th');
    let $check = this.getDocument().createElement('input');
    $check.type = "checkbox";
    $check.addEventListener('click', ()=>{
      let c = $check.checked;
      let checks = this.$ul.querySelectorAll('td > input[type="checkbox"]')
      let checkedCount = Array.from(checks).filter((e)=>(e as HTMLInputElement).checked).length;
      let setall = (cc) =>{
        checks.forEach((e)=>(e as HTMLInputElement).checked=cc)
      }
      if (checkedCount === checks.length) {
        setall(false)
      } else {
        setall(true)
      }
    })
    $b.appendChild($check)
    $u.appendChild($b);
    let $t = this.getDocument().createElement('th');
    $t.innerText = "Benutzername";
    $u.appendChild($t);
    let $a = this.getDocument().createElement('th');
    $a.innerText = 'Flags';
    $u.appendChild($a);
    let $c = this.getDocument().createElement('th');
    $c.innerText = "#";
    $u.appendChild($c);
    $thead.appendChild($u);
    this.$ul = this.getDocument().createElement('tbody');
    $table.classList.add('adminUserTable')
    $table.appendChild($thead);
    $table.appendChild(this.$ul);
    this.$el.appendChild($table)
    this.updateData();
    return this.$el;
  }

  
  async updateData() {
    let data = await this.loadData();
    if (typeof data !== 'object') return;
    
    this.$ul.innerHTML = "";

    for (let u of data.users) {
      let $u = this.getDocument().createElement('tr');
      
      let $b = this.getDocument().createElement('td');
      let $check = this.getDocument().createElement('input');
      $check.type = "checkbox";
      $check.value = u.name;
      $b.appendChild($check)
      $u.appendChild($b);

      let $t = this.getDocument().createElement('td');
      $t.innerText = u.name;
      $u.appendChild($t);
      
      let $a = this.getDocument().createElement('td');
      $a.innerText = (u.is_group_manager ? 'A' : '') + (u.hasPin ? 'P' : '');
      $a.title = (u.is_group_manager ? 'Admin ' : '') + (u.hasPin ? 'PIN ' : '');
      $u.appendChild($a);
      
      let $c = this.getDocument().createElement('td');
      $c.innerText = ""+u.components;
      $u.appendChild($c);

      $check.addEventListener('click', (e)=>{
        e.stopPropagation();
      })
      $u.addEventListener('click', (e)=>{
        e.stopPropagation();
        $check.click();
      })
      
      this.$ul.appendChild($u);
    }
    this.$userInfo.innerText = 'Benutzer: ' + data.users.length + ' von ' + (data.maxUsers !== null ? 'maximal '+data.maxUsers : 'unbegrenzt') + '.'
    if (data.maxUsers !== null && data.users.length >= data.maxUsers) {
      this.$userInfo.style.color = '#ff6655';
    } else {
      this.$userInfo.style.color = '';
    }
    this.$groupName.innerText = data.groupTitle;
    this.$open.checked = data.isOpen;
  }
  

  async loadData() {
    return CircuitLib.loadManagerData();
  }

  popup() {
    return (new PopNotification(this.$document));
  }

  async onOpenSwitch() {
    let waiter = this.popup()
      .text(this.$open.checked ? "Gruppe wird geöffnet" : "Gruppe wird geschlossen.")
      .waiting();
    await wait(2000);
    await CircuitLib.setGroupOpen(this.$open.checked);
    waiter.done();
  }

  async onAction(event: MouseEvent) {
    let action = this.$actions.value;
    this.$actions.value = "";
    if (!action) return;
    let actionMethod = 'action_'+action;
    if (typeof this[actionMethod] === 'undefined') {
      alert('Aktion nicht unterstützt');
      return;
    }
    await this[actionMethod]();
  }

  getSelectedUsers() {
    let checks = this.$ul.querySelectorAll('td > input[type="checkbox"]:checked');
    return Array.from(checks).map((e)=>(e as HTMLInputElement).value);
  }




  async action_add() {
    let name = await (new StringPrompt(this.getDocument())).show("Neuer Benutzername:", "");
    if (!name) return;
    
    let waiter = this.popup().text("Benutzer wird gespeichert.").waiting();
    await wait(2000);

    try {
      await CircuitLib.createUser({name});
      await this.updateData();
      waiter.done()
    } catch (e) {
      this.handleException(e);
      waiter.failed()
    }
  }

  async action_del() {
    let users = this.getSelectedUsers();
    if (users.length === 0) return;
    if (!confirm("Sollen " + users.length + " Benutzer gelöscht werden?")) return;
    let waiter = this.popup().text("Benutzer wird gelöscht.").waiting();
    for (let u of users) {
      await CircuitLib.deleteUser(u);
    }
    waiter.done()
    await this.updateData();
  }

  async action_pin() {
    let users = this.getSelectedUsers();
    if (users.length === 0) return;
    let pin = await (new StringPrompt(this.getDocument())).show("Neue PIN für "+users.length+" Benutzer:", "");
    if (pin===null) return;
    if (pin.length>10) { // TODO use validator
      alert("PIN zu lang. Wird nicht geändert.");
      return;
    }
    let waiter = this.popup().text("Benutzer PIN wird gesetzt.").waiting();
    for (let u of users) {
      await CircuitLib.setUserPIN(u, pin);
    }
    waiter.done();
    await this.updateData();
  }

  async action_rename() {
    let users = this.getSelectedUsers();
    if (users.length === 0) return;
    if (users.length > 1) {
      alert("Es kann nur ein Benutzer auf einmal umbenannt werden.");
      return;
    }
    let u = users[0]
    let newname = await (new StringPrompt(this.getDocument())).show("Neuer Benutzername:", u);
    if (newname===null) return;
    if (newname.length>50) { // TODO use validator
      alert("Name zu lang. Wird nicht geändert.");
      return;
    }
    let waiter = this.popup().text("Benutzer wird umbenannt.").waiting();
    try {
      await CircuitLib.renameUser(u, newname);
      await this.updateData();
      waiter.done();
    } catch (e) {
      this.handleException(e);
      waiter.failed();
    }
  }

  async action_gpw() {
    let pw = await (new StringPrompt(this.getDocument())).show("Neues Passwort für die Gruppe:", "");
    if (pw===null) return;
    if (pw.length>50) { // TODO use validator
      alert("Passwort zu lang. Wird nicht geändert.");
      return;
    }
    let waiter = this.popup().text("Gruppenpasswort wird gesetzt.").waiting();
    try {
      await CircuitLib.updateGroup({password: pw});
      waiter.done();
    } catch (e) {
      this.handleException(e);
      waiter.failed();
    }
  }

  handleException(e) {
    if (e instanceof RequestException) {
      alert(e.getMessage());
      return;
    }
    if (typeof e === 'string') alert(e);
    else if (typeof e.msg === 'string') alert(e.msg);
    else if (typeof e.toString !== 'undefined') alert(e.toString())
    else alert('Unbekannter Fehler');
  }

}
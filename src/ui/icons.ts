


export default {

   export: `<svg height="40" width="40" viewBox="0 0 100 100">
   <path d="M80,20 L20,50 L80,80" class="strokeGray" style="fill:none; stroke-width:3.7; stroke-linecap:round; stroke-linejoin:round;"></path>
   <circle cx="80" cy="20" r="10" class="fillTheme"></circle>   
   <circle cx="80" cy="80" r="10" class="fillTheme"></circle>   
   </svg>`,


  table: `<svg height="40" width="40" viewBox="0 0 100 100">
   <rect class="strokeGray" style="fill:none; stroke-width:3.7;  " x="20" y="20" width="60" height="60" rx="5" />
   <line x1="50" y1="20" x2="50" y2="80" class="strokeGray" style="stroke-width:3.7; stroke-linecap:round;" />
   <line x1="20" y1="30" x2="80" y2="30" class="strokeGray" style="stroke-width:3.7; stroke-linecap:round;" />
   <text x="28" y="65" class="fillTheme" style="font-size:30px;" >0</text>
   <text x="58" y="65" class="fillTheme" style="font-size:30px;" >1</text>
   </svg>`,

  impulse: `<svg height="40" width="40" viewBox="0 0 100 100">
   <path d="M10,30 h20 v-12 h30 v12 h10 v-12 h20" class="strokeGray" style="fill:none; stroke-width:3.7;"></path>
   <path d="M10,70 h40 v-12 h10 v12 h20 v-12 h10" class="strokeTheme" style="fill:none; stroke-width:3.7;"></path>
   </svg>`,


  'toggle-rubber': `<svg height="40" width="40" viewBox="0 0 100 100">
  <rect class="strokeTheme" style="fill:none; stroke-width:3.7; stroke-dasharray:5,5; " x="20" y="20" width="60" height="60" />
  <line class="toggle-rubber-line strokeGray" x1="10" y1="90" x2="90" y2="10" style="display:none;stroke-width:3.7; stroke-linecap:round;" />
  </svg>`,

  in: `<svg height="40" width="40" viewBox="0 0 100 100">
  <g id="addin">
 <path
    class="strokeGray"
    style="opacity:1;fill:none;fill-opacity:1;stroke-width:3.70000005;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
    d="m 76.378261,8.76492 h -35.35534 c -3.91737,0 -7.07107,3.15369 -7.07107,7.07106 v 65.94647 c 0,3.91737 3.1537,7.07107 7.07107,7.07107 h 35.35534"  />
 <rect
    rx="6.8185296"
    ry="6.8185272"
    y="40.857605"
    x="-39.527985"
    height="13.637054"
    width="29.29438"
    class="fillTheme"
    style="opacity:1;fill-opacity:1;stroke:none;stroke-width:3.77952766;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
    transform="scale(-1,1)" />
 <g
    transform="matrix(3.9127228,0,0,3.9127228,-203.75833,-2002.8691)"
    class="fillTheme"
    style="font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;font-size:16px;line-height:1.25;font-family:'Proza Libre';-inkscape-font-specification:'Proza Libre';font-variant-ligatures:normal;font-variant-caps:normal;font-variant-numeric:normal;font-feature-settings:normal;text-align:start;letter-spacing:0px;word-spacing:0px;writing-mode:lr-tb;text-anchor:start;fill-opacity:1;stroke:none"
    aria-label="+">
   <path
      d="m 69.693901,523.90662 v -3.46094 h 1.054688 v 3.46094 h 3.367187 v 1.05468 h -3.367187 v 3.46094 h -1.054688 v -3.46094 h -3.367187 v -1.05468 z" />
 </g></g></svg>`,



  out: `<svg height="40" width="40" viewBox="100 0 100 100">  <g
  id="addout">
 <path
    class="strokeGray"
    d="m 121.51998,8.76492 h 35.35534 c 3.91737,0 7.07107,3.15369 7.07107,7.07106 v 65.94647 c 0,3.91737 -3.1537,7.07107 -7.07107,7.07107 h -35.35534"
    style="opacity:1;fill:none;fill-opacity:1;stroke-width:3.70000005;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1" />
 <rect
    style="opacity:1;fill-opacity:1;stroke:none;stroke-width:3.77952766;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
    class="fillTheme"
    width="29.29438"
    height="13.637054"
    x="158.37025"
    y="40.857597"
    ry="6.8185272"
    rx="6.8185296" />
 <path
    class="fillTheme"
    style="font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;font-size:16px;line-height:1.25;font-family:'Proza Libre';-inkscape-font-specification:'Proza Libre';font-variant-ligatures:normal;font-variant-caps:normal;font-variant-numeric:normal;font-feature-settings:normal;text-align:start;letter-spacing:0px;word-spacing:0px;writing-mode:lr-tb;text-anchor:start;fill-opacity:1;stroke:none;stroke-width:3.91272283"
    d="M 128.96365,47.032277 V 33.490578 h -4.1267 v 13.541699 h -13.17487 v 4.126671 h 13.17487 v 13.541698 h 4.1267 V 51.158948 h 13.17487 v -4.126671 z" />
</g></svg>`,


  component: `<svg height="40" width="40" viewBox="0 100 100 100">  <g
  id="addcomp"
  inkscape:label="#g1380">
 <g
    class="fillTheme"
    style="font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;font-size:16px;line-height:1.25;font-family:'Proza Libre';-inkscape-font-specification:'Proza Libre';font-variant-ligatures:normal;font-variant-caps:normal;font-variant-numeric:normal;font-feature-settings:normal;text-align:start;letter-spacing:0px;word-spacing:0px;writing-mode:lr-tb;text-anchor:start;fill-opacity:1;stroke:none;stroke-width:0.73196679"
    id="g863-6"
    transform="matrix(5.3454922,0,0,5.3454922,-324.71297,-2648.6278)">
   <path
      d="m 69.693901,523.90662 v -3.46094 h 1.054688 v 3.46094 h 3.367187 v 1.05468 h -3.367187 v 3.46094 h -1.054688 v -3.46094 h -3.367187 v -1.05468 z"
      style="stroke-width:0.73196679"
      id="path861-7"
      inkscape:connector-curvature="0" />
 </g>
 <path
    class="fillTheme"
    style="opacity:1;fill-opacity:0.53125;stroke:none;stroke-width:1.37129247;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
    d="m 76.09339,113.56262 h -49.6069 c -3.22749,0 -3.82581,0.50389 -3.82581,1.12979 v 10.53674 c 0,0.62591 0.59832,1.12979 3.82581,1.12979 h 49.6069 c 3.22749,0 3.8258,-0.50388 3.8258,-1.12979 v -10.53674 c 0,-0.6259 -0.59831,-1.12979 -3.8258,-1.12979 z"
    id="path1016" />
 <rect
    style="opacity:1;fill-opacity:1;stroke:none;stroke-width:2.34540224;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
    class="fillGray"
    width="11.869254"
    height="9.8751221"
    x="84.241745"
    y="132.19579"
    ry="3.1697941"
    rx="3.5355315" />
 <rect
    rx="3.5355315"
    ry="3.1697941"
    y="132.19579"
    x="-16.110994"
    height="9.8751221"
    width="11.869254"
    class="fillGray"
    style="opacity:1;fill-opacity:1;stroke:none;stroke-width:2.34540224;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
    transform="scale(-1,1)" />
 <rect
    style="opacity:1;fill-opacity:1;stroke:none;stroke-width:2.34540224;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
    class="fillGray"
    width="11.869254"
    height="9.8751221"
    x="-16.110994"
    y="152.19579"
    ry="3.1697941"
    rx="3.5355315"
    transform="scale(-1,1)" />
 <path
    id="path996"
    d="M 78.71507,108.70583 H 23.35973 c -3.91737,0 -7.07107,3.15369 -7.07107,7.07106 v 65.94647 c 0,3.91737 3.1537,7.07107 7.07107,7.07107 h 55.35534 c 3.91737,0 7.07107,-3.1537 7.07107,-7.07107 v -65.94647 c 0,-3.91737 -3.1537,-7.07106 -7.07107,-7.07106 z"
    style="opacity:1;fill:none;fill-opacity:1;stroke-width:3.70000005;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
    class="strokeTheme" />
</g>
</svg>`,

  'zoom-in': `<svg height="40" width="40" viewBox="300 100 100 100">  <g
  id="zoomin"
  inkscape:label="#g1390">
 <ellipse
    style="opacity:1;fill:none;fill-opacity:1;stroke-width:3.70000029;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
    class="strokeGray"
    cx="340.85986"
    cy="140.54378"
    rx="25.001217"
    ry="25.003004" />
 <path
    d="m 359.46345,158.79485 24.82038,24.77774"
    class="strokeGray"
    style="fill:none;stroke-width:3.70000005;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" />
 <g
    class="fillTheme"
    style="font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;font-size:16px;line-height:1.25;font-family:'Proza Libre';-inkscape-font-specification:'Proza Libre';font-variant-ligatures:normal;font-variant-caps:normal;font-variant-numeric:normal;font-feature-settings:normal;text-align:start;letter-spacing:0px;word-spacing:0px;writing-mode:lr-tb;text-anchor:start;fill-opacity:1;stroke:none"
    id="g863-6-56-1"
    transform="matrix(3.9127228,0,0,3.9127228,65.85809,-1911.1631)">
   <path
      d="m 69.693901,523.90662 v -3.46094 h 1.054688 v 3.46094 h 3.367187 v 1.05468 h -3.367187 v 3.46094 h -1.054688 v -3.46094 h -3.367187 v -1.05468 z"
      id="path861-7-2-2"
      inkscape:connector-curvature="0" />
 </g>
</g>
</svg>`,
  'zoom-out': `<svg height="40" width="40" viewBox="400 100 100 100">  <g
  id="zoomout"
  inkscape:label="#g1400">
 <ellipse
    ry="25.003004"
    rx="25.001217"
    cy="139.54378"
    cx="441.14557"
    class="strokeGray"
    style="opacity:1;fill:none;fill-opacity:1;stroke-width:3.70000029;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1" />
 <path
    style="fill:none;stroke-width:3.70000005;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
    d="m 459.74916,157.79485 24.82038,24.77774"
    class="strokeGray" />
 <g
    transform="matrix(3.9127228,0,0,3.9127228,166.1438,-1912.1631)"
    class="fillTheme"
    style="font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;font-size:16px;line-height:1.25;font-family:'Proza Libre';-inkscape-font-specification:'Proza Libre';font-variant-ligatures:normal;font-variant-caps:normal;font-variant-numeric:normal;font-feature-settings:normal;text-align:start;letter-spacing:0px;word-spacing:0px;writing-mode:lr-tb;text-anchor:start;fill-opacity:1;stroke:none"
    aria-label="+">
   <path
      inkscape:connector-curvature="0"
      id="path1144"
      d="m 74.115776,523.90662 v 1.05468 h -7.789062 v -1.05468 z"
      sodipodi:nodetypes="ccccc" />
 </g>
</g>
</svg>`,




  delete: `<svg height="40" width="40" viewBox="100 100 100 100"><g
  id="delete">
 <path
    class="fillTheme"
    d="m 141.40414,149.26484 -26.941,-26.941 8.21001,-8.21001 26.941,26.941 26.2112,-26.2112 8.20995,8.20995 -26.2112,26.2112 26.941,26.941 -8.21001,8.21001 -26.941,-26.941 -26.2112,26.2112 -8.20994,-8.20995 z"
    style="font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;font-size:16px;line-height:1.25;font-family:'Proza Libre';-inkscape-font-specification:'Proza Libre';font-variant-ligatures:normal;font-variant-caps:normal;font-variant-numeric:normal;font-feature-settings:normal;text-align:start;letter-spacing:0px;word-spacing:0px;writing-mode:lr-tb;text-anchor:start;fill-opacity:1;stroke:none;stroke-width:11.00866508" />
</g></svg>`,


  node: `<svg height="40" width="40" viewBox="200 100 100 100">  <g
     id="node"
     inkscape:label="#g1414">
    <path
       style="fill:none;stroke-width:3.70000005;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
       class="strokeGray"
       d="m 215.42857,138.80536 35.71428,32.14286 33.21429,-30.35714" />
    <circle
       style="opacity:1;fill-opacity:1;stroke:none;stroke-width:3.77999997;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
       class="fillTheme"
       cx="250.78572"
       cy="168.44818"
       r="11.071428" />
    <g
       transform="matrix(3.9127228,0,0,3.9127228,-24.3277,-1919.5879)"
       class="fillTheme"
       style="font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;font-size:16px;line-height:1.25;font-family:'Proza Libre';-inkscape-font-specification:'Proza Libre';font-variant-ligatures:normal;font-variant-caps:normal;font-variant-numeric:normal;font-feature-settings:normal;text-align:start;letter-spacing:0px;word-spacing:0px;writing-mode:lr-tb;text-anchor:start;fill-opacity:1;stroke:none"
       aria-label="+">
      <path
         inkscape:connector-curvature="0"
         id="path861-7-2"
         d="m 69.693901,523.90662 v -3.46094 h 1.054688 v 3.46094 h 3.367187 v 1.05468 h -3.367187 v 3.46094 h -1.054688 v -3.46094 h -3.367187 v -1.05468 z" />
    </g>
  </g>
</svg>`,


disconnect: `<svg height="40" width="40" viewBox="200 200 100 100">  <g
id="disconnect"
inkscape:label="#g1450">
<path
  sodipodi:nodetypes="cc"
  inkscape:connector-curvature="0"
  id="path1416"
  d="m 219.46918,227.69369 62.9884,43.25453"
  class="strokeTheme"
  style="fill:none;stroke-width:3.70000005;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" />
<circle
  r="11.071428"
  cy="271.47864"
  cx="284.12079"
  class="fillGray"
  style="opacity:1;fill-opacity:1;stroke:none;stroke-width:3.77999997;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1" />
<g
  class="fillTheme"
  style="font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;font-size:16px;line-height:1.25;font-family:'Proza Libre';-inkscape-font-specification:'Proza Libre';font-variant-ligatures:normal;font-variant-caps:normal;font-variant-numeric:normal;font-feature-settings:normal;text-align:start;letter-spacing:0px;word-spacing:0px;writing-mode:lr-tb;text-anchor:start;fill-opacity:1;stroke:none"
  id="g1422"
  transform="matrix(2.7667128,-2.7667128,2.7667128,2.7667128,-1409.9233,-993.0455)">
 <path
    d="m 69.693901,523.90662 v -3.46094 h 1.054688 v 3.46094 h 3.367187 v 1.05468 h -3.367187 v 3.46094 h -1.054688 v -3.46094 h -3.367187 v -1.05468 z"
    id="path1420"
    inkscape:connector-curvature="0" />
</g>
<circle
  style="opacity:1;fill-opacity:1;stroke:none;stroke-width:3.77999997;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
  class="fillGray"
  cx="214.46086"
  cy="225.01161"
  r="11.071428" />
</g>
</svg>`,


  clear: `<svg height="40" width="40" viewBox="200 300 100 100">  <g
  id="clear"
  inkscape:label="#g1476">
 <path
    style="fill:none;stroke-width:3.70000005;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
    d="m 251.49732,313.92972 -0.0763,46.58612"
    class="strokeGray" />
 <path
    style="opacity:1;fill:none;fill-opacity:1;stroke-width:3.70000005;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
    d="m 274.15709,382.05227 c 2.8589,-3.52904 2.01777,-8.71751 2,-13.10293 -0.0159,-3.91734 -3.15295,-7.07227 -7.07032,-7.07227 h -34.9375 c -3.91737,0 -7.07031,2.92976 -7.07031,7.07227 0.0824,3.8954 0.29314,10.51666 -2,13.10293"
    class="strokeGray" />
 <path
    class="strokeGray"
    d="m 235.49732,363.92972 c -0.16589,6.10478 1.21624,15.87627 -2.58138,18.06074"
    style="fill:none;stroke-width:3.70000005;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" />
 <path
    style="fill:none;stroke-width:3.70000005;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
    d="m 243.49732,363.92972 c -0.16589,6.10478 1.21624,15.87627 -2.58138,18.06074"
    class="strokeGray" />
 <path
    class="strokeGray"
    d="m 251.49732,363.92972 c -0.16589,6.10478 1.21624,15.87627 -2.58138,18.06074"
    style="fill:none;stroke-width:3.70000005;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" />
 <path
    style="fill:none;stroke-width:3.70000005;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
    d="m 259.49732,363.92972 c -0.16589,6.10478 1.21624,15.87627 -2.58138,18.06074"
    class="strokeGray" />
 <path
    class="strokeGray"
    d="m 267.49732,363.92972 c -0.16589,6.10478 1.21624,15.87627 -2.58138,18.06074"
    style="fill:none;stroke-width:3.70000005;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" />
</g>
</svg>`,



  save: `<svg height="40" width="40" viewBox="100 300 100 100">  <g
  id="save"
  inkscape:label="#g1508">
 <path
    class="strokeGray"
    d="m 184.15709,355.33199 v 13.10293 c 0,3.91737 -3.15295,7.07227 -7.07032,7.07227 h -54.9375 c -3.91737,0 -7.07031,-3.1549 -7.07031,-7.07227 v -13.10293"
    style="opacity:1;fill:none;fill-opacity:1;stroke-width:3.70000005;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1" />
 <g
    transform="translate(-121.57143,-394.57143)"
    id="g1212">
   <path
      d="m 270.72089,760.38624 c -2.10022,0 -18.55194,-28.4952 -17.50183,-30.31404 1.05011,-1.81884 33.95354,-1.81885 35.00365,0 1.05011,1.81884 -15.4016,30.31404 -17.50182,30.31404 z"
      class="fillTheme"
      style="opacity:1;fill-opacity:1;stroke:none;stroke-width:3.70000005;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
       />
   <rect
      ry="3.16979"
      rx="3.5355301"
      y="710.37738"
      x="262.49173"
      height="31.31473"
      width="16.162441"
      class="fillTheme"
      style="opacity:1;fill-opacity:1;stroke:none;stroke-width:3.70000005;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1" />
 </g>
</g>
</svg>`,




  remove: `<svg height="40" width="40" viewBox="0 200 100 100">  <g
  id="remove"
  inkscape:label="#g1513">
 <g
    class="strokeGray"
    transform="translate(-221.57143,-488)"
    id="g1240">
   <path
      style="opacity:1;fill:none;fill-opacity:1;stroke-width:3.70000005;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
      d="m 305.72852,749.90342 v 13.10293 c 0,3.91737 -3.15295,7.07227 -7.07032,7.07227 h -54.9375 c -3.91737,0 -7.07031,-3.1549 -7.07031,-7.07227 v -13.10293"  />
 </g>
 <path
    class="fillTheme"
    style="font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;font-size:16px;line-height:1.25;font-family:'Proza Libre';-inkscape-font-specification:'Proza Libre';font-variant-ligatures:normal;font-variant-caps:normal;font-variant-numeric:normal;font-feature-settings:normal;text-align:start;letter-spacing:0px;word-spacing:0px;writing-mode:lr-tb;text-anchor:start;fill-opacity:1;stroke:none;stroke-width:11.00866508"
    d="m 44.788328,242.0911 -16.633288,-16.63329 5.068833,-5.06883 16.633289,16.63329 16.182712,-16.18271 5.068796,5.06879 -16.182712,16.18271 16.633288,16.6333 -5.068833,5.06882 -16.633289,-16.63328 -16.182712,16.18271 -5.068795,-5.06879 z" />
</g>
</svg>`,




  rename: `<svg height="40" width="40" viewBox="100 200 100 100">  <g
  id="rename"
  inkscape:label="#g1534">
 <g
    id="g1517"
    transform="translate(-121.57143,-488)"
    class="strokeGray">
   <path
      sodipodi:nodetypes="cssssc"
      inkscape:connector-curvature="0"
      id="path1515"
      d="m 305.72852,749.90342 v 13.10293 c 0,3.91737 -3.15295,7.07227 -7.07032,7.07227 h -54.9375 c -3.91737,0 -7.07031,-3.1549 -7.07031,-7.07227 v -13.10293"
      style="opacity:1;fill:none;fill-opacity:1;stroke-width:3.70000005;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1" />
 </g>
 <path
    class="fillTheme"
    d="m 137.38672,212.50781 v 7.16797 h 8.88672 v 42.83203 h -8.88672 v 7.16797 h 24.9414 v -7.16797 h -8.88671 v -42.83203 h 8.88671 v -7.16797 z"
    style="font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;font-size:16px;line-height:1.25;font-family:'Proza Libre';-inkscape-font-specification:'Proza Libre';font-variant-ligatures:normal;font-variant-caps:normal;font-variant-numeric:normal;font-feature-settings:normal;text-align:start;letter-spacing:0px;word-spacing:0px;writing-mode:lr-tb;text-anchor:start;fill-opacity:1;stroke:none;stroke-width:11.00866508" />
</g>
</svg>`,




  share_extern: `<svg height="40" width="40" viewBox="300 300 100 100">  <g
  id="share"
  inkscape:label="#g1540">
 <circle
    style="opacity:1;fill-opacity:1;stroke:none;stroke-width:3.70000005;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
    class="fillGray"
    cx="327.23016"
    cy="350.66248"
    r="6.5873017" />
 <circle
    r="6.5873017"
    cy="329.03549"
    cx="373.69843"
    class="fillGray"
    style="opacity:1;fill-opacity:1;stroke:none;stroke-width:3.70000005;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1" />
 <circle
    style="opacity:1;fill-opacity:1;stroke:none;stroke-width:3.70000005;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
    class="fillGray"
    cx="373.34128"
    cy="373.93234"
    r="6.5873017" />
 <path
    class="strokeGray"
    d="m 368.53815,371.79802 -41.33012,-20.14031 39.82525,-19.27357"
    style="fill:none;stroke-width:3.70000005;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1" />
</g>
</svg>`,


  share_group: `<svg height="40" width="40" viewBox="200 400 100 100">
  <g
     id="share_group"
     inkscape:label="#g907">
    <path
       style="opacity:1;fill-opacity:1;stroke:none;stroke-width:3.7;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
       d="M 247.875 434.52148 C 240.41202 434.51948 234.35752 440.68356 234.35352 448.39062 C 234.35352 448.39062 234.35352 448.39258 234.35352 448.39258 L 234.35352 448.39453 C 234.36401 453.68012 237.2939 458.48249 241.88867 460.81055 A 1.850185 1.850185 0 0 1 241.5293 464.25 C 232.38553 466.69078 226.10212 474.01651 225.31836 482.49023 L 247.86719 482.39453 L 270.41406 482.28516 C 269.54615 473.90164 263.29459 466.68322 254.24414 464.25781 A 1.850185 1.850185 0 0 1 253.88281 460.82227 C 258.46982 458.48399 261.38801 453.67768 261.38867 448.39453 C 261.38657 440.68858 255.33638 434.52348 247.875 434.52148 z "
       class="fillTheme" />
    <path
       style="opacity:1;fill:none;fill-opacity:1;stroke-width:3.70000005;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
       d="M 226.66211 425.17773 C 219.19913 425.17573 213.14462 431.33982 213.14062 439.04688 L 213.14062 439.04883 L 213.14062 439.05078 C 213.15113 444.33637 216.08101 449.13874 220.67578 451.4668 A 1.850185 1.850185 0 0 1 220.31641 454.90625 C 211.17264 457.34703 204.88923 464.67276 204.10547 473.14648 L 224.4043 473.06055 C 227.11987 468.18333 231.55304 464.21241 237.06641 461.91016 C 233.10859 458.61747 230.66134 453.72901 230.65234 448.39844 A 1.850185 1.850185 0 0 1 230.65234 448.39453 C 230.65409 442.03724 234.00551 436.46864 239 433.39258 C 236.89292 428.53954 232.16585 425.17921 226.66211 425.17773 z "
       class="strokeGray" />
    <path
       style="opacity:1;fill-opacity:1;stroke:none;stroke-width:3.70000005;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
       d="M 268.58398 425.42969 C 263.25403 425.42826 258.65527 428.58091 256.45312 433.18555 C 261.60883 436.23124 265.08783 441.9127 265.08984 448.39453 C 265.08917 453.72406 262.6489 458.6147 258.69727 461.91406 C 264.28856 464.25206 268.77119 468.30822 271.47266 473.28906 L 291.12305 473.19336 C 290.25514 464.80984 284.00358 457.59143 274.95312 455.16602 A 1.850185 1.850185 0 0 1 274.5918 451.73047 C 279.17881 449.39219 282.097 444.58588 282.09766 439.30273 C 282.09566 431.59678 276.04536 425.43169 268.58398 425.42969 z "
       class="fillTheme" />
  </g>
</svg>`,

embed: `<svg height="40" width="40" viewBox="0 500 100 100">
<g
id="embed"
inkscape:label="#g948">
<path
  id="path996-2"
  d="m 77.467271,509.4557 h -55.35534 c -3.91737,0 -7.07107,3.15369 -7.07107,7.07106 v 65.94647 c 0,3.91737 3.1537,7.07107 7.07107,7.07107 h 55.35534 c 3.91737,0 7.07107,-3.1537 7.07107,-7.07107 v -65.94647 c 0,-3.91737 -3.1537,-7.07106 -7.07107,-7.07106 z"
  style="opacity:1;fill:none;fill-opacity:1;stroke-width:3.70000005;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
  class="strokeGray" />
<path
  class="strokeTheme"
  style="opacity:1;fill:none;fill-opacity:1;stroke-width:3.70000005;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
  d="M 62.80411,533.74141 H 30.775093 c -2.26662,0 -4.091375,1.77548 -4.091375,3.98089 v 37.12681 c 0,2.20542 1.824755,3.9809 4.091375,3.9809 H 62.80411 c 2.266619,0 4.091374,-1.77548 4.091374,-3.9809 V 537.7223 c 0,-2.20541 -1.824755,-3.98089 -4.091374,-3.98089 z"
  id="path944" />
</g>
</svg>`,

"enter-embedded": `<svg height="40" width="40" viewBox="300 500 100 100">
<g
inkscape:label="#g163"
id="enter-embedded2"
transform="translate(100)">
<path
  class="strokeTheme"
  style="opacity:1;fill:none;fill-opacity:1;stroke-width:3.70000005;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
  d="m 233.03697,570.76073 v 3.87409 c 0,2.20542 1.82476,3.9809 4.09138,3.9809 h 32.02902 c 2.26662,0 4.09137,-1.77548 4.09137,-3.9809 v -37.12681 c 0,-2.20541 -1.82475,-3.98089 -4.09137,-3.98089 h -32.02902 c -2.26662,0 -4.09138,1.77548 -4.09138,3.98089 v 4.58519"
  id="path108" />
<path
  class="strokeGray"
  style="opacity:1;fill:none;fill-opacity:1;stroke-width:3.70000005;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
  d="m 277.61012,509.09856 h -55.35534 c -3.91737,0 -7.07107,3.15369 -7.07107,7.07106 v 65.94647 c 0,3.91737 3.1537,7.07107 7.07107,7.07107 h 55.35534 c 3.91737,0 7.07107,-3.1537 7.07107,-7.07107 v -65.94647 c 0,-3.91737 -3.1537,-7.07106 -7.07107,-7.07106 z"
  id="path110" />
<g
  transform="matrix(0,-0.65006204,0.65006204,0,-235.22236,732.37503)"
  id="g116"
  style="stroke-width:1.5383147">
 <path
    style="opacity:1;fill-opacity:1;stroke:none;stroke-width:5.69176435;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
    class="fillTheme"
    d="m 270.72089,760.38624 c -2.10022,0 -18.55194,-28.4952 -17.50183,-30.31404 1.05011,-1.81884 33.95354,-1.81885 35.00365,0 1.05011,1.81884 -15.4016,30.31404 -17.50182,30.31404 z"
    />
 <rect
    style="opacity:1;fill-opacity:1;stroke:none;stroke-width:5.69176435;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
    class="fillTheme"
    width="16.162441"
    height="31.31473"
    x="262.49173"
    y="710.37738"
    rx="3.5355301"
    ry="3.16979" />
</g>
</g>
</svg>`,



"enter-embedded-pen": `<svg height="40" width="40" viewBox="100 500 100 100">
<g
id="enter-embedded"
transform="translate(-2.7142858)"
inkscape:label="#g960">
<path
  class="strokeGray"
  style="opacity:1;fill:none;fill-opacity:1;stroke-width:3.70000005;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
  d="m 180.32441,509.09856 h -55.35534 c -3.91737,0 -7.07107,3.15369 -7.07107,7.07106 v 65.94647 c 0,3.91737 3.1537,7.07107 7.07107,7.07107 h 55.35534 c 3.91737,0 7.07107,-3.1537 7.07107,-7.07107 v -65.94647 c 0,-3.91737 -3.1537,-7.07106 -7.07107,-7.07106 z"
  id="path950" />
<path
  id="path952"
  d="m 154.05357,533.38427 h -20.42134 c -2.26662,0 -4.09137,1.77548 -4.09137,3.98089 v 37.12681 c 0,2.20542 1.82475,3.9809 4.09137,3.9809 h 32.02902 c 2.26662,0 4.09137,-1.77548 4.09137,-3.9809 V 547.5"
  style="opacity:1;fill:none;fill-opacity:1;stroke-width:3.70000005;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
  class="strokeTheme" />
<g
  id="g974"
  transform="rotate(43.71378,163.2547,545.51234)"
  inkscape:transform-center-x="21.963242"
  inkscape:transform-center-y="-0.26011054">
 <path
    class="fillTheme"
    d="m 156.51786,523.03577 h 5.35714 c 1.18714,0 2.14286,0.95571 2.14286,2.14285 v 28.21432 c 0,1.18714 -0.95572,2.14285 -2.14286,2.14285 h -2.58929 -2.76785 c -1.18715,0 -2.14286,-0.95571 -2.14286,-2.14285 v -28.21432 c 0,-1.18714 0.95571,-2.14285 2.14286,-2.14285 z"
    style="opacity:1;fill-opacity:1;stroke:none;stroke-width:3.70000005;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1" />
 <path
    class="fillTheme"
    d="m 154.33789,555.16602 0.0371,0.0742 v -0.0742 z m 9.67969,0 v 0.11523 l 0.0566,-0.11523 z m -0.92383,1.98828 c -0.34589,0.23924 -0.76454,0.38086 -1.21875,0.38086 h -2.58984 -2.76758 c -0.44048,0 -0.84774,-0.13323 -1.1875,-0.35938 l 3.87695,7.85547 z"
    style="opacity:1;fill-opacity:1;stroke:none;stroke-width:3.70000029;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1" />
</g>
</g>
</svg>`,

  "leave-embedded": `<svg height="40" width="40" viewBox="200 500 100 100">
  <g
     id="leave-embedded"
     inkscape:label="#g163">
    <path
       id="path944-5"
       d="m 227.03697,568.76073 v 5.87409 c 0,2.20542 1.82476,3.9809 4.09138,3.9809 h 32.02902 c 2.26662,0 4.09137,-1.77548 4.09137,-3.9809 v -37.12681 c 0,-2.20541 -1.82475,-3.98089 -4.09137,-3.98089 h -32.02902 c -2.26662,0 -4.09138,1.77548 -4.09138,3.98089 v 6.58519"
       style="opacity:1;fill:none;fill-opacity:1;stroke-width:3.70000005;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
       class="strokeTheme" />
    <path
       class="strokeGray"
       d="m 277.61012,509.09856 h -55.35534 c -3.91737,0 -7.07107,3.15369 -7.07107,7.07106 v 65.94647 c 0,3.91737 3.1537,7.07107 7.07107,7.07107 h 55.35534 c 3.91737,0 7.07107,-3.1537 7.07107,-7.07107 v -65.94647 c 0,-3.91737 -3.1537,-7.07106 -7.07107,-7.07106 z"
       style="opacity:1;fill:none;fill-opacity:1;stroke-width:3.70000005;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1" />
    <g
       style="stroke-width:1.5383147"
       id="g1226-3-3"
       transform="matrix(0,-0.65006204,-0.65006204,0,713.57952,732.37503)">
      <path
         d="m 270.72089,760.38624 c -2.10022,0 -18.55194,-28.4952 -17.50183,-30.31404 1.05011,-1.81884 33.95354,-1.81885 35.00365,0 1.05011,1.81884 -15.4016,30.31404 -17.50182,30.31404 z"
         class="fillTheme"
         style="opacity:1;fill-opacity:1;stroke:none;stroke-width:5.69176435;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
         sodipodi:type="star" />
      <rect
         ry="3.16979"
         rx="3.5355301"
         y="710.37738"
         x="262.49173"
         height="31.31473"
         width="16.162441"
         class="fillTheme"
         style="opacity:1;fill-opacity:1;stroke:none;stroke-width:5.69176435;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1" />
    </g>
  </g>
  </svg>`,

  timer: `<svg height="40" width="40" viewBox="200 0 100 100">  <g
  id="timer"
  inkscape:label="#g112">
 <path
    style="fill:none;stroke-width:3.70000005;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
    d="m 207.00333,57.87256 h 18.88953 v -20 h 20.53571 v 20 h 23.56316"
    id="path1416-3"
    class="strokeTheme" />
 <rect
    rx="6.8185296"
    ry="6.8185272"
    y="50.824333"
    x="265.53137"
    height="13.637054"
    width="29.29438"
    class="fillGray"
    style="opacity:1;fill-opacity:1;stroke:none;stroke-width:3.77952766;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1" />
</g>
</svg>`,  

reload: `<svg height="40" width="40" viewBox="300 400 100 100">
<g
id="reload"
transform="rotate(-28.681076,353.91189,451.72847)"
inkscape:label="#g1241">
<path
  d="m 375.13886,435.72907 a 27.598429,27.603109 0 0 1 -1.39951,32.93735 27.598429,27.603109 0 0 1 -31.94975,8.10529 27.598429,27.603109 0 0 1 -16.92648,-28.28825 27.598429,27.603109 0 0 1 22.24091,-24.3311"
  style="opacity:1;fill:none;fill-opacity:1;stroke-width:3.70000005;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
  class="strokeTheme"
  transform="matrix(0.99999999,-1.3241296e-4,-1.3239051e-4,0.99999999,0,0)" />
<path
  transform="matrix(0.45474624,-0.23936263,0.23468612,0.44617531,111.31322,326.25935)"
  style="opacity:1;fill-opacity:1;stroke:none;stroke-width:7.26928663;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
  class="fillTheme"
  d="m 372.5,441.38394 -35.71661,-0.004 17.86178,-30.92948 z"
  id="path1237" />
</g>
</svg>`,  

admin: `<svg height="40" width="40" viewBox="400 400 100 100">
<g
id="admin"
inkscape:label="#g961">
<path
  class="strokeTheme"
  d="m 444.2399,413.7895 c -4.43048,0.64614 -8.70491,2.09929 -12.61118,4.28734 l 1.62065,9.65512 c -2.11307,1.50875 -4.00376,3.3068 -5.61665,5.34148 l -9.69764,-1.58626 c -2.26943,3.87404 -3.80707,8.13238 -4.53623,12.56259 l 8.30967,4.61512 c -0.0415,0.5299 -0.0679,1.06091 -0.0789,1.59232 0.0111,2.13968 0.26972,4.27089 0.77087,6.35109 l -7.11591,6.4381 c 1.61288,4.19401 3.98388,8.05536 6.99451,11.3911 l 8.60304,-3.54277 c 2.03679,1.82197 4.33173,3.33284 6.81041,4.4836 l 0.58877,9.46089 c 3.62003,1.17062 7.40039,1.76995 11.20499,1.77644 0.67902,-0.005 1.35775,-0.0297 2.03542,-0.0728 l 2.60399,-8.97327 c 2.7011,-0.47132 5.31736,-1.34068 7.76337,-2.57969 l 7.84026,5.4912 c 3.72887,-2.47739 6.97188,-5.61752 9.56813,-9.26463 l -5.52965,-7.82607 c 1.27065,-2.35874 2.19275,-4.8892 2.73751,-7.51246 l 9.44675,-2.78404 c 0.0956,-1.02989 0.14763,-2.06337 0.1558,-3.09766 -0.005,-3.4258 -0.49188,-6.83386 -1.44463,-10.12452 l -9.87164,-0.57461 c -1.02389,-2.38619 -2.37789,-4.61674 -4.02231,-6.62627 l 3.7876,-9.30509 c -3.24573,-3.06835 -7.02539,-5.51704 -11.15238,-7.22515 l -6.65865,7.42141 c -2.4052,-0.67594 -4.89071,-1.02372 -7.38906,-1.03389 -0.0709,0.002 -0.14165,0.005 -0.21245,0.008 z"
  style="opacity:1;fill:none;fill-opacity:1;stroke-width:3.70000005;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1" />
<path
  class="strokeGray"
  d="m 449.3568,431.03194 c 10.61854,-8.8e-4 19.22711,8.60676 19.22734,19.22527 -2.4e-4,10.61852 -8.6088,19.22615 -19.22734,19.22526 -10.61775,-2.3e-4 -19.22508,-8.60754 -19.22532,-19.22526 2.3e-4,-10.61773 8.60756,-19.22503 19.22532,-19.22527 z"
  style="opacity:1;fill:none;fill-opacity:1;stroke-width:3.70000005;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1" />
</g>
</svg>`,

clone: `<svg height="40" width="40" viewBox="400 500 100 100">
<g
id="clone"
inkscape:label="#g952">
<path
  id="path942"
  d="m 456.98452,535.07683 h -39.01845 c -2.76125,0 -4.9842,2.16125 -4.9842,4.84584 v 45.19354 c 0,2.68461 2.22295,4.84585 4.9842,4.84585 h 39.01845 c 2.76124,0 4.98419,-2.16124 4.98419,-4.84585 v -45.19354 c 0,-2.68459 -2.22295,-4.84584 -4.98419,-4.84584 z"
  style="opacity:1;fill:none;fill-opacity:1;stroke-width:3.70000005;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
  class="strokeGray" />
<path
  class="strokeTheme"
  style="opacity:1;fill:none;fill-opacity:1;stroke-width:3.70000005;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1"
  d="M 481.34895,510.73133 H 442.3305 c -2.76124,0 -4.98419,2.16124 -4.98419,4.84583 v 45.19355 c 0,2.6846 2.22295,4.84585 4.98419,4.84585 h 39.01845 c 2.76125,0 4.9842,-2.16125 4.9842,-4.84585 v -45.19355 c 0,-2.68459 -2.22295,-4.84583 -4.9842,-4.84583 z"
  id="path948" />
</g>
</svg>`,
}
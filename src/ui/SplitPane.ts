import Ui from "./Ui";
import { init_ui } from "./UiManager";

export default class SplitPane extends Ui {
  $p1: HTMLElement;
  $p2: HTMLElement;
  direction: string;
  paneSize: any;
  sizedPane: any;
  $splitter: HTMLDivElement;
  splitterWidth: number;

  _splitterDrag: any;
  _splitterMouseUp: any;
  
  constructor(root, element) {
    super(root, element);



    if (!this.$element.firstElementChild) throw 'SplitPane.constructor: no p1'
    if (!this.$element.lastElementChild) throw 'SplitPane.constructor: no p2'
    this.$p1 = this.$element.firstElementChild as HTMLElement;
    this.$p2 = this.$element.lastElementChild as HTMLElement;

    this.$p1.classList.add('pane');
    this.$p2.classList.add('pane');

    let dir = this.$element.dataset['direction'];
    this.direction = typeof dir !== 'undefined' ? dir : 'horizontal';

    // horizontal
    // ----|---

    // vertical
    // i
    // i
    //---
    // i
    // i

    this.paneSize = this.$p1.dataset['size'];
    this.sizedPane = 1;
    if (!this.paneSize) {
      this.paneSize = this.$p2.dataset['size'];
      this.sizedPane = 2;
    }


    this.$splitter = this.getDomDocument().createElement('div');
    this.$splitter.classList.add('splitter');
    this.$splitter.classList.add(this.direction);
    this.$element.insertBefore(this.$splitter, this.$p2);
    this.splitterWidth = 6;

    // TODO warum nicht teilweise in css datei?
    this.$splitter.style.position = 'absolute';


    this.$p1.style.position = 'absolute';
    this.$p1.style.top = "0";
    this.$p1.style.left = "0";

    this.$p2.style.position = 'absolute';
    if (this.direction == 'vertical') {
      this.$p2.style.left = "0";
      this.$splitter.style.height = this.splitterWidth+'px';
      this.$splitter.style.left = "0"
    } else {
      this.$p2.style.top = "0";
      this.$splitter.style.width = this.splitterWidth+'px';
      this.$splitter.style.top = "0"
    }

    this._splitterDrag = this.splitterDrag.bind(this);
    this._splitterMouseUp = this.splitterMouseUp.bind(this);
    this.$splitter.addEventListener('mousedown', this.splitterMouseDown.bind(this));
    this.$splitter.addEventListener('touchstart', this.splitterMouseDown.bind(this));
  }

  splitterMouseDown(e) {
    if(e.stopPropagation) e.stopPropagation();
    if(e.preventDefault) e.preventDefault();
    e.cancelBubble=true;
    e.returnValue=false;
    this.getDomDocument().defaultView.addEventListener('mousemove', this._splitterDrag);
    this.getDomDocument().defaultView.addEventListener('mouseup', this._splitterMouseUp);
    this.getDomDocument().defaultView.addEventListener('touchmove', this._splitterDrag);
    this.getDomDocument().defaultView.addEventListener('touchend', this._splitterMouseUp);
    return false;
  }
  


  splitterDrag(e) {
    if(e.stopPropagation) e.stopPropagation();
    if(e.preventDefault) e.preventDefault();
    e.cancelBubble=true;
    e.returnValue=false;

    /* TODO wozu ist das?  vielleicht wenn man außerhalb die maustaste loslässt?
    if (e.which == 0) {
      this.getDomDocument().defaultView.trigger('mouseup');
      this.getDomDocument().defaultView.dispatchEvent('mouseup');
      return false;
    }*/
    let x = e.clientX;
    let y = e.clientY;
    if (typeof x === 'undefined') {
      x = e.changedTouches[0].clientX;
      y = e.changedTouches[0].clientY;
    }

    if (this.direction == 'vertical') {
      if (this.sizedPane = 1) {
        this.paneSize = y;
      } else {
        this.paneSize = this.$element.clientHeight - y;
      }
    } else {
      if (this.sizedPane = 1) {
        this.paneSize = x;
      } else {
        this.paneSize = this.$element.clientWidth - x;
      }
    }

    this.onSizeChanged();

    return false;
  }

  splitterMouseUp(e) {
    this.getDomDocument().defaultView.removeEventListener('mousemove', this._splitterDrag);
    this.getDomDocument().defaultView.removeEventListener('mouseup', this._splitterMouseUp);
    this.getDomDocument().defaultView.removeEventListener('touchmove', this._splitterDrag);
    this.getDomDocument().defaultView.removeEventListener('touchend', this._splitterMouseUp);
  }





  onSizeChanged() {
    let w = this.$element.clientWidth;
    let h = this.$element.clientHeight;

    var h1, h2;
    let w1, w2;
    if (/\%$/.test(this.paneSize)) {
      let s = this.direction == 'vertical' ? h : w;
      let p = parseFloat(this.paneSize) / 100;
      this.paneSize = s * p;
    }

    if (this.direction == 'vertical') {
      w1 = w;
      w2 = w;
      if (this.sizedPane == 1) {
        h1 = this.paneSize<1 ? this.paneSize*h : this.paneSize;
        h2 = h - h1;
      } else {
        h2 = this.paneSize<1 ? this.paneSize*h : this.paneSize;
        h1 = h - h2;
      }
      h1 -= this.splitterWidth/2;
      h2 -= this.splitterWidth/2;
      this.$p1.style.width = w + 'px'
      this.$p1.style.height = h1 + 'px'
      this.$p2.style.width = w + 'px'
      this.$p2.style.top = (h1 + this.splitterWidth)+'px'
      this.$p2.style.height = h2 + 'px'
      this.$splitter.style.top = h1 + 'px'
      this.$splitter.style.width = w + 'px'
    } else { // horizontal
      h1 = h;
      h2 = h;
      if (this.sizedPane == 1) {
        w1 = this.paneSize<1 ? this.paneSize*w : this.paneSize;
        w2 = w - w1;
      } else {
        w2 = this.paneSize<1 ? this.paneSize*w : this.paneSize;
        w1 = w - w2;
      }
      w1 -= this.splitterWidth/2;
      w2 -= this.splitterWidth/2;
      this.$p1.style.width = w1 + 'px'
      this.$p1.style.height = h + 'px'
      this.$p2.style.width = w2 + 'px'
      this.$p2.style.left = (w1 + this.splitterWidth)+'px'
      this.$p2.style.height = h + 'px'
      this.$splitter.style.left = w1 + 'px'
      this.$splitter.style.height = h + 'px'
    }

    this.sizeElements(this.$p2);
    this.sizeElements(this.$p1);

  }


}

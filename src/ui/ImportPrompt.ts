import { Prompt } from "./Prompt"
import { ToplevelCircuit } from "../components/ToplevelCircuit";
import { SerializedCircuit } from "../types";
import { TitlePrompt } from "./TitlePrompt";


const TestText = `<?xml version="1.0"?>
<schaltung name="beispiel.cir" version="2">
  <settings type="complex">
    <PropValue name="description" type="string"></PropValue>
    <PropValue name="extra" type="complex">
      <PropValue name="clock_active" type="bool">false</PropValue>
      <PropValue name="clock_input" type="string"></PropValue>
      <PropValue name="clock_intervall" type="int">1000</PropValue>
      <PropValue name="zoom" type="double">1</PropValue>
    </PropValue>
    <PropValue name="plan" type="complex">
      <PropValue name="alpha" type="double">0.2</PropValue>
      <PropValue name="file" type="string"></PropValue>
      <PropValue name="over" type="bool">false</PropValue>
      <PropValue name="scale" type="double">1</PropValue>
      <PropValue name="show" type="bool">false</PropValue>
      <PropValue name="x" type="int">0</PropValue>
      <PropValue name="y" type="int">0</PropValue>
    </PropValue>
  </settings>
  <circuit>
    <in name="A0" value="off"/>
    <in name="A1" value="on"/>
    <in name="A2" value="off"/>
    <in name="B0" value="off"/>
    <in name="B1" value="on"/>
    <in name="B2" value="on"/>
    <out name="y0"/>
    <out name="y1"/>
    <out name="y2"/>
    <component name="or_0" cirid="or" x="227" y="325">
      <settings type="complex">
        <PropValue name="description" type="string"></PropValue>
        <PropValue name="markedVisible" type="bool">false</PropValue>
      </settings>
    </component>
    <component name="not_0" cirid="not" x="88" y="305">
      <settings type="complex">
        <PropValue name="description" type="string"></PropValue>
        <PropValue name="markedVisible" type="bool">false</PropValue>
      </settings>
    </component>
    <component name="and_0" cirid="and" x="156" y="317">
      <settings type="complex">
        <PropValue name="description" type="string"></PropValue>
        <PropValue name="markedVisible" type="bool">false</PropValue>
      </settings>
    </component>
    <component name="node_0" cirid="node" x="45" y="325">
      <settings type="complex">
        <PropValue name="description" type="string"></PropValue>
        <PropValue name="markedVisible" type="bool">false</PropValue>
      </settings>
    </component>
    <component name="container_0" cirid="container" x="350" y="149">
      <settings type="complex">
        <PropValue name="description" type="string"></PropValue>
        <PropValue name="markedVisible" type="bool">false</PropValue>
      </settings>
      <circuit>
        <in name="L0" value="on"/>
        <in name="L1" value="on"/>
        <in name="L2" value="on"/>
        <component name="led_0" cirid="led" x="97" y="14">
          <settings type="complex">
            <PropValue name="color" type="selection">rot</PropValue>
            <PropValue name="description" type="string"></PropValue>
            <PropValue name="markedVisible" type="bool">true</PropValue>
          </settings>
        </component>
        <component name="led_1" cirid="led" x="76" y="85">
          <settings type="complex">
            <PropValue name="color" type="selection">orange</PropValue>
            <PropValue name="description" type="string"></PropValue>
            <PropValue name="markedVisible" type="bool">true</PropValue>
          </settings>
        </component>
        <component name="led_2" cirid="led" x="154" y="64">
          <settings type="complex">
            <PropValue name="color" type="selection">violett</PropValue>
            <PropValue name="description" type="string"></PropValue>
            <PropValue name="markedVisible" type="bool">true</PropValue>
          </settings>
        </component>
        <connect from="in.L0" to="led_0.i"/>
        <connect from="in.L1" to="led_2.i"/>
        <connect from="in.L2" to="led_1.i"/>
      </circuit>
    </component>
    <component name="node_1" cirid="node" x="31" y="383">
      <settings type="complex">
        <PropValue name="description" type="string"></PropValue>
        <PropValue name="markedVisible" type="bool">false</PropValue>
      </settings>
    </component>
    <component name="xor_0" cirid="xor" x="109" y="380">
      <settings type="complex">
        <PropValue name="description" type="string"></PropValue>
        <PropValue name="markedVisible" type="bool">false</PropValue>
      </settings>
    </component>
    <component name="clock_0" cirid="clock" x="216" y="216">
      <settings type="complex">
        <PropValue name="description" type="string"></PropValue>
        <PropValue name="interval" type="int">500</PropValue>
        <PropValue name="markedVisible" type="bool">false</PropValue>
        <PropValue name="run" type="bool">true</PropValue>
      </settings>
    </component>
    <component name="digit_0" cirid="digit" x="151" y="53">
      <settings type="complex">
        <PropValue name="description" type="string"></PropValue>
        <PropValue name="markedVisible" type="bool">false</PropValue>
      </settings>
    </component>
    <component name="clock_1" cirid="clock" x="124" y="216">
      <settings type="complex">
        <PropValue name="description" type="string"></PropValue>
        <PropValue name="interval" type="int">1800</PropValue>
        <PropValue name="markedVisible" type="bool">false</PropValue>
        <PropValue name="run" type="bool">true</PropValue>
      </settings>
    </component>
    <component name="node_2" cirid="node" x="294" y="103">
      <settings type="complex">
        <PropValue name="description" type="string"></PropValue>
        <PropValue name="markedVisible" type="bool">false</PropValue>
      </settings>
    </component>
    <component name="text_0" cirid="text" x="119" y="437">
      <settings type="complex">
        <PropValue name="description" type="string"></PropValue>
        <PropValue name="font_size" type="int">16</PropValue>
        <PropValue name="markedVisible" type="bool">false</PropValue>
        <PropValue name="text" type="string">logisches Schaltnetz</PropValue>
      </settings>
    </component>
    <component name="text_1" cirid="text" x="157" y="258">
      <settings type="complex">
        <PropValue name="description" type="string"></PropValue>
        <PropValue name="font_size" type="int">16</PropValue>
        <PropValue name="markedVisible" type="bool">false</PropValue>
        <PropValue name="text" type="string">Taktgeber</PropValue>
      </settings>
    </component>
    <component name="text_2" cirid="text" x="344" y="293">
      <settings type="complex">
        <PropValue name="description" type="string"></PropValue>
        <PropValue name="font_size" type="int">16</PropValue>
        <PropValue name="markedVisible" type="bool">false</PropValue>
        <PropValue name="text" type="string">eingebettete Schaltung</PropValue>
      </settings>
    </component>
    <component name="text_3" cirid="text" x="479" y="22">
      <settings type="complex">
        <PropValue name="description" type="string"></PropValue>
        <PropValue name="font_size" type="int">16</PropValue>
        <PropValue name="markedVisible" type="bool">false</PropValue>
        <PropValue name="text" type="string">Ausg&#xE4;nge</PropValue>
      </settings>
    </component>
    <component name="text_4" cirid="text" x="20" y="57">
      <settings type="complex">
        <PropValue name="description" type="string"></PropValue>
        <PropValue name="font_size" type="int">16</PropValue>
        <PropValue name="markedVisible" type="bool">false</PropValue>
        <PropValue name="text" type="string">Eing&#xE4;nge</PropValue>
      </settings>
    </component>
    <component name="node_3" cirid="node" x="181" y="200">
      <settings type="complex">
        <PropValue name="description" type="string"></PropValue>
        <PropValue name="markedVisible" type="bool">false</PropValue>
      </settings>
    </component>
    <component name="node_4" cirid="node" x="107" y="199">
      <settings type="complex">
        <PropValue name="description" type="string"></PropValue>
        <PropValue name="markedVisible" type="bool">false</PropValue>
      </settings>
    </component>
    <component name="text_5" cirid="text" x="134" y="144">
      <settings type="complex">
        <PropValue name="description" type="string"></PropValue>
        <PropValue name="font_size" type="int">16</PropValue>
        <PropValue name="markedVisible" type="bool">false</PropValue>
        <PropValue name="text" type="string">7-Segment-Anzeige</PropValue>
      </settings>
    </component>
    <component name="node_5" cirid="node" x="536" y="339">
      <settings type="complex">
        <PropValue name="description" type="string"></PropValue>
        <PropValue name="markedVisible" type="bool">false</PropValue>
      </settings>
    </component>
    <component name="text_6" cirid="text" x="268" y="81">
      <settings type="complex">
        <PropValue name="description" type="string"></PropValue>
        <PropValue name="font_size" type="int">16</PropValue>
        <PropValue name="markedVisible" type="bool">false</PropValue>
        <PropValue name="text" type="string">Ecke / Verteiler</PropValue>
      </settings>
    </component>
    <component name="text_7" cirid="text" x="150" y="13">
      <settings type="complex">
        <PropValue name="description" type="string"></PropValue>
        <PropValue name="font_size" type="int">20</PropValue>
        <PropValue name="markedVisible" type="bool">false</PropValue>
        <PropValue name="text" type="string">PiiRi Beispielschaltung</PropValue>
      </settings>
    </component>
    <connect from="in.A0" to="digit_0.0"/>
    <connect from="in.A0" to="out.y0"/>
    <connect from="in.A1" to="digit_0.1"/>
    <connect from="in.A2" to="digit_0.2"/>
    <connect from="in.B0" to="not_0.a"/>
    <connect from="in.B1" to="node_0.i"/>
    <connect from="in.B2" to="node_1.i"/>
    <connect from="or_0.x" to="container_0.L2"/>
    <connect from="or_0.x" to="node_5.i"/>
    <connect from="not_0.x" to="and_0.a"/>
    <connect from="and_0.x" to="or_0.a"/>
    <connect from="node_0.o" to="and_0.b"/>
    <connect from="node_0.o" to="xor_0.a"/>
    <connect from="node_1.o" to="xor_0.b"/>
    <connect from="xor_0.x" to="or_0.b"/>
    <connect from="clock_0.o" to="container_0.L1"/>
    <connect from="clock_1.o" to="node_3.i"/>
    <connect from="node_2.o" to="out.y1"/>
    <connect from="node_2.o" to="container_0.L0"/>
    <connect from="node_3.o" to="node_4.i"/>
    <connect from="node_3.o" to="node_2.i"/>
    <connect from="node_4.o" to="digit_0.3"/>
    <connect from="node_5.o" to="out.y2"/>
  </circuit>
</schaltung>
`;
export class ImportPrompt extends TitlePrompt<SerializedCircuit> {
  $el: HTMLDivElement;

  constructor() {
    super();
    this.setTitle('Schaltung importieren');
  }

  show(): Promise<SerializedCircuit> {
    this.start((new ToplevelCircuit()).toJsonObject() /* dummy */ );
    this.setOpaque();
    return this.result();
  }

  createMainContent(): void {
    function getFileExtension(file: File): string {
        const fileName = file.name;
        const extension = fileName.substring(fileName.lastIndexOf('.') + 1);
        return extension;
    }

    let $text = this.getDocument().createElement('textarea');
    $text.style.flex = '1 1 auto';
    let $buttons = this.getDocument().createElement('div');
    $buttons.style.display = 'flex';
    $buttons.style.flexWrap = 'nowrap';
    $buttons.style.alignItems = 'center';
    $buttons.style.overflowX = 'auto';    
    $buttons.style.flex = '0 0 auto';
    let $ok = this.getDocument().createElement('button');
    $ok.textContent = 'Import';
    $ok.addEventListener('click', ()=>{
      let data = JSON.parse($text.value);
      if (typeof data === 'object') {
        this.resolve(data);
      }
    });
    $buttons.appendChild($ok)

    let $file = this.getDocument().createElement('input');
    $file.type = 'file';
    this.$el.append($file);    
    $file.addEventListener('change', (event) => {
      const input = event.target as HTMLInputElement;
      if (input.files && input.files[0]) {
          const file = input.files[0];
          const reader = new FileReader();
          reader.onload = (e) => {
              const text = e.target?.result;
              if (text instanceof ArrayBuffer) {
                console.log('is arraybuf')
                return;
              }
              let json: SerializedCircuit = null;
              if (file.type === 'application/json') {
                json = JSON.parse(text);
              } else if (getFileExtension(file) === 'cir') {
                alert('Das Importieren von .cir-Dateien ist noch nicht richtig verwendbar.');
                json = this.parseCirFile(text);
              }
              $text.value = JSON.stringify(json, null, '  ');
          };
          reader.readAsText(file);
      }
    });
    this.$el.appendChild($text)
    this.$el.appendChild($buttons)

  }


  parseCirFile(cirData: string): SerializedCircuit|null {
    const parser = new DOMParser();
    const xmlDoc = parser.parseFromString(cirData, "text/xml");

    function readSettings($e: Element) {
      const reader = {
        complex: function($e: Element) {
          let ret: {[key:string]: any} = {};
          let $children = $e.querySelectorAll(':scope > PropValue');
          for (let i = 0; i < $children.length; i++) {
            let $item = $children.item(i);
            const name = $item.getAttribute('name');
            const type = $item.getAttribute('type');
            if (typeof reader[type] !== 'function') {
              console.error('settings reader', type, 'not defined')
              continue;
            }
            ret[name] = reader[type]($item);
          }
          return ret;
        },
        int: function($e: Element) { return parseInt($e.textContent); },
        double: function($e: Element) { return parseFloat($e.textContent); },
        bool: function($e: Element) { return {true: true, false: false}[$e.textContent.trim()] },
        string: function($e: Element) { return $e.textContent.trim(); },
        selection: function($e: Element) { return $e.textContent.trim(); },
      }
      return reader.complex($e);
    }

    const componentMapping = {
      'container': function(value, $e: Element, settings) {
        value.component = 'embedded';
        value.componentData = parseCircuit($e.querySelector(':scope > circuit'));
        return value;
      },
      'led': function(value, $e: Element, settings) {
        value.props.color = settings.color; // TODO evtl übersetzen?
        return value;
      },
      'clock': function(value, $e: Element, settings) {
        value.props.running = settings.run;
        value.props.interval = settings.interval;
        return value;
      },
      'text': function(value, $e: Element, settings) {
        value.props.text = settings.text;
        value.props.fontSize = settings.font_size;
        value.props.color = 'black';
        return value;
      },
      'node': function(value, $e: Element, settings) {
        value.props.pinCount = settings.size;
        return value;
      },

      // TODO Problem: wenn io pins integrierter componenten anders heißen,

      'switch': function(value, $e: Element, settings) {
        return value;
        /*
        old props: 
        new props: size, pinCount, multipin, direction
        in/out: "out" / "0","1","2",...
        */
      },
      'indicator': function(value, $e: Element, settings) {
        return value;
      },
      '8switch': function(value, $e: Element, settings) {
        value.component = 'switch';
        return value;
      },
      '8indicator': function(value, $e: Element, settings) {
        value.component = 'indicator';
        return value;
      },

      
      /*'container': function(value, $e: Element, settings) {
        value.component = 'embedded';
      },*/

      // digit, digit2, letter, adapter, one, zero,
      // switch (=1), 8switch
      // indicator (=1) 8indicator
      // function
      // ram
      
      //  pincount: size!

  };

    function parseChildComponent($e: Element) {
      const name = $e.getAttribute('name');
      const cirid = $e.getAttribute('cirid');
      const x = parseInt($e.getAttribute('x'));
      const y = parseInt($e.getAttribute('y'));
      const settings = readSettings($e.querySelector('settings'));
      let ret = {
        component: cirid,
        name,
        componentData: null,
        pos: {x, y},
        props: {
          name,
          description: settings.description ?? '',
          stayVisible: settings.markedVisible ?? false,
        },
      };
      if (typeof componentMapping[cirid] !== 'undefined') {
        let mapping = componentMapping[cirid];
        if (typeof mapping === 'string') {
          ret.component = mapping;
        } else if (typeof mapping === 'function') {
          ret = mapping(ret, $e, settings);
        }
      }
      return ret;
    }

    function parseConnection($e: Element) {
      return {
        from: $e.getAttribute('from'),
        to: $e.getAttribute('to'),
      }
    }

    function parseCircuit($circuit: Element): SerializedCircuit {
      let json: SerializedCircuit = {
        name: '',
        isGroupComponent: false,
        overwritesGroupComponent: false,
        inputs: [],
        outputs: [],
        children: [],
        connections: [],
        props: {},
      };
      const $ins = $circuit.querySelectorAll('in');
      const $outs = $circuit.querySelectorAll('out');
      const $components = $circuit.querySelectorAll(':scope > component');
      const $connections = $circuit.querySelectorAll(':scope > connect');

      // TODO pincount
      for (let i = 0; i < $ins.length; i++) {
        json.inputs.push({name: $ins.item(i).getAttribute('name'), pinCount: 1});
      }
  
      // TODO pincount
      for (let i = 0; i < $outs.length; i++) {
        json.outputs.push({name: $outs.item(i).getAttribute('name'), pinCount: 1});
      }
  
      for (let i = 0; i < $components.length; i++) {
        json.children.push(parseChildComponent($components.item(i)));
      }
  
      for (let i = 0; i < $connections.length; i++) {
        json.connections.push(parseConnection($connections.item(i)));
      }
      return json;
    }

    const $circuit = xmlDoc.querySelector('schaltung > circuit');
    let json = parseCircuit($circuit);
    let name = xmlDoc.documentElement.getAttribute('name').replace(/\.cir$/, '');
    let settings = readSettings(xmlDoc.querySelector('schaltung > settings'));
    json.name = name;
    json.props.name = name;
    json.props.description = settings.description ?? '';

    return json;
  }
  

}


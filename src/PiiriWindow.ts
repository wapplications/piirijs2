import {Component} from "./components/Component";
import { Size } from "./types";
import {ComponentVisual} from "./visuals/ComponentVisual";
import {ContainerVisual} from "./visuals/ContainerVisual";
import { Container } from "./components/Container";
import PiiriPane from "./ui/PiiriPane";
import BodyScreen from "./ui/BodyScreen";



export default class PiiriWindow {
  piiriPane: PiiriPane;
  canvas: HTMLCanvasElement;
  component: Container
  visual: ContainerVisual

  rootComponent: Container
  rootVisual: ContainerVisual
  
  ctx: CanvasRenderingContext2D;
  mainPos;
  openWindows: PiiriWindow[] = [];

  constructor(eventTarget, pane: PiiriPane, container: Container) {
    this.component = container;
    this.rootComponent = container;
    this.piiriPane = pane
    this.canvas = pane.canvas;
    let c = this.canvas.getContext("2d");
    if (!c) throw "kein kontext"
    this.ctx = c;
    this.mainPos = {x:0, y: 0};

    let doc = pane.getDomDocument();

    /*
    doc.defaultView.addEventListener('unload', (event) => {
      this.unload()
    }, true)
    */
    
    // TODO über tab gehen??
    eventTarget.addEventListener('piiri:refresh', this.onRefresh.bind(this));
    eventTarget.addEventListener('piiri:selection-changed', this.onSelectionChanged.bind(this));
    eventTarget.addEventListener('piiri:command:zoom-in', ()=>{
      this.setZoom(this.piiriPane.zoom*1.1)
    });
    eventTarget.addEventListener('piiri:command:zoom-out', ()=>{
      this.setZoom(this.piiriPane.zoom/1.1)
    });
  }

  setComponent(c: Container) {
    this.component = c;
    this.rootComponent = c;
  }




  start() {
    this.visual = new ContainerVisual(this.component);
    this.rootVisual = this.visual;
    this.piiriPane.setVisual(this.visual)
  }

  unload() {
    //console.log('piiriWindow unload event '+this.piiriPane.getDomDocument().defaultView.name+'');
    this.closeSubWindows();
  }

  close() {
    if (this.hasDomWindow()) {
      this.getDomWindow().close()
    }
    this.visual.clear();
    this.component.clear();
  }



  setZoom(zoom: number) {
    this.piiriPane.zoom = zoom;
    this.piiriPane.refresh(true, false);
  }



  get width() { return this.ctx.canvas.width }
  get height() { return this.ctx.canvas.height }
  get size(): Size { return { w: this.width, h: this.height } }

  onSelectionChanged() {
    this.piiriPane.refresh(false, false);
    this.selectionChanged()
  }

  onRefresh(event: CustomEvent) {
    //console.log('Refresh Event in ', this.getDomWindow().name);
    let {resize, update} = event.detail;
    this.refresh(resize, update)
  }

  refresh(resize, update) {
    //console.log('PiiriWindow.refresh() ', this.getDomWindow().name);
    this.piiriPane.refresh(resize, update)
    for (let win of this.openWindows) {
      win.refresh(resize, update);
    }
  }


  closeRelatedWindows(vis: ComponentVisual) {
    let toClose = [];
    for (let win of this.openWindows) {
      if (win.component === vis.getComponent()) {
        toClose.push(win);
      }
    }
    for (let win of toClose) {
      win.close();
    }
  }
  
  closeSubWindows() {
    for (let win of this.openWindows) {
      win.close();
    }
  }


  selectionChanged() {
  }


  /*openContainerWindow(container: Container) {
    let win = this.getDomWindow().open("", "_blank", "popup=yes,width=400,height=300")
    if (!win) return;
    win.name = container.name + 'View';
    var link  = win.document.createElement('link');
    link.rel  = 'stylesheet';
    link.type = 'text/css';
    let styleUrl = this.getDomWindow().location.protocol + '//' + this.getDomWindow().location.host + '/ui.css';
    link.href =  styleUrl
    link.media = 'all';
    win.document.head.appendChild(link);
    win.document.body.innerHTML = `<div class="uiPiiriPane" data-interactive="1" data-ref="subPane" style="overflow:auto;">
    <canvas  tabindex="1" style="box-sizing:border-box;" />
    </div>`


    setTimeout(()=>{
      if (!win) return;
      let body = new BodyScreen(win.document.body);
      body.init();
      body.doSizeChanged();
      let piiriPane = body.refMap['subPane'] as PiiriPane;

      let piiriWindow = new PiiriWindow(body.$element, piiriPane, container);
      this.openWindows.push(piiriWindow);
      win['_piiriWindow'] = piiriWindow;
      piiriWindow.start();
      piiriPane.setViewOnly(true)
      piiriWindow.refresh(true, false);
      win.addEventListener('unload', () => {
        this.openWindows = this.openWindows.filter((w)=>(w!==piiriWindow))
      });
    }, 100);

  }*/

  hasDomWindow(): boolean {
    // TODO make this configurabel, because we want to use DomWindow-less and DomWindowed PiiriWindows
    return false;
  }

  getDomWindow(): Window {
    return this.piiriPane.getDomDocument().defaultView;
  }

  getDomDocument(): Document {
    return this.piiriPane.getDomDocument();
  }

}

import {Component} from './components/Component'
import { clock } from './helpers';
import { SIMULATION_DEBUG } from './types';


enum SimulationTyp {
  ST_NORMAL,
  ST_STEP
}

class SimulationManger {
  public cir_deque: Component[] = [];
  public timeoutClock: number;
  public timeoutDelta: number = 1000;
  public steptime: number = 100;
  public simtyp: SimulationTyp = SimulationTyp.ST_NORMAL;

  constructor() {
  }



  public list_push(cir: Component) {
    if (this.cir_deque.includes(cir)) {
        return;
    }
    this.cir_deque.push(cir);
  }

  public clear_list(child: Component | null = null) {
      if (child == null) {
          console.log("clear_list(NULL) [sollte nicht sein.]");
          this.cir_deque = [];
      } else {
        this.cir_deque.splice(this.cir_deque.indexOf(child), 1);
      }
  }

  public get_deque() : Component[] {
      return this.cir_deque;
  }

  public is_step_sim() : boolean {
      return this.simtyp == SimulationTyp.ST_STEP;
  }

  public is_normal_sim() : boolean {
      return this.simtyp == SimulationTyp.ST_NORMAL;
  }

  timeout_test(): void {
    if (this.is_normal_sim() && (clock() >= this.timeoutClock)) {
        throw "Zeitlimit überschritten";
    }
  }


  // step(): void {
  //   if (SIMULATION_DEBUG) {
  //       console.log("## STEP");
  //       console.log("## DEQUE: [");
  //       this.dump_deque();
  //       console.log("]");
  //   }

  //   let p_obj: CircuitChild | undefined;
  //   let d: CircuitChild[] = [...this.get_deque()]; // kopie
  //   this.cir_deque = [];
  //   // kopie abarbeiten
  //   while(d.length > 0) {
  //       p_obj = d.shift();
  //       if (typeof p_obj === 'undefined') break;
  //       p_obj.stepSimulate();
  //   }
  // }

  dump_deque(): void {
    for (let cc of this.get_deque()) {
        console.log(cc.getName(), ", ");
    }
  }

}


const Simulation = new SimulationManger();

export default Simulation;
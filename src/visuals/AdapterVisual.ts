import VisualSettings from './VisualSettings';
import {ComponentVisual} from "./ComponentVisual";
import { Size } from '../types';
//import {AdapterComponent} from '../components/AdapterComponent';
import { compileColor } from '../helpers';




export class AdapterVisual extends ComponentVisual {

  innerSize(cr: CanvasRenderingContext2D, x: number, y: number, children: boolean): Size {
    return { w: 20, h: 0 }
  }


  paintSelf(cr: CanvasRenderingContext2D, x: number, y: number, children: boolean): void {
    super.paintSelf(cr, x, y, false);
    let adapter = this.getComponent();
    let size = adapter['size'];
    let dir = adapter['direction'];
    let vsize = this.getSize();
    cr.save();
    let output;
    let input;
    let posin, posout;
    cr.lineWidth = 1;
    if (dir === 'bundle') {
      output = adapter.outputs[0];
      posout = output.ps_getToPos();
    } else {
      input = adapter.inputs[0];
      posin = input.ps_getFromPos();
    }
    for (let i = 0; i < size; i++) {
      cr.beginPath();
      if (dir === 'bundle') {
        input = adapter.inputs[i];
        posin = input.ps_getFromPos();
        if (input.getValue()) cr.strokeStyle = compileColor(VisualSettings.HOT_COL)
        else cr.strokeStyle = compileColor(VisualSettings.WIRE_COL);
      } else {
        output = adapter.outputs[i];
        posout = output.ps_getToPos();
        if (output.getValue()) cr.strokeStyle = compileColor(VisualSettings.HOT_COL)
        else cr.strokeStyle = compileColor(VisualSettings.WIRE_COL);
      }
      cr.moveTo(x+posin.x, y+posin.y);
      cr.lineTo(x+vsize.w+posout.x, y+posout.y+i-size/2);
      cr.stroke();
    }
    cr.restore();
  }

}



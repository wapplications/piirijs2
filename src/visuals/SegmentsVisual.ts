import {ComponentVisual} from "./ComponentVisual";
import { Size } from '../types';




export class SegmentsVisual extends ComponentVisual {
  segmentDisplay: SegmentDisplay;

  init() {
    super.init();
    this.segmentDisplay = new SegmentDisplay()
    this.outWidth = 0;
  }

  innerSize(cr: CanvasRenderingContext2D, x: number, y: number, children: boolean): Size {
    this.outWidth = 6;

    //let segments = this.getSegments();
    let comp = this.getComponent();
    let digits = comp['digits']
    let digitSize = comp['digitSize'];
    return {
      w: digitSize * digits + this.segmentDisplay.digitDistance*(digits-1) + digitSize*0.4,
      h: digitSize*2 + digitSize*0.4+2
    }
  }


  paintSelf(cr: CanvasRenderingContext2D, x: number, y: number, children: boolean): void {
    super.paintSelf(cr, x, y, false);
    //let segments = this.getSegments();
    let comp = this.getComponent();
    let digits = comp['digits']
    let digitSize = comp['digitSize'];
    let decoder = comp['decoder'];
    let mode = comp['mode'];
    let multipin = comp['multipin'];
    let base = mode === 'dez' ? 10 : 16;
    let vsize = this.getSize();

    cr.beginPath()
    cr.rect(x+this.inWidth, y+this.titleHeight, vsize.w-this.inWidth-this.outWidth, vsize.h-this.titleHeight-2)
    cr.fillStyle = '#334333'
    cr.fill()
    
    this.segmentDisplay.digitWidth = digitSize;
    this.segmentDisplay.digitHeight = digitSize*2;
    this.segmentDisplay.digitDistance = digitSize*0.2
    this.segmentDisplay.segmentWidth = digitSize*0.2

    if (decoder) {
      let value = ""+comp.getInputNumber(true).toString(base);
      if (value.length > digits) value = value.substring(value.length - digits)

      this.segmentDisplay.value = ""+value;
      if (this.segmentDisplay.value.length < digits) {
        this.segmentDisplay.value = " ".repeat(digits-this.segmentDisplay.value.length) + this.segmentDisplay.value
      }
      this.segmentDisplay.pattern = "#".repeat(digits)
      this.segmentDisplay.draw(cr, x+this.inWidth+digitSize*0.2, y+this.titleHeight+digitSize*0.2);
    } else {
      let segnames = ['a', 'b', 'c', 'd', 'e', 'f', 'g']
      let segments = [];
      for (let i = 0; i < digits; i++) {
        let value = 0;
        let segs = [];
        if (multipin) {
          // jede Stelle entspricht einem 7pin-multi-input
          value = comp.getInputValue(""+i);
          for (let s = 0; s < 7; s++) {
            if ((value & 1<<s) !== 0) {
              segs.push(segnames[s]);
            }
          }
        } else {
          // eine Stelle bracht 7 einzel inputs 0..6, 7..13 ...
          for (let s = 0; s < 7; s++) {
            if (comp.getInputValue(''+(s+7*i)) !== 0) {
              segs.push(segnames[s]);
            }
          }
        }
        segments.push(segs)
      }
      this.segmentDisplay.drawSegments(cr, x+this.inWidth+digitSize*0.2, y+this.titleHeight+digitSize*0.2, segments);
    }
  }

}


class SegmentDisplay {
  pattern         = '##';
  value           = '12';
  digitHeight     = 20;
  digitWidth      = 10;
  digitDistance   = 0;
  displayAngle    = 0;
  segmentWidth    = 2.5;
  segmentDistance = 0.2;
  segmentCount
  cornerType
  colorOn         = 'rgb(93, 200, 15)';
  colorOff        = 'rgba(93, 299, 15, 0.2)';

  context;

  // Segment display types
  static SevenSegment    = 7;
  static FourteenSegment = 14;
  static SixteenSegment  = 16;

  // Segment corner types
  static SymmetricCorner = 0;
  static SquaredCorner   = 1;
  static RoundedCorner   = 2;

  constructor() {
    this.segmentCount    = SegmentDisplay.SevenSegment;
    this.cornerType      = SegmentDisplay.RoundedCorner;
  }  

  
  draw(context, x,y) {
    this.startDrawing(context, x, y)
    // draw segments
    var xPos = 0;
    var size = (this.value) ? this.value.length : 0;
    for (var i = 0; i < this.pattern.length; i++) {
      var mask  = this.pattern.charAt(i);
      var value = (i < size) ? this.value.charAt(i).toLowerCase() : ' ';
      xPos += this.drawDigit(context, xPos, mask, value);
    }
    this.endDrawing(context)
  };

  startDrawing(context, x, y) {
    // compute skew factor
    var angle = -1.0 * Math.max(-45.0, Math.min(45.0, this.displayAngle));
    var skew  = Math.tan((angle * Math.PI) / 180.0);
    
    // compute scale factor
    var scale = 1; //Math.min(this.width / (width + Math.abs(skew * this.digitHeight)), this.height / this.digitHeight);
    
    // compute display offset
    var offsetX = 0;// (this.width - (width + skew * this.digitHeight) * scale) / 2.0;
    var offsetY = 0; //(this.height - this.digitHeight * scale) / 2.0;
    
    // context transformation
    context.save();
    context.translate(x, y);
    context.scale(scale, scale);
    context.transform(1, 0, skew, 1, 0, 0);
  }

  endDrawing(context) {
    context.restore();
  }

  drawSegments(context, x, y, segLists) {
    this.startDrawing(context, x, y)
    let vars = this.getVars();
    /**
     *  --      a
     * |  |   f   b
     *  --      g
     * |  |   e   c
     *  --      d
     */
    let xPos = 0;
    for (let segList of segLists) {
      let segColors = {
        'a': this.colorOff,
        'b': this.colorOff,
        'c': this.colorOff,
        'd': this.colorOff,
        'e': this.colorOff,
        'f': this.colorOff,
        'g': this.colorOff,
      };
      segList.forEach((s)=>segColors[s] = this.colorOn)
      for (let s in segColors) {
        this.drawSegment(context, s, xPos, vars, segColors[s])
      }
      xPos += this.digitDistance + this.digitWidth;
    }
    this.endDrawing(context)
  }

  drawSegment(context, seg, xPos, vars, fill) {
    const painters = {
      'a': ()=>{
        let x = xPos;
        let y = 0;
        context.fillStyle = fill;
        context.beginPath();
        switch (this.cornerType) {
          case SegmentDisplay.SymmetricCorner:
            context.moveTo(x + vars.s + vars.d, y + vars.s);
            context.lineTo(x + this.segmentWidth + vars.d, y);
            context.lineTo(x + this.digitWidth - this.segmentWidth - vars.d, y);
            context.lineTo(x + this.digitWidth - vars.s - vars.d, y + vars.s);
            break;
          case SegmentDisplay.SquaredCorner:
            context.moveTo(x + vars.s + vars.e, y + vars.s - vars.e);
            context.lineTo(x + this.segmentWidth, y);
            context.lineTo(x + this.digitWidth - this.segmentWidth, y);
            context.lineTo(x + this.digitWidth - vars.s - vars.e, y + vars.s - vars.e);
            break;
          default:
            context.moveTo(x + this.segmentWidth - vars.f, y + this.segmentWidth - vars.f - vars.d);
            context.quadraticCurveTo(x + this.segmentWidth - vars.g, y, x + this.segmentWidth, y);
            context.lineTo(x + this.digitWidth - this.segmentWidth, y);
            context.quadraticCurveTo(x + this.digitWidth - this.segmentWidth + vars.g, y, x + this.digitWidth - this.segmentWidth + vars.f, y + this.segmentWidth - vars.f - vars.d);
        }
        context.lineTo(x + this.digitWidth - this.segmentWidth - vars.d, y + this.segmentWidth);
        context.lineTo(x + this.segmentWidth + vars.d, y + this.segmentWidth);
        context.fill();
      },
      'b': ()=>{
        let x = xPos + this.digitWidth - this.segmentWidth;
        let y = 0;
        context.fillStyle = fill;
        context.beginPath();
        switch (this.cornerType) {
          case SegmentDisplay.SymmetricCorner:
            context.moveTo(x + vars.s, y + vars.s + vars.d);
            context.lineTo(x + this.segmentWidth, y + this.segmentWidth + vars.d);
            break;
          case SegmentDisplay.SquaredCorner:
            context.moveTo(x + vars.s + vars.e, y + vars.s + vars.e);
            context.lineTo(x + this.segmentWidth, y + this.segmentWidth);
            break;
          default:
            context.moveTo(x + vars.f + vars.d, y + this.segmentWidth - vars.f);
            context.quadraticCurveTo(x + this.segmentWidth, y + this.segmentWidth - vars.g, x + this.segmentWidth, y + this.segmentWidth);
        }
        context.lineTo(x + this.segmentWidth, y + vars.h + this.segmentWidth - vars.d);
        context.lineTo(x + vars.s, y + vars.h + this.segmentWidth + vars.s - vars.d);
        context.lineTo(x, y + vars.h + this.segmentWidth - vars.d);
        context.lineTo(x, y + this.segmentWidth + vars.d);
        context.fill();
      },
      'c': ()=>{
        let x = xPos + this.digitWidth - this.segmentWidth;
        let y = vars.h + this.segmentWidth;
        context.fillStyle = fill;
        context.beginPath();
        context.moveTo(x, y + this.segmentWidth + vars.d);
        context.lineTo(x + vars.s, y + vars.s + vars.d);
        context.lineTo(x + this.segmentWidth, y + this.segmentWidth + vars.d);
        context.lineTo(x + this.segmentWidth, y + vars.h + this.segmentWidth - vars.d);
        switch (this.cornerType) {
          case SegmentDisplay.SymmetricCorner:
            context.lineTo(x + vars.s, y + vars.h + this.segmentWidth + vars.s - vars.d);
            context.lineTo(x, y + vars.h + this.segmentWidth - vars.d);
            break;
          case SegmentDisplay.SquaredCorner:
            context.lineTo(x + vars.s + vars.e, y + vars.h + this.segmentWidth + vars.s - vars.e);
            context.lineTo(x, y + vars.h + this.segmentWidth - vars.d);
            break;
          default:
            context.quadraticCurveTo(x + this.segmentWidth, y + vars.h + this.segmentWidth + vars.g, x + vars.f + vars.d, y + vars.h + this.segmentWidth + vars.f); //
            context.lineTo(x, y + vars.h + this.segmentWidth - vars.d);
        }
        context.fill();
      },
      'd': ()=>{
        let x = xPos;
        let y = this.digitHeight - this.segmentWidth;
        context.fillStyle = fill;
        context.beginPath();
        context.moveTo(x + this.segmentWidth + vars.d, y);
        context.lineTo(x + this.digitWidth - this.segmentWidth - vars.d, y);
        switch (this.cornerType) {
          case SegmentDisplay.SymmetricCorner:
            context.lineTo(x + this.digitWidth - vars.s - vars.d, y + vars.s);
            context.lineTo(x + this.digitWidth - this.segmentWidth - vars.d, y + this.segmentWidth);
            context.lineTo(x + this.segmentWidth + vars.d, y + this.segmentWidth);
            context.lineTo(x + vars.s + vars.d, y + vars.s);
            break;
          case SegmentDisplay.SquaredCorner:
            context.lineTo(x + this.digitWidth - vars.s - vars.e, y + vars.s + vars.e);
            context.lineTo(x + this.digitWidth - this.segmentWidth, y + this.segmentWidth);
            context.lineTo(x + this.segmentWidth, y + this.segmentWidth);
            context.lineTo(x + vars.s + vars.e, y + vars.s + vars.e);
            break;
          default:
            context.lineTo(x + this.digitWidth - this.segmentWidth + vars.f, y + vars.f + vars.d);
            context.quadraticCurveTo(x + this.digitWidth - this.segmentWidth + vars.g, y + this.segmentWidth, x + this.digitWidth - this.segmentWidth, y + this.segmentWidth);
            context.lineTo(x + this.segmentWidth, y + this.segmentWidth);
            context.quadraticCurveTo(x + this.segmentWidth - vars.g, y + this.segmentWidth, x + this.segmentWidth - vars.f, y + vars.f + vars.d);
            context.lineTo(x + this.segmentWidth - vars.f, y + vars.f + vars.d);
        }
        context.fill();
      },
      'e': ()=>{
        let x = xPos;
        let y = vars.h + this.segmentWidth;
        context.fillStyle = fill;
        context.beginPath();
        context.moveTo(x, y + this.segmentWidth + vars.d);
        context.lineTo(x + vars.s, y + vars.s + vars.d);
        context.lineTo(x + this.segmentWidth, y + this.segmentWidth + vars.d);
        context.lineTo(x + this.segmentWidth, y + vars.h + this.segmentWidth - vars.d);
        switch (this.cornerType) {
          case SegmentDisplay.SymmetricCorner:
            context.lineTo(x + vars.s, y + vars.h + this.segmentWidth + vars.s - vars.d);
            context.lineTo(x, y + vars.h + this.segmentWidth - vars.d);
            break;
          case SegmentDisplay.SquaredCorner:
            context.lineTo(x + vars.s - vars.e, y + vars.h + this.segmentWidth + vars.s - vars.d + vars.e);
            context.lineTo(x, y + vars.h + this.segmentWidth);
            break;
          default:
            context.lineTo(x + this.segmentWidth - vars.f - vars.d, y + vars.h + this.segmentWidth + vars.f); 
            context.quadraticCurveTo(x, y + vars.h + this.segmentWidth + vars.g, x, y + vars.h + this.segmentWidth);
        }
        context.fill();
      },
      'f': ()=>{
        let x = xPos;
        let y = 0;
        context.fillStyle = fill;
        context.beginPath();
        context.moveTo(x + this.segmentWidth, y + this.segmentWidth + vars.d);
        context.lineTo(x + this.segmentWidth, y + vars.h + this.segmentWidth - vars.d);
        context.lineTo(x + vars.s, y + vars.h + this.segmentWidth + vars.s - vars.d);
        context.lineTo(x, y + vars.h + this.segmentWidth - vars.d);
        switch (this.cornerType) {
          case SegmentDisplay.SymmetricCorner:
            context.lineTo(x, y + this.segmentWidth + vars.d);
            context.lineTo(x + vars.s, y + vars.s + vars.d);
            break;
          case SegmentDisplay.SquaredCorner:
            context.lineTo(x, y + this.segmentWidth);
            context.lineTo(x + vars.s - vars.e, y + vars.s + vars.e);
            break;
          default:
            context.lineTo(x, y + this.segmentWidth);
            context.quadraticCurveTo(x, y + this.segmentWidth - vars.g, x + this.segmentWidth - vars.f - vars.d, y + this.segmentWidth - vars.f); 
            context.lineTo(x + this.segmentWidth - vars.f - vars.d, y + this.segmentWidth - vars.f); 
        }
        context.fill();
      },
      'g': ()=>{
        let x = xPos;
        let y = (this.digitHeight - this.segmentWidth) / 2.0;
        context.fillStyle = fill;
        context.beginPath();
        context.moveTo(x + vars.s + vars.d, y + vars.s);
        context.lineTo(x + this.segmentWidth + vars.d, y);
        context.lineTo(x + this.digitWidth - this.segmentWidth - vars.d, y);
        context.lineTo(x + this.digitWidth - vars.s - vars.d, y + vars.s);
        context.lineTo(x + this.digitWidth - this.segmentWidth - vars.d, y + this.segmentWidth);
        context.lineTo(x + this.segmentWidth + vars.d, y + this.segmentWidth);
        context.fill();
      }

    }
    painters[seg]();
  }

  getVars() {
    var r = Math.sqrt(this.segmentWidth * this.segmentWidth / 2.0);
    var d = Math.sqrt(this.segmentDistance * this.segmentDistance / 2.0);
    var e = d / 2.0; 
    var f = (this.segmentWidth - d) * Math.sin((45.0 * Math.PI) / 180.0);
    var g = f / 2.0;
    var h = (this.digitHeight - 3.0 * this.segmentWidth) / 2.0;
    var w = (this.digitWidth - 3.0 * this.segmentWidth) / 2.0;
    var s = this.segmentWidth / 2.0;
    var t = this.digitWidth / 2.0;
    return {r,d,e,f,g,h,w,s,t}
  }

  
  drawDigit(context, xPos, mask, c) {
    switch (mask) {
      case '#':
        let vars = this.getVars();
        // draw segment a (a1 and a2 for 16 segments)
        if (this.segmentCount != 16) {
          this.drawSegment(context, 'a', xPos, vars, this.getSegmentColor(c, '02356789acefp', '02356789abcdefgiopqrstz@'));
        }
        // draw segment a (a1 and a2 for 16 segments)
        /*if (this.segmentCount == 16) {
          var x = xPos;
          var y = 0;
          context.fillStyle = this.getSegmentColor(c, null, '02356789abcdefgiopqrstz@%');
          context.beginPath();
          switch (this.cornerType) {
            case SegmentDisplay.SymmetricCorner:
              context.moveTo(x + s + d, y + s);
              context.lineTo(x + this.segmentWidth + d, y);
              break;
            case SegmentDisplay.SquaredCorner:
              context.moveTo(x + s + e, y + s - e);
              context.lineTo(x + this.segmentWidth, y);
              break;
            default:
              context.moveTo(x + this.segmentWidth - f, y + this.segmentWidth - f - d);
              context.quadraticCurveTo(x + this.segmentWidth - g, y, x + this.segmentWidth, y);
          }
          context.lineTo(x + t - d - s, y);
          context.lineTo(x + t - d, y + s);
          context.lineTo(x + t - d - s, y + this.segmentWidth);
          context.lineTo(x + this.segmentWidth + d, y + this.segmentWidth);
          context.fill();
          
          var x = xPos;
          var y = 0;
          context.fillStyle = this.getSegmentColor(c, null, '02356789abcdefgiopqrstz@');
          context.beginPath();
          context.moveTo(x + this.digitWidth - this.segmentWidth - d, y + this.segmentWidth);
          context.lineTo(x + t + d + s, y + this.segmentWidth);
          context.lineTo(x + t + d, y + s);
          context.lineTo(x + t + d + s, y);
          switch (this.cornerType) {
            case SegmentDisplay.SymmetricCorner:
              context.lineTo(x + this.digitWidth - this.segmentWidth - d, y);
              context.lineTo(x + this.digitWidth - s - d, y + s);
              break;
            case SegmentDisplay.SquaredCorner:
              context.lineTo(x + this.digitWidth - this.segmentWidth, y);
              context.lineTo(x + this.digitWidth - s - e, y + s - e);
              break;
            default:
              context.lineTo(x + this.digitWidth - this.segmentWidth, y);
              context.quadraticCurveTo(x + this.digitWidth - this.segmentWidth + g, y, x + this.digitWidth - this.segmentWidth + f, y + this.segmentWidth - f - d);
          }
          context.fill();
        }*/

        // draw segment b
        this.drawSegment(context, 'b', xPos, vars, this.getSegmentColor(c, '01234789adhpy', '01234789abdhjmnopqruwy'))
        
        // draw segment c
        this.drawSegment(context, 'c', xPos, vars, this.getSegmentColor(c, '013456789abdhnouy', '01346789abdghjmnoqsuw@', '%'))

        
        // draw segment d (d1 and d2 for 16 segments)
        if (this.segmentCount != 16) {
          this.drawSegment(context, 'd', xPos, vars, this.getSegmentColor(c, '0235689bcdelotuy_', '0235689bcdegijloqsuz_=@'))
        }

        // draw segment d (d1 and d2 for 16 segments)
        /*if (this.segmentCount == 16) {
          x = xPos;
          y = this.digitHeight - this.segmentWidth;
          context.fillStyle = this.getSegmentColor(c, null, '0235689bcdegijloqsuz_=@');
          context.beginPath();
          context.moveTo(x + this.segmentWidth + d, y);
          context.lineTo(x + t - d - s, y);
          context.lineTo(x + t - d, y + s);
          context.lineTo(x + t - d - s, y + this.segmentWidth);
          switch (this.cornerType) {
            case SegmentDisplay.SymmetricCorner:
              context.lineTo(x + this.segmentWidth + d, y + this.segmentWidth);
              context.lineTo(x + s + d, y + s);
              break;
            case SegmentDisplay.SquaredCorner:
              context.lineTo(x + this.segmentWidth, y + this.segmentWidth);
              context.lineTo(x + s + e, y + s + e);
              break;
            default:
              context.lineTo(x + this.segmentWidth, y + this.segmentWidth);
              context.quadraticCurveTo(x + this.segmentWidth - g, y + this.segmentWidth, x + this.segmentWidth - f, y + f + d);
              context.lineTo(x + this.segmentWidth - f, y + f + d);
          }
          context.fill();
  
          x = xPos;
          y = this.digitHeight - this.segmentWidth;
          context.fillStyle = this.getSegmentColor(c, null, '0235689bcdegijloqsuz_=@', '%');
          context.beginPath();
          context.moveTo(x + t + d + s, y + this.segmentWidth);
          context.lineTo(x + t + d, y + s);
          context.lineTo(x + t + d + s, y);
          context.lineTo(x + this.digitWidth - this.segmentWidth - d, y);
          switch (this.cornerType) {
            case SegmentDisplay.SymmetricCorner:
              context.lineTo(x + this.digitWidth - s - d, y + s);
              context.lineTo(x + this.digitWidth - this.segmentWidth - d, y + this.segmentWidth);
              break;
            case SegmentDisplay.SquaredCorner:
              context.lineTo(x + this.digitWidth - s - e, y + s + e);
              context.lineTo(x + this.digitWidth - this.segmentWidth, y + this.segmentWidth);
              break;
            default:
              context.lineTo(x + this.digitWidth - this.segmentWidth + f, y + f + d);
              context.quadraticCurveTo(x + this.digitWidth - this.segmentWidth + g, y + this.segmentWidth, x + this.digitWidth - this.segmentWidth, y + this.segmentWidth);
          }
          context.fill();
        }*/
        
        // draw segment e
        this.drawSegment(context, 'e', xPos, vars, this.getSegmentColor(c, '0268abcdefhlnoprtu', '0268acefghjklmnopqruvw@'))
        
        // draw segment f
        this.drawSegment(context, 'f', xPos, vars, this.getSegmentColor(c, '045689abcefhlpty', '045689acefghklmnopqrsuvwy@', '%'))
  
        // draw segment g for 7 segments
        if (this.segmentCount == 7) {
          this.drawSegment(context, 'g', xPos, vars, this.getSegmentColor(c, '2345689abdefhnoprty-='))
        }
        
        /*
        // draw inner segments for the fourteen- and sixteen-segment-display
        if (this.segmentCount != 7) {
          // draw segment g1
          x = xPos;
          y = (this.digitHeight - this.segmentWidth) / 2.0;
          context.fillStyle = this.getSegmentColor(c, null, '2345689aefhkprsy-+*=', '%');
          context.beginPath();
          context.moveTo(x + s + d, y + s);
          context.lineTo(x + this.segmentWidth + d, y);
          context.lineTo(x + t - d - s, y);
          context.lineTo(x + t - d, y + s);
          context.lineTo(x + t - d - s, y + this.segmentWidth);
          context.lineTo(x + this.segmentWidth + d, y + this.segmentWidth);
          context.fill();
          
          // draw segment g2
          x = xPos;
          y = (this.digitHeight - this.segmentWidth) / 2.0;
          context.fillStyle = this.getSegmentColor(c, null, '234689abefghprsy-+*=@', '%');
          context.beginPath();
          context.moveTo(x + t + d, y + s);
          context.lineTo(x + t + d + s, y);
          context.lineTo(x + this.digitWidth - this.segmentWidth - d, y);
          context.lineTo(x + this.digitWidth - s - d, y + s);
          context.lineTo(x + this.digitWidth - this.segmentWidth - d, y + this.segmentWidth);
          context.lineTo(x + t + d + s, y + this.segmentWidth);
          context.fill();
          
          // draw segment j 
          x = xPos + t - s;
          y = 0;
          context.fillStyle = this.getSegmentColor(c, null, 'bdit+*', '%');
          context.beginPath();
          if (this.segmentCount == 14) {
            context.moveTo(x, y + this.segmentWidth + this.segmentDistance);
            context.lineTo(x + this.segmentWidth, y + this.segmentWidth + this.segmentDistance);
          } else {
            context.moveTo(x, y + this.segmentWidth + d);
            context.lineTo(x + s, y + s + d);
            context.lineTo(x + this.segmentWidth, y + this.segmentWidth + d);
          }
          context.lineTo(x + this.segmentWidth, y + h + this.segmentWidth - d);
          context.lineTo(x + s, y + h + this.segmentWidth + s - d);
          context.lineTo(x, y + h + this.segmentWidth - d);
          context.fill();
          
          // draw segment m
          x = xPos + t - s;
          y = this.digitHeight;
          context.fillStyle = this.getSegmentColor(c, null, 'bdity+*@', '%');
          context.beginPath();
          if (this.segmentCount == 14) {
            context.moveTo(x, y - this.segmentWidth - this.segmentDistance);
            context.lineTo(x + this.segmentWidth, y - this.segmentWidth - this.segmentDistance);
          } else {
            context.moveTo(x, y - this.segmentWidth - d);
            context.lineTo(x + s, y - s - d);
            context.lineTo(x + this.segmentWidth, y - this.segmentWidth - d);
          }
          context.lineTo(x + this.segmentWidth, y - h - this.segmentWidth + d);
          context.lineTo(x + s, y - h - this.segmentWidth - s + d);
          context.lineTo(x, y - h - this.segmentWidth + d);
          context.fill();
          
          // draw segment h
          x = xPos + this.segmentWidth;
          y = this.segmentWidth;
          context.fillStyle = this.getSegmentColor(c, null, 'mnx\\*');
          context.beginPath();
          context.moveTo(x + this.segmentDistance, y + this.segmentDistance);
          context.lineTo(x + this.segmentDistance + r, y + this.segmentDistance);
          context.lineTo(x + w - this.segmentDistance , y + h - this.segmentDistance - r);
          context.lineTo(x + w - this.segmentDistance , y + h - this.segmentDistance);
          context.lineTo(x + w - this.segmentDistance - r , y + h - this.segmentDistance);
          context.lineTo(x + this.segmentDistance, y + this.segmentDistance + r);
          context.fill();
          
          // draw segment k
          x = xPos + w + 2.0 * this.segmentWidth;
          y = this.segmentWidth;
          context.fillStyle = this.getSegmentColor(c, null, '0kmvxz/*', '%');
          context.beginPath();
          context.moveTo(x + w - this.segmentDistance, y + this.segmentDistance);
          context.lineTo(x + w - this.segmentDistance, y + this.segmentDistance + r);
          context.lineTo(x + this.segmentDistance + r, y + h - this.segmentDistance);
          context.lineTo(x + this.segmentDistance, y + h - this.segmentDistance);
          context.lineTo(x + this.segmentDistance, y + h - this.segmentDistance - r);
          context.lineTo(x + w - this.segmentDistance - r, y + this.segmentDistance);
          context.fill();
          
          // draw segment l
          x = xPos + w + 2.0 * this.segmentWidth;
          y = h + 2.0 * this.segmentWidth;
          context.fillStyle = this.getSegmentColor(c, null, '5knqrwx\\*');
          context.beginPath();
          context.moveTo(x + this.segmentDistance, y + this.segmentDistance);
          context.lineTo(x + this.segmentDistance + r, y + this.segmentDistance);
          context.lineTo(x + w - this.segmentDistance , y + h - this.segmentDistance - r);
          context.lineTo(x + w - this.segmentDistance , y + h - this.segmentDistance);
          context.lineTo(x + w - this.segmentDistance - r , y + h - this.segmentDistance);
          context.lineTo(x + this.segmentDistance, y + this.segmentDistance + r);
          context.fill();
          
          // draw segment n
          x = xPos + this.segmentWidth;
          y = h + 2.0 * this.segmentWidth;
          context.fillStyle = this.getSegmentColor(c, null, '0vwxz/*', '%');
          context.beginPath();
          context.moveTo(x + w - this.segmentDistance, y + this.segmentDistance);
          context.lineTo(x + w - this.segmentDistance, y + this.segmentDistance + r);
          context.lineTo(x + this.segmentDistance + r, y + h - this.segmentDistance);
          context.lineTo(x + this.segmentDistance, y + h - this.segmentDistance);
          context.lineTo(x + this.segmentDistance, y + h - this.segmentDistance - r);
          context.lineTo(x + w - this.segmentDistance - r, y + this.segmentDistance);
          context.fill();
        }*/
        
        return this.digitDistance + this.digitWidth;
        
      case '.':
        context.fillStyle = (c == '#') || (c == '.') ? this.colorOn : this.colorOff;
        this.drawPoint(context, xPos, this.digitHeight - this.segmentWidth, this.segmentWidth);
        return this.digitDistance + this.segmentWidth;
        
      case ':':
        context.fillStyle = (c == '#') || (c == ':') ? this.colorOn : this.colorOff;
        var y = (this.digitHeight - this.segmentWidth) / 2.0 - this.segmentWidth;
        this.drawPoint(context, xPos, y, this.segmentWidth);
        this.drawPoint(context, xPos, y + 2.0 * this.segmentWidth, this.segmentWidth);
        return this.digitDistance + this.segmentWidth;
        
      default:
        return this.digitDistance;    
    }
  };
  
  drawPoint(context, x1, y1, size) {
    var x2 = x1 + size;
    var y2 = y1 + size;
    var d  = size / 4.0;
    
    context.beginPath();
    context.moveTo(x2 - d, y1);
    context.quadraticCurveTo(x2, y1, x2, y1 + d);
    context.lineTo(x2, y2 - d);
    context.quadraticCurveTo(x2, y2, x2 - d, y2);
    context.lineTo(x1 + d, y2);
    context.quadraticCurveTo(x1, y2, x1, y2 - d);
    context.lineTo(x1, y1 + d);
    context.quadraticCurveTo(x1, y1, x1 + d, y1);
    context.fill();
  }; 
  
  getSegmentColor(c, charSet7 = "", charSet14 = "", charSet16 = "") {
    if (c == '#') {
      return this.colorOn;
    } else {
      switch (this.segmentCount) {
        case 7:  return (charSet7.indexOf(c) == -1) ? this.colorOff : this.colorOn;
        case 14: return (charSet14.indexOf(c) == -1) ? this.colorOff : this.colorOn;
        case 16: var pattern = charSet14 + (charSet16 === undefined ? '' : charSet16);
                 return (pattern.indexOf(c) == -1) ? this.colorOff : this.colorOn;
        default: return this.colorOff;
      }
    }
  };
  
  


}











import {ComponentVisual} from "./ComponentVisual";
import { Point, Size } from '../types';
import { compileColor } from '../helpers';
import { PLAComponent } from "../components/PLAComponent";
import VisualSettings from './VisualSettings';




export class PLAVisual extends ComponentVisual {

  getGridSize() {
    let pla = this.getComponent() as PLAComponent;
    let gridSize = this.ioHeight*1.5;
    return {
      gridSize,
      andWidth: (pla.andBits+0.5) * gridSize,
      orHeight: (pla.outBits+0.5) * gridSize,
      andHeight: pla.inputs.length*gridSize*2,
    }
  }

  innerSize(cr: CanvasRenderingContext2D, x: number, y: number, children: boolean): Size {
    let gridSize = this.getGridSize();
    return {
      w: gridSize.andWidth+gridSize.gridSize*4, 
      h: gridSize.andHeight + gridSize.orHeight
    }; 
  }

  paintSelf(cr: CanvasRenderingContext2D, x: number, y: number, children: boolean): void {
    super.paintSelf(cr, x, y, false);
    let pla = this.getComponent() as PLAComponent;

    let gridSize = this.getGridSize();

    // IN
    cr.save();
    for (let i = 0; i < pla.inBits; i++) {
      let input = pla.inputs[i];
      let v = input.getBooleanValue();
      let inPos = input.ps_getFromPos();
      // text
      cr.beginPath();
      cr.strokeStyle = compileColor(VisualSettings.WIRE_COL);
      cr.textBaseline = 'middle';
      cr.textAlign = 'left';
      cr.fillText(""+i, x+this.inWidth+gridSize.andWidth+gridSize.gridSize, y+this.titleHeight+(i*2+1)*gridSize.gridSize);
      // wire
      cr.beginPath();
      cr.moveTo(x+inPos.x, y+inPos.y);
      cr.lineTo(x+inPos.x+gridSize.gridSize/2, y + this.titleHeight + (2*i+0.5)*gridSize.gridSize);
      cr.lineTo(x+inPos.x+gridSize.gridSize/2+gridSize.andWidth, y + this.titleHeight + (2*i+0.5)*gridSize.gridSize);
      if (v) cr.strokeStyle = compileColor(VisualSettings.HOT_COL);
      else cr.strokeStyle = compileColor(VisualSettings.WIRE_COL);
      cr.stroke();
      // connection dots
      cr.beginPath();
      for (let c = 0; c < pla.andBits; c++) {
        let a = pla.andMatrix[i*2][c];
        if (a) {
          cr.ellipse(x+this.inWidth + (c+2)*gridSize.gridSize, y+this.titleHeight + (i*2+0.5)*gridSize.gridSize, gridSize.gridSize/5, gridSize.gridSize/5, 0, 0, 2*Math.PI);
        }
      }
      cr.fillStyle = compileColor(VisualSettings.WIRE_COL);
      cr.fill();
    }
    cr.restore();
    // not IN
    cr.save();
    for (let i = 0; i < pla.inBits; i++) {
      let input = pla.inputs[i];
      let v = !input.getBooleanValue();
      let inPos = input.ps_getFromPos();

      cr.beginPath();
      cr.moveTo(x+inPos.x, y+inPos.y);
      cr.lineTo(x+inPos.x+(0.5)*gridSize.gridSize, y+this.titleHeight + (2*i+1.5)*gridSize.gridSize);
      cr.lineTo(x+inPos.x+gridSize.gridSize/2+gridSize.andWidth, y+this.titleHeight + (2*i+1.5)*gridSize.gridSize);
      if (v) cr.strokeStyle = compileColor(VisualSettings.HOT_COL);
      else cr.strokeStyle = compileColor(VisualSettings.WIRE_COL);
      cr.stroke();
      // Not-Symbol
      cr.beginPath();
      cr.moveTo(x+inPos.x+(0.7)*gridSize.gridSize, y+this.titleHeight + (2*i+1.5-0.3)*gridSize.gridSize);
      cr.lineTo(x+inPos.x+(0.7)*gridSize.gridSize, y+this.titleHeight + (2*i+1.5+0.3)*gridSize.gridSize);
      cr.lineTo(x+inPos.x+(1.3)*gridSize.gridSize, y+this.titleHeight + (2*i+1.5)*gridSize.gridSize);
      cr.closePath();
      cr.fillStyle = "white";
      cr.fill();
      cr.stroke();
      cr.beginPath();
      cr.arc(
        x+inPos.x+1.3*gridSize.gridSize, y+this.titleHeight + (2*i+1.5)*gridSize.gridSize,
        3.0,
        0.0,
        2 * Math.PI
      );
      cr.fill();
      cr.stroke();
      // connection dots
      cr.beginPath();
      for (let c = 0; c < pla.andBits; c++) {
        let a = pla.andMatrix[i*2+1][c];
        if (a) {
          cr.ellipse(x+this.inWidth + (c+2)*gridSize.gridSize, y+this.titleHeight+(i*2+1.5)*gridSize.gridSize, gridSize.gridSize/5, gridSize.gridSize/5, 0, 0, 2*Math.PI);
        }
      }
      cr.fillStyle = compileColor(VisualSettings.WIRE_COL);
      cr.fill();
    }
    cr.restore();

    let andValues = pla.getAndValues();
    let orValues = pla.getOrValues(andValues);

    // Vertikale linien
    cr.save();
    for (let i = 0; i < pla.andBits; i++) {
      cr.beginPath();
      cr.moveTo(x+this.inWidth+(i+2)*gridSize.gridSize, y+this.titleHeight);
      cr.lineTo(x+this.inWidth+(i+2)*gridSize.gridSize, y+this.titleHeight + gridSize.andHeight + gridSize.orHeight);
      if (andValues[i]) cr.strokeStyle = compileColor(VisualSettings.HOT_COL);
      else cr.strokeStyle = compileColor(VisualSettings.WIRE_COL);
      cr.stroke();
      // und symbol
      cr.beginPath();
      cr.moveTo(x+this.inWidth+(i+2-0.2)*gridSize.gridSize, y+this.titleHeight+gridSize.andHeight);
      cr.lineTo(x+this.inWidth+(i+2+0.2)*gridSize.gridSize, y+this.titleHeight+gridSize.andHeight);
      cr.lineTo(x+this.inWidth+(i+2+0.2)*gridSize.gridSize, y+this.titleHeight+gridSize.andHeight+0.3*gridSize.gridSize);
      cr.bezierCurveTo(
        x+this.inWidth+(i+2+0.2)*gridSize.gridSize, y+this.titleHeight+gridSize.andHeight+0.5*gridSize.gridSize,
        x+this.inWidth+(i+2-0.2)*gridSize.gridSize, y+this.titleHeight+gridSize.andHeight+0.5*gridSize.gridSize,
        x+this.inWidth+(i+2-0.2)*gridSize.gridSize, y+this.titleHeight+gridSize.andHeight+0.3*gridSize.gridSize
      )
      cr.closePath();
      cr.fillStyle = "white";
      cr.fill();
      cr.stroke();
    }
    cr.restore();

    // horz lines to output
    cr.save();
    for (let i = 0; i < pla.outBits; i++) {
      let output = pla.outputs[i];
      let outPos = output.ps_getToPos();
      // text
      cr.beginPath();
      cr.strokeStyle = compileColor(VisualSettings.WIRE_COL);
      cr.textBaseline = 'middle';
      cr.textAlign = 'right';
      cr.fillText(""+i, x+this.inWidth+1.5*gridSize.gridSize, y+this.titleHeight+ gridSize.andHeight+(i+1)*gridSize.gridSize);
      // wire
      cr.beginPath();
      cr.moveTo(x+this.inWidth+2*gridSize.gridSize, y+this.titleHeight + gridSize.andHeight + (i+1)*gridSize.gridSize);
      cr.lineTo(x+this.inWidth+2*gridSize.gridSize+gridSize.andWidth, y+this.titleHeight + gridSize.andHeight + (i+1)*gridSize.gridSize);
      cr.lineTo(x+this.getSizeW()+outPos.x, y+outPos.y);
      if (orValues[i]) cr.strokeStyle = compileColor(VisualSettings.HOT_COL);
      else cr.strokeStyle = compileColor(VisualSettings.WIRE_COL);
      cr.stroke();
      // oder symbol
      cr.beginPath();
      cr.moveTo(x+this.inWidth+1*gridSize.gridSize+gridSize.andWidth, y+this.titleHeight + gridSize.andHeight + (i+1-0.2)*gridSize.gridSize);
      cr.bezierCurveTo(
        x+this.inWidth+(1+0.1)*gridSize.gridSize+gridSize.andWidth, y+this.titleHeight + gridSize.andHeight + (i+1-0.2)*gridSize.gridSize,
        x+this.inWidth+(1+0.1)*gridSize.gridSize+gridSize.andWidth, y+this.titleHeight + gridSize.andHeight + (i+1+0.2)*gridSize.gridSize,
        x+this.inWidth+(1)*gridSize.gridSize+gridSize.andWidth, y+this.titleHeight + gridSize.andHeight + (i+1+0.2)*gridSize.gridSize
      );
      cr.bezierCurveTo(
        x+this.inWidth+(1+0.2)*gridSize.gridSize+gridSize.andWidth, y+this.titleHeight + gridSize.andHeight + (i+1+0.2)*gridSize.gridSize,
        x+this.inWidth+(1+0.5)*gridSize.gridSize+gridSize.andWidth, y+this.titleHeight + gridSize.andHeight + (i+1+0.2)*gridSize.gridSize,
        x+this.inWidth+(1+0.6)*gridSize.gridSize+gridSize.andWidth, y+this.titleHeight + gridSize.andHeight + (i+1)*gridSize.gridSize
      );
      cr.bezierCurveTo(
        x+this.inWidth+(1+0.5)*gridSize.gridSize+gridSize.andWidth, y+this.titleHeight + gridSize.andHeight + (i+1-0.2)*gridSize.gridSize,
        x+this.inWidth+(1+0.2)*gridSize.gridSize+gridSize.andWidth, y+this.titleHeight + gridSize.andHeight + (i+1-0.2)*gridSize.gridSize,
        x+this.inWidth+1*gridSize.gridSize+gridSize.andWidth, y+this.titleHeight + gridSize.andHeight + (i+1-0.2)*gridSize.gridSize
      );
      cr.closePath();
      cr.fillStyle = "white";
      cr.fill();
      cr.stroke();
      // connection dots or-bereich
      cr.beginPath();
      for (let c = 0; c < pla.andBits; c++) {
        let a = pla.orMatrix[i][c];
        if (a) {
          cr.ellipse(x+this.inWidth+gridSize.gridSize*(c+2), y+this.titleHeight + gridSize.andHeight + (i+1)*gridSize.gridSize, gridSize.gridSize/5, gridSize.gridSize/5, 0, 0, 2*Math.PI);
        }
      }
      cr.fillStyle = compileColor(VisualSettings.WIRE_COL);
      cr.fill();
    }
    cr.restore();

  }

  onInnerClick(pos: Point) {
    let sizes = this.getGridSize();
    let pla = this.getComponent() as PLAComponent;
    let c = Math.round((pos.x-sizes.gridSize) / sizes.gridSize)-1;
    if (c < 0 && c >= pla.andBits) return;
    if (pos.y < sizes.andHeight) {
      let r = Math.round((pos.y-sizes.gridSize/2)/sizes.gridSize);
      if (r<0&&r>=pla.inBits*2) return;
      pla.andMatrix[r][c] = pla.andMatrix[r][c] ? 0 : 1;
      let oppR = (r % 2 == 0) ? r+1 : r-1; 
      pla.andMatrix[oppR][c] = pla.andMatrix[r][c] ? 0 : 1;
      pla.writeMatrixToProps();
      pla.onPropsChanged({'grid':true})
    } else if (pos.y > sizes.andHeight+sizes.gridSize/2) {
      let r = Math.round((pos.y-sizes.andHeight)/sizes.gridSize)-1;
      if (r<0&&r>=pla.outBits) return;
      pla.orMatrix[r][c] = pla.orMatrix[r][c] ? 0 : 1;
      pla.writeMatrixToProps();
      pla.onPropsChanged({'grid':true})
    }
  }


}



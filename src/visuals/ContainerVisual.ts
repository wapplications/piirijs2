import { Output } from '../components/IO'
import {ComponentVisual} from "./ComponentVisual";
import createVisual from "./factory"
import { Point, Size } from '../types';
import {Component} from '../components/Component'
import { Container } from '../components/Container';



type AdjoinVisualResult = {
    visual: ComponentVisual | null,
    isNew: boolean
} | null;


export class ContainerVisual extends ComponentVisual {


    adjoinVisual(o: Component | null): AdjoinVisualResult {
        if (!o) {
            return null;
        }

        let result: AdjoinVisualResult = {
            visual: null,
            isNew: false
        };
        
        let v = this.childVisuals.get(o);
        if (typeof v !== 'undefined') {
            result.visual = v;
            return result;
        }

        result.isNew = true;
        result.visual = createVisual(o);
    
        if (result.visual !== null) {
            this.childVisuals.set(o, result.visual);
        }
    
        return result;
    }
    
    
    public isEmpty(): boolean {
        return !(this.getComponent().hasChildren() || (this.getComponent().getInputs().length>0) || (this.getComponent().getOutputs().length>0));
    }

    innerSize(cr: CanvasRenderingContext2D, x:number=0, y:number=0, children:boolean=true): Size {
        // visuals für alle kinderlein sicherstellen
        for (let child of this.getContainer().getChildren()) {
            if (children || child.staysVisible) {
                if (!this.childVisuals.has(child)) {
                    this.adjoinVisual(child); // ergebnis brauchen wir hier nicht
                }
            }
        }
        return super.innerSize(cr, x, y, children);
    }

    paintWiresBetween(cr: CanvasRenderingContext2D, x: number, y: number, children: boolean) {
        // Linien für alle elemente hinten raus
        if (!children) {
            return;
        }

        cr.save();
        for (let child of this.getContainer().getChildren()) {
            let svis = this.childVisuals.get(child);
            if (typeof svis === 'undefined') continue;
            cr.lineWidth = 1;
            for (let o of child.getOutputs()) {
                let pos = o.ps_getFromPos();
                let abspos = svis.getAbsPos();
                let px = abspos.x + svis.getSizeW() + pos.x;
                let py = abspos.y +                    pos.y;
                let pc = o.getPinCount();
    
                for (let io of o.getForwardings()) {
                    let evis: ComponentVisual | undefined;
                    if (io.getOwner() === this.getComponent()) {
                        evis = this;
                    } else {
                        evis = this.childVisuals.get(io.getOwner());
                        if (typeof evis === 'undefined') return; // TODO evtl continue ?
                    }
                    let abspos2 = evis.getAbsPos();
                    let pos2 = io.ps_getToPos();
                    let px2 = abspos2.x + pos2.x;
                    let py2 = abspos2.y + pos2.y;
                    if (io instanceof Output) {
                        px2 += evis.getSize().w;
                    }
                    let startNode = child.getCirName() === 'node';
                    let endNode = io.getOwner().getCirName() === 'node';
                    this.paintWire(cr, px, py, px2, py2, pc, io.isSelected() || o.isSelected(), o.getValue(), startNode, endNode);
                } // forwardings
            } // inputs
        } // children
        cr.restore();

                // der deaktivierte rest ist ganz unten in der Datei

    }

    paintAll(cr: CanvasRenderingContext2D, x: number, y: number, children: boolean) :void {
        super.paintAll(cr, x, y, children);

        this.paintWiresBetween(cr, x, y, children);

        // children:
        let ChildOffsetX: number;
        let ChildOffsetY: number;
        let vis : ComponentVisual | undefined;
    
        for (let child of this.getContainer().getChildren()) {
            vis = this.childVisuals.get(child);
            if (typeof vis !== 'undefined') {
                ChildOffsetX = x + this.inWidth;
                ChildOffsetY = y + this.titleHeight;
    
                if (children || child.staysVisible) { // vis->getObj()->
                    vis.paintAll(
                        cr,
                        ChildOffsetX + vis.getPosX() - this.min_x,
                        ChildOffsetY + vis.getPosY() - this.min_y,
                        false
                        );
                }
            }
        }
    }

    getContainer(): Container {
        return this.getComponent() as Container;
    }

    getVisualFromPoint(pos: Point): ComponentVisual | null {
        let x1,x2,y1,y2;
        let vis: ComponentVisual | undefined;
        let children = this.getContainer().getChildren();
        let count = children.length;
        for (let i = count-1; i >= 0; i--) {
            let child = children[i];
            vis = this.childVisuals.get(child);
            if (typeof vis === 'undefined') continue;
            x1 =      vis.getPosX();
            x2 = x1 + vis.getSizeW();
            y1 =      vis.getPosY();
            y2 = y1 + vis.getSizeH();
            if ((pos.x >= x1) && (pos.x <= x2) && (pos.y >= y1) && (pos.y <= y2) ) {
                return vis;
            }
        }
        return null;
    }
 
    getVisualFromRect(pos1: Point, pos2: Point): ComponentVisual[] {
        let c;
        if (pos1.x > pos2.x) {c = pos1.x; pos1.x = pos2.x; pos2.x = c;}
        if (pos1.y > pos2.y) {c = pos1.y; pos1.y = pos2.y; pos2.y = c;}
        let x1,x2,y1,y2;
        let vis: ComponentVisual | undefined;
        let result: ComponentVisual[] = [];
        let children = this.getContainer().getChildren();
        let count = children.length;
        for (let i = count-1; i >= 0; i--) {
            let child = children[i];
            vis = this.childVisuals.get(child);
            if (typeof vis === 'undefined') continue;
            x1 =      vis.getPosX();
            x2 = x1 + vis.getSizeW();
            y1 =      vis.getPosY();
            y2 = y1 + vis.getSizeH();
            if ((x1 >= pos1.x) && (y1 >= pos1.y) && (x2 <= pos2.x) && (y2 <= pos2.y)) {
                result.push(vis);
            }
        }
        return result;
    }
 
}



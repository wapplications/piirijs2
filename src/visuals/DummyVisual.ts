import VisualSettings from './VisualSettings';
import {ComponentVisual} from "./ComponentVisual";
import { Size } from '../types';
import { compileColor } from '../helpers';




export class DummyVisual extends ComponentVisual {

  innerSize(cr: CanvasRenderingContext2D, x: number, y: number, children: boolean): Size {
    return { w: 24, h: 24 }
  }


  paintSelf(cr: CanvasRenderingContext2D, x: number, y: number, children: boolean): void {
    super.paintSelf(cr, x, y, false);
    cr.save();

    cr.beginPath();
    cr.rect(x + VisualSettings.STRICH, y, this.getSizeW() - 2 * VisualSettings.STRICH, this.getSizeH());
    if (this.selected) cr.strokeStyle = compileColor(VisualSettings.CIR_FRAME_SEL_COL);
    else cr.strokeStyle = "#ff0000";
    cr.lineWidth = 1.0;
    cr.stroke();

    cr.beginPath();
    cr.moveTo(x+this.inWidth+4,y+this.titleHeight+4);
    cr.lineTo(x+this.inWidth+20,y+this.titleHeight+20);
    cr.moveTo(x+this.inWidth+20,y+this.titleHeight+4);
    cr.lineTo(x+this.inWidth+4,y+this.titleHeight+20);
    cr.strokeStyle = "#ff0000";
    cr.lineWidth = 3.0;
    cr.stroke();

    cr.restore();
  }

}



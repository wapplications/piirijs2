import {Component} from '../components/Component'
//import ToplevelCircuit from "../components/ToplevelCircuit";
//import ToplevelCircuitVisual from "./ToplevelCircuitVisual";
import {ComponentVisual} from "./ComponentVisual";
import {ContainerVisual} from "./ContainerVisual";
import {GateVisual} from "./GateVisual";
import {NodeVisual} from "./NodeVisual";
import {LEDVisual} from "./LEDVisual";
import {ValueVisual} from "./ValueVisual";
import {ClockVisual} from "./ClockVisual";
import {AdapterVisual} from "./AdapterVisual";
import {SegmentsVisual} from "./SegmentsVisual";
import { Container } from '../components/Container';
import { IndicatorVisual, SwitchVisual } from '../components/IOComponent';
import { TextVisual } from '../components/TextComponent';
import { DummyComponent } from '../components/DummyComponent';
import { DummyVisual } from './DummyVisual';
import { PLAVisual } from './PLAVisual';


export default function createVisual(o: Component) : ComponentVisual {
    let n = o.getCirName();
    if (n=="") {
        console.warn("Cir name not set ", "Constructor: ", Object.getPrototypeOf(o).constructor.name, "Name: ", o.getName())
    }
    if ((n == "and") || (n == "nand") || (n == "or") || (n == "nor") || (n == "not") || (n == "xor")) { 
        return new GateVisual(o, this);
    }
    if (n == "node") return new NodeVisual(o, this);
    if (n == "segments") return new SegmentsVisual(o, this);
    if (n == "adapter") return new AdapterVisual(o, this);
    if (n == 'value') return new ValueVisual(o, this);
    if (n == "clock") return new ClockVisual(o, this);
    if (n == "led") return new LEDVisual(o, this);
    if (n == "switch") return new SwitchVisual(o, this);
    if (n == "indicator") return new IndicatorVisual(o, this);
    if (n == "text") return new TextVisual(o, this);
    if (n == "pla") return new PLAVisual(o, this);
    //if (n == "digit") v = new DigitalZifferVisual(dynamic_cast<DigitalZiffer*>(o), this);
    //if (n == "digit2") v = new DigitalZiffer2Visual(dynamic_cast<DigitalZiffer2*>(o), this);
    //if (n == "letter") v = new LetterVisual(dynamic_cast<LetterComponent*>(o), this);
    //console.warn('Using Default Visual for:', n)
    //if (typeof o['children'] !== 'undefined') return new ContainerVisual(o, this);
    if (o instanceof DummyComponent) return new DummyVisual(o, this);
    if (o instanceof Container) return new ContainerVisual(o, this);
    return new ComponentVisual(o, this);
}

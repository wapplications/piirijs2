import { Component } from '../components/Component'
import {
  Input,
  Output
} from '../components/IO'
import { ContainerVisual } from "./ContainerVisual";
import { Signal } from '../Signal';
import VisualSettings from './VisualSettings';
import { COMPONENT_AREA, Point, Selectable, Size } from '../types';
import { compileColor, curvedWire, line, relativeLine, wire } from '../helpers';


type SizeChangedConnection = (w: number, h: number) => void;
type VisualsMap = Map<Component, ComponentVisual>;



export class ComponentVisual implements Selectable {

  protected parentVisual: ContainerVisual | null;
  protected component: Component;

  childVisuals: VisualsMap;

  showName: boolean = true;
  titleHeight: number = 15;
  ioHeight: number = 15;
  inWidth: number = 20;
  inExtraWidth: number = 0;
  outWidth: number = 20;
  ioSpace: number = 20;
  vSpace: number = 10;
  selected: boolean = false;
  ps_w: number; ps_h: number; abs_x: number; abs_y: number;
  min_x: number; min_y: number; // für marked visible kompaktifizierung
  min_w: number; min_h: number;

  signal_size_changed: Signal<SizeChangedConnection>;

  constructor(cir: Component, parent: ContainerVisual | null = null) {
    this.parentVisual = parent,
      this.component = cir;
    this.signal_size_changed = new Signal<SizeChangedConnection>();
    this.childVisuals = new Map<Component, ComponentVisual>();
    this.init();
  }

  init(): void {
    let size = { w: this.outWidth + this.inWidth, h: 35 }
    this.setMinSize(size); // reinfolge wichtig
    this.setSize(size);
  }

  empty() {
    this.childVisuals.forEach(cv => cv.clear())
    this.childVisuals.clear();
  }

  clear() {
    this.signal_size_changed.clear();
    this.childVisuals.forEach(cv => cv.clear())
    this.childVisuals.clear();
  }

  select(): void {
    this.selected = true;
  }

  deselect(): void {
    this.selected = false;
  }

  isSelected(): boolean {
    return this.selected;
  }


  paintSelf(cr: CanvasRenderingContext2D, x: number, y: number, children: boolean): void {

    cr.save();


    // Hintergrund
    // rahmen
    cr.beginPath();
    cr.roundRect(x + VisualSettings.STRICH, y, this.getSizeW() - 2 * VisualSettings.STRICH, this.getSizeH(), VisualSettings.CIR_BORDER_RADIUS);
    cr.fillStyle = compileColor(VisualSettings.CIR_BACK_COL);
    cr.lineWidth = 1.0;
    cr.fill();
    if (this.selected) cr.strokeStyle = compileColor(VisualSettings.CIR_FRAME_SEL_COL);
    else {
      cr.strokeStyle = compileColor(VisualSettings.CIR_FRAME_COL);
    }
    if (this.getComponent().getCirName() === 'embedded') {
      cr.setLineDash([2,2]);
    } else {
      cr.setLineDash([]);
    }
    cr.stroke();
    cr.setLineDash([]);


    // titel-hintergrund
    cr.beginPath();
    cr.roundRect(x + VisualSettings.STRICH + 1, y + 1, this.getSizeW() - 2 - 2 * VisualSettings.STRICH, this.titleHeight - 2, [VisualSettings.CIR_BORDER_RADIUS, VisualSettings.CIR_BORDER_RADIUS, 0, 0]);
    if (this.getComponent().staysVisible) {
      cr.fillStyle = compileColor(VisualSettings.STAY_VISIBLE_COL);
    } else {
      cr.fillStyle = compileColor(VisualSettings.CIR_TITLE_BACK_COL);
    }
    cr.fill();


    // io bereiche
    cr.beginPath();
    if (this.inWidth > 0) {
      cr.roundRect(x + 1 + VisualSettings.STRICH, y + this.titleHeight, this.inWidth - 1 - VisualSettings.STRICH, this.getSizeH() - this.titleHeight - 1, [0, 0, 0, VisualSettings.CIR_BORDER_RADIUS]);
    }
    if (this.outWidth > 0) {
      cr.roundRect(x + this.getSizeW() - this.outWidth, y + this.titleHeight, this.outWidth - 1 - VisualSettings.STRICH, this.getSizeH() - this.titleHeight - 1, [0, 0, VisualSettings.CIR_BORDER_RADIUS, 0]);
    }
    cr.fillStyle = compileColor(VisualSettings.CIR_IO_BACK);
    cr.fill();

    // Output strichelchen
    let offs: number = 0;
    cr.lineWidth = 1;
    for (let o of this.getComponent().getOutputs()) {
      let pc = o.getPinCount();
      for (let pi = 0; pi < pc; pi++) {
        cr.beginPath();
        if (o.isSelected()) cr.strokeStyle = compileColor(VisualSettings.WIRE_SEL_COL);
        else if (o.getValue() & (1 << pi)) cr.strokeStyle = compileColor(VisualSettings.HOT_COL);
        else cr.strokeStyle = compileColor(VisualSettings.WIRE_COL);
        relativeLine(cr, x + this.getSizeW() - VisualSettings.STRICH, y + (this.ioHeight / 2) + this.titleHeight + offs - pc / 2 + pi, VisualSettings.STRICH, 0);
        cr.stroke();
      }
      offs += this.ioHeight;
    }


    // Input strichelchen
    offs = 0;
    for (let i of this.getComponent().getInputs()) {
      let pc = i.getPinCount();
      for (let pi = 0; pi < pc; pi++) {
        cr.beginPath();
        if (i.isSelected()) cr.strokeStyle = compileColor(VisualSettings.WIRE_SEL_COL);
        else if ((i.getValue() & (1 << pi)) > 0) cr.strokeStyle = compileColor(VisualSettings.HOT_COL);
        else cr.strokeStyle = compileColor(VisualSettings.WIRE_COL);
        relativeLine(cr, x + VisualSettings.STRICH, y + (this.ioHeight / 2) + this.titleHeight + offs - pc / 2 + pi, -VisualSettings.STRICH, 0);
        cr.stroke();
      }
      offs += this.ioHeight;
    }




    // Titel
    if (this.showName) {
      let name = this.getComponent().getName();
      cr.font = VisualSettings.TITLE_FONT;
      cr.textBaseline = 'middle';
      cr.textAlign = 'center';
      cr.fillStyle = compileColor(VisualSettings.CIR_TITLE_COL);
      //cr.fillText(name, x + 2+VisualSettings.STRICH, y+this.titleHeight/2 )
      cr.fillText(name, x + this.getSizeW() / 2, y + this.titleHeight / 2)
    }


    cr.restore();


    this.paintIOText(cr, x, y);

  }

  paintWire(cr: CanvasRenderingContext2D, px, py, px2, py2, pc, selected, value, sn1: boolean, sn2: boolean) {
    for (let pi = 0; pi < pc; pi++) {
      cr.beginPath();
      wire(cr, px, py - pc / 2 + pi, px2, py2 - pc / 2 + pi, sn1, sn2);
      if (selected) cr.strokeStyle = compileColor(VisualSettings.WIRE_SEL_COL);
      else if (value & (1 << pi)) cr.strokeStyle = compileColor(VisualSettings.HOT_COL);
      else cr.strokeStyle = compileColor(VisualSettings.WIRE_COL);
      cr.stroke();
    }
  }

  paintAll(cr: CanvasRenderingContext2D, x: number, y: number, children: boolean): void {
    this.paintSelf(cr, x, y, false);
    if (!children) {
      return;
    }

    // Linien von den inputs aus
    cr.save();
    cr.lineWidth = 1;
    for (let i of this.getComponent().getInputs()) {
      let pos = i.ps_getFromPos();
      let px = x + pos.x; // wir verschieben das mal in updateIOPos + this.inWidth; // warum +inWidth?
      let py = y + pos.y;
      let pc = i.getPinCount();

      for (let io of i.getForwardings()) {
        let evis: ComponentVisual | undefined;
        if (io.getOwner() === this.getComponent()) {
          evis = this;
        } else {
          evis = this.childVisuals.get(io.getOwner());
          if (typeof evis === 'undefined') return; // TODO evtl continue ?
        }
        let abspos2 = evis.getAbsPos();
        let pos2 = io.ps_getToPos();
        let px2 = abspos2.x + pos2.x;
        let py2 = abspos2.y + pos2.y;

        if (io instanceof Output) {
          px2 += evis.getSize().w;
        }

        let startNode = false; // da wir hier von den inputs der komonente starten
        let endNode = io.getOwner().getCirName() === 'node';
        this.paintWire(cr, px, py, px2, py2, pc, io.isSelected() || i.isSelected(), i.getValue(), startNode, endNode);

        // der deaktivierte rest ist ganz unten in der Datei

      } // forwardings
    } // inputs
    cr.restore();
  }

  paintIOText(cr: CanvasRenderingContext2D, x: number, y: number): void {

    let px: number;
    let py: number;

    cr.save();

    cr.font = VisualSettings.IO_FONT;
    cr.textBaseline = 'middle';

    // Output text
    cr.textAlign = 'right';
    px = x + this.getSizeW() - 3 - VisualSettings.STRICH;
    py = y + this.titleHeight + this.ioHeight / 2;
    for (let o of this.getComponent().getOutputs()) {
      let text = o.getNameEx();
      if (o.isSelected()) cr.fillStyle = compileColor(VisualSettings.WIRE_SEL_COL);
      else if (o.getValue()) cr.fillStyle = compileColor(VisualSettings.HOT_COL);
      else cr.fillStyle = compileColor(VisualSettings.WIRE_COL);
      cr.fillText(text, px, py);
      py += this.ioHeight;
    }

    // Input text
    cr.textAlign = 'left';
    px = x + VisualSettings.STRICH + 3;
    py = y + this.titleHeight + this.ioHeight / 2;
    for (let i of this.getComponent().getInputs()) {
      let text = i.getNameEx();
      if (i.isSelected()) cr.fillStyle = compileColor(VisualSettings.WIRE_SEL_COL);
      else if (i.getValue()) cr.fillStyle = compileColor(VisualSettings.HOT_COL);
      else cr.fillStyle = compileColor(VisualSettings.WIRE_COL);
      cr.fillText(text, px + this.inExtraWidth, py);
      py += this.ioHeight;
    }

    cr.restore();

  }

  getComponent(): Component {
    return this.component;
  }

  setSize(size: Size): void {
    size.w = Math.max(size.w, this.min_w);
    size.h = Math.max(size.h, this.min_h);
    let ch: boolean = false;
    if ((this.ps_w != size.w) || (this.ps_h != size.h)) ch = true;
    this.ps_w = size.w;
    this.ps_h = size.h;
    if (ch) this.signal_size_changed.emit(this.ps_w, this.ps_h);
  }

  setSizeW(w: number): void {
    this.setSize({ w: w, h: this.ps_h });
  }

  setSizeH(h: number): void {
    this.setSize({ w: this.ps_w, h: h });
  }

  setAbsPos(x: number, y: number): void {
    this.abs_x = x;
    this.abs_y = y;
  }

  getAbsPosX(): number {
    return this.abs_x;
  }

  getAbsPosY(): number {
    return this.abs_y;
  }

  getAbsPos(): Point {
    return { x: this.abs_x, y: this.abs_y };
  }


  getSize(): Size {
    return { w: this.ps_w, h: this.ps_h };
  }

  getSizeW(): number {
    return this.ps_w;
  }

  getSizeH(): number {
    return this.ps_h;
  }


  setPos(pos: Point) {
    if (pos.x < 0) pos.x = 0;
    if (pos.y < 0) pos.y = 0;
    this.getComponent().setPos(pos);
  }

  getPos(): Point {
    return this.getComponent().getPos();
  }

  getPosX(): number {
    return this.getPos().x;
  }

  getPosY(): number {
    return this.getPos().y;
  }


  updateSize(cr: CanvasRenderingContext2D, x: number = 0, y: number = 0, children: boolean = true): void {
    let size: Size = { w: 0, h: 0 };
    this.setAbsPos(x, y);
    let iw = 0;
    let ow = 0;
    let textSize: TextMetrics;
    cr.font = VisualSettings.IO_FONT;
    for (let i of this.getComponent().getInputs()) {
      textSize = cr.measureText(i.getNameEx())
      iw = Math.max(iw, textSize.width)
    }
    for (let o of this.getComponent().getOutputs()) {
      textSize = cr.measureText(o.getNameEx())
      ow = Math.max(ow, textSize.width)
    }

    this.inWidth = iw + 6 + this.inExtraWidth + VisualSettings.STRICH;
    this.outWidth = ow + 6 + VisualSettings.STRICH;

    let innerSize = this.innerSize(cr, x, y, children);

    let ioTotalHeight = Math.max(this.getComponent().getInputs().length, this.getComponent().getOutputs().length) * this.ioHeight;

    size.w = innerSize.w + this.inWidth + this.outWidth;
    size.h = this.titleHeight + Math.max(innerSize.h, ioTotalHeight);

    cr.font = VisualSettings.TITLE_FONT;
    textSize = cr.measureText(this.getComponent().getName());
    size.w = Math.max(size.w, textSize.width + 2 * VisualSettings.STRICH + 4)

    this.setSize(size);
    this.updateIOPos();
  }


  onInnerClick(pos: Point) {
    // empty
  }


  innerSize(cr: CanvasRenderingContext2D, x: number = 0, y: number = 0, children: boolean = true): Size {
    let x2 = 0, y2 = 0;
    let size: Size = { w: 0, h: 0 };
    if (this.childVisuals.size > 0) {
      let ChildOffsetX = x + this.inWidth;
      let ChildOffsetY = y + this.titleHeight;;
      let first = true;
      this.min_x = 0;
      this.min_y = 0;

      this.childVisuals.forEach((value: ComponentVisual, key: Component) => {
        let pos: Point = value.getPos();
        let s: Size = value.getSize();
        let circuit = value.getComponent();
        if (children || circuit.staysVisible) {
          value.updateSize(cr, ChildOffsetX + pos.x, ChildOffsetY + pos.y, false);
          s = value.getSize(); // neu holen
          s.h += 5;
        }
        // maximale Ausdehnung nach unten rechts
        if ((pos.x + s.w) > x2) {
          x2 = pos.x + s.w;
        }
        if ((pos.y + s.h) > y2) {
          y2 = pos.y + s.h;
        }

        // minimale position
        if (!children && circuit.staysVisible) {
          if ((pos.x < this.min_x) || first) {
            this.min_x = pos.x;
          }
          if ((pos.y < this.min_y) || first) {
            this.min_y = pos.y;
          }
          first = false;
        }
      });

      size.w = x2 - this.min_x;
      size.h = y2 - this.min_y;
      if (children) {
        size.w += this.ioSpace;
        size.h += this.vSpace;
      }
    }
    return size;
  }

  setMinSize(size: Size) {
    this.min_w = size.w;
    this.min_h = size.h;
  }

  updateIOPos(): void {
    let offs = 0;
    for (let o of this.getComponent().getOutputs()) {
      o.ps_setToPos({ x: - this.outWidth, y: (this.ioHeight / 2) + this.titleHeight + offs });
      o.ps_setFromPos({ x: 0, y: (this.ioHeight / 2) + this.titleHeight + offs });
      offs += this.ioHeight;
    }
    offs = 0;
    // + this.inExtraWidth
    for (let i of this.getComponent().getInputs()) {
      i.ps_setToPos({ x: 0, y: (this.ioHeight / 2) + this.titleHeight + offs });
      i.ps_setFromPos({ x: 0 + this.inWidth /* alternativ zu paint all */, y: (this.ioHeight / 2) + this.titleHeight + offs });
      offs += this.ioHeight;
    }
  }

  /**
   * x,y: Zur Componente links oben relative koordinaten
   */

  areaOnComponent(p: Point): COMPONENT_AREA {
    let x = p.x;
    let y = p.y; // TODO
    if ((x < 0) || (y < 0) || (x > this.getSizeW()) || (y > this.getSizeH())) return COMPONENT_AREA.NONE;
    if (y <= (this.titleHeight)) return COMPONENT_AREA.TITLE;
    if ((x < (this.getSizeW() - this.outWidth)) && (x > (this.inWidth))) {
      return COMPONENT_AREA.INNER;
    }
    let r: COMPONENT_AREA = COMPONENT_AREA.NONE;
    // macht das sinn? in und out gleichzeitig?? Scheint es wohl in spezialfällen zu geben evtl bei node?
    if (x >= (this.getSizeW() - this.outWidth)) r |= COMPONENT_AREA.OUT;
    if (x <= (this.inWidth)) r |= COMPONENT_AREA.IN;
    return r;
  }


  /**
   * Berechnet zum angegebenen Bereich rel. koordinaten
   * x,y: zur komponente rel koord.
   */
  componentToArea(area: COMPONENT_AREA, p: Point): Point {
    let ox = 0, oy = 0;
    switch (area) {
      case COMPONENT_AREA.INNER:
        ox = this.inWidth;
        oy = this.titleHeight;
        break;
      case COMPONENT_AREA.TITLE:
        break;
      case COMPONENT_AREA.IN:
      case (COMPONENT_AREA.IN | COMPONENT_AREA.OUT): // sondefall wenn i und o zusammenfallen
        oy = this.titleHeight;
        break;
      case COMPONENT_AREA.OUT:
        oy = this.titleHeight;
        ox = this.getSizeW() - this.outWidth;
        break;
    }
    return { x: p.x - ox, y: p.y - oy };
  }

  /**
   * x,y: rel. zum in-Bereich
   */
  getInputFromPoint(p: Point): Input | null {
    let index = Math.floor((p.y) / this.ioHeight);
    let i = this.getComponent().getInputs()[index];
    if (typeof i === 'undefined') return null;
    return i;
  }

  /**
   * x,y: rel. zum in-Bereich
   */
  getOutputFromPoint(p: Point): Output | null {
    let index = Math.floor((p.y) / this.ioHeight);
    let o = this.getComponent().getOutputs()[index];
    if (typeof o === 'undefined') return null;
    return o;
  }

  /**
   * Rechnet parent-koordinaten in koordinaten relativ zur Componente links oben
   * x,y: screen koord
   */
  parentToComponent(p: Point): Point {
    return { x: p.x - this.getPosX(), y: p.y - this.getPosY() };
  }

}



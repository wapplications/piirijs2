import { compileColor } from "../helpers";
import {ComponentVisual} from "./ComponentVisual";
import VisualSettings from './VisualSettings';



export class NodeVisual extends ComponentVisual {
 
  init(): void {
    super.init();
    this.titleHeight = 5;
    this.ioHeight = 4;
    this.inWidth = 4;
    this.outWidth = 4;
    this.ioSpace = 0;
    let noParent = this.getComponent().getParent() === null;
    if (noParent)this.setMinSize({w: 20, h: 20})
    else this.setMinSize({w: 0, h: 0})
    // wird in updateSize ja irgendwie alles neu gesetzt
  }


  alpha(a: number[], alpha: number): number[] {
    let c = [...a];
    c.push(alpha)
    return c;
  }

  paintSelf(cr: CanvasRenderingContext2D, x: number, y: number, children: boolean): void {
    let size = this.getSize();
    cr.save();

    let noParent = this.getComponent().getParent() === null;

    cr.beginPath();
    cr.arc(x+size.w/2, y+size.h/2,   size.h/2,    0, Math.PI  )
    //if (this.getNode().pin.getValue() > 0) {
    //  cr.fillStyle = compileColor(this.alpha(VisualSettings.HOT_COL, 0.5));
    //} else {
      cr.fillStyle = "rgba(100,100,100, 0.3)";
    //}
    cr.fill();

    
    cr.beginPath();
    cr.arc(x+size.w/2, y+size.h/2, size.h/2, 0, Math.PI*2)
    cr.moveTo(x+size.w/2, y+size.h);
    cr.lineTo(x+size.w/2, y+size.h/2);
    if (noParent) {
      // für vorschau
      cr.strokeStyle = "rgba(0,0,0, 1)";
    } else {
      if (this.selected) cr.strokeStyle = compileColor(VisualSettings.CIR_FRAME_SEL_COL);
      else cr.strokeStyle = "rgba(100,100,100, 0.3)";
    }
    cr.lineWidth = 0.7;
    cr.stroke();


    if (this.selected || noParent) {
      cr.font = VisualSettings.TITLE_FONT;
      cr.textAlign = "center"
      if (noParent) cr.fillStyle = 'black';
      else cr.fillStyle = compileColor(VisualSettings.CIR_TITLE_SEL_COL);
      cr.fillText(
        this.getComponent().getName(),
        x + size.w/2,
        y - 2
      );
    }
  
    if (this.selected) {
      cr.font = VisualSettings.IO_FONT;
      cr.textAlign = "center";
      cr.textBaseline = "top"
      cr.fillStyle = compileColor(VisualSettings.CIR_TITLE_SEL_COL);
      cr.fillText(""+this.getComponent().getInput('i').getPinCount(), x+size.w/2, y+size.h+1);
    }

    cr.restore();
  }
  
  updateSize(cr: CanvasRenderingContext2D, x: number = 0, y: number = 0, children: boolean = true): void {
    this.setAbsPos(x,y);
    this.inWidth  = 6;
    this.titleHeight = 6;
    this.outWidth = this.inWidth;
    this.ioHeight = this.inWidth;
    this.setSize({w: this.inWidth*2, h: this.ioHeight + this.titleHeight});
    let size = this.getSize();
    let pin = this.getComponent().getInput('i');
    let pout = this.getComponent().getOutput('o');
    try {
      pin.ps_setToPos(   {x:  size.w/2,   y: this.ioHeight/2 + this.titleHeight});
      pin.ps_setFromPos( {x:  size.w/2,   y: this.ioHeight/2 + this.titleHeight});
      pout.ps_setFromPos({x: -size.w/2,   y: this.ioHeight/2 + this.titleHeight});
      pout.ps_setToPos(  {x: -size.w/2,   y: this.ioHeight/2 + this.titleHeight});
    } catch (e) {
      console.error(e, this.getComponent());
    }
  }
  
}

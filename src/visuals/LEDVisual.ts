import { Input } from '../components/IO';
//import LEDComponent from "../components/LEDComponent";
import { compileColor, drawLED } from '../helpers';
import { Point, Size } from '../types';
import {ComponentVisual} from "./ComponentVisual";
import VisualSettings from './VisualSettings'



export class LEDVisual extends ComponentVisual {

  static color_palette = {
    blue: "rgb(40, 60, 255)",
    yellow: "rgb(220, 200, 55)",
    red: "rgb(220, 40, 55)",
    green: "rgb(40, 200, 55)",
  }


  init(): void {
    super.init();
    this.inWidth = 5;
    this.setMinSize({w:0, h:0})
  }


  getCurrentColor(): string {
    return LEDVisual.color_palette[this.getComponent()['color']] ?? "#f0f0f0";
  }

  innerSize(cr: CanvasRenderingContext2D, x: number, y: number, children: boolean): Size {
    this.outWidth = 0;
    this.inWidth = 13;
    this.titleHeight = 0;
    return { w: 35, h: 35 }
  }

  private getInput(): Input {
    let i = this.getComponent().getInput("i");
    if (i === null) throw "LED has no input";
    return i;
  }

  updateIOPos() {
    this.getInput().ps_setFromPos({ x: 0, y: 17 });
    this.getInput().ps_setToPos({ x: 0, y: 17 });
  }

  paintSelf(cr: CanvasRenderingContext2D, x: number, y: number, children: boolean): void {
    let component = this.getComponent();
    let size = this.getSize();

    cr.save();

    if (this.selected) {
      cr.beginPath()
      cr.roundRect(x, y, size.w, size.h, VisualSettings.CIR_BORDER_RADIUS);
      cr.lineWidth = 1.0;
      cr.strokeStyle = compileColor(VisualSettings.CIR_FRAME_SEL_COL);
      cr.stroke();
    }

    let input = this.getInput();
    let inputPos = input.ps_getToPos();
    let b = input.getBooleanValue();
    let center = {
      x: x + (size.w + this.inWidth) / 2,
      y: y + size.h / 2
    }
    let radius = size.h / 2.1;


    // input line
    cr.beginPath();
    cr.moveTo(inputPos.x + x, inputPos.y + y-1)
    cr.lineTo(center.x, inputPos.y + y);
    cr.lineWidth = 1
    if (input.isSelected()) {
      cr.strokeStyle = compileColor(VisualSettings.WIRE_SEL_COL);
    } else if (b) {
      cr.strokeStyle = compileColor(VisualSettings.HOT_COL);
    } else {
      cr.strokeStyle = compileColor(VisualSettings.WIRE_COL);
    }
    cr.stroke();
    
    drawLED(cr, center, radius, b, this.getCurrentColor());

    if (component.staysVisible) {
      cr.lineWidth = 1.5;
      cr.strokeStyle = compileColor(VisualSettings.STAY_VISIBLE_COL);
      cr.beginPath()
      cr.arc(center.x, center.y, radius, 0,2*Math.PI);
      cr.stroke();
    }

    cr.restore();
  }

  // x,y: rel. zum in-Bereich
  getInputFromPoint(p: Point): Input | null {
    let i = this.getInput();
    let pos = i.ps_getToPos();
    let xx = p.x - pos.x;
    let yy = pos.y - this.titleHeight - p.y;
    let d = yy*yy;
    if ((xx <= this.inWidth) /*&& (d <= 15)*/) return i;
    return null;
  }
}

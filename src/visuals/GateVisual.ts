import VisualSettings from './VisualSettings';
import {ComponentVisual} from "./ComponentVisual";
import { Size } from '../types';
import { compileColor } from '../helpers';




class GateVisualBasic extends ComponentVisual {
}


class GateVisualIEC6061712 extends GateVisualBasic {
  innerSize(cr: CanvasRenderingContext2D, x: number, y: number, children: boolean): Size {
    let gateSymbol = GateVisualIEC6061712.get_gate_symbol(this.getComponent().getCirName())
    cr.save();
    cr.font = VisualSettings.GATE_FONT;
    cr.textBaseline = "middle";
    let textMetrics = cr.measureText(gateSymbol);
    cr.restore();
    return { w: textMetrics.actualBoundingBoxLeft + textMetrics.actualBoundingBoxRight + 4, h: textMetrics.actualBoundingBoxAscent + textMetrics.actualBoundingBoxDescent + 4 }
  }

  paintSelf(cr: CanvasRenderingContext2D, x: number, y: number, children: boolean): void {
    super.paintSelf(cr, x, y, false);
    let type = this.getComponent().getCirName();
    let gateSymbol = GateVisualIEC6061712.get_gate_symbol(type)
    let isNot = false;
    switch (type) {
      case "nand":
      case "nor":
      case "not":
        isNot = true;
    }
    cr.save();
    // Gate-Symbol
    cr.font = VisualSettings.GATE_FONT;
    cr.textBaseline = "middle";
    cr.textAlign = "center";
    cr.fillStyle = compileColor(VisualSettings.CIR_TITLE_COL);
    cr.fillText(
      gateSymbol,
      x + this.inWidth + (this.getSizeW() - this.inWidth - this.outWidth) / 2,
      y + this.titleHeight + (this.getSizeH() - this.titleHeight) / 2 + 2
    );
    if (isNot) {
      let out = this.getComponent().getOutput("x");
      if (!out) throw "Gate has no output"
      let pos = out.ps_getFromPos();
      cr.beginPath();
      cr.arc(
        x + this.getSizeW() + pos.x - 2,
        y + pos.y,
        3.0,
        0.0,
        2 * Math.PI
      );
      cr.fillStyle = "white";
      cr.fill();
      cr.lineWidth = 1;
      if (out.isSelected()) cr.strokeStyle = compileColor(VisualSettings.WIRE_SEL_COL);
      else if (out.getValue() > 0) cr.strokeStyle = compileColor(VisualSettings.HOT_COL);
      else cr.strokeStyle = compileColor(VisualSettings.WIRE_COL);
      cr.stroke();
    }
    cr.restore();
  }

  static gate_symbolic = {
    short: {
      font: VisualSettings.GATE_FONT,
      symbols: {
        and: "&",
        nand: "&",
        or: "≥1",
        nor: "≥1",
        xor: "=1",
        not: "1"
      } 
    },
    long: {
      font: VisualSettings.GATE_FONT2,
      symbols: {
        and: "and",
        nand: "nand",
        or: "or",
        nor: "nor",
        xor: "xor",
        not: "not"
      } 
    }
  };

  static get_gate_symbol(type: string): string {
    let symbolic = 'short';
    return GateVisualIEC6061712.gate_symbolic[symbolic].symbols[type];
  }
}


class GateVisualSwitcher extends GateVisualBasic {
  innerSize(cr: CanvasRenderingContext2D, x: number, y: number, children: boolean): Size {
    let size = super.innerSize(cr, x, y, children);
    let type = this.getComponent().getCirName();
    switch (type) {
      case 'nand':
        break;
      case 'and':
        return { w: Math.max(size.w,20+30), h: size.h };
      case 'nor':
        break;
      case 'or':
        return { w: Math.max(size.w,30), h: size.h };
      case 'not':
        return { w: Math.max(size.w,30), h: Math.max(size.h, 25) };
      case 'xor':
        return { w: Math.max(size.w,55), h: size.h};
    }
    return { w: 100, h: 100 };
  }
  paintSelf(cr: CanvasRenderingContext2D, x: number, y: number, children: boolean): void {
    super.paintSelf(cr, x, y, false);
    cr.save()
    let type = this.getComponent().getCirName();
    switch (type) {
      case 'nand':
        break;
      case 'and':
        this.paintAnd(cr, x, y, children);
        break;
      case 'nor':
        break;
      case 'or':
        this.paintOr(cr, x, y, children);
        break;
      case 'not':
        this.paintNot(cr, x, y, children);
        break;
      case 'xor':
        this.paintXor(cr, x, y, children);
        break;
    }
    cr.restore();
  }


  paintXor(cr: CanvasRenderingContext2D, x: number, y: number, children: boolean): void {
    let size = this.getSize();
    let c = this.getInputHotCount();
    let xx;
    let yy;
    let ia = this.getComponent().getInput('a').getBooleanValue()
    let ib = this.getComponent().getInput('b').getBooleanValue()
    let ox = this.getComponent().getOutput('x').getBooleanValue()


    // vom output
    cr.beginPath();
    cr.moveTo(
      x+(size.w-this.outWidth),
      y+(this.titleHeight+this.ioHeight/2)-2
    );
    cr.lineTo(
      x+(this.inWidth+size.w-this.outWidth)/2,
      y+(this.titleHeight+this.ioHeight/2)-2
    );
    cr.lineTo(
      x+(this.inWidth+size.w-this.outWidth)/2,
      y+(this.titleHeight+this.ioHeight/2)+2
    );
    cr.rect(
      x+(this.inWidth+size.w-this.outWidth)/2-1,
      y+(this.titleHeight+this.ioHeight/2)+2,
      2, 1)
    if (ox) {
      cr.strokeStyle = compileColor(VisualSettings.HOT_COL);
    } else {
      cr.strokeStyle = compileColor(VisualSettings.WIRE_COL);
    }
    cr.stroke();



    // in a
    cr.beginPath();
    cr.moveTo(
      x+(this.inWidth),
      y+(this.titleHeight+this.ioHeight/2)
    );
    cr.lineTo(
      x+(this.inWidth+10),
      y+(this.titleHeight+this.ioHeight/2)
    );
    cr.lineTo(
      x+(this.inWidth+10),
      y+(this.titleHeight+this.ioHeight)
    )
    if (ia) {
      cr.strokeStyle = compileColor(VisualSettings.HOT_COL);
    } else {
      cr.strokeStyle = compileColor(VisualSettings.WIRE_COL);
    }
    cr.stroke();

    // in b
    cr.beginPath();
    cr.moveTo(
      x+(this.inWidth),
      y+(this.titleHeight+this.ioHeight/2+this.ioHeight)
    );
    cr.lineTo(
      x+(this.inWidth+10),
      y+(this.titleHeight+this.ioHeight/2+this.ioHeight)
    );
    cr.lineTo(
      x+(this.inWidth+10),
      y+(this.titleHeight+this.ioHeight)
    )
    if (ib) {
      cr.strokeStyle = compileColor(VisualSettings.HOT_COL);
    } else {
      cr.strokeStyle = compileColor(VisualSettings.WIRE_COL);
    }
    cr.stroke();

    // in hebel
    cr.beginPath();
    cr.moveTo(
      x+(this.inWidth+10),
      y+(this.titleHeight+this.ioHeight)
    );
    cr.lineTo(
      x+(this.inWidth+size.w-this.outWidth)/2 -10 + c*10,
      y+(this.titleHeight+this.ioHeight)
    );
    cr.lineTo(
      x+(this.inWidth+size.w-this.outWidth)/2 -10 + c*10,
      y+(this.titleHeight+this.ioHeight-this.ioHeight/4)-1
    );
    cr.arc(
      x+(this.inWidth+size.w-this.outWidth)/2 -10 + c*10,
      y+(this.titleHeight+this.ioHeight-this.ioHeight/4)-1,
      2, 0, Math.PI*2)
    if (ia || ib) {
      cr.strokeStyle = compileColor(VisualSettings.HOT_COL);
    } else {
      cr.strokeStyle = compileColor(VisualSettings.WIRE_COL);
    }
    cr.stroke();

  }

  paintNot(cr: CanvasRenderingContext2D, x: number, y: number, children: boolean): void {
    let size = this.getSize();
    let ox;
    let oy;
    let a = this.getComponent().getInput('a').getBooleanValue()


    cr.save()
    // vom input
    ox = x+this.inWidth;
    oy = y+this.titleHeight+this.ioHeight/2
    cr.beginPath();
    cr.moveTo(ox, oy);
    cr.lineTo(x+(this.inWidth+size.w-this.outWidth)/2+(a?4:0),  y+this.titleHeight+this.ioHeight/2+5+(a?1:0));
    if (a) {
      cr.strokeStyle = compileColor(VisualSettings.HOT_COL);
    } else {
      cr.strokeStyle = compileColor(VisualSettings.WIRE_COL);
    }
    cr.setLineDash([2,2])
    cr.stroke();
    cr.restore()



    // von unten
    ox = x+(this.inWidth+size.w-this.outWidth)/2;
    oy = y+this.titleHeight
    cr.beginPath();
    cr.moveTo(ox, y+size.h);
    cr.lineTo(ox, oy+this.ioHeight/2+10);
    cr.strokeStyle = compileColor(VisualSettings.HOT_COL);
    cr.stroke();

    // zum outout
    ox = x+(this.inWidth+size.w-this.outWidth)/2;
    oy = y+this.titleHeight+this.ioHeight/2
    cr.beginPath();
    // -this.outWidth
    cr.moveTo(x+size.w-this.outWidth, oy);
    cr.lineTo(ox, oy);
    cr.lineTo(ox+(a?5:0), oy+10-(a?1:0));
    if (this.getComponent().getOutput('x').getBooleanValue()) {
      cr.strokeStyle = compileColor(VisualSettings.HOT_COL);
    } else {
      cr.strokeStyle = compileColor(VisualSettings.WIRE_COL);
    }
    cr.stroke();


  }


  paintOr(cr: CanvasRenderingContext2D, x: number, y: number, children: boolean): void {
    let size = this.getSize();
    let ox;
    let oy;
    let a = this.getComponent().getInput('a').getBooleanValue()
    let b = this.getComponent().getInput('b').getBooleanValue()

    // input a
    ox = x+this.inWidth;
    oy = y+this.titleHeight+this.ioHeight/2;
    cr.beginPath();
    cr.moveTo(ox, oy)
    cr.lineTo(ox+10+(a?10:0), oy);
    cr.arc(ox+10+(a?10:0), oy, 2, 0, Math.PI*2)
    if (a) {
      cr.strokeStyle = compileColor(VisualSettings.HOT_COL);
    } else {
      cr.strokeStyle = compileColor(VisualSettings.WIRE_COL);
    }
    cr.stroke();

    // input b
    ox = x+this.inWidth;
    oy = y+this.titleHeight+this.ioHeight/2 + this.ioHeight;
    cr.beginPath();
    cr.moveTo(ox, oy)
    cr.lineTo(ox+10+(b?10:0), oy);
    cr.arc(ox+10+(b?10:0), oy, 2, 0, Math.PI*2)
    if (b) {
      cr.strokeStyle = compileColor(VisualSettings.HOT_COL);
    } else {
      cr.strokeStyle = compileColor(VisualSettings.WIRE_COL);
    }
    cr.stroke();

    // STrich zum output
    ox = x+this.inWidth;
    oy = y+this.titleHeight+this.ioHeight/2;
    cr.beginPath();
    cr.moveTo(x+size.w-this.outWidth, oy)
    cr.lineTo(ox+20, oy);
    cr.rect(ox+20-1, oy-1, 1, 2)
    cr.moveTo(ox+20, oy);
    cr.lineTo(ox+20, oy + 1*this.ioHeight);
    cr.rect(ox+20-1, oy + 1*this.ioHeight-1, 1, 2)
    cr.moveTo(ox+20, oy + 1*this.ioHeight);
    if (this.getComponent().getOutput('x').getBooleanValue()) {
      cr.strokeStyle = compileColor(VisualSettings.HOT_COL);
    } else {
      cr.strokeStyle = compileColor(VisualSettings.WIRE_COL);
    }
    cr.stroke();
  }

  paintAnd(cr: CanvasRenderingContext2D, x: number, y: number, children: boolean): void {
    let count = 0;
    let c = this.getInputHotCount();
    let inc = this.getComponent().inputs.length

    let size = this.getSize();
    let ox = x+this.inWidth;
    let oy = y+this.titleHeight+this.ioHeight/2;

    cr.beginPath()
    cr.moveTo(ox+5, oy)
    cr.lineTo(ox+10, oy)
    cr.lineTo(ox + 10 + (c*(size.w-this.inWidth-this.outWidth-20))/inc, oy)
    cr.moveTo(ox + 10 + (c*(size.w-this.inWidth-this.outWidth-20))/inc, oy)
    cr.arc(ox + 10 + (c*(size.w-this.inWidth-this.outWidth-20))/inc, oy, 2, 0, Math.PI*2)
    if (c>0) {
      cr.strokeStyle = compileColor(VisualSettings.HOT_COL);
    } else {
      cr.strokeStyle = compileColor(VisualSettings.WIRE_COL);
    }
    cr.stroke();


    // strich am output
    cr.beginPath()
    cr.moveTo(x+size.w-this.outWidth, oy)
    cr.lineTo(x+size.w-this.outWidth-10, oy)
    cr.rect(x+size.w-this.outWidth-10-1, oy-1, 1, 2)
    if (this.getComponent().getOutput('x').getBooleanValue()) {
      cr.strokeStyle = compileColor(VisualSettings.HOT_COL);
    } else {
      cr.strokeStyle = compileColor(VisualSettings.WIRE_COL);
    }
    cr.stroke();


    cr.beginPath()
    cr.moveTo(ox, oy)
    cr.lineTo(ox+5, oy)
    if (this.getComponent().getInput('a').getBooleanValue()) {
      cr.strokeStyle = compileColor(VisualSettings.HOT_COL);
    } else {
      cr.strokeStyle = compileColor(VisualSettings.WIRE_COL);
    }
    cr.stroke();

    cr.beginPath()
    cr.moveTo(ox, oy+this.ioHeight)
    cr.lineTo(ox+5, oy+this.ioHeight)
    cr.lineTo(ox+5, oy)
    if (this.getComponent().getInput('b').getBooleanValue()) {
      cr.strokeStyle = compileColor(VisualSettings.HOT_COL);
    } else {
      cr.strokeStyle = compileColor(VisualSettings.WIRE_COL);
    }
    cr.stroke();

  }

  getInputHotCount(): number {
    let c = 0;
    for (let inp of this.getComponent().inputs) {
      if (inp.getBooleanValue()) c++;
    }
    return c;
  }
}






export const GateVisual = GateVisualIEC6061712;
//export const GateVisual = GateVisualSwitcher;

//import VisualSettings from './VisualSettings';
import {ComponentVisual} from "./ComponentVisual";
import { Point, Size } from '../types';
//import ClockComponent from "../components/ClockComponent";
import { drawSwitch, getViewPortSize } from '../helpers';




export class ClockVisual extends ComponentVisual {

  innerSize(cr: CanvasRenderingContext2D, x: number, y: number, children: boolean): Size {
    return {w: 42, h: 22}; 
  }


  paintSelf(cr: CanvasRenderingContext2D, x: number, y: number, children: boolean): void {
    super.paintSelf(cr, x, y, false);
    drawSwitch(cr, {x: x+this.inWidth+1, y: y+this.titleHeight+1}, 20, this.getComponent()['running']);
  }

  onInnerClick(pos: Point) {
    let clock = this.getComponent();
    clock.propProxies.running.setValue(!clock['running'])
  }


}



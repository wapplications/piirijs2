import VisualSettings from './VisualSettings';
import {ComponentVisual} from "./ComponentVisual";
import { Size } from '../types';
import { compileColor } from '../helpers';




export class ValueVisual extends ComponentVisual {

  innerSize(cr: CanvasRenderingContext2D, x: number, y: number, children: boolean): Size {
    let gateSymbol = this.getSymbol()
    cr.save();
    cr.font = VisualSettings.GATE_FONT;
    cr.textBaseline = "middle";
    let textMetrics = cr.measureText(gateSymbol);
    cr.restore();
    return { w: textMetrics.actualBoundingBoxLeft + textMetrics.actualBoundingBoxRight + 4, h: textMetrics.actualBoundingBoxAscent + textMetrics.actualBoundingBoxDescent + 4 }
  }

  private getSymbol() {
    return ""+this.getComponent()['value']
  }

  paintSelf(cr: CanvasRenderingContext2D, x: number, y: number, children: boolean): void {
    super.paintSelf(cr, x, y, false);
    let gateSymbol = this.getSymbol();
    cr.save();
    cr.font = VisualSettings.GATE_FONT;
    cr.textBaseline = "middle";
    cr.textAlign = "center";
    cr.fillStyle = compileColor(VisualSettings.CIR_TITLE_COL);
    cr.fillText(
      gateSymbol,
      x + this.inWidth + (this.getSizeW() - this.inWidth - this.outWidth) / 2,
      y + this.titleHeight + (this.getSizeH() - this.titleHeight) / 2 + 2
    );
    cr.restore();
  }

}



import VisualSettings from './VisualSettings';
import {ContainerVisual} from "./ContainerVisual";
import { compileColor, relativeLine } from '../helpers';




export class ToplevelCircuitVisual extends ContainerVisual {

    init(): void {
        super.init();
        this.inExtraWidth = 14;
    }

    paintSelf(cr: CanvasRenderingContext2D, x: number, y: number, children: boolean): void {
        cr.save();
    
        let offs: number = 0;
        cr.lineWidth = 1.0;
        // Output strichelchen
        for (let o of this.getComponent().getOutputs()) {
            let pc = o.getPinCount();
            for (let pi=0; pi<pc; pi++) {
                cr.beginPath();
                if (o.isSelected()) cr.strokeStyle = compileColor(VisualSettings.WIRE_SEL_COL);
                else if (o.getValue() & (1<<pi)) cr.strokeStyle = compileColor(VisualSettings.HOT_COL);
                else cr.strokeStyle = compileColor(VisualSettings.WIRE_COL);

                relativeLine(cr, x + this.getSizeW() - VisualSettings.STRICH,   y + (this.ioHeight/2) + this.titleHeight + offs -pc/2+pi,     VisualSettings.STRICH, 0);
                relativeLine(cr, x + this.getSizeW()-this.outWidth, y + (this.ioHeight/2) + this.titleHeight + offs  -pc/2+pi,   -2, 0);
                
                cr.stroke();
            }
    
            offs += this.ioHeight;
        }
    
        // Input strichelchen
        offs = 0;
        //int c =0;
        for (let i of this.getComponent().getInputs()) {
            // in-Pfeil
            cr.beginPath();
            relativeLine(cr, 
                x + 1 + VisualSettings.STRICH,
                y + this.titleHeight + offs + this.ioHeight-1,
                0, -(this.ioHeight-2));
            cr.lineTo(
                x + 1 + VisualSettings.STRICH + this.ioHeight-1,
                y + this.titleHeight + offs + this.ioHeight-1 + -(this.ioHeight-2) + (this.ioHeight-2)/2); // vereinfachen
            cr.closePath();
            if (i.getValue() > 0) cr.fillStyle = compileColor(VisualSettings.HOT_COL);
            else cr.fillStyle = compileColor(VisualSettings.WIRE_COL);
            cr.fill();
            if (i.isSelected()) {
                cr.strokeStyle = compileColor(VisualSettings.WIRE_SEL_COL);
                cr.stroke();
            }
    
            // kleines streichelchen innen
            let pc = i.getPinCount();
            for (let pi=0; pi<pc; pi++) {
                cr.beginPath();
                relativeLine(cr,
                    x + this.inWidth - 1,
                    y + (this.ioHeight/2) + this.titleHeight + offs  -pc/2+pi,
                    2, 0);
                if (i.isSelected()) cr.strokeStyle = compileColor(VisualSettings.WIRE_SEL_COL);
                else if ((i.getValue() & (1<<pi)) > 0) cr.strokeStyle = compileColor(VisualSettings.HOT_COL);
                else cr.strokeStyle = compileColor(VisualSettings.WIRE_COL);
                cr.stroke();
            }
            offs += this.ioHeight;
            //c++;
        }
        cr.restore();
    
        this.paintIOText(cr, x, y);
    } // ToplevelCircuitVisual::paintSelf
}



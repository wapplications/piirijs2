
const settings: {
  CIR_BACK_COL: number[]|string,
  CIR_FRAME_SEL_COL: number[]|string,
  CIR_FRAME_COL: number[]|string,
  CIR_TITLE_BACK_COL: number[]|string,
  STAY_VISIBLE_COL: number[]|string,
  CIR_IO_BACK: number[]|string,
  WIRE_COL: number[]|string,
  HOT_COL: number[]|string,
  CIR_TITLE_COL: number[]|string, // will be set later
  CIR_TITLE_SEL_COL: number[]|string, // will be set later
  WIRE_SEL_COL: number[]|string, // will be set later

  WIRE_WIDTH: number,
  WIRE_HOT_WIDTH: number,
  WIRE_SEL_WIDTH: number,
  STRICH: number,

  IO_FONT: string,
  GATE_FONT: string,
  GATE_FONT2: string,
  TEXT_FONT: string,
  TITLE_FONT: string,

  WIRE_MODE: string,
  CIR_BORDER_RADIUS: number,

} = {
  CIR_BACK_COL: [1.0, 1.0, 1.0],
  CIR_FRAME_SEL_COL: [0.0, 0.0, 0.8],
  CIR_FRAME_COL: [0.0, 0.0, 0.0],
  CIR_TITLE_BACK_COL: [0.8, 0.8, 0.8],
  STAY_VISIBLE_COL: [1.0, 0.8, 0.8],
  CIR_IO_BACK: [0.9, 0.9, 0.8],
  WIRE_COL: [0.1, 0.1, 0.1],
  HOT_COL: [0.8, 0.0, 0.0],
  CIR_TITLE_COL: [0,0,0], // will be set later
  CIR_TITLE_SEL_COL: [0,0,0], // will be set later
  WIRE_SEL_COL: [0,0,0], // will be set later

  WIRE_WIDTH: 1,
  WIRE_HOT_WIDTH: 2,
  WIRE_SEL_WIDTH: 1,
  STRICH: 5,

  IO_FONT: "normal 10px sans-serif",
  GATE_FONT: "bold 20px serif",
  GATE_FONT2: "bold 12px serif",
  TEXT_FONT: "normal 12px sans-serif",
  TITLE_FONT: "normal 12px sans-serif",

  WIRE_MODE: 'bezier', // 'line'
  CIR_BORDER_RADIUS: 3,
  
};








let rootStyles: CSSStyleDeclaration|null;
function getCSSVar(name:string): string {
  if (rootStyles === null) rootStyles = getComputedStyle(document.body);
  const value = rootStyles.getPropertyValue('--'+name).trim();
  return value;
}


export function refreshSettings() {
  rootStyles = null;
  settings.CIR_FRAME_COL = getCSSVar('cir-frame')
  settings.CIR_BACK_COL = getCSSVar('cir-bg');
  settings.CIR_IO_BACK = getCSSVar('cir-io-bg');
  settings.WIRE_COL = getCSSVar('cir-wire-fg');
  settings.HOT_COL = getCSSVar('cir-hotwire-fg');
  settings.WIRE_SEL_COL = getCSSVar('cir-sel-fg');
  settings.CIR_FRAME_SEL_COL = settings.WIRE_SEL_COL;
  settings.CIR_TITLE_SEL_COL = settings.WIRE_SEL_COL;
  
  settings.CIR_TITLE_BACK_COL = settings.CIR_IO_BACK;
  settings.CIR_TITLE_COL = settings.WIRE_COL;
  
  settings.STAY_VISIBLE_COL = getCSSVar('cir-stay-visible');  
}


refreshSettings();

export default settings;

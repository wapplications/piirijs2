import { ALLOWED_NAME_CHARACTERS, ALLOWED_NAME_CHARACTERS_LOWER, dispatchRefreshCurrentTab, testName } from "./helpers";
import { NumberPrompt, StringPrompt, TextfieldPrompt } from "./ui/Prompt";


export class PropValidatorException<T> {
  prop: PropProxy<T>;
  message: string;

  constructor(p: PropProxy<T>, msg: string = "[no message]") {
    this.prop = p;
    this.message = msg;
  }

  toString() {
    return 'Fehler beim Verarbeiten der Eigenschaft `' + this.prop.name + '`: '+this.message;
  }
}

export type Props = {
  [key: string]: any
}

export interface HandlesPropsChanges {
  onPropsChanged(oldValues: {[key: string]: any}): void;
}



export type PropProxies= {
  [key: string]: PropProxy<any>
}

export type ValidatorResult = string | true;
export type PropValidator<T> = (v: T) => ValidatorResult;

export function NameValidatorFactory(allowUpperCase: boolean): PropValidator<string> {
  return (v: string) => {
    if (v === "") return "Der Name ist leer.";
    let result = testName(v, allowUpperCase);
    if (result === true) return true;
    return "Der Name enhält unerlaubte Zeichen. Erlaubt sind: " + (allowUpperCase ? ALLOWED_NAME_CHARACTERS : ALLOWED_NAME_CHARACTERS_LOWER);
  }
}

export function AndValidator<T>(v1: PropValidator<T>, v2: PropValidator<T>): PropValidator<T> {
  return (v: T) => {
    let result = v1(v);
    if (typeof result === 'string') return result;
    return v2(v);
  }
}
// autocapitalize="none"





export abstract class PropProxy<T> {
  name: string
  defaultValue: T
  title: string
  data: any
  subject: HandlesPropsChanges

  $row: HTMLElement;
  $titleCell: HTMLElement;
  $valueCell: HTMLElement;
  readOnly: boolean = false;
  validator: PropValidator<T> | null = null

  constructor(subject: HandlesPropsChanges, name: string, title: string, defaultValue: T, readOnly: boolean = false, data: any = null) {
    this.subject = subject;
    this.name = name;
    this.title = title;
    this.defaultValue = defaultValue;
    this.data = data
    this.readOnly = readOnly;
    this.setValueSilent(this.defaultValue)
    this.init();
  }

  init() {

  }

  setValidator(validator: PropValidator<T>) {
    this.validator = validator;
  }

  getValue(): T {
    return this.subject[this.name];
  }

  setValueSilent(v: T) {
    this.subject[this.name] = v;
  }

  setValue(v: T) {
    if (this.validator !== null) {
      let result = this.validator(v);
      if (result !== true) throw new PropValidatorException(this, result);
    }

    let old = this.subject[this.name];
    this.setValueSilent(v);
    if (old !== v) {
      this.subject.onPropsChanged({[this.name]: old});
      //dispatchRefreshCurrentTab(); // TODO this should not be here
      if (typeof this.$valueCell !== 'undefined') {
        this.update();
      }
    }
  }

  public render($row: HTMLElement, readOnly: boolean = false): void {
    if (readOnly) this.readOnly = true;
    this.$row = $row;
    this.createDom();
    if (!this.readOnly) {
      this.$valueCell.addEventListener('click', this.activateEdit.bind(this))
    }
    this.update();
  }

  protected abstract activateEdit(): void
  public abstract update(): void

  
  protected createDom() {
    let doc = this.$row.ownerDocument
    this.$titleCell = doc.createElement('td');
    this.$valueCell = doc.createElement('td');
    this.$titleCell.innerText = this.title;
    this.$titleCell.setAttribute('title', this.name);
    this.$row.appendChild(this.$titleCell)
    this.$row.appendChild(this.$valueCell)
  }

  updateIfRendered() {
    if (typeof this.$row === 'undefined') return;
    this.update();
  }
  // wenn die componente intern etwas am Wert ändert wird hier mitgeteilt, dass sich was geändert hat damit die prop-row im prop editor neu gerendert werden kann
  /*dispatchUpdate() {
    // this is for the app
    if (typeof this.$row === 'undefined') return;
    this.$row.dispatchEvent(new CustomEvent('piiri:props:update', {detail: this}))
  }*/
}





export class LineProp extends PropProxy<string> {

  update() {
    this.$valueCell.innerText = this.getValue();
  }

  /*activateEdit() {
    while(true) {
      let v = this.getValue();
      v = prompt("Wert?", v);
      if (v === null) return;
      try {
        this.setValue(v);
        return;
      } catch (e) {
        if (e instanceof PropValidatorException) {
          alert(e.toString());
        } else {
          throw e;
        }
      }
    }
  }*/

  async activateEdit() {
    let v = this.getValue();
    let p = new StringPrompt(document);
    p.setValidator(this.validator);
    p.title = this.title;
    p.description = "";
    p.start(v);
    v = await p.result();
    if (v === null) return;
    this.setValue(v);
  }

}




export class TextProp extends LineProp {
  async activateEdit() {
    let v = this.getValue();
    let p = new TextfieldPrompt(document);
    p.setValidator(this.validator);
    p.title = this.title;
    p.description = "";
    p.start(v);
    v = await p.result();
    if (v === null) return;
    this.setValue(v);
  }
}

abstract class NumberProp extends PropProxy<number> {
  update() {
    this.$valueCell.innerText = "" + this.getValue();
  }

  async activateEdit() {
    let v = this.getValue();
    let p = this.createPrompt();
    p.setValidator(this.validator);
    p.title = this.title;
    p.start(v);
    v = await p.result();
    if (v === null) return;
    this.setValue(v);
    // TODO binär und hex siehe unten:
  }
  /*activateEdit() {
    if (this.readOnly) return;
    let v = this.getValue();
    let str = prompt("Wert?", ""+v);
    if (str !== null) {
      let base = 10;
      if (/^b/i.test(str)) {
        str = str.substring(1);
        base = 2;
      } else if (/^x/i.test(str)) {
        str = str.substring(1);
        base = 16;
      }
      v = parseInt(str, base);
      if (isNaN(v)) return;
      this.setValue(v);
      //this.dispatchUpdate();
    }
  }*/
  abstract createPrompt(): NumberPrompt;
}

export class CountingNumberProp extends NumberProp {
  createPrompt(): NumberPrompt {
    let p = new NumberPrompt(document);
    p.inputType = 'number';
    p.description = "";
    return p;
  }
}

export class MultiBaseNumberProp extends NumberProp {
  createPrompt(): NumberPrompt {
    let p = new NumberPrompt(document);
    p.inputType = 'text';
    p.description = "binär: b10011; hexadezimal: x1A";
    return p;
  }
}





export class BooleanProp extends PropProxy<boolean> {
  update() {
    this.$valueCell.innerText = this.getValue() ? 'ja' : 'nein';
  }

  activateEdit() {
    let v = !this.getValue();
    this.setValue(v);
    //this.dispatchUpdate();
  }
}



export class CarouselProp extends PropProxy<string> {
  update() {
    this.$valueCell.innerText = this.getDisplayValue();
  }

  private hasArrayData() {
    return Array.isArray(this.data);
  }

  private getDisplayValue() {
    let key = this.getValue();
    if (this.hasArrayData()) {
      return key;
    }
    return this.data[key];
  }

  activateEdit() {
    let v = this.getValue();
    let i;
    let arr;
    if (this.hasArrayData()) {
      arr = this.data;
    } else {
      arr = Object.keys(this.data);
    }
    i = arr.indexOf(v);
    if (i < 0 || i == arr.length-1) i = 0;
    else i++;
    v = arr[i];
    this.setValue(v);
  }
}

export class SelectProp extends PropProxy<string> {

  $select: HTMLSelectElement;

  public render($row: HTMLElement): void {
    // TODO ermögliche wert undanzeige text sich zu unterscheiden data: {a: "a text", b: "b text"...}
    this.$row = $row;
    this.createDom();
    let doc = this.$row.ownerDocument
    this.$select = doc.createElement('select')
    this.$select.style.width = "100%";
    for (let o of this.data) {
      let $option = doc.createElement('option')
      $option.value = o;
      $option.innerText = o;
      this.$select.appendChild($option)
    }
    this.$valueCell.appendChild(this.$select);
    if (!this.readOnly) {
      this.$select.addEventListener('change', this.optionChanged.bind(this))
    }
    this.update();
  }

  update() {
    let v = this.getValue();
    this.$select.value = v;
  }

  optionChanged() {
    this.setValue(this.$select.value)
    //this.dispatchUpdate();
  }

  protected activateEdit() {
    // nicht benötigt, ungeschickt
  }
}

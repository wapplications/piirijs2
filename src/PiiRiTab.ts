import {Component, CircuitNotification} from "./components/Component";
import { Input, Output, IOBase } from "./components/IO";
import {NodeComponent} from "./components/NodeComponent";
import { ALLOWED_NAME_CHARACTERS_LOWER, getDomElementByIdOrThrow, handleComponentErrors, makeName, PiiriCircularException, TemporaryEvents, testName, toolbarButtonEnabledIf, wait } from "./helpers";
import { isBuiltInComponent} from "./palette";
import { PropProxies } from "./Props";
import { COMPONENT_AREA, CircuitStateChanged, ComponentFactoryError, Point, SerializedCircuit } from "./types";
import {ComponentVisual} from "./visuals/ComponentVisual";
import {ContainerVisual} from "./visuals/ContainerVisual";

import CircuitLib from './components/api';
import { Container } from "./components/Container";
import PiiriPane from "./ui/PiiriPane";

import PiiriWindow from "./PiiriWindow"

import { ToplevelCircuitVisual } from "./visuals/ToplevelCircuitVisual";
import { UiTab } from "./ui/Tabs";
import { ButtonPrompt, StringPrompt } from "./ui/Prompt";
import { PopNotification } from "./ui/PopNotification";
import AnalysisView from "./ui/AnalysisView";
import createVisual from "./visuals/factory";
import { ExportPrompt } from "./ui/ExportPrompt";


export default class PiiRiTab extends PiiriWindow {
  stepInterval = null;
  piiriPane: PiiriPane;
  analysis: AnalysisView;
  canvas: HTMLCanvasElement;
  component: Container
  visual: ContainerVisual
  ctx: CanvasRenderingContext2D;
  mainPos;

  $timerSelect: HTMLSelectElement;
  $timerCheck: HTMLInputElement;
  $timerInterval: HTMLInputElement;

  private timerTimer = null;
  private timerInput = "t";
  private timerInterval = 1000;
  private uitab: UiTab;
  private dirty = false;

  constructor(uitab: UiTab, pane: PiiriPane, container: Container, analysis: AnalysisView) {
    super(uitab.container, pane, container)

    this.uitab = uitab;
    this.stepInterval = null;
    this.analysis = analysis;

    this.analysis.setMainPiiriWindow(this);

    let doc = pane.getDomDocument();
    doc.defaultView.name = 'mainView';

    this.$timerSelect = uitab.container.querySelector('#timerSelect') as HTMLSelectElement;
    this.$timerCheck = uitab.container.querySelector('#timerCheck') as HTMLInputElement;
    this.$timerInterval = uitab.container.querySelector('#timerInterval') as HTMLInputElement;
    this.$timerInterval.value = ""+this.timerInterval;
    this.$timerCheck.checked = this.timerTimer !== null
    this.$timerSelect.value = this.timerInput;
    this.$timerSelect.addEventListener('change', ()=>{
      this.restartTimerIfNeeded()
    })
    this.$timerCheck.addEventListener('click', ()=>{
      this.restartTimerIfNeeded()
    })
    this.$timerInterval.addEventListener('input', ()=>{
      this.restartTimerIfNeeded()
    })
    

    // events from the PiiRiPane about moving and connecting stuff
    uitab.listen('piiri:state', this.onState.bind(this));

    uitab.listen('piiri:component', this.onComponent.bind(this));
    uitab.listen('piiri:command:in', this.addInViaPrompt.bind(this));
    uitab.listen('piiri:command:out', this.addOutViaPrompt.bind(this));
    uitab.listen('piiri:command:component', this.addComponentViaPrompt.bind(this));
    uitab.listen('piiri:command:node', this.addNode.bind(this));
    uitab.listen('piiri:command:disconnect', this.disconnectSelected.bind(this));
    uitab.listen('piiri:command:delete', this.deleteCommand.bind(this));
    uitab.listen('piiri:command:embed', this.embedCommand.bind(this));
    uitab.listen('piiri:command:enter-embedded', this.enterEmbeddedCommand.bind(this));
    uitab.listen('piiri:command:leave-embedded', this.leaveEmbeddedCommand.bind(this));
    uitab.listen('piiri:command:clear', this.clearCommand.bind(this));
    uitab.listen('piiri:command:clone', this.cloneCommand.bind(this));

    uitab.listen('piiri:command:table', this.openTableView.bind(this));
    uitab.listen('piiri:command:impulse', this.openImpulseView.bind(this));

    uitab.listen('piiri:command:toggle-rubber', this.toggleRubber.bind(this));

    uitab.listen('piiri:open-component', (e: CustomEvent<Component>)=>{
      let container = e.detail;
      if (!(container instanceof Container)) return;
      this.openContainerWindow(container)
    });

    uitab.listen('piiri:command:export', this.exportCommand.bind(this));
    uitab.listen('piiri:command:reload', this.reloadCommand.bind(this));

    uitab.listen('tab:activate', ()=>{
      this.selectionChanged();
    });
    uitab.listen('tab:deactivate', ()=>{
    });
    uitab.listen('tab:update', this.onRefresh.bind(this));
    uitab.listen('tab:wants-close', ()=>{
      if (this.isDirty()) {
        if (!confirm("Schaltung schließen? Ungespeichertes geht verloren.")) return;
      }
      this.unload();
      this.selectionChanged(); // piiriPane.desecect dose not fire selection changed event
      uitab.close();
    });

    uitab.listen('piiri:command:save', this.saveCommand.bind(this));


    this.canvas.addEventListener('mouseup', (e) => {
      this.canvas.focus()
    });
    
  }




  isGroupManager(): boolean {
    return this.uitab.data.app.isGroupManager();
  }

  isDemo(): boolean {
    return this.uitab.data.app.userInfo === null;
  }

  start() {
    this.rootVisual = new ToplevelCircuitVisual(this.rootComponent);
    this.visual = this.rootVisual;
    this.rootComponent.signal_inputs_changed.connect(this.inputsChanged.bind(this))
    this.rootComponent.signal_outputs_changed.connect(this.outputsChanged.bind(this))
    // events from within the circuit (all children)
    this.rootComponent.signal_circuit_notification.connect(this.onCircuitNotification.bind(this));
    this.piiriPane.setVisual(this.visual)
    this.updateUi()
  }

  unload() {
    super.unload();
    this.piiriPane.deselect();
    this.rootVisual.clear();
    this.rootComponent.clear();
    this.analysis.clear();
  }

  isDirty() {
    return this.dirty;
  }

  //private group_comp_save_warning = false;
  setDirty(d: boolean = true) {
    /*if (!this.group_comp_save_warning && d && this.rootComponent.isGroupComponent && !this.isGroupManager()) {
      this.group_comp_save_warning = true;
      alert('Achtung: Änderungen an Gruppen-Komponente können nicht gespeichert werden.');
    }*/
    this.dirty = d;
    this.updateUi();
  }


  isUsed(): boolean {
    return this.rootComponent.getCirName() !== ""
    || this.rootComponent.hasChildren() 
    || this.rootComponent.inputs.length>0
    || this.rootComponent.outputs.length>0
  }

  onCircuitNotification(n: CircuitNotification, s: string, a: any) {
    const texts = {
      [CircuitNotification.TIMEOUT_ERROR]: "TIMEOUT_ERROR",
      [CircuitNotification.UPDATE]: "UPDATE",
      [CircuitNotification.PROPS_CHANGED]: "PROPS_CHANGED",
    }
    //console.log('onCircuitNotification', texts[n])
    if (n == CircuitNotification.TIMEOUT_ERROR) {
      this.stopTimer();
      console.warn("TIMEOUT_ERROR");
      // TODO timeout bessere meldung
      ///os << "<b>" << msg << " (" << CircuitChild::timeoutDelta << ")</b>\nDie Schaltung nimmt keinen <a href=\"piiri:help:circ\">stabilen Zustand</a> an.";
      /// App::getMW().open_infobar(Gtk::MESSAGE_WARNING, os.str());
    } else if (n == CircuitNotification.UPDATE) {
      this.refresh(false, true);
    } else if (n === CircuitNotification.PROPS_CHANGED) {
      // a: {source: this, component: this.owner, oldValues}
      this.refresh(true, true);
      this.setDirty();
      this.updateUi();
    }
  }


  /*shareCommand() {
    // sollte das besser die api im backend machen?
    let deps = [...this.component.getDependencies()];
    if (deps.length === 0 || deps[0]==="") {
      return;
    }
    let backdrop = this.openOnBackdrop('shareDialog')
    let tempEvents = new TemporaryEvents(backdrop);
    let close = () => {this.closeOnBackdrop(); tempEvents.clear();}
    tempEvents.listen('backdrop:share:startup', (event: CustomEvent) => {
      let alpinInstance = event.detail;
      alpinInstance['deps'] = deps.filter((e)=>e!==this.component.getCirName());
      alpinInstance['component'] = this.component.getCirName();
    });
    tempEvents.listen('backdrop:share:cancel', (event: CustomEvent) => {
      close();
    });
    tempEvents.listen('backdrop:share:execute', async (event: CustomEvent) => {
      close();
      for (let d of deps) {
        await CircuitLib.setShared(d);
      }
      alert(deps.length + ' Komponenten wurden freigegeben')
    });
  }*/

  /*sharedCommand() {
    let backdrop = this.openOnBackdrop('sharedDialog')
    let tempEvents = new TemporaryEvents(backdrop);
    let close = () => {this.closeOnBackdrop(); tempEvents.clear();}
    tempEvents.listen('backdrop:shared:startup', async (event: CustomEvent) => {
      let alpinInstance = event.detail;
      let shared = await CircuitLib.getShared();
      alpinInstance['shared'] = shared;
    });
    tempEvents.listen('backdrop:shared:cancel', (event: CustomEvent) => {
      close();
    });
    tempEvents.listen('backdrop:shared:import', async (event: CustomEvent) => {
      close();
      let componentsToImport = event.detail;
      console.log('componentsToImport', componentsToImport)
      let names = await CircuitLib.importComponents(componentsToImport);
      for(let n of names) {
        this.addToPalette(n);
      }
    });
  }*/


  openContainerWindow(container: Container) {
    this.analysis.view(container);
  }



  async openTableView() {

    let component: Component;



    
    if (this.piiriPane.selection.length > 1) return;

    if (this.piiriPane.selection.length === 1) {
      if (!(this.piiriPane.selection[0] instanceof ComponentVisual)) return;
      let visual = this.piiriPane.selection[0] as ComponentVisual;
      component = visual.getComponent();
    } else {
      component = this.component; // current component (nicht zwingend root)
    }

    this.analysis.valueTable(component.getNameRecursive() ?? 'Diese Schaltung', component);

  }



  openImpulseView() {
    this.analysis.startImpulseDiagram();
  }



  deleteCommand() {
    this.commandSelectionDelete();
  }




  async exportCommand() {
    let p = new ExportPrompt(this.rootComponent);
    await p.show();
  }

  async reloadCommand() {
    if (this.piiriPane.selection.length === 0) return;
    // TODO wenn nichts markiert: alles neu laden
    // TODO momory leaks detecten!!
    let err = [];
    for (let n = 0; n < this.piiriPane.selection.length; n++) {
      let visual = this.piiriPane.selection[n];
      if (!(visual instanceof ContainerVisual)) continue;
      let component = visual.getContainer();
      if (component.getCirName() === 'embedded') continue;
      // 1. io merken
      let connections: {from: string, to: string}[] = [];
      let inputs = component.getInputs();
      let outputs = component.getOutputs();
      for (let i = 0; i < inputs.length; i++ ) {
        let inp = inputs[i];
        let connection = {from: '', to: component.getName() + '.' + inp.getName()};
        let fromIO = inp.getFrom();
        if (fromIO === null) continue;
        let owner = fromIO.getOwner() === this.component ? 'in' : fromIO.getOwner().getName();
        connection.from = owner + '.' + fromIO.getName();
        connections.push(connection);
      }
      for (let o = 0; o < outputs.length; o++ ) {
        let out = outputs[o];
        let forwardings = out.getForwardings();
        let fromIdentifier = component.getName() + '.' + out.getName();
        for (let f = 0; f < forwardings.length; f++) {
          let connection = {from: fromIdentifier, to: ''};
          let toIO = forwardings[f];
          if (toIO.getOwner() === component) continue; // selbst-verbindung wurde oben schon behandelt
          let owner = toIO.getOwner() === this.component ? 'out' : toIO.getOwner().getName();
          connection.to = owner + '.' + toIO.getName();
          connections.push(connection);
        }
      }
      // 2. leeren
      component.empty();
      // 3. neu laden
      // TODO try catch PiiriCircularException
      let cirData = await CircuitLib.get(component.getCirName());
      await component.loadCircuit(cirData, err)
      // 4. io wieder verbinden
      for (let c = 0; c < connections.length; c++) {
        let connection = connections[c];
        if (!this.component.connect(connection.from, connection.to)) {
          err.push(connection);
        }
      }
    }
    // 5. Warnen wenn fehler aufgetreten sind (fehlenden componenten oder nicht mögliche verbindungen)
    if (err.length>0) {
      alert('Beim Laden sind Fehler aufgetreten: fehlende Komponenten oder inkompatible Anschlüsse.');
    }
    // 6. neu zeichnen
    this.refresh(true, true);
    // 7. componente view update?

  }

  embeddedModeStack: Array<{component: Container, visual: ContainerVisual}> = [];
  isEmbeddedEditorMode() {
    return this.embeddedModeStack.length > 0;
  }

  enterEmbeddedCommand() {
    if (this.piiriPane.selection.length !== 1) return;
    let visual = this.piiriPane.selection[0];
    if (!(visual instanceof ContainerVisual)) return;

    this.piiriPane.deselect()
    this.selectionChanged();
    this.stopTimer();
    this.$timerCheck.checked = false;

    let comp = visual.getComponent() as Container;
    this.embeddedModeStack.push({
      component: this.component,
      visual: this.visual
    })
    this.component = comp;
    
    this.component.signal_inputs_changed.connect(this.inputsChanged.bind(this))
    this.component.signal_outputs_changed.connect(this.outputsChanged.bind(this))

    this.visual = new ContainerVisual(this.component);
    this.piiriPane.setVisual(this.visual)
    this.refresh(true, true);
    this.updateUi()
  }

  leaveEmbeddedCommand() {
    // TODO this.visual irgendwie auflösen / löschen freigeben clear / unload etc
    if (!this.isEmbeddedEditorMode()) return;
    
    this.piiriPane.deselect()
    this.selectionChanged();
    this.stopTimer();
    this.$timerCheck.checked = false;

    this.component.signal_inputs_changed.clear();
    this.component.signal_outputs_changed.clear();

    let stackElement = this.embeddedModeStack.pop();
    this.component = stackElement.component;
    this.visual = stackElement.visual;
    this.piiriPane.setVisual(this.visual)
    this.refresh(true, true);
    this.updateUi()
  }

  async cloneCommand() {
    let changed = false;
    let components = [];
    for (let visual of this.piiriPane.selection) {
      if (visual instanceof ComponentVisual) {
        let comp = visual.getComponent();
        components.push(comp);
      }
    }
    this.piiriPane.deselect();
    for (let comp of components) {
      let err = [];
      let cir = comp.getCirName();
      let pos = {...comp.getPos()};
      pos.x+=10; pos.y+=10;
      let props = comp.getProps();
      delete props.name; // sonst wird der name ersetzt durch das original
      let c = await this.component.addComponent(cir, "", pos, props, err);
      if (cir === 'embedded') {
        let serialized = comp.toJsonObject();
        await (c as Container).loadCircuit(serialized, err);
      }
      this.selectComponent(c, true);
      changed = true;
    }
    if (changed) this.setDirty();
    this.refresh(true, true);
    this.selectionChanged(); // doppelt gemoppelt
  }

  embedCommand() {
    let changed = false;
    for (let visual of this.piiriPane.selection) {
      if (visual instanceof ContainerVisual) {
        let comp = visual.getComponent() as Container;
        comp.embed();
        changed = true;
      }
    }
    if (changed) this.setDirty();
    this.selectionChanged();
  }

  toggleRubber() {
    // unschöne simulation eines on-off-schalters, weil ich gerade keine Lust habe das als Komponente zu portieren.
    this.piiriPane.rubberBandSelectionMode = !this.piiriPane.rubberBandSelectionMode;
    let el = this.uitab.container.querySelector('[data-command="toggle-rubber"] .toggle-rubber-line') as HTMLElement;
    el.style.display = this.piiriPane.rubberBandSelectionMode ? 'none' : '';
  }

  updateUi() {
    let embeddedMode = ()=>this.isEmbeddedEditorMode();
    let hasComponents = ()=>(this.rootComponent.getChildren().length > 0);
    let dirty = ()=>this.isDirty();
    let somethingSelected = ()=>(this.piiriPane.selection.length > 0);
    let oneSelected = ()=>(this.piiriPane.selection.length === 1);
    let iOSelected = ()=>(somethingSelected() && this.piiriPane.selection[0] instanceof IOBase);
    let componentsSelected = ()=>(somethingSelected() && this.piiriPane.selection[0] instanceof ComponentVisual);
    let selectedComponent = ()=>(this.piiriPane.selection[0] instanceof ComponentVisual ? this.piiriPane.selection[0].getComponent() as Component : null);
    let rootHasInputs = ()=>(this.rootComponent.getInputs().length > 0);

    //this.toolbarButtonEnabledIf('table', !this.isDemo());
    //this.toolbarButtonEnabledIf('impulse', !this.isDemo());
    this.toolbarButtonEnabledIf('embed',  !this.isDemo() && selectedComponent() instanceof Container && (selectedComponent().getCirName() !== 'embedded'));
    this.toolbarButtonEnabledIf('leave-embedded', embeddedMode() );
    this.toolbarButtonEnabledIf('enter-embedded', oneSelected() && componentsSelected() && (selectedComponent().getCirName() === 'embedded'));
    this.toolbarButtonEnabledIf('clear', !embeddedMode() && hasComponents());
    this.toolbarButtonEnabledIf('save', /*!embeddedMode() &&*/ dirty());
    this.toolbarButtonEnabledIf('timer', !embeddedMode() && rootHasInputs());
    this.toolbarButtonEnabledIf('node',  !this.isDemo() && iOSelected());
    this.toolbarButtonEnabledIf('disconnect', iOSelected() || componentsSelected());
    this.toolbarButtonEnabledIf('delete', somethingSelected());
    this.toolbarButtonEnabledIf('clone', componentsSelected() && !this.isDemo());

    this.updateCirNameOnUi();
  }

  toolbarButtonEnabledIf(command: string, enabled: boolean) {
    toolbarButtonEnabledIf(this.uitab.container, command, enabled);
  }


  async openComponent(cir: string | SerializedCircuit) {
    let waiter = (new PopNotification(this.getDomDocument())).text("Komponente wird geladen.").waiting();
    this.emptyCircuit();
    let data;
    if (typeof cir === 'string') {
      data = await CircuitLib.get(cir);
    } else {
      data = cir;
    }
    if (!data) {
      waiter.failed();
      return;
    }
    let componentErrors = [];
    await this.rootComponent.loadCircuit(data, componentErrors);
    handleComponentErrors(componentErrors);
    this.refresh(true, true);
    this.updateTimerControl();
    this.updateUi();
    waiter.done();
  }



  clearCommand() {
    if (!confirm('Alles aus der Schlatung entfernen?')) return;
    this.emptyCircuit();
    this.refresh(true, true);
    this.inputsChanged();
  }


  emptyCircuit() {
    this.rootComponent.setCirName('');
    this.piiriPane.deselect()
    this.selectionChanged();
    this.stopTimer();
    this.$timerCheck.checked = false;
    this.rootComponent.empty();
    this.rootVisual.empty();
    this.updateUi();  
    this.closeSubWindows();
  }

  clear() {
    this.rootComponent.setCirName('');
    this.piiriPane.deselect()
    this.stopTimer();
    this.$timerCheck.checked = false;
    this.rootComponent.clear();
    this.rootVisual.clear();
    this.inputsChanged();
    this.selectionChanged();
    this.updateUi();  
  }

  updateCirNameOnUi() {
    let t = this.rootComponent.getCirName();
    if (!t) t = '<unbenannt>'

    if (this.isEmbeddedEditorMode()) {
      // cirname:name:name
      for (let i = 1; i < this.embeddedModeStack.length; i++) {
        t += ':'+this.embeddedModeStack[i].component.getName();
      }
      t += ':'+this.component.getName();
    }
    this.uitab.setTitle(t);
  }

  async saveCommand() {
    let neu = false;
    if (this.rootComponent.isGroupComponent && !this.isGroupManager()) {

      let p = new ButtonPrompt(this.getDomDocument());
      let answer = await p.show(
        "Freigegebene Komponente als eigene Komponente speichern?",
        "Dadurch wird die ursprüngliche Komponente durch die eigene, bearbeitbare Komponente ersetzt.",
        {
          overwrite: "Komponente ersetzen",
          newname: "Unter neuem Namen speichern",
          cancel: "Abbrechen"
        },
        '');
      if (answer === 'cancel') return;
      if (answer === 'overwrite') {
        this.rootComponent.isGroupComponent = false;
        this.rootComponent.overwritesGroupComponent = true;
      };
      if (answer === 'newname') {
        this.rootComponent.setCirName('');
        this.rootComponent.isGroupComponent = false;
        this.rootComponent.overwritesGroupComponent = false;
      };
    }
    if (this.rootComponent.getCirName() === '') {
      let p = new StringPrompt(this.getDomDocument());
      p.title = "Komponente speichern";
      p.description = "Name für die neue Komponente ("+ALLOWED_NAME_CHARACTERS_LOWER+"):"
      p.setValidator((v)=>{
        if (!testName(v.toLowerCase())) return 'Name genügt nicht den Vorgaben.';
        return true;
      })
      p.start("");
      //let cirname = prompt("Name für die neue Komponente:");
      let cirname = await p.result();

      cirname = makeName(cirname)
      if (!testName(cirname)) return;
      if (isBuiltInComponent(cirname)) {
        alert("Kann nicht den Namen einer eingebauten Komponente verwenden.");
        return;
      }
      let exists = await CircuitLib.get(cirname);
      if (exists) {
        // TODO Group-Kompónents können nach bestätigung überschrieben werden
        alert("Kann nicht andere Komponente überschreiben");
        return;
      }
      this.rootComponent.setCirName(cirname);
      neu = true;
    }
    let waiter = (new PopNotification(this.getDomDocument())).text("Komponente wird gespeichert.").waiting();
    try {
      let data = this.rootComponent.toJsonObject()
      let a = await CircuitLib.store(data);
      if (!a) throw "Fehler beim Speichern";
      this.dispatchPaletteUpdate(this.rootComponent.getCirName(), neu);
      this.setDirty(false);
      this.updateUi();
      waiter.done();
    } catch (e) {
      if (typeof e === 'string') {
        alert(e);
      } else if (typeof e.toString !== 'undefined') {
        alert(e.toString());
      } else {
        console.error(e);
      }
      waiter.failed();
    }
  }

  dispatchPaletteUpdate(cir: string, isNew: boolean) {
    let event = new CustomEvent('piiri:update-palette', {detail:{cirname: cir, isNew}});
    this.piiriPane.getDomDocument().dispatchEvent(event);
  }

  dispatchPaletteRename(old: string, cirname: string) {
    let event = new CustomEvent('piiri:update-palette', {detail:{old, cirname}});
    this.piiriPane.getDomDocument().dispatchEvent(event);
  }

  private getSwitchableInput(name) {
    let input = this.rootComponent.getInput(name);
    if (!input) {
      for (let i of this.rootComponent.getInputs()) {
        if (i.getName().startsWith(name)) {
          input = i;
          break;
        }
      }
    }
    if (input && input.getPinCount()>1) input = null;
    return input;
  }
  



  /**
   * Used for DnD on PiiriPane
   * @param event 
   */
  async onComponent(event: CustomEvent<{cir: string, pos: Point|null}>) {
    let {cir, pos} = event.detail;
    let componentErrors = [];
    let c = await this.addComponent(cir, pos, componentErrors);
    handleComponentErrors(componentErrors);
    if (typeof c !== 'undefined') {
      if (pos === null) {
        // Mitte des angezeigten bereiches finden
        let w = this.piiriPane.$element.clientWidth
        let h = this.piiriPane.$element.clientHeight
        let sx = this.piiriPane.$element.scrollLeft;
        let sy = this.piiriPane.$element.scrollTop;
        //this.refresh(true, false);
        let vis = createVisual(c);
        vis.updateSize(this.piiriPane.canvas.getContext("2d"), 0,0, false);
        let cw = vis.ps_w;
        let ch = vis.ps_h;
        // keine Ahnung was das für eine verkonkste Rechnung ist - und doch nicht genau!??!
        let x = (w/this.piiriPane.zoom-cw*this.piiriPane.zoom)/3; // absichtlich nicht in der mitte
        let y = (h/this.piiriPane.zoom-ch*this.piiriPane.zoom)/3;
        x += sx/=this.piiriPane.zoom;
        y += sy/=this.piiriPane.zoom;
        c.setPos({x, y});
        this.refresh(true, false);
      }
    }
  }


  private stateToString(state: CircuitStateChanged) {
    return {
      [CircuitStateChanged.SWITCHED]: "SWITCHED",
      [CircuitStateChanged.MOVING]: "MOVING",
      [CircuitStateChanged.MOVED]: "MOVED",
      [CircuitStateChanged.SELECTING]: "SELECTING",
      [CircuitStateChanged.SELECTED]: "SELECTED",
      [CircuitStateChanged.CONNECTING]: "CONNECTING",
      [CircuitStateChanged.CONNECTED]: "CONNECTED",
    }[state];
  }

  onState(event: CustomEvent) {
    let {state} = event.detail;
    //console.log(this.stateToString(state));
    if ([CircuitStateChanged.MOVED, CircuitStateChanged.CONNECTED].includes(state)) {
      this.setDirty();
    }
    // TODO es gibt verschiedene Wege wir das gemacht wird: vereinheitlichen!
    if (state === CircuitStateChanged.SELECTED) {
      this.selectionChanged();
    }
  }




  disconnectSelected() {
    this.piiriPane.selection.forEach(e=>{
      if (e instanceof ComponentVisual) {
        e.getComponent().disconnect()
      } else {
        e.disconnect();
      }
    })
    this.setDirty();
    this.refresh(false, true);
  }






  private restartTimerIfNeeded() {
    this.stopTimer();
    if (this.isEmbeddedEditorMode()) return;
    this.readTimerSettings()
    if (this.$timerCheck.checked) {
      this.startTimer();
    }
  }

  private readTimerSettings() {
    this.timerInterval = this.$timerInterval.valueAsNumber;
    if (this.timerInterval>5000) this.timerInterval = 5000;
    if (this.timerInterval<200) this.timerInterval = 200;
    this.timerInput = this.$timerSelect.value;
  }
  
  stopTimer() {
    if (this.timerTimer) {
      clearInterval(this.timerTimer);
      this.timerTimer = null;
    }
  }
  startTimer() {
    this.stopTimer()
    this.timerTimer = setInterval(this.onTimer.bind(this), this.timerInterval);
  }
  onTimer() {
    let input = this.rootComponent.getInput(this.timerInput)
    if (!input || input.getPinCount() > 1) return;
    input.setBooleanValue(!input.getBooleanValue());
    this.refresh(false, true);
  }
  




  async addComponentViaPrompt() {
    let p = new StringPrompt(this.getDomDocument());
    p.title = "Neue Komponente einfügen";
    p.description = "Name der Komponente:"
    p.start("");
    let cir = await p.result();
    if (!cir) return;
    //let pos = {x:0, y:0};
    this.uitab.container.dispatchEvent(new CustomEvent('piiri:component', {detail: {cir: cir.toLowerCase(), pos:null}}))
    //let componentErrors = [];
    //let c = await this.addComponent(cir.toLowerCase(), pos, componentErrors);
    //handleComponentErrors(componentErrors);
  }
  



  commandSelectionDelete() {
    if (this.piiriPane.selection.length > 0) {
      if (this.piiriPane.selection[0] instanceof IOBase) {
        this.commandDeleteSelectedIO();
      } else if (this.piiriPane.selection[0] instanceof ComponentVisual) {
        this.commandDeleteSelectedComponent();
      }
    }
    this.piiriPane.deselect();
    this.selectionChanged();
    this.refresh(true, true);
  }

  private commandDeleteSelectedIO() {
    if (this.piiriPane.selection.length !== 1) return;
    let io = this.piiriPane.selection[0];
    if (!(io instanceof Input || io instanceof Output)) return;
    if (io.getOwner() === this.visual.getComponent()) {
      let editableCircuit = this.visual.getComponent() as Container;
      editableCircuit.deleteIO(io);
    } else {
      if (io instanceof Input) {
        io.disconnect(false, true);
      } else if (io instanceof Output) {
        io.disconnect(true, false);
      }
    }
  }

  private commandDeleteSelectedComponent() {
    // Am einfachsten: beim löschen alle fenster schließen:
    // aber eig. nur view windows

    let c: boolean = false;

    for (let vis of this.piiriPane.selection) {
        if (!(vis instanceof ComponentVisual)) break;
        let circuit = vis.getComponent();
        this.closeRelatedWindows(vis);
        this.visual.childVisuals.delete(circuit); // Visual aus liste entfernen.
        let editableCircuit = this.visual.getComponent() as Container;
        editableCircuit.deleteComponent(circuit);
        c = true;
    }

    if (c) {
      this.setDirty();
      //new GeneralUndoItem("Lö", this, m_undo);
        /* TODO m_undo.addItem(new GeneralUndoItem("Löschen", this, m_undo));
        changed(true);*/
    }
  }

  
  async addNode() {
    let circuit = this.visual.getContainer();
    let x = 0;
    let y = 0;

    for (let io of this.piiriPane.selection) {
      if (!(io instanceof Input || io instanceof Output)) {
        return;
      }
      let inOrMainOut = ((io.getOwner() != circuit) && (io instanceof Input))
                          || ( (io.getOwner() == circuit) && (io instanceof Output));
      // Prüfen ob überhaupt verbindungen da sind
      if (inOrMainOut) {
          if (io.getFrom() == null) return;
      } else {
          if (io.getForwardings().length == 0) return;
      }

      // Node-Komponente erzeugen
      let componentErrors = [];
      let node = (await circuit.addComponent("node","n", {x:0, y:0}, null, componentErrors)) as NodeComponent;
      node.setPinCount(io.getPinCount());

      // Eingang bzw Main ausgang hat nur eine leitung
      if (inOrMainOut) {
        let io2 = io.getFrom();
        if (io2) {
          let vis1, vis2;
          if (io2.getOwner() == circuit) vis1 = this.visual;
          else vis1 = this.visual.childVisuals.get(io2.getOwner());
          if (io.getOwner() == circuit) vis2 = this.visual;
          else vis2 = this.visual.childVisuals.get(io.getOwner());

          let ioToPos = io.ps_getToPos();
          let io2FromPos = io2.ps_getFromPos();
          let vis1AbsPos = vis1.getAbsPos();
          let vis2AbsPos = vis2.getAbsPos();

          //this.point(vis1AbsPos.x+io2FromPos.x, vis1AbsPos.y+io2FromPos.y, "blue")
          //this.point(vis2AbsPos.x+ioToPos.x, vis2AbsPos.y+ioToPos.y, "green")

          x = vis1AbsPos.x + vis2AbsPos.x;
          y = vis1AbsPos.y + vis2AbsPos.y;
          x += io2FromPos.x + ioToPos.x;
          y += io2FromPos.y + ioToPos.y;
          if (io2 instanceof Output) x += vis1.getSizeW(); // TODO warum steckt das nicht in io2From/ToPos drin?
          if (io instanceof Output) x += vis2.getSizeW();

          io2.removeForwarding(io);
          io2.addForwarding(node.getInput("i"));
          node.getOutput("o").addForwarding(io);
        }
      } else { // main eingang und andere ausgänge haben mehrere leitungen
        let cop = io.getForwardings();
        let a = [...cop];
        let vis1, vis2;
        if (io.getOwner() == circuit) vis1 = this.visual;
        else vis1 = this.visual.childVisuals.get(io.getOwner());
        // die punkte am ziel werden gemittelt (um mit dem startpunkt die mitte zu finden für die position des knotens)
        x = 0;
        y = 0;
        for (let io2 of cop) {
            if (io2.getOwner() == circuit) vis2 = this.visual;
            else vis2 = this.visual.childVisuals.get(io2.getOwner());
            let io2ToPos = io2.ps_getToPos();
            let vis2AbsPos = vis2.getAbsPos();
            x += io2ToPos.x;
            y += io2ToPos.y;
            x += vis2AbsPos.x;
            y += vis2AbsPos.y;
            if (io2 instanceof Output) x += vis2.getSizeW();
        }
        x /= cop.length;
        y /= cop.length;
        let ioFromPos = io.ps_getFromPos();
        let vis1AbsPos = vis1.getAbsPos();
        x += ioFromPos.x;
        y += ioFromPos.y;
        x += vis1AbsPos.x;
        y += vis1AbsPos.y;
        if (io instanceof Output) x += vis1.getSizeW();
        
        io.disconnect(true, false, false);
        io.addForwarding(node.getInput("i"));
        let o = node.getOutput("o");
        for (let io3 of a) {
            o.addForwarding(io3);
        }
      }

      let nodeVisualResult = this.visual.adjoinVisual(node);
      ///this.visual.updateSize(this, START_X, START_Y);
      //this.update(); // …All
      x -= nodeVisualResult.visual.getSize().w;
      y -= nodeVisualResult.visual.getSize().h;
      x /= 2;
      y /= 2;

      //this.point(x,y, "red")

      let pos = this.visual.componentToArea(COMPONENT_AREA.INNER, {x,y});
      nodeVisualResult.visual.setPos(pos);
    }
    this.setDirty();
    this.refresh(true, true);
  }
  
  private async promptForIO(title, ioCreator: (n: string, pc: number)=>IOBase): Promise<{names: string[], pinCount: number}[] | null> {
    let result: {names: string[], pinCount: number}[] = [];
    let regex = /^(?:([a-zäöüßA-ZÄÖÜ0-9]+)|([a-zäöüßA-ZÄÖÜ0-9]*[a-zäöüßA-ZÄÖÜ])\[(\d+)\])(?:\s(\d+))?$/;
    let sep = ',';

    let validator = (v: string) => {
      let list = v.split(sep).map((v)=>v.trim());
      for (let i = 0; i < list.length; i++) {
        // nummeriert darf nicht mit 0-9 aufhören
        let r = list[i].match(regex);
        // Array(5) [ "abc[3] 7", undefined, "abc", "3", "7" ]
        // Array(5) [ "abc 5", "abc", undefined, undefined, "5" ]
        if (!r) return "Das Eingabe-Format ist nicht korrekt.";
        let [, name, prefix, count, pinCount] = r;
        if (typeof name !== 'undefined') {
          let io = ioCreator(name,1);
          let validatorResult = io.propProxies.name.validator(name);
          if (typeof validatorResult === 'string') {
            return validatorResult;
          }
        }
      }
      return true;
    }
    
    let promptResult = await 
    (new StringPrompt(this.getDomDocument()))
    .setTitle(title)
    .setDescription("Anzahl nummerierter Anschlüsse in []; Anzahl Leitungen mit Leerzeichen getrennt. Beispiel: a, a 8, a[3], a[3] 8")
    .setValidator(validator)
    .start("")
    .result();
    if (!promptResult) return null;

    let list = promptResult.split(sep).map((v)=>v.trim());
    for (let i = 0; i < list.length; i++) {
      let r = list[i].match(regex);
      let [, name, prefix, strCount, strPinCount] = r;
  
      let pinCount = 1;
      if (typeof strPinCount !== 'undefined') {
        pinCount = parseInt(strPinCount);
      }
  
      let names: string[] = [];
      if (typeof prefix !== 'undefined' && typeof strCount !== 'undefined') {
        let count = parseInt(strCount);
        for (let i = 0; i < count; i++) {
          names.push(""+prefix+i);
        }
      } else {
        names.push(name);
      }
      result.push({names, pinCount});
    }

    return result;
  }

  async addInViaPrompt() {
    // Test Input: a[3] 4; a 2; v; c[3]; t6
    let ionames = await this.promptForIO("Input", (n, pc)=>new Input(this.component,n,pc));
    if (ionames === null) return;
    let ios: Input[] = [];
    for (let i = 0; i < ionames.length; i++) {
      let {names, pinCount} = ionames[i];
      for(let n of names) {
        let io = new Input(this.component, n, pinCount);
        let validatorResult = io.propProxies.name.validator(n);
        if (typeof validatorResult === 'string') {
          continue;
        }
        ios.push(io);
      }
    }
    ios.forEach((io) => {
      this.component.addInDirectly(io);
    });
    this.component.signalInputsChanged();
    this.updateUi();
    this.refresh(true, false);
  }
  
  async addOutViaPrompt() {
    let ionames = await this.promptForIO("Output", (n, pc)=>new Output(this.component,n,pc));
    if (ionames === null) return;
    let ios: Output[] = [];
    for (let i = 0; i < ionames.length; i++) {
      let {names, pinCount} = ionames[i];
      for(let n of names) {
        let io = new Output(this.component, n, pinCount);
        let validatorResult = io.propProxies.name.validator(n);
        if (typeof validatorResult === 'string') {
          continue;
        }
        ios.push(io);
      }
    }
    ios.forEach((io) => {
      this.component.addOutDirectly(io);
    })
    this.component.signalOutputsChanged();
    this.updateUi();
    this.refresh(true, false);
  }
  



  inputsChanged() {
    this.setDirty();
    if (!this.isEmbeddedEditorMode()) {
      this.updateTimerControl();
    }
  }

  outputsChanged() {
    this.setDirty();
  }

  updateTimerControl() {
    // update the options in timer control
    this.$timerSelect.innerHTML = "";
    let found = false;
    for (let i of this.rootComponent.getInputs()) {
      if (i.getPinCount() > 1) continue;
      let sel = i.getName() === this.timerInput ? 'selected' : '';
      if (sel) found = true;
      this.$timerSelect.innerHTML += `<option value="${i.getName()}" ${sel}>${i.getName()}</option>`;
    }
    if (!found) {
      this.stopTimer();
      this.$timerCheck.checked = false;
    }
  }














  selectionChanged() {
    let $cirProperties = getDomElementByIdOrThrow('cirProperties');
    $cirProperties.innerHTML = "";
    if (this.piiriPane.selection.length == 1) {
      if (this.piiriPane.selection[0] instanceof ComponentVisual) {
        let component = this.piiriPane.selection[0].getComponent();
        this.renderProperties(component.propProxies, component, $cirProperties);
      } else if (this.piiriPane.selection[0] instanceof IOBase) {
        let io = this.piiriPane.selection[0];
        if (!(io instanceof Input || io instanceof Output)) return;
        if (io.getOwner() == this.component) {
          this.renderProperties(io.propProxies, io, $cirProperties);
        } else {
          this.renderProperties(io.propProxies, io, $cirProperties, true);
        }
      }
    }
    this.updateUi();
  }

  renderProperties(properties: PropProxies, subject: Input | Output | Component, $target: HTMLElement, readOnly: boolean = false) {
    let a = "";
    if (subject instanceof Input) {
      a = '<strong>IN</strong>';
    } else if (subject instanceof Output) {
      a = '<strong>OUT</strong>';
    } else if (subject instanceof Component) {
      a = '<strong>'+subject.getCirName().toUpperCase()+'</strong> ';
    }
    $target.innerHTML += '<div class="info">'+a+'</div>';

    let doc = this.getDomDocument();

    let $table = doc.createElement('table')
    $table.classList.add("props");

    for (let prop in properties) {
      let propProxy = properties[prop]
      let $row = doc.createElement('tr');
      propProxy.render($row, readOnly);
      //$row.addEventListener('piiri:props:update', this.onPropRowUpdate.bind(this))
      //$row.addEventListener('piiri:props:error', this.onPropRowError.bind(this))
      $table.appendChild($row);
      $target.appendChild($table);
    };
  }

  /*private onPropRowUpdate(event: CustomEvent) {
    event.stopPropagation();
    let propProxy = event.detail;
    propProxy.update();
    this.setDirty();
    console.log('onPropRowUpdate', propProxy);
  }*/

  /*private onPropRowError(event: CustomEvent) {
    let {propProxy, exception} = event.detail;
    console.warn(propProxy, exception);
  }*/

  startSteps() {
    /*if (this.stepInterval) {
      clearInterval(this.stepInterval);
    }
    this.stepInterval = setInterval(function() {
      CircuitComponent.step();
      this.mainVisual.paintAll(this.ctx, this.mainPos.x, this.mainPos.y, true);
      if (CircuitComponent.cir_deque.length == 0) {
        clearInterval(this.stepInterval);
        this.stepInterval = null;
      }
    }, 100);*/
  }

  selectComponent(c: Component, add = false) {
    let v = this.visual.adjoinVisual(c);
    this.piiriPane.select(v.visual, add);
    this.refresh(true, true);
    this.selectionChanged();
  }


  async addComponent(cir, pos, componentErrors: ComponentFactoryError[]): Promise<Component | undefined> {
    if (typeof pos == 'undefined' || pos === null) {
      pos = {x: 0, y: 0};
    }
    try {
      let c = await this.component.addComponent(cir, cir, pos, null, componentErrors);
      if (!c) {
        alert('Komponente nicht erstellt')
        return;
      }
      this.setDirty();
      this.selectComponent(c);
      return c;
    } catch (e) {
      let msg = "Komponente konnte nicht erstellt werden.";
      if (e instanceof PiiriCircularException) {
        msg = e.toString();
      } else {
        if (typeof e === 'string') {
          msg = e;
        } else if (typeof e.toString === 'function') {
          msg = e.toString();
        }
      }
      alert(msg);
    }
  }


  remove() {
    /*while(this.selection.length) {
      var s = this.selection.pop();
      s.remove();
    }
    this.selectSomething(null);
    this.refresh(true, true);*/
  }

}

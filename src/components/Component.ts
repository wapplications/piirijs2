import { Container } from "./Container";
import { COMMAND_LOG, Point, SerializedChildComponent, SerializedCircuit, SIMULATION_DEBUG } from '../types';
import {
  Input,
  IO,
  IOType,
  IOValue,
  Output
} from './IO';
import { t, ui_text, clock } from '../helpers';
import { Signal } from '../Signal';
import Simulation from '../Simulation'
import { BooleanProp, TextProp, HandlesPropsChanges, LineProp, PropProxies, Props, AndValidator, NameValidatorFactory, } from '../Props'


export enum CircuitNotification {
  TIMEOUT_ERROR,
  UPDATE,
  PROPS_CHANGED
}


type IOChangedSignalConnection = (s: string, v: IOValue) => void;
type CircuitNotifycationConnection = (n: number, s: string, a: any) => void;
type InputsChangedConnection = () => void;
type OutputsChangedConnection = () => void;



/**
    hat namen
    hat io
    Hat properties richtet diese ein
    hat parent
    ist basisklasse der gates und sonstiger fester komponenten
*/
export class Component implements HandlesPropsChanges {


  name: string
  staysVisible: boolean
  description: string = "";

  cir_name: string;
  //visible: boolean;
  pos: Point;
  simulate: boolean;

  inputs: Input[] = [];
  outputs: Output[] = [];

  m_pParentContainer: Container | null;

  simulation = Simulation;

  propProxies: PropProxies = {};

  public isBuiltInComponent: boolean = true;
  public isGroupComponent: boolean = false;
  public overwritesGroupComponent: boolean = false;

  public constructor(name: string, parent: Container | null, sim: boolean = true) {
    this.name = name;
    this.cir_name = '';
    //this.visible = true;
    this.simulate = sim;
    this.m_pParentContainer = parent;
    this.setPos({ x: 0, y: 0 });

    this.propProxies.name = new LineProp(this, 'name', 'Name', name)
    this.propProxies.description = new TextProp(this, 'description', 'Beschreibung', '')
    this.propProxies.staysVisible = new BooleanProp(this, 'staysVisible', 'bleibt sichtbar', false)

    this.propProxies.name.setValidator(AndValidator(NameValidatorFactory(true), (v: string) => {
      let children = this.getParent().getChildren();
      for (let child of children) {
        if (child === this) continue;
        if (child.getName() === v) {
          return "Komponente mit diesem Namen schon vorhanden."
        }
      }
      return true;
    }));

    this.signal_input_changed = new Signal<IOChangedSignalConnection>();
    this.signal_output_changed = new Signal<IOChangedSignalConnection>();
    this.signal_circuit_notification = new Signal<CircuitNotifycationConnection>;
    this.signal_inputs_changed = new Signal<InputsChangedConnection>;
    this.signal_outputs_changed = new Signal<OutputsChangedConnection>;
  }

  public init(props: Props): void {
  }

  getProps(): { [key: string]: unknown } {
    let result = {};
    for (let k in this.propProxies) {
      result[k] = this.propProxies[k].getValue()
    }
    return result;
  }

  setProps(props: { [key: string]: any }) {
    let olds: { [key: string]: any } = {}
    for (let p in this.propProxies) {
      if (typeof props[p] === 'undefined') continue;
      let old = this[p];
      if (old !== props[p]) {
        this[p] = props[p];
        olds[p] = old;
      }
    }
    if (Object.keys(olds).length > 0) {
      this.onPropsChanged(olds);
    }
  }

  onPropsChanged(oldValues: { [key: string]: any }): void {
    this.signal_circuit_notification.emit(CircuitNotification.PROPS_CHANGED, "", {source: this, component: this, oldValues});
  }



  public dump(): void {
    console.log('# ' + this.getName() + ' (' + this.getCirName() + ')');
    for (let i of this.inputs) {
      console.log('IN:  ' + i.getName() + '=' + i.getValue());
    }
    for (let o of this.outputs) {
      console.log('OUT: ' + o.getName() + '=' + o.getValue());
    }
  }









  public getCirName(): string {
    return this.cir_name;
  }

  public setCirName(name: string): void {
    this.cir_name = name.toLowerCase();
  }

  public getName(): string {
    return this.name;
  }

  public setName(n: string) {
    this.name = n;
  }

  public getPos(): Point {
    return this.pos;
  }

  public getPosX(): number {
    return this.pos.x;
  }

  public setPosX(x: number) {
    this.pos.x = x;
  }

  public getPosY(): number {
    return this.pos.y;
  }

  public setPosY(y: number) {
    this.pos.y = y;
  }

  public setPos(pos: Point) {
    this.pos = pos;
  }

  onInputChanged(name: string) {
    //console.log('CC onInputChanged()')
  }

  public getParent(): Container | null {
    return this.m_pParentContainer;
  }

  getParentRecursive(): Container | null {
    if (this.m_pParentContainer == null) return null;
    let pCc = this.m_pParentContainer.getParentRecursive();
    if (pCc == null) return this.m_pParentContainer;
    return pCc;
  }

  getNameRecursive(last: boolean = true): string {
    let s: string = "";
    let p = this.getParent();
    if (p) s = p.getNameRecursive(last);
    if (p || (!p && last)) {
      if (s !== "") s += ">";
      s += this.getName();
    }
    return s;
  }

  provideUiText(textmap: object): void {
    textmap["/description"] = ui_text(t("Beschreibung"), t("Zum Beispiel: Welche Aufgabe hat diese Komponente?"));
    textmap["/markedVisible"] = ui_text(t("Bleibt sichtbar"), t("Bleibt sichtbar, wenn die Schaltung als Komponente verwendet wird."));
  }

  getInfo(): string {
    return "";
  }


  //     /** SIMULATION */


  update(): void {
    if (!this.isSimulated()) return;

    if (SIMULATION_DEBUG) {
      console.log("######################### START UPDATE", this.getNameRecursive());
    }

    if (this.simulation.is_step_sim()) {
      // invalidate
      this.simulation.list_push(this);
    } else {
      this.simulation.timeoutClock = clock() + this.simulation.timeoutDelta;
      this.reset(); // zum test
      try {
        this.listSimulate();
      } catch (s) {
        if (typeof s === 'string') {
          console.log('timeout_test() hit: ' + s);
          this.signal_circuit_notification.emit(CircuitNotification.TIMEOUT_ERROR, s, null);
        } else {
          console.log('listSimulate exception: ', s);
        }
      }
    }
    if (SIMULATION_DEBUG) {
      console.log("######################### END UPDATE");
    }
  }





  processChildren(): void {
    // empty here
  }

  stepSimulate(): void {
    if (SIMULATION_DEBUG) {
      console.log("Do ", this.getName() + ' stepSimulate()');
    }
    // eingänge weiterleiten
    this.inputs.forEach((i: Input) => i.list_forward());
    this.processChildren();

    if (SIMULATION_DEBUG) {
      this.simulation.dump_deque();
    }

    if (!this.hasChildren()) {
      if (SIMULATION_DEBUG) {
        console.log(this.getName(), " has NO components");
        console.log("inputs: ");
        this.inputs.forEach((i: Input) => { console.log(i.getName() + "=" + i.getValue()) })
      }

      // berechnen
      this.outputs.forEach((o: Output) => {
        if (o.getFrom() !== null) return; // wenn dieser ausgang ein signal zugeleitet bekommt
        let v: IOValue = this.calculateOutputValue(o.getName());
        if (SIMULATION_DEBUG) {
          console.log("calc OUT " + o.getOwner().getName() + "." + o.getName() + "=" + v);
        }
        o.list_setValue(v);
      });
    }
  }


  // Simulation
  listSimulate(): void {
    let child: Component
      | undefined;
    if (SIMULATION_DEBUG) {
      console.log("Do " + this.getName() + ' listSimulate()');
    }
    this.simulation.timeout_test();
    // eingänge weiterleiten
    for (let i of this.inputs) {
      i.list_forward();
    }
    this.processChildren();
    if (SIMULATION_DEBUG) {
      this.simulation.dump_deque();
    }
    if (this.hasChildren()) {
      if (SIMULATION_DEBUG) {
        console.log("listSimulate Children");
      }
      let deque = this.simulation.get_deque();
      while (deque.length > 0) {
        child = deque.shift();
        if (typeof child === 'undefined') break;
        child.listSimulate();
      }
    } else {
      if (SIMULATION_DEBUG) {
        console.log(this.getName() + " has NO components");
      }

      let v: IOValue;
      // berechnen
      for (let o of this.outputs) {
        if (o.getFrom() !== null) continue; // wenn dieser ausgang ein signal zugeleitet bekommt
        v = this.calculateOutputValue(o.getName());
        if (SIMULATION_DEBUG) {
          console.log("calc OUT " + o.getOwner().getName() + "." + o.getName() + "=" + v);
        }
        o.list_setValue(v);
      }
    }
  }

  pushToSimulationList() {
    this.simulation.list_push(this);
  }

  removeFromSimulationList() {
    this.simulation.clear_list(this);
  }

  hasChildren(): boolean {
    return false;
  }

  setSimulate(s: boolean): void {
    this.simulate = s;
  }

  isSimulated(): boolean {
    return this.simulate;
  }




  getOutput(name: string): Output | null {
    for (let io of this.outputs) {
      if (io.getName() === name) return io;
    }
    return null;
  }

  getInput(name: string): Input | null {
    for (let io of this.inputs) {
      if (io.getName() === name) return io;
    }
    return null;
  }

  getInputs(): Input[] {
    return this.inputs;
  }

  getOutputs(): Output[] {
    return this.outputs;
  }

  getOutputNumber(): number {
    let num: number = 0;
    for (let o of this.outputs) {
      num = num << o.getPinCount();
      num |= o.getValue();
    }
    return num;
  }

  getInputNumber(revers: boolean = false): number {
    let num: number = 0;
    let shift = 0;
    for (let i of this.inputs) {
      if (revers) {
        num |= i.getValue() << shift;
        shift += i.getPinCount();
      } else {
        num = num << i.getPinCount();
        num |= i.getValue();
      }
    }
    return num;
  }


  clearInputs(): void {
    this.disconnect(true, false);
    this.inputs = [];
  }

  clearOutputs(): void {
    this.disconnect(false, true);
    this.outputs = [];
  }


  disconnect(disconnectInputs: boolean = true, disconnectOutputs: boolean = true): void {
    if (disconnectOutputs) {
      for (let o of this.outputs) {
        o.disconnect(true, false, true);
      }
    }
    if (disconnectInputs) {
      for (let i of this.inputs) {
        i.disconnect(false, true, true);
      }
    }
  }

  setInput(name: string, v: IOValue) {
    let i: Input | null = this.getInput(name);
    if (i === null) return;
    i.setValue(v);
  }

  public inputChanged(name: string, value: IOValue) {
    this.signal_input_changed.emit(name, value);
  }

  public outputChanged(name: string, value: IOValue) {
    this.signal_output_changed.emit(name, value);
  }

  protected calculateOutputValue(out: string): IOValue {
    return 0;
  }

  getOutputPinCount(): number {
    let outbits: number = 0;
    for (let o of this.outputs) {
      outbits += o.getPinCount();
    }
    return outbits;
  }

  getInputPinCount(): number {
    let inbits: number = 0;
    for (let i of this.inputs) {
      inbits += i.getPinCount();
    }
    return inbits;
  }



  getInputValue(name: string): number {
    let i: Input | null = this.getInput(name);
    if (i === null) return 0;
    return i.getValue();
  }

  getOutputValue(name: string): number {
    let o: Output | null = this.getOutput(name);
    if (o === null) return 0;
    return o.getValue();
  }


  setInputValue(v: IOValue): void {
    // v wird nach rechts durchgeschiftet. dh. der wert steht  immer ganz rechts in den unteren bits
    for (let i of this.inputs) {
      // bsp: pincount = 3:
      // (1 << 3) - 1 = $1000 - 1 = $111
      let mask: number = (1 << i.getPinCount()) - 1;
      i.setValue(v & mask);
      v = v >> i.getPinCount();
    }
  }


  reset(): void {
    this.ioReset();
  }

  ioReset(): void {
    for (let i of this.inputs) {
      i.setUnset();
    }
    for (let o of this.outputs) {
      o.setUnset();
    }
  }


  getDependencies(includeMe: boolean = true): Set<string> {
    let deps = new Set<string>();
    if (this instanceof Container) { // TODO inplace container berücksichtigen
      deps.add(this.getCirName());
      let cc = this.getChildren();
      for (let c of cc) {
        deps = new Set<string>([...deps, ...c.getDependencies(true)]);
      }
    }
    return deps;
  }


  /*
      public toEditorObject(): PropObject {
          let result = {
              name: {
                  name: 'name',
                  title: 'Name',
                  type: 'string',
                  value: this.getName(),
              },
              description: {
                  name: 'descrption',
                  title: 'Beschreibung',
                  type: 'string',
                  value: this.description,
              },
          };
          return result;
      }
  
      public fromEditorObject(props: PropObject): boolean {
          let changed = false
          if (props.name.value !== this.getName()) {
              let newName = props.name.value;
              if (newName === "") {
                  throw {name: "name", msg: "Name darf nicht leer sein."};
              }
              if (this.getParent()) {
                  let otherComp = this.getParent().findChildByName(newName);
                  if (otherComp && otherComp !== this) {
                      throw {name: "name", msg: "Name ist schon vergeben."};
                  }
              }
              this.setName(newName)
              changed = true;
          }
          if (props.description.value !== this.description) {
              this.description = props.description.value;
              changed = true;
          }
          return changed;
      }
  */


  //     /*
  //     clone(parent: Container): Component {
  //         // TODO das ist doof, dass hier createComponent
  //         // verwendet wird. vielleict sollte es in der componentfactory eine clone funktion geben?
  //         let pch: Component = createComponent(parent, this.getCirName(), this.getName());
  //         pch.getProperties().clone(this.getProperties());
  //         pch.on_propertyChanged();
  //         return pch;
  //     }*/


  //         /*

  //     protected:

  //         void setParent(Container *p) {m_pParentContainer=p;}
  //         */

  empty() {
    this.simulation.clear_list(this);
    this.clearInputs();
    this.clearOutputs();
  }


  clear() {
    this.signal_circuit_notification.clear();
    this.signal_inputs_changed.clear();
    this.signal_input_changed.clear();
    this.signal_output_changed.clear();
    this.simulation.clear_list(this);
    this.clearInputs();
    this.clearOutputs();
  }

  toJson(): SerializedChildComponent {
    return {
      component: this.getCirName(),
      componentData: null,
      name: this.getName(),
      pos: {...this.getPos()},
      props: {...this.getProps()}
    }
  }



  signal_input_changed: Signal<IOChangedSignalConnection>;
  signal_output_changed: Signal<IOChangedSignalConnection>;
  signal_circuit_notification: Signal<CircuitNotifycationConnection>;
  signal_inputs_changed: Signal<InputsChangedConnection>;
  signal_outputs_changed: Signal<OutputsChangedConnection>;

}



import { PiiriCircularException } from "../helpers";
import { Props } from "../Props";
import { COMMAND_LOG, ComponentFactoryError, Point, SerializedCircuit } from "../types";
import { Component } from "./Component";
import { DummyComponent } from "./DummyComponent";
import createComponent from "./factory";
import { Input, IO, IOType, IOValue, Output } from "./IO";
import {NodeComponent} from "./NodeComponent";

type IOThing = {
  circuit: Component | null,
  io: IO | null,
  type: IOType
}


/**
hat kinder: integrierte schaltung
hat dateinamen
ist basisklasse von toplevel, wo aber properties geschichten neu implementiert werden!
*/
export class Container extends Component {
  protected children: Component[] = [];
  protected filename: string;
  //public embedded: boolean = false;

  public init(props: Props) {
    this.setCirName("");
    this.isBuiltInComponent = false;
  }

  embed() {
    //this.embedded = true;
    this.setCirName('embedded');
    this.children.forEach((ch: Component) => {
      if (ch instanceof Container) {
        ch.embed();
      }
    });
  }

  hasChildren(): boolean { return (this.children.length > 0); };
  getChildren(): Component[] { return this.children; };
  getFilename(): string { return this.filename; }
  setFilename(fn: string) { this.filename = fn; }

  reset(): void {
    super.reset();
    this.children.forEach((ch: Component) => ch.reset());
  }

  parseIOThingFromString(s: string, type: IOType): IOThing {
    let thing: IOThing = {} as IOThing;
    thing.type = type;
    let sp: string[] = s.split('.');
    if ((sp[0] == "in") || (sp[0] == "out")) {
      thing.circuit = this;
      thing.type = sp[0];
    } else {
      thing.circuit = this.findChildByName(sp[0]);
    }
    if (thing.circuit == null) return thing;

    if (thing.type == 'in') {
      thing.io = thing.circuit.getInput(sp[1]);
      return thing;
    }
    if (thing.type == 'out') {
      thing.io = thing.circuit.getOutput(sp[1]);
      return thing;
    }
    console.error("Container::parseIOThingFromString: Unbekannter IO Typ!");
    thing.io = null;
    return thing;
  }

  findChildByName(name: string): Component | null {
    for (let child of this.children) {
      if (child.getName() === name) return child;
    }
    return null;
  }

  addInDirectly(io: Input, value: IOValue = 0) {
    this.inputs.push(io);
    if (this.getParent() == null) io.setValue(value);
  }

  addOutDirectly(io: Output) {
    this.outputs.push(io);
  }
  
  addIn(name: string, pinCount: number = 1, value: IOValue = 0): Input {
    if (COMMAND_LOG) {
      console.log("Circuit '" + this.getName() + "' gets new input '" + name + "'.");
    }
    let io: Input = new Input(this, name, pinCount);
    this.addInDirectly(io);
    this.signalInputsChanged();
    return io;
  }

  addOut(name: string, pinCount: number = 1): Output {
    if (COMMAND_LOG) {
      console.log("Circuit '" + this.getName() + "' gets new output '" + name + "'.");
    }
    let io: Output = new Output(this, name, pinCount);
    this.addOutDirectly(io);
    this.signalOutputsChanged();
    return io;
  }


  signalInputsChanged() {
    this.signal_inputs_changed.emit();
  }

  signalOutputsChanged() {
    this.signal_outputs_changed.emit();
  }








  addComponentInstance(neues_element: Component): Component {
    this.children.push(neues_element);
    neues_element.signal_circuit_notification.connect(this.onChildNotification.bind(this));
    return neues_element;
  }

  /**
   * point ist hier eigentlich nich sinnvoll - da es zum visual gehört, wobei es aber zum layout der schaltung gehört
   */
  async addComponent(cir: string, name: string, pos: Point, props: { [key: string]: any } | null = null, componentErrors: ComponentFactoryError[]): Promise<Component | null> {
    let p: Container = this;
    do {
      if (p.getCirName() == cir && cir !== 'embedded') {
        throw new PiiriCircularException(this.getCirName(), cir);
      }
    } while (p = p.getParent());

    let c = await createComponent(this, cir, name, this.isSimulated(), componentErrors);
    if (c !== null) {
      c.setPos(pos);
      if (props !== null && !(c instanceof DummyComponent)) {
        c.setProps(props)
      }
      this.addComponentInstance(c);
    }
    return c;
  }

  removeComponent(c: Component) {
    this.children.splice(this.children.indexOf(c), 1);
    c.clear();
  }

  deleteComponent(pObj: Component): void {
    if (!pObj || (pObj.getParent() !== this)) {
        return;
    }

    let from: IO | null = null;
    let to: IO[] = [];

    // Node bekommt sonderbehandlung
    if (pObj.getCirName() == "node") {
        let pe: NodeComponent = pObj as NodeComponent;
        from = pe.pin.getFrom();
        if (from) { // nur wenn eine verbindung komplett hierüber geht
            // merken wohin das obj das signal verteilt hat, denn bevor die neuen verbindungen gesetzt werden können, müssen die alten entfernt werden.
            for (let io of pe.pout.getForwardings()) {
                to.push(io);
            }
        }
    }
    
    // Normales löschen
    pObj.disconnect();
    pObj.removeFromSimulationList();
    this.removeComponent(pObj);
    

    if (from) { // direkte forwardings schalten
        for (let io of to) {
            from.addForwarding(io);
        }
        try {
            this.update();
        } catch (i) {
            console.error("Exception: ", i );
        }
    }
  }


  deleteIO(io: IO): void {
    io.disconnect();
    io.clear();
    if (io instanceof Input) {
        this.inputs.splice(this.inputs.indexOf(io), 1);
        this.signal_inputs_changed.emit();
    } else if (io instanceof Output) {
        this.outputs.splice(this.outputs.indexOf(io as Output), 1);
        this.signal_inputs_changed.emit();
    }
  }



  connect(from: string, to: string): boolean {
    let iofrom: IOThing = this.parseIOThingFromString(from, "out");
    let ioto: IOThing = this.parseIOThingFromString(to, "in");
    if (iofrom.circuit && iofrom.io && ioto.circuit && ioto.io) {
      // DummyComponent weiß nicht welche pinCount gebraucht wird...
      if (iofrom.io.getPinCount() === ioto.io.getPinCount() || iofrom.circuit instanceof DummyComponent || ioto.circuit instanceof DummyComponent) {
        iofrom.io.addForwarding(ioto.io);
      }
      return true;
    } else {
      console.warn("In " + this.getName() + " (" + this.getCirName() + "): Verbindung zwischen " + from + " und " + to + " konnte nicht hergestellt werden.");
      if (!iofrom.circuit) {
        console.warn("from componente not found")
      }
      if (!ioto.circuit) {
        console.warn("to componente not found")
      }
      if (!iofrom.io) {
        console.warn("from io not found")
      }
      if (!ioto.io) {
        console.warn("to io not found")
      }
    }
    return false;
  }

  processChildren(): void {
    let isstepsim: boolean = this.simulation.is_step_sim();
    // elemente ohne inputs werden auch angestoßen
    for (let child of this.children) {
      if (isstepsim) {
        this.simulation.list_push(child);
      } else {
        if (child.getInputs().length == 0)
          child.listSimulate();
      }
    }
  }


  onChildNotification(n: number, s: string, a: any): void {
    this.signal_circuit_notification.emit(n, s, a);
  };

  empty() {
    super.empty();
    this.children.forEach(c => c.clear())
    this.children = [];
  }

  clear(): void {
    super.clear();
    this.children.forEach(c => c.clear())
    this.children = [];
  }


  toJsonObject(): SerializedCircuit {
    let result = {
      name: this.cir_name,
      inputs: [],
      outputs: [],
      children: [],
      connections: [],
      props: {},
      isGroupComponent: this.isGroupComponent,
      overwritesGroupComponent: this.overwritesGroupComponent
    } as SerializedCircuit;
    /*for (let k in this.propProxies) {
        result.props[k] = this.propProxies[k].getValue()
    }*/
    for (let i of this.inputs) {
      result.inputs.push({ name: i.getName(), pinCount: i.getPinCount() })
    }
    for (let o of this.outputs) {
      result.outputs.push({ name: o.getName(), pinCount: o.getPinCount() })
    }
    for (let c of this.children) {
      let childData;
      if (c instanceof DummyComponent) {
        throw 'Kann Schaltung mit Platzhalter-Komponente nicht speichern!';
      }
      if (c instanceof Container && c.getCirName() === 'embedded') {
        childData = {
          component: 'embedded',
          componentData: c.toJsonObject(),
          name: c.getName(),
          pos: c.getPos(),
          props: c.getProps()
        };
      } else {
        childData = c.toJson();
      }
      result.children.push(childData)
    }
    // connections from inputs
    for (let i of this.inputs) {
      for (let f of i.getForwardings())
        result.connections.push({
          from: 'in.' + i.getName(),
          to: (f.getOwner() === this ? 'out' : f.getOwner().getName()) + '.' + f.getName()
        });
    }
    for (let c of this.children) {
      for (let o of c.getOutputs()) {
        for (let f of o.getForwardings())
          result.connections.push({
            from: o.getOwner().getName() + '.' + o.getName(),
            to: (f.getOwner() === this ? 'out' : f.getOwner().getName()) + '.' + f.getName()
          });
      }
    }
    return result;
  }



  async loadCircuit(data: SerializedCircuit, componentErrors: ComponentFactoryError[]) {
    this.isGroupComponent = data.isGroupComponent;
    this.overwritesGroupComponent = data.overwritesGroupComponent;
    //this.isBuiltInComponent = false;
    this.cir_name = data.name;
    if (Array.isArray(data.inputs)) {
      for (let i of data.inputs) {
        let io: Input = new Input(this, i.name, i.pinCount);
        this.addInDirectly(io);
      }
    }
    if (Array.isArray(data.outputs)) {
      for (let o of data.outputs) {
        let io: Output = new Output(this, o.name, o.pinCount);
        this.addOutDirectly(io);
      }
    }
    if (Array.isArray(data.children)) {
      for (let c of data.children) {
        try {
          let comp = await this.addComponent(c.component, c.name, c.pos, c.props, componentErrors)
          if (c.component === 'embedded') {
            await (comp as Container).loadCircuit(c.componentData, componentErrors);
          }
        } catch (e) {
          if (e instanceof PiiriCircularException) {
            console.warn(e.toString());
          } else {
            throw e;
          }
        }
      }
    }
    if (Array.isArray(data.connections)) {
      for (let c of data.connections) {
        this.connect(c.from, c.to)
      }
    }
  }






  topmost(obj: Component): boolean {
    /*type_elements_iterator i,to;

    if (getChildren().back() != obj)
    {
        getChildren().remove(obj);
        getChildren().push_back(obj);
        return true;
    }*/
    return false;
}

bottommost(obj: Component): boolean {
    /*
    if (getChildren().front() != obj)
    {
        getChildren().remove(obj);
        getChildren().push_front(obj);
        return true;
    }*/
    return false;
}

up(obj: Component) {
    /*
    type_elements_iterator i,to;

    if (getChildren().back() != obj)
    {
        for (i = getChildren().begin(); i != getChildren().end(); i++)
        {
            if ( (*i) == obj) break;
        }
        if ((i != getChildren().end())) // wenn gefunden
        {
            to = i;
            to++;
            to++;
            if (to != getChildren().end())
            {
                getChildren().splice(to, getChildren(), i);
            }
            else
            {
                getChildren().remove(obj);
                getChildren().push_back(obj);
            }
            return true;
        }
    }*/
    return false;
}

down(obj: Component): boolean {
    /*
    type_elements_iterator i,to;

    if (getChildren().front() != obj)
    {
        for (i = getChildren().begin(); i != getChildren().end(); i++)
        {
            if ( (*i) == obj) break;
        }
        if ((i != getChildren().end())) // wenn gefunden
        {
            to = i;
            to--;
            getChildren().splice(to, getChildren(), i);
            return true;
        }
    }*/
    return false;
}


}


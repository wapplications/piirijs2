import { BooleanProp, CountingNumberProp, Props } from '../Props';
import {Component, CircuitNotification} from './Component'
import { Output } from './IO';


export class ClockComponent extends Component {
  value: boolean;
  interval: number;
  running: boolean;
  del: boolean;
  timer: any = null;

  public init(props: Props) {
    super.init(props);
    this.setCirName("clock")
    this.value = false;
    this.del = false;
    this.propProxies.interval = new CountingNumberProp(this, 'interval', 'Intervall', 1000)
    this.propProxies.running = new BooleanProp(this, 'running', 'Aktiv', false)
    this.outputs.push(new Output(this, "o"));
    if (this.running) {
      this.start();
    }
  }

  clear() {
    super.clear();
    this.stop();
  }

  protected calculateOutputValue(out: string): number {
    return this.value ? 1 : 0;
  }

  timerCallback() {
    if (typeof this === 'undefined') {
      return;
    }
    this.value = !this.value;
    this._update();
  }

  onPropsChanged(oldValues: {[key: string]: any}): void {
    let restart = false;
    if ('running' in oldValues) {
      if (this.running) {
        this.start()
      } else {
        this.stop()
      }
    } else if ('interval' in oldValues) {
      if (this.interval < 20) {
        this.interval = 20;
        this.propProxies.interval.updateIfRendered();
      }
      if (this.running) {
        this.start();
      }
    }
    super.onPropsChanged(oldValues);
  }

  start() {
    if (!this.isSimulated()) return;
    if (this.timer) this.stop();
    this.timer = setInterval(this.timerCallback.bind(this), this.interval)
  }

  stop() {
    if (this.timer) {
      clearInterval(this.timer)
      this.timer = null;
    }
  }

  _update() {
    // TODO wozu das?
    //let p = this.getParent();
    //if (!p) return;
    //p.update();
    this.signal_circuit_notification.emit(CircuitNotification.UPDATE, "", null);
  }
}

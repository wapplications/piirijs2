import { CarouselProp, Props } from "../Props";
import {Component} from './Component'
import { Input } from "./IO";



export class LEDComponent extends Component {
  color: string;

  public init(props: Props) {
    super.init(props);
    this.setCirName("led")
    this.inputs.push(new Input(this, "i"));

    this.propProxies.color = new CarouselProp(this, 'color', 'Farbe', 'blue', false, {
      blue:'blau', yellow:'gelb', red:'rot', green:'grün'
    })
  }
};
  
import { Props } from "../Props";
import {Component} from './Component'
import { Input, Output } from "./IO";

/**
 * Erzeugt io wenn angefragt - aber leider nur mit pinCount = 1
 * wird in Container.connect berücksichtigt
 */
export class DummyComponent extends Component {
  public init(props: Props) {
    super.init(props);
    this.setCirName('dummy')
  }

  getInput(name: string): Input | null {
    let io = super.getInput(name);
    if (!io) {
      io = new Input(this, name, 1);
      this.inputs.push(io);
    }
    return io;
  }

  getOutput(name: string): Output | null {
    let io = super.getOutput(name);
    if (!io) {
      io = new Output(this, name, 1);
      this.outputs.push(io);
    }
    return io;
  }

}



/*class FunctionComponent : public CircuitChild {
  protected:
      int count;
      string term;

      void on_propertyChanged();
      void createInputs();
  public:
      FunctionComponent(string n, CircuitContainer* p, int c, string t = "");
      virtual ~FunctionComponent() {}
      type_iovalue calculateOutputValue(const string& out);
      virtual string getInfo();
      virtual void provideUiText(ui_text_map& textmap);
};


FunctionComponent::FunctionComponent(string n, CircuitContainer* p, int c, string t) : CircuitChild(n, p, "function"),
    count(c), term(t)
{
    getProperties().set_int("size", c);
    getProperties().set_string("term", t);

	outputs.push_back(new CircuitOutput(this, "y"));
    createInputs();
}

type_iovalue FunctionComponent::calculateOutputValue(const string& out)
{
    map<string,bool> assign;
    type_inputs_iterator iit;
    for (iit = inputs.begin(); iit != inputs.end(); iit++)
    {
        assign[(*iit)->getName()] = ((*iit)->getValue()==1);
    }
    string errstr;
    bool b = LogicBin::eval(term, assign, errstr);
    if (!errstr.empty())
    {
        cerr << errstr << endl;
        return false;
    }
    return b;
}


void FunctionComponent::on_propertyChanged()
{
	if (count != getProperties().get_int("size"))
	{
		count = getProperties().get_int("size");
		clearInputs();
		createInputs();
	}
	if (term != getProperties().get_string("term"))
	{
		term = getProperties().get_string("term");
		update();
	}
}

void FunctionComponent::createInputs()
{
    ostringstream str;
    for (int i = 0; i < count; i++)
    {
        str << "x" << i;
        inputs.push_back(new CircuitInput(this, str.str()));
        str.str("");
    }
}

string FunctionComponent::getInfo()
{
    return "y = " + term;
}

void FunctionComponent::provideUiText(ui_text_map& textmap)
{
    CircuitChild::provideUiText(textmap);
    textmap["/size"] = ui_text("Eingangsbreite", "Anzahl der Eingänge.");
    textmap["/term"] = ui_text("Funktionsterm", "Logischer Funktionsterm (Variablen x0, x1 ...).");
}


*/
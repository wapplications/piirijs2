import { CarouselProp, CountingNumberProp, Props } from "../Props";
import {Component} from './Component'
import { Input, Output } from "./IO";



export class AdapterComponent extends Component {

  size: number;
  direction: "bundle" | "unbundle";

  public init(props: Props) {
    super.init(props);
    this.setCirName("adapter")
    this.propProxies.size = new CountingNumberProp(this, 'size', 'Leitungen', props?.size ?? 1)
    this.propProxies.direction = new CarouselProp(this, 'direction', 'Richtung', props?.direction ?? "bundle", false, 
      {bundle: "zusammenfassen", unbundle: "aufsplitten"} )
    this.createIO();
  }

  onPropsChanged(oldValues: {[key: string]: any}): void {
    if ('size' in oldValues || 'direction' in oldValues) {
      this.createIO();
    }
    super.onPropsChanged(oldValues);
  }

  createIO() {
    this.clearInputs();
    this.clearOutputs();

    if (this.direction == "bundle") {
      // BUNDLE
      // 1-Pin-Eingänge    mehr-Pin-Ausgang
      let prefix = ""; // "in"
      for (let i=0; i< this.size; i++) {
          this.inputs.push(new Input(this, prefix+i, 1));
      }
      this.outputs.push(new Output(this, "out", this.size));
    } else {
      // UNBUNDLE
      // mehr-Pin-Eingang    1-Pin-Ausgänge
      let prefix = ""; // "out"
      for (let i=0; i< this.size; i++) {
        this.outputs.push(new Output(this, prefix+i, 1));
      }
      this.inputs.push(new Input(this, "in", this.size));
    }
  }

  protected calculateOutputValue(out: string): number {
    if (this.direction == "bundle") {
        //  = -  viele auf eins
        let v = 0;
        let pi = 0;
        for (let inp of this.inputs) {
            v |= ((inp.getBooleanValue() ? 1 : 0) << pi);
            pi++;
        }
        return v;
    } else {
        //  - =  eins auf viele
        // nummer des outputs ermitteln
        let p = out.match(/(\d+)$/)
        if (!p) return 0; // sollte nicht vorkommen
        let num = parseInt(p[1])
        return (( this.inputs[0].getValue() & (1 << num)) != 0) ? 1 : 0
    }
  }

};
  
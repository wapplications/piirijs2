import { SIMULATION_DEBUG, Point } from '../types';
import { CircuitNotification, Component } from './Component'
import { toBoolean, toIOValue } from '../helpers';
import { HandlesPropsChanges, LineProp, PropProxies, NameValidatorFactory, AndValidator, MultiBaseNumberProp, CountingNumberProp, } from '../Props'
import { ToplevelCircuit } from "./ToplevelCircuit";

export type IOValue = number;
export type IOType = "in" | "out"
export type IO = Input | Output;

export class IOBase implements HandlesPropsChanges {
  protected owner: Component;
  protected value: IOValue;
  protected pinCount: number;
  protected name: string;
  protected isset: boolean;
  protected forwardings: IO[] = [];
  protected from: IO | null;
  protected changed: boolean; // TODO: used??
  protected selected: boolean; // eigentlich presentation
  protected visited: boolean;

  protected ps_from: Point;
  protected ps_to: Point;

  propProxies: PropProxies = {};

  public constructor(owner: Component, name: string, pinCount: number = 1) {
    this.owner = owner;
    this.value = 0;
    this.pinCount = pinCount > 0 ? pinCount : 1;
    this.name = name;
    this.isset = false;
    this.from = null;
    this.changed = true;
    this.selected = false;
    this.visited = false;

    this.propProxies.name = new LineProp(this, 'name', 'Name', name)
    this.propProxies.pinCount = new CountingNumberProp(this, 'pinCount', 'Breite', this.pinCount)

    this.propProxies.name.setValidator(AndValidator(NameValidatorFactory(true), (v: string) => {
      if (this instanceof Input) {
        let ios = this.owner.getInputs();
        for (let io of ios) {
          if (io === this) continue;
          if (io.getName() === v) {
            return "Input mit diesem Namen schon vorhanden."
          }
        }
      } else if (this instanceof Output) {
        let ios = this.owner.getOutputs();
        for (let io of ios) {
          if (io === this) continue;
          if (io.getName() === v) {
            return "Output mit diesem Namen schon vorhanden."
          }
        }
      }
      return true;
    }));
    
  }

  clear() {
    // nothing
  }

  onPropsChanged(oldValues: { [key: string]: any }): void {
    if ('pinCount' in oldValues) {
      this.disconnect();
    }
    if ('name' in oldValues || 'pinCount' in oldValues) {
      this.owner.signal_inputs_changed.emit();
    }
    this.owner.signal_circuit_notification.emit(CircuitNotification.PROPS_CHANGED, "", {source: this, component: this.owner, oldValues});
  }

  select(): void {
    this.selected = true;
  }

  deselect(): void {
    this.selected = false;
  }

  isSelected(): boolean {
    return this.selected;
  }

  getSimulation() {
    return this.owner.simulation;
  }

  public getFrom(): IO | null {
    return this.from;
  }
  public setFrom(from: IO | null): void {
    this.from = from;
  }

  public hasChanged(): boolean { return this.changed; }
  public getPinCount(): number { return this.pinCount; };
  public setPinCount(pc: number) {
    this.disconnect();
    this.propProxies.pinCount.setValue(pc)
  };

  public list_setValue(v: IOValue): void { throw "dont use IO directly" };
  public setValue(v: IOValue): void { throw "dont use IO directly" };

  public list_forward(): void {
    this.getSimulation().timeout_test();
    if (this.forwardings.length === 0) {
      if (SIMULATION_DEBUG) {
        console.log("   " + this.getOwner().getName() + "." + this.getName() + "=" + this.value + " (no forwardings)");
      }
      return;
    }
    if (SIMULATION_DEBUG) {
      console.log("# forw " + this.getOwner().getName() + "." + this.getName() + "=" + this.value);
    }
    for (let forw of this.forwardings) {
      if (SIMULATION_DEBUG) {
        console.log(" - to " + forw.getOwner().getName() + "." + forw.getName() + " = " + this.value);
      }
      forw.list_setValue(this.value);
    }
  }

  public getBooleanValue(): boolean {
    return toBoolean(this.value);
  }

  public setBooleanValue(v: boolean): void {
    this.setValue(toIOValue(v));
  }

  public list_setBooleanValue(v: boolean): void {
    this.list_setValue(toIOValue(v));
  }

  public getValue(): IOValue {
    return this.value;
  }

  public isSet(): boolean {
    return this.isset;
  }
  public setUnset(): void {
    this.isset = false;
  }
  public setSet(): void {
    this.isset = true;
  }
  public getName(): string {
    return this.name;
  }
  public getNameEx(): string {
    let os: string = "" + this.name;
    if (this.getPinCount() > 1) os += "(" + this.getPinCount() + ")";
    return os;
  }
  public setName(n: string): void {
    this.name = n;
  }
  public getOwner(): Component {
    return this.owner;
  }
  public setOwner(owner: Component): void {
    this.owner = owner;
  }
  public dump(): void {
    console.log(this.getName() + " = " + this.getValue() + (this.isSet() ? "" : "*"));
  }
  public addForwarding(fwd: IO): void {
    if ((fwd.getFrom() == null) && (this.getPinCount() === fwd.getPinCount())) {
      this.forwardings.push(fwd);
      fwd.setFrom(this as unknown as IO);
    }
  }
  public removeForwarding(fwd: IO): void {
    this.forwardings = this.forwardings.filter(e => e !== fwd);
    fwd.setFrom(null);
  }
  public getForwardings(): IO[] {
    return this.forwardings;
  }
  /**
   * Eingehende verbindungen entfernen:
   * beim from
   * Eingang befreien von weiterleitungen, alle forwardings entfernen
   */
  public disconnect(to: boolean = true, bfrom: boolean = true, off: boolean = true): void {
    let from: IO | null = this.getFrom();
    if (bfrom && from) {
      from.removeForwarding(this as unknown as IO);
      if (off) {
        this.setValue(0);
      }
    }
    if (to) {
      while (this.forwardings.length > 0) {
        let pio: IO = this.forwardings[0];
        if (off) {
          pio.setValue(0);
        }
        this.removeForwarding(pio);
      }
    }
  }
  public ps_setFromPos(p: Point) { this.ps_from = p; }
  public ps_setToPos(p: Point) {
    this.ps_to = p;
  }
  public ps_getFromPos(): Point { return this.ps_from; }
  public ps_getToPos(): Point { return this.ps_to; }


  protected ioPinMask(): number {
    return ((1 << this.pinCount) - 1);
  }

}



export class Input extends IOBase {
  testingValue: number = 0;

  public constructor(owner: Component, name: string, pinCount: number = 1) {
    super(owner, name, pinCount);

    if (owner instanceof ToplevelCircuit) {
      this.propProxies.testingValue = new MultiBaseNumberProp(this, 'testingValue', 'Wert', 0)
    }
  }

  onPropsChanged(oldValues: { [key: string]: any }): void {
    super.onPropsChanged(oldValues);
    if ('testingValue' in oldValues) {
      this.setValue(this.testingValue);
    }
  }

  public list_setValue(v: IOValue): void {
    this.getSimulation().timeout_test();
    if (SIMULATION_DEBUG) {
      console.log("set Input " + this.getOwner().getName() + "." + this.getName() + "=" + v + "(" + this.value + ")");
    }
    this.changed = false;
    let old: IOValue = this.value;
    v &= this.ioPinMask();

    if (v !== this.value || !this.isSet()) {
      this.changed = true;
      this.value = v;
      // TODO: benötigt??
      if (this.getOwner().getCirName() === "node") {
        if (SIMULATION_DEBUG) {
          console.log("Node Spezialbehandlung: ");
        }
        this.list_forward();
      } else {
        this.getOwner().onInputChanged(this.getName());
        this.getSimulation().list_push(this.getOwner());
        if (SIMULATION_DEBUG) {
          console.log(this.getOwner().getName() + " changed --> List");
        }
      }
      if (this.value != old || !this.isSet()) {
        if (this.getOwner()) this.getOwner().inputChanged(this.getName(), v);
      }
    }
    this.setSet();
  }

  public setValue(v: IOValue): void {
    let old: IOValue = this.value;
    this.value = v & this.ioPinMask();
    if (old !== v) {
      if (this.getOwner()) this.getOwner().inputChanged(this.getName(), v);
    }
  }
}



export class Output extends IOBase {
  protected connectedInputs: Input[];

  public constructor(owner: Component, name: string, pinCount: number = 1) {
    super(owner, name, pinCount);
  }

  public list_setValue(v: IOValue): void {
    this.getSimulation().timeout_test();
    if (SIMULATION_DEBUG) {
      console.log("set Ouput " + this.getOwner().getName() + "." + this.getName() + "=" + v + "(" + this.value + ")");
    }
    this.changed = false;
    let old: IOValue = this.value;
    this.changed = true;
    this.value = v & this.ioPinMask();
    if (this.value !== old || !this.isSet()) {
      if (this.getOwner()) this.getOwner().outputChanged(this.getName(), v);
    }
    this.list_forward();
    this.setSet();
  }
  
  public setValue(v: IOValue): void {
    let old: IOValue = this.value;
    this.value = v & this.ioPinMask();
    if (old !== v) {
      if (this.getOwner()) this.getOwner().outputChanged(this.getName(), v);
    }
  }
}




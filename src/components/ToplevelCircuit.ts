import { t, ui_text } from "../helpers";
import {Container} from "./Container";

/**
implementiert properties geschichten neu (und damit das laden aus xml?)
stellt eine editierbare schaltung dar
*/
export class ToplevelCircuit extends Container {
    // extraProperties: Properties;
    
    constructor() {
        super("", null);
    }

    // Properties
    provideUiText(textmap: object): void {
        textmap["/description"] = ui_text(t("Beschreibung"), t("Eine Beschreibung dieser Schaltung."));
        textmap["/plan"] = ui_text(t("Hintergrund"), t("Ein Hintergrundbild einstellen."));
        textmap["/plan/show"] = ui_text(t("Anzeigen"), t("Soll das Hintergrundbild angezeigt werden?"));
        textmap["/plan/over"] = ui_text(t("Drüberliegend"), t("Soll das Hintergrundbild über der Schaltung angezeigt werden?"));
        textmap["/plan/alpha"] = ui_text(t("Tranzparenz"), t("Tranzparenz des Hintergrundbildes:")+" 0.0 .. 1.0.");
        textmap["/plan/scale"] = ui_text(t("Skalierung"), t("Skalierungsfaktor des Bildes: Zum Vergrößern oder Verkleinern."));
        textmap["/plan/file"] = ui_text(t("Dateiname"), t("Der Pfad zur Bild-Datei (relativ zu ") + this.getPlanPath() + ").");
        textmap["/plan/x"] = ui_text(t("Position (x)"), t("Position der linken oberen Ecke des Bildes."));
        textmap["/plan/y"] = ui_text(t("Position (y)"), t("Position der linken oberen Ecke des Bildes."));
    }

    getPlanPath(): string {
        // TODO von extern liefern - evtl über constructor?
        return '';
    }

    on_propertyChanged(): void {
        // empty
    }


}

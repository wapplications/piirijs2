import { ui_text } from '../helpers';
import {
  Input, 
  Output
} from './IO'
import {Component} from './Component'
import { CountingNumberProp, Props } from '../Props';

export class NodeComponent extends Component {
  pin: Input;
  pout: Output;
  pinCount: number;

  public init(props: Props) {
    super.init(props);
    this.setCirName("node")
    this.propProxies.pinCount = new CountingNumberProp(this, 'pinCount', 'Pins', 1)
    this.pin  = new Input(this, "i", this.pinCount);
    this.pout = new Output(this, "o", this.pinCount);
    this.pin.addForwarding(this.pout);
    this.inputs.push(this.pin);
    this.outputs.push(this.pout);
  }

  setPinCount(pinCount: number) {
    this.propProxies.pinCount.setValue(pinCount); // damit onPropChanged aufgerufen wird
  }

  getPinCount(): number {
    return this.pinCount;
  }

  onPropsChanged(oldValues: {[key: string]: any}): void {
    if ('pinCount' in oldValues) {
      this.pin.setPinCount(this.pinCount);
      this.pout.setPinCount(this.pinCount);
      this.pin.addForwarding(this.pout);
    }
    super.onPropsChanged(oldValues);
  }
    
  /*
    Wird das überhaupt gebraucht? wir haben ja forwarding!
    funktioniert auch nicht ohne forwarding
    calculateOutputValue(name: string): IOValue {
    return this.getInput("i").getValue();
  }*/

  provideUiText(textmap: object): void {
    super.provideUiText(textmap);
    textmap["/size"] = ui_text("Leitungen", "Anzahl der Leitungen.");
  }

}


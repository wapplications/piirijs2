import { compileColor, dispatchRefreshCurrentTab, drawLED, drawSwitch, ui_text } from '../helpers'; // dispatchRefresh
import { BooleanProp, CarouselProp, CountingNumberProp, Props } from '../Props';
import { Point, Size } from '../types';
import { ComponentVisual } from '../visuals/ComponentVisual';
import { Component, } from './Component'
import { Input, IOValue, Output } from './IO';
import VisualSettings from '../visuals/VisualSettings';

abstract class IOComponent extends Component {
  size: number;
  pinCount: number;
  multipin: boolean;
  direction: "v" | "h";

  public init(props: Props): void {
    super.init(props);
    this.propProxies.size = new CountingNumberProp(this, "size", "Größe", 20)
    this.propProxies.pinCount = new CountingNumberProp(this, "pinCount", "Leitungen", 2)
    this.propProxies.multipin = new BooleanProp(this, "multipin", "Multipin", true)
    this.propProxies.direction = new CarouselProp(this, "direction", "Ausrichtung", "h", false, { v: 'vertikal', h: 'horizontal' })
    this.createIO();
  }

  //abstract getValue(n: number): boolean;
  abstract createIO();

  onPropsChanged(oldValues: { [key: string]: any; }): void {
    if ("pinCount" in oldValues || "multipin" in oldValues) {
      this.createIO();
    }
    super.onPropsChanged(oldValues);
  }

};

export class Switch extends IOComponent {

  public init(props: Props): void {
    super.init(props);
    this.setCirName('switch');
  }

  calculateOutputValue(out: string): IOValue {
    let op = this.getOutput(out);
    if (!op) return 0;
    return op.getValue();
  }

  createIO() {
    this.clearOutputs();
    if (this.multipin) {
      this.outputs.push(new Output(this, "out", this.pinCount));
    } else {
      for (let i = 0; i < this.pinCount; i++) {
        this.outputs.push(new Output(this, "" + i));
      }
    }
  }
};

export class Indicator extends IOComponent {

  public init(props: Props): void {
    super.init(props);
    this.setCirName('indicator');
  }

  calculateOutputValue(out: string): IOValue {
    return 0;
  }

  createIO() {
    this.clearInputs();
    if (this.multipin) {
      this.inputs.push(new Input(this, "in", this.pinCount));
    } else {
      for (let i = 0; i < this.pinCount; i++) {
        this.inputs.push(new Input(this, "" + i));
      }
    }
  }
};






abstract class IOVisual extends ComponentVisual {

  protected getMyProps() {
    let comp = this.getComponent();
    let direction = comp.propProxies.direction.getValue() as string;
    let pinCount = comp.propProxies.pinCount.getValue() as number;
    let size = comp.propProxies.size.getValue() as number;
    return { direction, pinCount, size }
  }

  public abstract onInnerClick(pos: Point): void;
  protected abstract getValue(n: number): boolean;
}







export class SwitchVisual extends IOVisual {
  innerSize(cr: CanvasRenderingContext2D, x?: number, y?: number, children?: boolean): Size {
    let { direction, pinCount, size } = this.getMyProps();
    return {
      w: direction == 'v' ? size * 2 : size * pinCount,
      h: (direction == 'v' ? size * pinCount : size * 2) + 2
    };
  }

  paintSelf(cr: CanvasRenderingContext2D, x: number, y: number, children: boolean): void {
    super.paintSelf(cr, x, y, children);
    let { direction, pinCount, size } = this.getMyProps();

    let isVertical = direction == 'v';
    cr.save()
    cr.textAlign = "center";
    cr.textBaseline = "middle";
    cr.font = "normal " + (size - 4) + "px sans-serif";
    for (let i = 0; i < pinCount; i++) {
      let v = this.getValue(i);
      let p = { x: 0 /* wird unten berechnet */, y: y + this.titleHeight }
      if (isVertical) {
        p.x = x + this.inWidth;
      } else {
        p.x = x + this.getSize().w
        - this.outWidth
        - pinCount*size;
      }
      cr.save()
      if (isVertical) {
        p.y += i * size;
      } else {
        p.x += (pinCount - i - 1) * size;
        cr.translate(p.x + size / 2 + size, p.y + size / 2)
        cr.rotate(-Math.PI / 2);
        cr.translate(-p.x - size / 2 - size, -p.y - size / 2 - size)
      }
      drawSwitch(cr, p, size, v, compileColor(VisualSettings.HOT_COL));
      let s = v ? '1' : '0';
      cr.restore();

      if (v) cr.fillStyle = 'white';
      else cr.fillStyle = 'white';
      if (isVertical) {
        cr.fillText(s, p.x + size / 2 + (v ? 0 : size), p.y + size / 2 + size * 0.1);
      } else {
        cr.fillText(s, p.x + size / 2, p.y + size / 2 + (v ? size : 0) + size * 0.1);
      }
    }
    cr.restore();

  }

  getValue(n: number): boolean {
    let c = this.getComponent();
    if (c.propProxies.multipin.getValue() as boolean) {
      let out = c.getOutput("out");
      if (out === null) return false;
      return ((out.getValue() & (1 << n)) != 0);
    } else {
      let out = c.getOutput("" + n);
      if (out === null) return false;
      return out.getBooleanValue();
    }
  }

  // TODO maybe use prop proxy for value
  toogleValue(n: number): void {
    let c = this.getComponent();
    if (c.propProxies.multipin.getValue() as boolean) {
      let out = c.getOutput("out");
      if (out === null) return;
      out.setValue(out.getValue() ^ (1 << n));
    }
    else {
      let out = c.getOutput("" + n);
      if (out === null) return;
      out.setBooleanValue(!out.getBooleanValue());
    }
  }


  public onInnerClick(pos: Point): void {
    let { direction, pinCount, size } = this.getMyProps();
    let n: number;
    if (direction == 'v') {
      n = Math.floor((pos.y) / size);
    } else { // horizontal
      n = Math.floor(((this.getSize().w - (this.inWidth + this.outWidth)) - pos.x) / size);
    }

    if ((n >= 0) && (n < pinCount)) {
      this.toogleValue(n);
      this.getComponent().getParent().update(); // nötig?
      dispatchRefreshCurrentTab(true, true); // TODO nicht so optimal
    }
  }
}




export class IndicatorVisual extends IOVisual {
  innerSize(cr: CanvasRenderingContext2D, x?: number, y?: number, children?: boolean): Size {
    let { direction, pinCount, size } = this.getMyProps();
    return {
      w: direction == 'v' ? size : size * pinCount,
      h: (direction == 'v' ? size * pinCount : size) + 2
    };
  }

  paintSelf(cr: CanvasRenderingContext2D, x: number, y: number, children: boolean): void {
    super.paintSelf(cr, x, y, false);
    let { direction, pinCount, size } = this.getMyProps();

    cr.save();
    let isVertical = direction == 'v';
    for (let i = 0; i < pinCount; i++) {
      let v = this.getValue(i);
      let p = { x: x + this.inWidth + size / 2, y: y + this.titleHeight + size / 2 };
      if (isVertical) {
        p.y += i * size;
      } else {
        p.x += (pinCount - i - 1) * size;
      }
      drawLED(cr, p, size / 2, v, compileColor(VisualSettings.HOT_COL));
    }
    cr.restore();
    return;

    x = x + this.inWidth; // darf das? seiteneffekt?
    y = y + this.titleHeight;

    cr.save();

    let px = x, py = y;
    let compSize = this.getSize();
    let w = compSize.w - this.outWidth - this.inWidth;
    let h = compSize.h - this.titleHeight;

    let ch, cw, cww, chh;

    // v / h switch - nötig?
    if (direction == 'v') {
      py += 0;
      h -= 0;
      ch = size;
      cw = 0;
      cww = size;
      chh = size;
    } else {
      px += 0;
      w -= 0;
      cw = -size;
      ch = 0;
      px += w;
      px -= size;
      cww = size;
      chh = size;
    }

    let v: boolean;
    let s: string;

    cr.font = 'normal ' + size + "px sans-serif";
    for (let i = 0; i < pinCount; i++) {
      v = this.getValue(i);
      cr.beginPath()
      if (v) cr.fillStyle = compileColor(VisualSettings.HOT_COL);
      else cr.fillStyle = compileColor(VisualSettings.WIRE_COL);
      cr.rect(px, py, cww, chh);
      cr.fill();
      s = v ? "1" : "0";
      cr.fillStyle = "white";
      cr.textAlign = 'center'
      cr.textBaseline = 'middle'
      cr.fillText(s, px + cww / 2, py + chh / 2);
      py += ch;
      px += cw;
    }

    cr.restore();
  }

  getValue(n: number): boolean {
    let c = this.getComponent();
    if (c.propProxies.multipin.getValue() as boolean) {
      let inp = c.getInput("in");
      if (inp === null) return false;
      return ((inp.getValue() & (1 << n)) != 0);
    } else {
      let inp = c.getInput("" + n);
      if (inp === null) return false;
      return inp.getBooleanValue();
    }
  }

  public onInnerClick(pos: Point): void {
    // empty
  }

}



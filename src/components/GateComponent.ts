import { ui_text } from "../helpers";
import { 
  Input, 
  IOValue, 
  Output,
} from "./IO";
import {Component} from './Component'
import { Props } from "../Props";




class GateComponent extends Component {
  inCount: number;
  
  public init(props: Props) {
    super.init(props);
    this.outputs.push(new Output(this, "x"));
    this.createIO();
  }


  protected getInputA(): Input {
    let i: Input | null = this.getInput('a');
    if (i) return i;
    throw "input a not found";
  }

  protected getInputB(): Input {
    let i: Input | null = this.getInput('b');
    if (i) return i;
    throw "input b not found";
  }

        /* TODO bool erg;
        type_inputs_iterator it = inputs.begin();
        erg = (*it)->getValueB();
        for (it++; it != inputs.end(); it++)
        {
            switch (type)
            {
                case AND:
                case NAND:
                    erg = erg & *(*it);
                    break;
                case OR:
                case NOR:
                    erg = erg | *(*it);
                    break;
                case XOR:
                    erg = erg ^ *(*it);
                    break;
                default:
                    break;
            }
        }


        if ((type == NAND) || (type == NOR) || (type == NOT))
        {
            erg = !erg;
        }

        return erg;*/

  public provideUiText(textmap: object): void {
    super.provideUiText(textmap);
    textmap["/size"] = ui_text("Breite", "Anzahl der Ein- bzw. Ausgänge.");
  }

  // TODO must be public??
  private createIO(): void {
    this.clearInputs();
    if (this.cir_name === 'not') {
      this.inputs.push(new Input(this, "a"));
    } else {
      if (this.inCount == 2) {
        this.inputs.push(new Input(this, "a"));
        this.inputs.push(new Input(this, "b"));
      }
    }
  }
}



export class AndGateComponent extends GateComponent {
  public init(props: Props) {
    this.setCirName('and');
    this.inCount = 2;
    super.init(props);
  }
  public calculateOutputValue(out: string): IOValue {
    if (this.inCount <= 2) {
        return this.getInputA().getValue() & this.getInputB().getValue();
    } else {
      // TODO mehr als 2 inputs
      return 0
    }
  }
}

export class NandGateComponent extends GateComponent {
  public init(props: Props) {
    this.setCirName('nand');
    this.inCount = 2;
    super.init(props);
  }
  public calculateOutputValue(out: string): IOValue {
    if (this.inCount <= 2) {
        return ~(this.getInputA().getValue() & this.getInputB().getValue());
    } else {
      // TODO mehr als 2 inputs
      return 0
    }
  }
}

export class OrGateComponent extends GateComponent {
  public init(props: Props) {
    this.setCirName('or');
    this.inCount = 2;
    super.init(props);
  }
  public calculateOutputValue(out: string): IOValue {
    if (this.inCount <= 2) {
        return this.getInputA().getValue() | this.getInputB().getValue();
    } else {
      // TODO mehr als 2 inputs
      return 0
    }
  }
}

export class NorGateComponent extends GateComponent {
  public init(props: Props) {
    this.setCirName('nor');
    this.inCount = 2;
    super.init(props);
  }
  public calculateOutputValue(out: string): IOValue {
    if (this.inCount <= 2) {
        return ~(this.getInputA().getValue() | this.getInputB().getValue());
    } else {
      // TODO mehr als 2 inputs
      return 0
    }
  }
}

export class XorGateComponent extends GateComponent {
  public init(props: Props) {
    this.setCirName('xor');
    this.inCount = 2;
    super.init(props);
  }
  public calculateOutputValue(out: string): IOValue {
    if (this.inCount <= 2) {
        return this.getInputA().getValue() ^ this.getInputB().getValue();
    } else {
      // TODO mehr als 2 inputs
      return 0
    }
  }
}

export class NotGateComponent extends GateComponent {
  public init(props: Props) {
    this.setCirName('not');
    this.inCount = 1;
    super.init(props);
  }
  public calculateOutputValue(out: string): IOValue {
    if (this.inCount == 1) {
        return ~this.getInputA().getValue();
    } else {
      // TODO mehr als 1 inputs
      return 0
    }
  }
}

import { wait } from "../helpers";
import { SerializedCircuit } from "../types";
//import lib from "./lib.json"

// https://github.com/tsironis/lockr
import { set as lsSet, get as lsGet, setPrefix as lsSetPrefix } from 'lockr';

const BASE_PATH = ""

type ManagerData = {
  isOpen: boolean,
  groupTitle: string,
  maxUsers: number|null,
  users: {
    name: string,
    components: number,
    hasPin: boolean,
    is_group_manager: boolean,
  }[]
};


export class RequestException {
  response: Response;
  answer: any;

  constructor(resp: Response, answer: any) {
    this.response = resp;
    this.answer = answer;
  }

  getAnswer() {
    return this.answer;
  }

  getMessage() {
    return (this.getAnswer())['msg'] ?? 'Unbekannter fehler.'
  }

  getStatus() {
    return this.response.status;
  }

  isNotAllowed() {
    return this.getStatus() === 403;
  }

  isBadRequest() {
    return this.getStatus() === 400;
  }

  isNotFound() {
    return this.getStatus() === 404;
  }
}

class CircuitLib {
  cachedData: {[key: string]: SerializedCircuit} = {};

  token: string | null = null;

  constructor() {
    lsSetPrefix('piiri_');
  }

  withLocalComponents<T>(callback: (all: {[key: string]: SerializedCircuit})=>T, writeBack: boolean = false): T {
    let all = lsGet('components', {});
    let result = callback(all);
    if (writeBack) {
      lsSet('components', all);
    }
    return result;
  }

  isLocalMode(): boolean {
    return !this.isServerMode();
  }

  isServerMode(): boolean {
    return this.token !== null;
  }

  resetCache(name?: string) {
    if (typeof name === 'undefined') {
      this.cachedData = {};
    }
    delete this.cachedData[name];
  }

  url() {
    let url = '?';
    return url;
  }

  async request<T>(method: string, command: string, params = null, data = null): Promise<T|null> {
    let request = {
      method,
      headers: {
        'Accept': 'application/json',
        'Authorization': this.token
      },
      body: null,
    };
    const urlParams = new URLSearchParams(Object.assign({
      c: command
    }, params));
    let url = this.url() + urlParams.toString();
    if (data) {
      request.headers['Content-Type'] = 'application/json';  
      request.body = JSON.stringify(data);
    }
    let resp = await fetch(url, request)
    let answer = null;
    try {
      answer = await resp.json();
    } catch (e) {}
    if (resp.status === 200) {
      return answer;
    }
    throw new RequestException(resp, answer);
  }




  async me(): Promise<any> {
    if (!this.isServerMode()) return;
    return this.request('GET', 'me');
  }


  async list(): Promise<string[]> {
    if (this.isServerMode()) {
      try {
        return this.request('GET', 'list');
      } catch (e) {
        return [];
      }
    } else {
      return this.withLocalComponents((all)=>Object.keys(all));
    }
  }

  async get(cir, resetCache: boolean = false): Promise<SerializedCircuit|null> {
    if (resetCache) {
      this.resetCache(cir);
    }
    if (typeof this.cachedData[cir] !== 'undefined') {
      return this.cachedData[cir];
    }
    const success = (data) => {
      this.cachedData[cir] = data;
      return data;
    }
    if (this.isServerMode()) {
      try {
        return success(await this.request('GET', 'component', {name: cir}))
      } catch (e) {
        return null;
      }
    } else {
      let data = this.withLocalComponents((all)=>all[cir]);
      if (typeof data !== 'undefined') {
        return success(data);
      }
      return null;
    }
  }

  async del(name): Promise<boolean> {
    this.resetCache(name)
    if (this.isServerMode()) {
      try {
        return this.request('DELETE', 'component', {name});
      } catch (e) {
        return false;
      }
    } else {
      this.withLocalComponents(all=>delete all[name], true);
      return true;
    }
  }

  async store(data: SerializedCircuit): Promise<boolean> {
    const success = (ans) => {
      this.cachedData[data.name] = data; // bissel komisch aber absicht!
      return ans;
    }
    if (this.isServerMode()) {
      try {
        return success(await this.request('POST', 'component', {name: data.name}, data))
      } catch (e) {
        return null;
      }
    } else {
      this.withLocalComponents((all)=>all[data.name] = data, true);
      return success(true);
    }
  }

  async rename(oldname, newname) {
    let c = this.cachedData[oldname];
    if (typeof c !== 'undefined') {
      delete this.cachedData[oldname];
      c.name = newname;
      this.cachedData[newname] = c;
    }
    if (this.isServerMode()) {
      return this.request('POST', 'rename', {name: oldname}, newname);
    } else {
      this.withLocalComponents(all=>{
        all[newname] = all[oldname];
        all[newname].name = newname;
        delete all[oldname];
      }, true)
    }
  }


  async shareInGroup(name: string) {
    if (this.isLocalMode()) return;
    return this.request('POST', 'share', {name});
  }

  async unshareInGroup(name: string) {
    if (this.isLocalMode()) return;
    return this.request('POST', 'unshare', {name});
  }




  /**** ADMIN SACHEN */

  async loadManagerData(): Promise<ManagerData> {
    if (!this.isServerMode()) return;
    return this.request('GET', 'managerData')
  }

  async createUser(u: {name}) {
    if (!this.isServerMode()) return;
    return this.request('PUT', 'user', null, u)
  }

  async deleteUser(u: string) {
    if (!this.isServerMode()) return;
    return this.request('DELETE', 'user', {name: u});
  }

  async setUserPIN(u: string, pin: string) {
    if (!this.isServerMode()) return;
    return this.request('POST', 'user', {name: u}, {pin});
  }

  async renameUser(u: string, newname: string) {
    if (!this.isServerMode()) return;
    return this.request('POST', 'user', {name: u}, {name: newname});
  }

  async setGroupOpen(o: boolean) {
    return this.updateGroup({is_open: o});
  }

  async updateGroup(g: {password?: string, is_open?: boolean}) {
    if (!this.isServerMode()) return;
    return this.request('POST', 'group', null, g);
  }
  


}


export default new CircuitLib();

import { BooleanProp, CountingNumberProp, Props, TextProp } from '../Props';
import { Component } from './Component'
import { Input, Output } from './IO';

export class PLAComponent extends Component {

  public inBits: number;
  public andBits: number;
  public outBits: number;
  public andArray: string='a';
  public orArray: string='b';

  public andMatrix: number[][];
  public orMatrix: number[][];
  
  protected values: number[] = [];

  public init(props: Props) {
    super.init(props);
    this.setCirName('pla')
    this.propProxies.inBits = new CountingNumberProp(this, "inBits", "Input-Bits", 4);
    this.propProxies.andBits = new CountingNumberProp(this, "andBits", "Terme", 3);
    this.propProxies.outBits = new CountingNumberProp(this, "outBits", "Output-Bits", 2);
    this.propProxies.andArray = new TextProp(this, "andArray", "UND-Matrix", "*");
    this.propProxies.orArray = new TextProp(this, "orArray", "ODER-Matrix", "#");
    this.andMatrix = [];
    this.orMatrix = [];
    this.createIO();
    this.createMatrix();
    this.writeMatrixToProps();
  }

  protected createIO(): void {
    if (this.inBits > this.inputs.length) {
      for (let i = this.inputs.length; i < this.inBits; i++) {
        this.inputs.push(new Input(this, ""+i));
      }
    } else if (this.inBits < this.inputs.length) {
      let l = this.inputs.length;
      for (let i = this.inBits; i < l; i++) {
        this.inputs.pop().disconnect(true, true, true);
      }
    }
    if (this.outBits > this.outputs.length) {
      for (let i = this.outputs.length; i < this.outBits; i++) {
        this.outputs.push(new Output(this, ""+i));
      }
    } else if (this.outBits < this.outputs.length) {
      let l = this.outputs.length;
      for (let i = this.outBits; i < l; i++) {
        this.outputs.pop().disconnect(true, true, true);
      }
    }
  }

  protected createMatrix(): void {
    let andRows = this.inBits * 2;
    let andCols = this.andBits;

    let orRows = this.outBits;
    let orCols = this.andBits;

    this.andMatrix = new Array(andRows);
    for (let i = 0; i < andRows; i++) {
      this.andMatrix[i] = new Array(andCols).fill(0);
    }
    this.orMatrix = new Array(orRows);
    for (let i = 0; i < orRows; i++) {
      this.orMatrix[i] = new Array(orCols).fill(0);
    }
  }

  protected parseMatrix(): void {
    let sp = this.andArray.split(/\n|\|/).map((r)=>r.trim().split(" ").map((e)=>parseInt(e.trim())))
    for (let r = 0; r < this.andMatrix.length; r++) {
      for (let c = 0; c < this.andMatrix[r].length; c++) {
        let v = (sp[r]??[])[c] ?? 0;
        if (isNaN(v)) v = 0;
        this.andMatrix[r][c] = v;
      }
    }
    sp = this.orArray.split(/\n|\|/).map((r)=>r.trim().split(" ").map((e)=>parseInt(e.trim())))
    for (let r = 0; r < this.orMatrix.length; r++) {
      for (let c = 0; c < this.orMatrix[r].length; c++) {
        let v = (sp[r]??[])[c] ?? 0;
        if (isNaN(v)) v = 0;
        this.orMatrix[r][c] = v;
      }
    }
    //console.log('parseMatrix', this.andArray,this.andMatrix)
  }

  public writeMatrixToProps(): void {
    let andContent = this.andMatrix.map((row)=>{
      return row.join(' ');
    }).join("\n");
    let orContent = this.orMatrix.map((row)=>{
      return row.join(' ');
    }).join("\n");
    //console.log('writeMatrixToProps', this.andMatrix, andContent)
    this.propProxies.andArray.setValueSilent(andContent);
    this.propProxies.orArray.setValueSilent(orContent);
    this.propProxies.orArray.updateIfRendered();
    this.propProxies.andArray.updateIfRendered();
  }

  protected calculateOutputValue(out: string): number {
    return this.values[parseInt(out)] ?? 0;
  }

  // TODO andValues: number[]  cache von andValues
  getAndValues(): number[] {
    let result: number[] = new Array(this.andBits).fill(0);
    for (let a = 0; a < this.andBits; a++) {
      let v = 1;
      for (let i = 0; i < this.inBits; i++) {
        let inv = this.inputs[i].getValue();
        if (this.andMatrix[i*2][a] && !inv) { // IN
          v = 0;
          break;
        }
        if (this.andMatrix[i*2+1][a] && inv) { // not IN
          v = 0;
          break;
        }
      }
      result[a] = v;
    }
    return result;
  }

  getOrValues(andValues: number[]): number[] {
    let result: number[] = new Array(this.outBits).fill(0);
    for (let o = 0; o < this.outBits; o++) {
      let v = 0;
      for (let a = 0; a < this.andBits; a++) {
        if (this.orMatrix[o][a] && andValues[a]) {
          v=1;
          break;
        }
      }
      result[o] = v;
    }
    return result;
  }


  onInputChanged(name: string): void {
    this.values = this.getOrValues(this.getAndValues());
  }

  public onPropsChanged(oldValues: { [key: string]: any; }): void {
    if ("inBits" in oldValues || "outBits" in oldValues) {
      this.createIO();
    }

    
    if ("andArray" in oldValues || "orArray" in oldValues) {
      this.createMatrix();
      this.parseMatrix();
      this.writeMatrixToProps();
    }

    if ("inBits" in oldValues || "andBits" in oldValues || "outBits" in oldValues) {
      //if (1 << this.addrBits != this.items.length) {
        this.createMatrix();
        this.parseMatrix();
        this.writeMatrixToProps();
      //}
    }
    super.onPropsChanged(oldValues);
  }

}
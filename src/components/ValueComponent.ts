import { BooleanProp, CountingNumberProp, LineProp, Props } from "../Props";
import {Component} from './Component'
import { Output } from "./IO";


export class ValueComponent extends Component {
  value: string;
  pinCount: number;
  multipin: boolean;


  public init(props: Props) {
    super.init(props);
    this.setCirName('value')
    this.propProxies.value = new LineProp(this, 'value', "Wert", "1")
    this.propProxies.pinCount = new CountingNumberProp(this, 'pinCount', "Leitungen", 1)
    this.propProxies.multipin = new BooleanProp(this, 'multipin', "Multipin", true)
    
    this.createIO();
  }

  onPropsChanged(oldValues: {[key: string]: any}): void {
    if ('pinCount' in oldValues || 'multipin' in oldValues) {
      this.createIO();
    }
    super.onPropsChanged(oldValues);
  }

  private parsedValue(): number {
    let str: string;
    let base = 10;
    if (/^b/i.test(this.value)) {
      str = this.value.substring(1);
      base = 2;
    } else if (/^x/i.test(str)) {
      str = this.value.substring(1);
      base = 16;
    } else {
      str = this.value;
    }
    let v = parseInt(str, base);
    if (isNaN(v)) return 0;
    return v;
  }
  
  protected calculateOutputValue(out: string): number {
    if (this.multipin) {
      return this.parsedValue();
    } else {
      let v = this.parsedValue();
      let p = out.match(/(\d+)$/)
      if (!p) return 0; // sollte nicht vorkommen
      let num = parseInt(p[1])
      return (( v & (1 << num)) != 0) ? 1 : 0
    }
  }

  createIO() {
    this.clearOutputs();
    if (this.multipin) {
      this.outputs.push(new Output(this, "out", this.pinCount));
    } else {
      for (let i=0; i < this.pinCount; i++) {
        this.outputs.push(new Output(this, ""+i, 1));
      }
    }
  }
}



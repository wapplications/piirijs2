import { BooleanProp, CountingNumberProp, Props, TextProp } from '../Props';
import { Component } from './Component'
import { Input, Output } from './IO';

export class RAMComponent extends Component {

  public addrBits: number;
  public contentBits: number;
  public content: string;
  public multipin: boolean;
  public writeback: boolean;

  protected items: number[];
  protected lastSet: boolean = false;
  protected value: number = 0;

  public init(props: Props) {
    super.init(props);
    this.setCirName('ram')
    this.propProxies.addrBits = new CountingNumberProp(this, "addrBits", "Adress-Bits", 4)
    this.propProxies.contentBits = new CountingNumberProp(this, "contentBits", "Inhalt-Bits", 4)
    this.propProxies.content = new TextProp(this, "content", "Inhalt (Hex)", "")
    this.propProxies.writeback = new BooleanProp(this, "writeback", "Permanent zurückschreiben", true)
    this.multipin = true;
    this.items = [];
    this.createIO();
    this.createItems();
    if (this.writeback) this.writeContentToProps()
  }


  protected createIO(): void {
    this.inputs.push(new Input(this, "S"));
    if (this.multipin) {
      this.inputs.push(new Input(this, "Adr", this.addrBits));
      this.inputs.push(new Input(this, "in", this.contentBits));
      this.outputs.push(new Output(this, "out", this.contentBits));
    } else {
      // TODO
      // kann man verzichten. will man einzelne Anschlüsse haben, kann man das ganze in
      // eine eigene Komponente wrappen und Adapter benutzen
    }
  }

  protected createItems(): void {
    let contentLength = 1 << this.addrBits;

    if (contentLength == this.items.length) return;
    if (contentLength > this.items.length) {
      for (let addr = this.items.length; addr < contentLength; addr++) {
        this.items[addr] = 0;
      }
    }
    this.items = this.items.slice(0, contentLength);
  }

  protected parseContent(): void {
    if (this.items.length <= 0 || this.content == '') return;
    let sp = this.content.split(/\s/)
    for (let i = 0; i<sp.length; i++) {
      if (i >= this.items.length) break;
      let v = parseInt(sp[i], 16);
      if (isNaN(v)) v = 0;
      this.items[i] = v;
    }
  }

  protected writeContentToProps(): void {
    let content = "";
    for (let i = 0; i < this.items.length; i++) {
      if (content !== "") content += " ";
      let hex = Number(this.items[i]).toString(16);
      if (hex.length == 1) hex = "0"+hex;
      content = content + hex.toUpperCase();
    }
    if (content !== this.content) {
      this.propProxies.content.setValueSilent(content);
      this.propProxies.content.updateIfRendered();
    }
  }

  protected calculateOutputValue(out: string): number {
    if (this.multipin) {
      return this.value;
    } else {
      /* TODO    string s(out);
          s.replace(0,3,"");
          istringstream is(s);
          unsigned int num;
          is >> num;
          return ((m_value & (1 << num))!=0) ;*/
    }
    return 0;
  }

  onInputChanged(name: string): void {
    if ((this.addrBits == 0)) {
      return;
    }

    let s = this.getInput("S").getBooleanValue();
    let addr = 0;
    if (!this.multipin) {
      /* TODO for (unsigned int i = 0; i < m_addr_bits; i++)
      {
          os << "A" << (m_addr_bits-i-1);
          addr <<= 1;
          addr |= (     getInputValue(os.str())     ? 1 : 0);
          os.str("");
      }*/
    } else {
      addr = this.getInputValue("Adr");
    }

    if (addr < this.items.length) {
      this.value = this.items[addr];
    }

    if (this.lastSet && !s) {// fallende Flanke?
      let inp = 0;
      if (!this.multipin) {
        /* TODO for (unsigned int i = 0; i < m_item_bits; i++)
        {
            os << "in" << (m_item_bits-i-1);
            in <<= 1;
            in |= (     getInputValue(os.str())     ? 1 : 0);
            os.str("");
        }*/
      } else {
        inp = this.getInputValue("in");
      }
      this.value = inp;
      if (addr < this.items.length) {
        this.items[addr] = this.value;
        if (this.writeback) this.writeContentToProps();
      }
    }
    this.lastSet = s;
  }

  public onPropsChanged(oldValues: { [key: string]: any; }): void {
    super.onPropsChanged(oldValues);
    if ("multipin" in oldValues || "addrBits" in oldValues || "contentBits" in oldValues) {
      this.clearInputs();
      this.clearOutputs();
      this.createIO();
    }

    if ("content" in oldValues) {
      this.parseContent();
    }

    if ("multipin" in oldValues || "addrBits" in oldValues || "contentBits" in oldValues) {
      if (1 << this.addrBits != this.items.length) {
        this.createItems();
        if (this.writeback) this.writeContentToProps();
      }
    }
  }

}
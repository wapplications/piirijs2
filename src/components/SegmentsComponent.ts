import { BooleanProp, CarouselProp, CountingNumberProp, Props } from '../Props';
import {Component} from './Component'
import { Input } from './IO';


export class SegmentsComponent extends Component {
  multipin: boolean;
  decoder: boolean;
  digitSize: number;
  digits: number;
  mode: string;

  public init(props: Props) {
    super.init(props);
    this.setCirName('segments');
    // TODO aus props lesen
    this.propProxies.multipin = new BooleanProp(this, 'multipin', 'Multipin', true);
    this.propProxies.decoder = new BooleanProp(this, 'decoder', 'Mit Dekodierer', true);
    this.propProxies.digits = new CountingNumberProp(this, 'digits', 'Stellen', 1);
    this.propProxies.mode = new CarouselProp(this, 'mode', 'Modus', 'hex', false, {hex: "hexadezimal", dez: "dezimal"});
    this.propProxies.digitSize = new CountingNumberProp(this, 'digitSize', 'Größe', 20);
    this.createIO();
  }

  onPropsChanged(oldValues: {[key: string]: any}): void {
    if ('digits' in oldValues || 'multipin' in oldValues || 'decoder' in oldValues) {
      this.createIO();
    }
    super.onPropsChanged(oldValues);
  }

  createIO() {
    this.clearInputs();
    let createInputs = (count) => {
      if (this.multipin) {
        if (!this.decoder) {
          for (let i=0; i<count/7; i++) {
            this.inputs.push(new Input(this, ""+i, 7))
          }
        } else {
          this.inputs.push(new Input(this, "in", count))
        }
      } else {
        for (let i=0; i<count; i++) {
          this.inputs.push(new Input(this, ""+i, 1))
        }
      }
    }
    if (this.decoder) {
      let base = this.mode === 'dez' ? 10 : 16;
      // für ziffern 0-9 brauche ich 4 pins  1001=9

      let pinCount = Math.round(Math.log2(Math.pow(base,this.digits)-1)+0.5);
      createInputs(pinCount)
    } else {
      createInputs(7*this.digits)
    }


  }

}
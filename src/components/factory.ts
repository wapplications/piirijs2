import {Component} from './Component'
import {AndGateComponent, NandGateComponent, OrGateComponent, NorGateComponent, XorGateComponent, NotGateComponent} from './GateComponent'
import {NodeComponent} from "./NodeComponent";
import {LEDComponent} from "./LEDComponent";
import { AdapterComponent } from './AdapterComponent'
import {ValueComponent} from "./ValueComponent";
import {ClockComponent} from "./ClockComponent";
import {SegmentsComponent} from './SegmentsComponent'
import CircuitLib from './api';
import { Container } from './Container';
import { RAMComponent } from './RAMComponent';
import { Switch, Indicator } from './IOComponent';
import { TextComponent } from './TextComponent';
import { ComponentFactoryError } from '../types';
import { DummyComponent } from './DummyComponent';
import { PLAComponent } from './PLAComponent';


function addComponentError(componentErrors: ComponentFactoryError[], componentError: ComponentFactoryError) {
  for (let ce of componentErrors) {
    if (ce.parentComponent === componentError.parentComponent && ce.childComponent === componentError.childComponent) {
      return;
    }
  }
  componentErrors.push(componentError);
}


export default async function createComponent(
  p: Container | null,
  cir: string,
  name: string,
  sim: boolean,
  componentErrors: ComponentFactoryError[]): Promise<Component | null> {

  // namens prefix ist der angegebene name, wenn leer, die cir-id
  let prefix: string = name === "" ? cir : name;
  let num: number = 0;
  if (name == "") name = prefix + "_" + num++; // falls name nicht gegeben fängt die numerierung gleich bei null an
  else name = prefix;
  if (p) {
    while (p.findChildByName(name) !== null) {
      name = prefix + "_" + num++;
    }
  }

  let neues_element = null;
  if (cir == "node") {
    neues_element = new NodeComponent(name, p, sim);
  } else
    if (cir == "or") {
      neues_element = new OrGateComponent(name, p, sim);
    } else if (cir == "and") {
      neues_element = new AndGateComponent(name, p, sim);
    } else if (cir == "nor") {
      neues_element = new NorGateComponent(name, p, sim);
    } else if (cir == "nand") {
      neues_element = new NandGateComponent(name, p, sim);
    } else if (cir == "xor") {
      neues_element = new XorGateComponent(name, p, sim);
    } else if (cir == "not") {
      neues_element = new NotGateComponent(name, p, sim);
    }


    /* TODO else if (cir == "digit") {
        neues_element = new DigitalZiffer(_name, p);
    } else if (cir == "digit2") {
        neues_element = new DigitalZiffer2(_name, p);
    } else if (cir == "letter") {
        neues_element = new LetterComponent(_name, p);
    }*/
    else if (cir == "adapter") {
      //{size: 3, direction: "bundle"}
        neues_element = new AdapterComponent(name, p, sim);
    } 
    else if (cir == "clock") {
        neues_element = new ClockComponent(name, p, sim);
    } 
    else if (cir == "value") {
      neues_element = new ValueComponent(name, p, sim); // , 1, 1
    }
    else if (cir == "segments") {
      neues_element = new SegmentsComponent(name, p, sim);
    }


    
    
    else if (cir == "text") {
        neues_element = new TextComponent(name, p, sim);
    }
    else if (cir == "switch") {
      neues_element = new Switch(name, p, sim);
    }
    else if (cir == "indicator") {
      neues_element = new Indicator(name, p, sim);
    }
    else if (cir == "led") {
      neues_element = new LEDComponent(name, p, sim);
    }
  /*else if (cir == "8switch") {
      neues_element = new Switch(_name, p, 8);
  } else if (cir == "8indicator") {
      neues_element = new Indicator(_name, p,8);
  }*/
  /*else if (cir == "function") {
      neues_element = new FunctionComponent(_name, p,2);
  }*/ 
  else if (cir == "ram") {
      neues_element = new RAMComponent(name, p, sim);
  }
  else if (cir == "pla") {
    neues_element = new PLAComponent(name, p, sim);
  }
  else if (cir == "embedded") {
    neues_element = new Container(name, p, sim);
    neues_element.init({});
    neues_element.setCirName("embedded");
    return neues_element;
  } /*else if (cir == "iow") {
      neues_element = new IOWComponent(_name, p);*/
  else if (cir !== "") {
    let circuit: Container = new Container(name, p, sim);
    neues_element = circuit;
    neues_element.init({});
    let data = await CircuitLib.get(cir);
    if (!data) {
      //let msg = "Die Komponente " + (p ? p.getName() : '') + " braucht eine " + cir + "-Komponente. Diese Komponente konnte aber nicht erzeugt werden.";
      addComponentError(componentErrors, {
        parentComponent: (p ? p.getCirName() : null),
        childComponent: cir,
      })
      //neues_element = null;
      neues_element = new DummyComponent(name, p, false);
      neues_element.init({});
      neues_element.setCirName(cir);
      neues_element.description = "Fehlende Komponente!";
    } else {
      await circuit.loadCircuit(data, componentErrors)
    }
    return neues_element; // früher return, damit nicht nochmal init
  }
  neues_element.init({});
  return neues_element;
}




import { compileColor } from '../helpers';
import { CarouselProp, CountingNumberProp, LineProp, Props } from '../Props';
import { Size } from '../types';
import { ComponentVisual } from '../visuals/ComponentVisual';
import {Component} from './Component'
import VisualSettings from '../visuals/VisualSettings';


const colors = {
    black: '#111111',
    white: '#ffffff',
    red: '#dd3333',
    green: '#22cc22',
    blue: '#4544ee',
    yellow: "#ddaa00",
    orange: "#ee5500",
    pink: "#ee00aa",
    cyan: "#33dddd"
}


export class TextComponent extends Component {
    text: string;
    fontSize: number;

    public init(props: Props): void {
        this.setCirName('text')
        this.propProxies.text = new LineProp(this, 'text', "Text", "ABC")
        this.propProxies.fontSize = new CountingNumberProp(this, 'fontSize', "Textgröße", 16)
        this.propProxies.color = new CarouselProp(this, 'color', "Farbe", "cyan", false, {
            white: 'weiß',
            red: 'rot',
            green: 'grün',
            blue: 'blau',
            yellow: 'gelb',
            orange: 'orange',
            pink: 'pink',
            cyan: 'türkis',
            black: 'schwarz',
        })
    }

    onPropsChanged(oldValues: { [key: string]: any; }): void {
        super.onPropsChanged(oldValues);

    }

}

export class TextVisual extends ComponentVisual {

    init(): void {
        this.inWidth = 0;
        this.outWidth = 0;
        this.titleHeight = 0;
        super.init();
    }

    getTextProps() {
        let c = this.getComponent();
        return {
            text: c.propProxies.text.getValue(),
            fontSize: c.propProxies.fontSize.getValue(),
            color: c.propProxies.color.getValue(),
        }
    }

    innerSize(cr: CanvasRenderingContext2D, x?: number, y?: number, children?: boolean): Size {
        let {fontSize, text} = this.getTextProps();
        cr.save();
        cr.font = 'normal '+fontSize+'px sans-serif';
        cr.textAlign = "center"
        cr.textBaseline = "middle"
        let tm = cr.measureText(text);
        this.inWidth = 0;
        this.outWidth = 0;
        cr.restore();
        return {
            w: tm.actualBoundingBoxLeft + tm.actualBoundingBoxRight, 
            h: tm.actualBoundingBoxAscent + tm.actualBoundingBoxDescent
        }
    }

    paintSelf(cr: CanvasRenderingContext2D, x: number, y: number, children: boolean): void {
        let size = this.getSize();
        cr.save();
        let {fontSize, text, color} = this.getTextProps();

        if (this.isSelected() || text === '') {
            cr.roundRect(x,y, size.w, size.h, VisualSettings.CIR_BORDER_RADIUS);
            cr.lineWidth = 1.0;
            if (this.isSelected()) cr.strokeStyle = compileColor(VisualSettings.CIR_TITLE_SEL_COL)
            else cr.strokeStyle = "#444444";
            cr.stroke();
        }
    

        cr.font = 'normal '+fontSize+'px sans-serif';
        cr.fillStyle = colors[color] ?? 'black';
        cr.textAlign = "center"
        cr.textBaseline = "top"
        cr.fillText(text, x+size.w/2, y);
    
        cr.restore();
    }
}



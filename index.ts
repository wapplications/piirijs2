import {ToplevelCircuit} from "./src/components/ToplevelCircuit";
import {ToplevelCircuitVisual} from "./src/visuals/ToplevelCircuitVisual";

import { commandEvent, getViewPortSize, wait } from './src/helpers';
import BodyScreen from './src/ui/BodyScreen'
import { register_ui } from './src/ui/UiManager';
import SplitPane from './src/ui/SplitPane';
import VerticalSplit from './src/ui/VerticalSplit';
import PiiriPane from './src/ui/PiiriPane';
import CirButton from './src/ui/CirButton';
import CommandButton from './src/ui/CommandButton';
import Icon from './src/ui/Icon';
import Tabs from './src/ui/Tabs';
import Log from "./src/ui/Log";
import Accordeon, {AccordeonTab} from "./src/ui/Accordeon";
import AnalysisView from "./src/ui/AnalysisView";


import App from './src/App';
import IntroPrompt from "./src/ui/IntroPrompt";

import PIIRI_META from './meta';

console.log('Hallo, hier ist PiiRi ' + PIIRI_META.version + ' by ' + PIIRI_META.author + '.');

console.log('Registriere UI Komponenten.')
register_ui('uiSplitPane', SplitPane)
register_ui('uiVerticalSplit', VerticalSplit)
register_ui('uiPiiriPane', PiiriPane)
register_ui('uiCirButton', CirButton)
register_ui('uiLog', Log)
register_ui('uiCommandButton', CommandButton)
register_ui('uiIcon', Icon)
register_ui('uiTabs', Tabs)
register_ui('uiAccordeon', Accordeon);
register_ui('uiAccordeonTab', AccordeonTab);
register_ui('uiAnalysisView', AnalysisView);



console.log('Initialisiere UI.')
let body = new BodyScreen(document.body);
body.init();
body.doSizeChanged();

//let log = body.refMap['log'] as Log;
/*console.log = function(...data: any[]) {
  log.log(...data)
};
console.warn = function(...data: any[]) {
  log.warn(...data)
};
console.error = function(...data: any[]) {
  log.error(...data)
};*/
let tabs = body.refMap['tabs'] as Tabs;


console.log('Initialisiere App.')
let app = new App(tabs);


let a = new ToplevelCircuit();
new ToplevelCircuitVisual(a) // das muss ich hier machen um circuläre problem zu verhindern , das ist scheiße!

window['commandEvent'] = commandEvent;

let introPrompt = new IntroPrompt(document);

console.log('Warte, damit jeder das Logo bewundern kann...')
wait(1000)
.then(()=>introPrompt.close())
.then(()=>app.start())
.then(()=>console.log('PiiRi App gestartet.'))
.catch((e)=>{
  console.error('Exception gefangen in index.ts: ', e)
});





/*window.addEventListener('beforeunload', (event: BeforeUnloadEvent)=>{
  event.preventDefault();
  return event.returnValue = "Are you sure you want to exit?";
}, true)*/

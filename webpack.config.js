//webpack.config.js
const path = require('path');
const webpack = require('webpack');
var PACKAGE = require('./package.json');
var versionComment = PACKAGE.name + ' - ' + PACKAGE.version + ' [' + (new Date).toISOString() + '] by ' + PACKAGE.author.name;
var metaValue = JSON.stringify({
  author: PACKAGE.author.name,
  version: PACKAGE.version
});

module.exports = {
  mode: "development",
  devtool: "inline-source-map",
  entry: {
    main: "./index.ts",
  },
  output: {
    path: path.resolve(__dirname, './dist'),
    filename: "app.js" // <--- Will be compiled to this single file
  },
  resolve: {
    extensions: [".ts", ".tsx", ".js"],
  },
  module: {
    rules: [
      { 
        test: /\.tsx?$/,
        loader: "ts-loader"
      }
    ]
  },
  plugins: [
    new webpack.BannerPlugin({
      entryOnly: true,
      raw: true,
      banner: `/* ${versionComment} */\nwindow.piiriMeta = ${metaValue};`
    })
  ]
};